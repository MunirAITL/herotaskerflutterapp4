import 'package:aitl/config/server/APIBadgeCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/EntityPropertyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgeAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgesModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortfulioModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../data/model/ads-on/action_req/UserBadgeModel.dart';
import '../../../../view_model/helper/action_req/BadgeAPIMgr.dart';
import 'profile_base.dart';

class ProfilePage extends StatefulWidget {
  final int profileUserID;
  const ProfilePage({Key key, this.profileUserID}) : super(key: key);
  @override
  State createState() => _ProfilePageState();
}

class _ProfilePageState extends BaseProfileStatefull<ProfilePage>
    with APIStateListener {
  List<UserRatingSummaryDataModel> listUserRatingSummary = [];
  List<UserRatingsModel> listUserRating = [];
  List<UserBadgesModel> listUserBadge = [];
  UserPortfulioModel userPortfulioModel;
  AboutModel aboutModel;

  MediaUploadFilesModel uploadFileModelCover;
  MediaUploadFilesModel uploadFileModelProfile;

  //  **************  app states start

  int userId;

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.entity_profile_cover_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetPublicProfile(userModel.id);
          }
        }
      }
      if (apiState.type == APIType.entity_profile_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetPublicProfile(userModel.id);
          }
        }
      }
      if (apiState.type == APIType.media_profile_cover_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            uploadFileModelCover =
                (model as MediaUploadFilesAPIModel).responseData.images[0];
            await APIViewModel().req<EntityPropertyAPIModel>(
              context: context,
              apiState: APIState(
                  APIType.entity_profile_cover_image, this.runtimeType, null),
              url: APIProfileCFg.ENTITY_PROPERTY_POST_URL,
              reqType: ReqType.Post,
              param: {
                "EntityId": userModel.id,
                "EntityName": "User",
                "PropertyName": "DefaultCoverId",
                "Value": uploadFileModelCover.id,
              },
            );
          }
        }
      }
      if (apiState.type == APIType.media_profile_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            uploadFileModelProfile =
                (model as MediaUploadFilesAPIModel).responseData.images[0];

            await APIViewModel().req<EntityPropertyAPIModel>(
              context: context,
              apiState: APIState(
                  APIType.entity_profile_image, this.runtimeType, null),
              url: APIProfileCFg.ENTITY_PROPERTY_POST_URL,
              reqType: ReqType.Post,
              param: {
                "EntityId": userModel.id,
                "EntityName": "User",
                "PropertyName": "DefaultPictureId",
                "Value": uploadFileModelProfile.id,
              },
            );
          }
        }
      }

      if (apiState.type == APIType.public_profile &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            userModel = (model as PublicProfileAPIModel).responseData.user;
            userId = userModel.id;
            if (widget.profileUserID == null) {
              userData.userModel = userModel;
            }
            StateProvider()
                .notify(ObserverState.STATE_RELOAD_USERPROFILE, null);
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.user_rating_summary &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRatingSummary = (model as UserRatingSummaryAPIModel)
                .responseData
                .userRatingSummaryData;

            try {
              profileController.setAverageRateAsPoster(
                  listUserRatingSummary[0].posterAverageRating.obs);
              profileController.setAverageRateAsTasker(
                  listUserRatingSummary[0].taskerAverageRating.obs);
              profileController.setCompletionRateAsPoster(
                  listUserRatingSummary[0].posterCompletionRate.obs);
              profileController.setCompletionRateAsTasker(
                  listUserRatingSummary[0].taskerCompletionRate.obs);
              profileController.setCountAsPoster(
                  listUserRatingSummary[0].completedPosterTaskCount.obs);
              profileController.setCountAsTasker(
                  listUserRatingSummary[0].completedTaskerTaskCount.obs);
            } catch (e) {
              myLog(e.toString());
            }
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.user_rating &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRating =
                (model as UserRatingAPIModel).responseData.userRatings;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.user_badge &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserBadge =
                (model as UserBadgeAPIModel).responseData.userBadges;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.portfolio &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            userPortfulioModel =
                (model as UserPortFolioAPIModel).responseData.userPortfulio;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.about_skills &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            aboutModel = (model as AboutAPIModel).responseData.about;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsGetPublicProfile(userId) async {
    try {
      await APIViewModel().req<PublicProfileAPIModel>(
        context: context,
        apiState: APIState(APIType.public_profile, this.runtimeType, null),
        url: APIProfileCFg.PUBLIC_USER_GET_URL
            .replaceAll("#userId#", userId.toString()),
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  isDuplicate(UserBadgeModel userBadge) {
    for (final model in listUserBadgeModelShowList) {
      if (model.id > userBadge.id && model.type == userBadge.type) {
        listUserBadgeModelShowList.remove(model);
        return model;
      }
    }
    return userBadge;
  }

  updateUserBadges() async {
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userModel.id,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;
                //List<UserBadgeModel> listUserBadgeModelShowList2 = [];
                for (UserBadgeModel userBadge in listUserBadgeModel) {
                  if (userBadge.isVerified) {
                    final mod = isDuplicate(userBadge);
                    if (!listUserBadgeModelShowList.contains(mod))
                      listUserBadgeModelShowList.add(mod);
                  }
                }
              } else {
                //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                showToast(context: context, msg: "Sorry, something went wrong");
              }
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog("profile screen new not in");
          }
        },
      );
    }
  }

  refreshData() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
      }
      if (mounted) {
        await APIViewModel().req<UserRatingSummaryAPIModel>(
          context: context,
          apiState:
              APIState(APIType.user_rating_summary, this.runtimeType, null),
          url: APIProfileCFg.GET_USER_RATING_SUMMARY_URL,
          reqType: ReqType.Get,
          isLoading: false,
          param: {"UserId": userId},
        );
      }
      if (mounted) {
        await APIViewModel().req<UserRatingAPIModel>(
          context: context,
          apiState: APIState(APIType.user_rating, this.runtimeType, null),
          url: APIProfileCFg.USER_RATING_GET_URL,
          param: {"UserId": userId},
          isLoading: false,
          reqType: ReqType.Get,
        );
      }
      if (mounted) {
        await APIViewModel().req<UserBadgeAPIModel>(
          context: context,
          apiState: APIState(APIType.user_badge, this.runtimeType, null),
          url: APIBadgeCfg.BADGE_USER_GET_URL
              .replaceAll("#userId#", userId.toString()),
          isLoading: false,
          reqType: ReqType.Get,
        );
      }
      if (mounted) {
        await APIViewModel().req<UserPortFolioAPIModel>(
          context: context,
          apiState: APIState(APIType.portfolio, this.runtimeType, null),
          url: APIProfileCFg.USER_PORTFOLIO_URL,
          param: {"UserId": userId},
          isLoading: false,
          reqType: ReqType.Get,
        );
      }
      if (mounted) {
        await APIViewModel().req<AboutAPIModel>(
          context: context,
          apiState: APIState(APIType.about_skills, this.runtimeType, null),
          url: APIProfileCFg.ABOUT_GET_URL,
          param: {"UserId": userId},
          isLoading: false,
          reqType: ReqType.Get,
        );
      }
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    userModel = null;
    listUserRatingSummary = null;
    listUserRating = null;
    listUserBadge = null;
    userPortfulioModel = null;
    aboutModel = null;
    uploadFileModelCover = null;
    uploadFileModelProfile = null;

    listItems = null;
    listUserBadgeModel = null;
    listUserBadgeModelShowList = null;
    userBadgeModel = null;

    try {
      taskCtrl.dispose();
      profileController.dispose();
    } catch (e) {}

    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    if (widget.profileUserID == null) {
      //  PROFILE USER
      isPublicUser = false;
      userModel = userData.userModel;
      userId = userModel.id;
      await wsGetPublicProfile(userId);
    } else {
      isPublicUser = true;
      profileUserID = widget.profileUserID;
      //userId =
      //taskCtrl.getTaskModel().userId; //  COMING FROM TASK DETAILS
      await wsGetPublicProfile(widget.profileUserID);
    }

    try {
      listItems = [
        {
          "icon": "assets/images/badge/phone_verified.png",
          "title": "Mobile",
          "desc":
              "You have received this badge because you're mobile number is verified.",
          // "You will receive this badge when you mobile number is verified. This will also allow you to receive Case related notifications and make calls* from the portal.",
          "btnTitle": null,
        },
        {
          "icon": "assets/images/badge/gmail_verified.png",
          "title": "Email",
          "desc": "You have received this badge, your email is verified.",

          // "You receive this badge upon verification of your email address. This will also allow you to receive case related notifications and send or receive emails from the portal.",
        },
        {
          "icon": "assets/images/badge/passport_verified.png",
          "title": "Passport / Photo ID",
          "desc": "You have received this badge, your ID Card is verified",
          // "You receive this badge when your ID is verified with your Passport, Driving License or any other acceptable form of ID.",
        },
        {
          "icon": "assets/images/badge/facebook_verified.png",
          "title": "Facebook",
          "desc":
              "You have received this badge, connected with Facebook account.",
          // "You receive this badge when you connect your Facebook account.",
        },
        {
          "icon": "assets/images/badge/customer_privacy_verified.png",
          "title": "Customer Privacy",
          "desc":
              "You have received this badge, Accepted Customer Privacy Statement",
          // "You receive this badge when you agree to the Customer Privacy Statement. Customer Privacy accepted at 16-Mar-2020 18:43",
        },
        {
          "icon": "assets/images/badge/eid_verified.png",
          "title": " National ID Card",
          "desc":
              "You have received this badge, Your ID is verified through National ID Card system.",
          // "You receive this badge when your ID is verified through an EID system.",
        },
        {
          "icon": "assets/images/badge/eid_verified.png",
          "title": "Electronic ID Verification",
          "desc":
              "You have received this badge, Your ID is verified through an EID system.",
          // "You receive this badge when your ID is verified through an EID system.",
        },
        //"Application tutorial"
      ];

      updateUserBadges();
      refreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //backgroundColor: MyTheme.parallexToolbarColor,
        //resizeToAvoidBottomPadding: true,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawHeader(userModel, this.runtimeType)),
      ),
    );
  }

  drawLayout() {
    //  @UserProfileActivity in kotlin
    return Container(
      //color: MyTheme.bgColor,
      color: Colors.white,
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          SizedBox(height: 20),
          drawBasicInfo(),
          drawBadges(),
          SizedBox(height: 10),
          drawUserSwitchView(),
          SizedBox(height: 20),
          drawUserRatingAndSummaryView(
              userModel, listUserRatingSummary, listUserRating),
          drawPortFolioView(userPortfulioModel),
          //drawBadgesView(listUserBadge),
          //drawMoreButtonView(),
          drawAboutSkillsView(aboutModel),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}
