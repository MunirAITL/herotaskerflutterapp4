class TaskerProfileVerificationData {
  int userId;
  int userPortfulioId;
  int taskAlertId;
  int userSkillId;
  int userProfilePictureId;
  int eIDVerificationId;
  int emailVerificationId;
  int mobileVerificationId;
  int paymentMethodVerificationId;

  TaskerProfileVerificationData(
      {this.userId,
      this.userPortfulioId,
      this.taskAlertId,
      this.userSkillId,
      this.userProfilePictureId,
      this.eIDVerificationId,
      this.emailVerificationId,
      this.mobileVerificationId,
      this.paymentMethodVerificationId});

  TaskerProfileVerificationData.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    userPortfulioId = json['UserPortfulioId'] ?? 0;
    taskAlertId = json['TaskAlertId'] ?? 0;
    userSkillId = json['UserSkillId'] ?? 0;
    userProfilePictureId = json['UserProfilePictureId'] ?? 0;
    eIDVerificationId = json['EIDVerificationId'] ?? 0;
    emailVerificationId = json['EmailVerificationId'] ?? 0;
    mobileVerificationId = json['MobileVerificationId'] ?? 0;
    paymentMethodVerificationId = json['PaymentMethodVerificationId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserPortfulioId'] = this.userPortfulioId;
    data['TaskAlertId'] = this.taskAlertId;
    data['UserSkillId'] = this.userSkillId;
    data['UserProfilePictureId'] = this.userProfilePictureId;
    data['EIDVerificationId'] = this.eIDVerificationId;
    data['EmailVerificationId'] = this.emailVerificationId;
    data['MobileVerificationId'] = this.mobileVerificationId;
    data['PaymentMethodVerificationId'] = this.paymentMethodVerificationId;
    return data;
  }
}
