import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/scan_id_doc_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScanIDDocPage extends StatefulWidget {
  const ScanIDDocPage({Key key}) : super(key: key);

  @override
  _ScanIDDocPageState createState() => _ScanIDDocPageState();
}

class _ScanIDDocPageState extends ScanIDDocBase<ScanIDDocPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Scan identity document"),
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.redColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return drawRow();
  }
}
