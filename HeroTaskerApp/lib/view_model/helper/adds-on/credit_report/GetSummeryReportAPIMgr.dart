import 'dart:convert';
import 'dart:developer';
import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/data/model/ads-on/credit_report/getSummeryAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class GetSummeryReportApiMgr with Mixin {
  static final GetSummeryReportApiMgr _shared =
      GetSummeryReportApiMgr._internal();

  factory GetSummeryReportApiMgr() {
    return _shared;
  }

  GetSummeryReportApiMgr._internal();

  getSummeryReport({
    BuildContext context,
    String validationIdentifier,
    int userId,
    int companyId,
    Function(GetSummeryAPIModel) callback,
  }) async {
    var params = {
      "UserIdentifier": validationIdentifier,
      "UserId": userId,
      "UserCompanyId": companyId
    };
    //print("getSummeryReport bank is = $params");
    final jsonString = JsonString(json.encode(params));
    log(jsonString.source);
    try {
      await NetworkMgr()
          .req<GetSummeryAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: params,
        url: APICreditReportCfg.GET_SUMMARY_REPORT_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log("GET_SUMMARY_REPORT_URL  ERROR = " + e.toString());
    }
  }
}
