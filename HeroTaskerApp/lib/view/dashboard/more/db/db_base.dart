import 'package:aitl/config/app/status/NotiStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_page.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskSummaryUserDataModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/messages/chat_page.dart';
import 'package:aitl/view/dashboard/messages/comments/offers_comments_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_task_alert.dart';
import 'package:aitl/view/dashboard/mytasks/taskdetails_page.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:aitl/view_model/rx/ProfileController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BaseDashboardMoreStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final taskCtrl = Get.put(TaskCtrl());
  final profileController = Get.put(ProfileController());
  int posterSwitchValue = 0;
  bool isTasker = true;
  bool isLoading = true;

  drawUserSwitchView() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: ToggleSwitch(
          isBorderColor: true,
          minWidth: getWP(context, 35),
          minHeight: getHP(context, MyTheme.switchBtnHpa),
          initialLabelIndex: posterSwitchValue,
          cornerRadius: 50.0,
          fontSize: 20,
          activeBgColor: MyTheme.heroTheamColors,
          activeFgColor: Colors.white,
          inactiveBgColor: Colors.white,
          inactiveFgColor: MyTheme.heroTheamColors,
          labels: ['As a Hero', 'As a Client'],
          //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
          onToggle: (index) {
            posterSwitchValue = index;
            isTasker = (posterSwitchValue == 0) ? true : false;
            setState(() {});
          },
        ),
      ),
    );
  }

  drawStats(TaskSummaryUserDataModel taskSummaryUserData,
      List<UserRatingSummaryDataModel> listUserRatingSummary) {
    int draftVal = 0;
    int bidOnVal = 0;
    int openOfferVal = 0;
    int assignedVal = 0;
    int overDueVal = 0;
    int awaitingPayVal = 0;
    int taskCompletedVal = 0;
    if (taskSummaryUserData != null) {
      draftVal = taskSummaryUserData.posterTaskDraftCount ?? 0;
      bidOnVal = taskSummaryUserData.taskBidOnCount;
      openOfferVal = taskSummaryUserData.posterTaskOpenForBidCount;
      assignedVal = taskSummaryUserData.taskAssignedCount;
      overDueVal = taskSummaryUserData.taskOverDueCount;
      awaitingPayVal = taskSummaryUserData.taskAwaitingPaymentCount;
      taskCompletedVal = (isTasker)
          ? taskSummaryUserData.taskCompletedCount
          : taskSummaryUserData.posterTaskCompletedCount;
    }

    return Container(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(
                top: 10, bottom: 10, left: (!isTasker) ? 10 : 0, right: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                (!isTasker)
                    ? _drawStatsBox(MyTheme.hotdipPink, "Draft", draftVal)
                    : SizedBox(),
                _drawStatsBox(
                    MyTheme.airGreenColor,
                    (isTasker) ? "Bid On" : "Open for\noffers",
                    (isTasker) ? bidOnVal : openOfferVal),
                _drawStatsBox(MyTheme.hotdipPink, "Assigned", assignedVal),
                _drawStatsBox(MyTheme.airOrange, "Overdue", overDueVal),
                _drawStatsBox(
                    MyTheme.airOrange, "Awaiting\nPayments", awaitingPayVal),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Container(
              child: IntrinsicHeight(
                child: Row(
                  children: [
                    Container(
                      width: getWP(context, 40),
                      decoration: BoxDecoration(
                          color: MyTheme.bgColor2,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: Colors.black, width: .5)),
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Txt(
                                txt: taskCompletedVal.toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize + 1.5,
                                txtAlign: TextAlign.center,
                                isBold: false),
                            Txt(
                                txt: "Completed",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: getWP(context, 1)),
                    Expanded(
                      child: Container(
                        color: Colors.grey[100],
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20, bottom: 20),
                          child:
                              drawUserRatingSummaryView(listUserRatingSummary),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _drawStatsBox(Color txt1Color, String title, int val) {
    return Flexible(
      child: Column(
        children: [
          Txt(
              txt: val.toString(),
              txtColor: txt1Color,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.center,
              isBold: false),
          Txt(
              txt: title,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawUserRatingSummaryView(
      List<UserRatingSummaryDataModel> listUserRatingSummary) {
    if (listUserRatingSummary == null) return SizedBox();
    int rate = 0;
    int completionRate = 0;
    int reviews = 0;
    if (!isTasker) {
      rate = profileController.aveargeRatingAsPoster.value;
      completionRate = profileController.completionRateAsPoster.value;
      reviews = profileController.countAsPoster.value;
    } else {
      if (listUserRatingSummary.length > 0) {
        rate = listUserRatingSummary[0].avgRating.toInt();
        completionRate = listUserRatingSummary[0].taskerCompletionRate;
        reviews = listUserRatingSummary[0].ratingCount;
      }
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
              alignment: Alignment.center,
              child: Container(
                  //color: Colors.yellow,
                  child: UIHelper().getStarRatingView(
                w: getW(context),
                h: getH(context),
                rate: rate,
                reviews: reviews,
                isRow: false,
                starColor: Colors.red,
                txtColor: MyTheme.gray4Color,
              ))),
          //SizedBox(height: 5),
          Container(
              //color: Colors.yellow,
              child: UIHelper().getCompletionText(
                  pa: completionRate,
                  txtColor: MyTheme.gray4Color,
                  callbackInfo: () {
                    if (isTasker) {
                      showAlert(
                          context: context,
                          msg:
                              "Completion rate is the percentage of tasks assigned to the 'Hero' which were successfully completed");
                    } else {
                      showAlert(
                          context: context,
                          msg:
                              "Completion rate is the percentage of tasks assigned by the 'Client' which were successfully completed");
                    }
                  })),
        ],
      ),
    );
  }

  drawNotiView(List<NotiModel> listNoti, bool isNotiTab) {
    return Container(
      color: (listNoti.length > 0) ? Colors.white : MyTheme.bgColor,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Padding(
          padding: EdgeInsets.only(
              left: 10,
              right: 10,
              top: (!isNotiTab) ? 10 : 0,
              bottom: (!isNotiTab) ? 10 : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              (!isNotiTab)
                  ? Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Txt(
                          txt: "Notifications",
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    )
                  : SizedBox(),
              (listNoti.length > 0)
                  ? ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      primary: false,
                      itemCount: (!isNotiTab && listNoti.length > 10)
                          ? 10
                          : listNoti.length,
                      itemBuilder: (BuildContext context, int index) {
                        return drawNotiItem(listNoti[index], isNotiTab, index);
                      },
                    )
                  : (!isLoading)
                      ? Column(
                          children: [
                            (isNotiTab)
                                ? Container(
                                    width: getWP(context, 60),
                                    height: getWP(context, 40),
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/nf/nf_noti.png"),
                                        //fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            Container(
                                child: Txt(
                                    txt:
                                        "This is where we'll let you know about tasks, comments and other stuff. How about posting a task or finding one to do through browse tasks?",
                                    txtColor: MyTheme.gray4Color,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false)),
                          ],
                        )
                      : SizedBox(),
              (!isNotiTab && listNoti.length > 10)
                  ? _go2Notification()
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }

  drawNotiItem(NotiModel notiModel, bool isNotiTab, int index) {
    try {
      final doubletik =
          (notiModel.isRead) ? "check2_icon.png" : "check1_icon.png";

      final map = getNotiEvents(notiModel);
      var msg = '';
      var sentBy = '';
      if (!isNotiTab)
        msg = notiModel.initiatorDisplayName +
            ' commented on ' +
            notiModel.message;
      else {
        msg = map['description'] + ' ' + notiModel.message;
        msg = msg.trim();
        if (msg == "") return SizedBox();
        if (map['isTaskDetails'] as bool) {
          sentBy = notiModel.initiatorDisplayName ?? '';
          msg =
              "A new task has been posted matching your skills: [" + msg + "]";
        }
      }

      return GestureDetector(
        onTap: () async {
          //  munir->dashboardnotifications
          if (map['callback'] != null) Function.apply(map['callback'], []);
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: Container(
            //height: getHP(context, 25),
            color: Colors.transparent,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: (!isNotiTab) ? 22 : 25,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl((notiModel != null)
                        ? notiModel.initiatorImageUrl
                        : ServerUrls.MISSING_IMG),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //SizedBox(height: 20),
                        Txt(
                            txt: msg,
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        SizedBox(height: 5),
                        sentBy != ''
                            ? Padding(
                                padding: const EdgeInsets.only(bottom: 5),
                                child: Txt(
                                    txt: "Sent by " + sentBy,
                                    txtColor: MyTheme.gray4Color,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              )
                            : SizedBox(),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //SizedBox(width: 10),
                            Txt(
                                txt: DateFun.getTimeAgoTxt(
                                    notiModel.publishDateTime),
                                txtColor: MyTheme.gray4Color,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            Image.asset(
                              "assets/images/icons/" + doubletik,
                              width: 15,
                              height: 15,
                              color: (notiModel.isRead)
                                  ? Colors.green
                                  : Colors.grey,
                            ),
                          ],
                        ),
                        //SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
    return SizedBox();
  }

  _go2Notification() {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
          onPressed: () {
            Get.to(() => NotiPage());
          },
          child: Txt(
            txt: "More...",
            txtColor: MyTheme.brandColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: false,
          )),
    );
  }

  getNotiEvents(NotiModel notiModel) {
    var description = '';
    var callback;
    bool isTaskDetails = false;
    final name = notiModel.initiatorDisplayName;
    if (notiModel.userId != userData.userModel.id &&
        notiModel.entityName == NotiStatusCfg.NOTIFICATION_NAME_Task) {
      description = "[Task Alert] " + name + " has posted this task:";
      callback = () async => go2TaskDetailsPage(notiModel, description, true);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskBidding) {
      description =
          "[Bid for this job] " + name + " has made an offer for your task. ";
      if (notiModel.taskPrice > 0)
        description += AppDefine.CUR_SIGN +
            notiModel.taskPrice.toStringAsFixed(2).replaceAll(".00", "");
      callback = () async => go2CommentsPage(notiModel, description);
    } else if (notiModel.entityName ==
            NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingComment ||
        notiModel.entityName ==
            NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingAdminComment) {
      description = "[Task Comments] " + name + " has commented on your task";
      switch (notiModel.notificationEventId) {
        case 1011:
          description =
              "[Private Message] " + name + " has sent a message on your task";
          callback = () async =>
              go2ChatPage(notiModel: notiModel, description: description);
          break;
        case 1012:
          description =
              "[Offer Message] " + name + " has sent a message on your offer";
          callback = () async => go2CommentsPage(notiModel, description);
          break;
        default:
          callback = () => go2OfferChatPage(notiModel, description);
        /*callback = () => go2ChatPage(
              taskId: notiModel.entityId,
              isPublicChat: true,
              description: description);*/ //  Public Chat Page
      }
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingRequestPayment) {
      description =
          "[Private Message] " + name + " has sent a message on your task";
      callback = () async =>
          go2ChatPage(notiModel: notiModel, description: description);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingAccepted) {
      description =
          "[Accept Offer] " + name + " has accepted your offer for task";
      callback = () async => go2CommentsPage(notiModel, description);
      //callback = () => go2OfferChatPage(notiModel, description);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskPaymentConfirmation) {
      description =
          "[Payment Confirmation] " + name + " has received payment for task";
      callback = () async => go2TaskPaymentConfirmation(notiModel);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_UserRating) {
      isTaskDetails = true;
      description =
          "[User Review] " + name + " has given rating and review for task";
      callback = () async => go2TaskDetailsPage(
          notiModel, description, true); //go2UserRating(notiModel);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_Resolution) {
      callback = () async =>
          go2ChatPage(notiModel: notiModel, description: description);
    } else {
      isTaskDetails = true;
      //callback = () => go2NotiTab(notiModel);
      callback = () async => go2TaskDetailsPage(notiModel, description, true);
    }
    return {
      'description': description,
      'callback': callback,
      'isTaskDetails': isTaskDetails,
    };
  }

  //  ******************************* NOTIFICATION EVENTS START HERE...

  //  go2
  go2TaskDetailsPage(NotiModel notiModel, description, bool isTaskid) async {
    try {
      await APIViewModel().req<GetTaskAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASK_URL.replaceAll(
            "#taskId#",
            (isTaskid)
                ? notiModel.entityId.toString()
                : notiModel.description.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              final taskModel = model.responseData.task;
              if (taskModel != null) {
                final taskModel2 =
                    taskCtrl.getTaskModel(); //  temporary store old task object
                await taskCtrl.setTaskModel(taskModel);
                await Get.to(() => TaskDetailsPage(
                      taskModel: model.responseData.task,
                      description: description,
                      title: notiModel.message,
                      body: notiModel.description,
                    )).then((value) {
                  if (mounted) {
                    Future.delayed(Duration(seconds: 1), () {
                      if (mounted) {
                        taskCtrl.setTaskModel(
                            taskModel2); //  re- store old task object back
                        if (value == "STATE_RELOAD_TAB_FIND_WORK") {
                          StateProvider()
                              .notify(ObserverState.STATE_RELOAD_TAB, 2);
                        }
                      }
                    });
                  }
                });
              }
            }
          } else {
            Get.to(() =>
                NofiTaskAlert(notiModel: notiModel, description: description));
          }
        },
      );
    } catch (e) {}
  }

  go2CommentsPage(NotiModel notiModel, description) async {
    try {
      await APIViewModel().req<TaskBiddingAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASKBIDDING_URL
            .replaceAll("#taskBiddingId#", notiModel.entityId.toString()),
        reqType: ReqType.Get,
        callback: (model2) async {
          if (model2 != null && mounted) {
            if (model2.success) {
              await APIViewModel().req<GetTaskAPIModel>(
                context: context,
                url: APIMyTasksCfg.GET_TASK_URL.replaceAll("#taskId#",
                    model2.responseData.taskBidding.taskId.toString()),
                reqType: ReqType.Get,
                callback: (model) async {
                  if (model != null && mounted) {
                    if (model.success) {
                      final taskModel = model.responseData.task;
                      if (taskModel != null) {
                        await taskCtrl.setTaskModel(taskModel);
                        await Get.to(() => OffersCommentsPage(
                              taskBiddingsModel:
                                  model2.responseData.taskBidding,
                              description: description,
                              title: notiModel.message,
                              body: notiModel.description,
                            )).then((value) {
                          if (mounted) {
                            Future.delayed(Duration(seconds: 1), () {
                              if (mounted) taskCtrl.setTaskModel(null);
                            });
                          }
                        });
                      }
                    }
                  }
                },
              );
            }
          }
        },
      );
    } catch (e) {}
  }

  go2OfferChatPage(NotiModel notiModel, description) async {
    try {
      var taskId = 0;
      try {
        final webUrlArr = notiModel.webUrl.toString().split('-');
        taskId = int.parse(webUrlArr[webUrlArr.length - 1]);
      } catch (e) {}
      await APIViewModel().req<GetTaskAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASK_URL
            .replaceAll("#taskId#", taskId.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              final taskModel = model.responseData.task;
              if (taskModel != null) {
                await taskCtrl.setTaskModel(taskModel);
                await Get.to(() => ChatPage(
                      isPublicChat: null,
                      description: description,
                    )).then((value) {
                  if (mounted) {
                    Future.delayed(Duration(seconds: 1), () {
                      if (mounted) taskCtrl.setTaskModel(null);
                    });
                  }
                });
              }
            }
          }
        },
      );
    } catch (e) {}
  }

  go2ChatPage(
      {NotiModel notiModel, bool isPublicChat, String description}) async {
    try {
      await APIViewModel().req<GetTaskAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASK_URL
            .replaceAll("#taskId#", notiModel.entityId.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              final taskModel = model.responseData.task;
              if (taskModel != null) {
                await taskCtrl.setTaskModel(taskModel);
                await Get.to(() => ChatPage(
                      isPublicChat: isPublicChat,
                      description: description,
                    )).then((value) {
                  if (mounted) {
                    Future.delayed(Duration(seconds: 1), () {
                      if (mounted) taskCtrl.setTaskModel(null);
                    });
                  }
                });
              }
            }
          } else {
            final taskModel = TaskModel.fromJson({
              'Id': notiModel.entityId,
              'Status': "ACTIVE", //notiModel.
              'OwnerName': notiModel.initiatorDisplayName,
              'Title': notiModel.entityTitle != ''
                  ? notiModel.entityTitle
                  : 'Private messages',
            });
            if (taskModel != null) {
              await taskCtrl.setTaskModel(taskModel);
              await Get.to(() => ChatPage(
                    isPublicChat: isPublicChat,
                    isSupport: true,
                    description: description,
                  )).then((value) {
                if (mounted) {
                  Future.delayed(Duration(seconds: 1), () {
                    if (mounted) taskCtrl.setTaskModel(null);
                  });
                }
              });
            }
          }
        },
      );
    } catch (e) {}
  }

  go2TaskPaymentConfirmation(NotiModel notiModel) async {
    await APIViewModel().req<TaskBiddingAPIModel>(
      context: context,
      url: APIMyTasksCfg.GET_TASKBIDDING_URL
          .replaceAll("#taskBiddingId#", notiModel.entityId.toString()),
      reqType: ReqType.Get,
      callback: (model2) async {
        if (model2 != null && mounted) {
          if (model2.success) {
            await APIViewModel().req<GetTaskAPIModel>(
              context: context,
              url: APIMyTasksCfg.GET_TASK_URL.replaceAll("#taskId#",
                  model2.responseData.taskBidding.taskId.toString()),
              reqType: ReqType.Get,
              callback: (model) async {
                if (model != null && mounted) {
                  if (model.success) {
                    final taskModel = model.responseData.task;
                    if (taskModel != null) {
                      await taskCtrl.setTaskModel(taskModel);
                      await Get.to(() => FundPaymentPage(
                              taskBidding: model2.responseData.taskBidding))
                          .then((value) {
                        if (mounted) {
                          Future.delayed(Duration(seconds: 1), () {
                            if (mounted) taskCtrl.setTaskModel(null);
                          });
                        }
                      });
                    }
                  }
                }
              },
            );
          }
        }
      },
    );

    await APIViewModel().req<TaskBiddingAPIModel>(
      context: context,
      url: APIMyTasksCfg.GET_TASKBIDDING_URL
          .replaceAll("#taskBiddingId#", notiModel.entityId.toString()),
      reqType: ReqType.Get,
      callback: (model) async {
        if (model != null && mounted) {
          if (model.success) {
            Get.to(() =>
                FundPaymentPage(taskBidding: model.responseData.taskBidding));
          }
        }
      },
    );
  }

  go2UserRating(NotiModel notiModel) {}

  //  ***************************************************** NOTIFICATION EVENTS END

  go2NotiTab(NotiModel notiModel) async {
    Get.to(() => NotiPage(
          notificationId: notiModel.id,
          title: notiModel.message,
          body: notiModel.description,
          webUrl: notiModel.webUrl,
        ));
  }

  go2TaskNotiWeb(NotiModel notiModel, String description) async {
    Get.to(() => WebScreen(url: notiModel.webUrl, title: notiModel.message));
  }

  @override
  drawLayout() {
    // TODO: implement drawLayout
    throw UnimplementedError();
  }

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }
}
