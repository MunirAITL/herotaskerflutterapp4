import 'Server.dart';

class APIActionReqCfg {
  //  case review, action required, agreement, digital sign
  static const String CASE_REVIEW = Server.BASE_URL +
      "/api/mortgagecaseinfo/getbyuseridandcompanyidandclientagreementstatus?UserId=#UserId#&UserCompanyInfoId=#UserCompanyInfoId#&ClientAgreementStatus=#ClientAgreementStatus#";

  static const String USERNOTE_PUT_URL = Server.BASE_URL + "/api/usernote/put";

  static const String USERNOTEBYENTITY_URL = Server.BASE_URL +
      "/api/usernote/getusernotebyentityidandentitynameanduseridandstatus?EntityId=#entityId#&EntityName=#entityName#&Status=#status#&UserId=#userId#";

  static const String CaseDigitalSignByCustomerURL =
      Server.BASE_URL + "/apps/case-digital-sign-by-customer/-#CaseID#";
  static const String CLIENTAGREEMENTURL =
      Server.BASE_URL + "/apps/client-agreement/-#CaseID#";

  //  submit Case
  static const String SUBMITCASE_URL =
      Server.BASE_URL + "/api/mortgagecasepaymentinfo/post";

  static const String NEWCASE_URL = Server.BASE_URL +
      "/api/task/taskinformationbysearchformobileapplication/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#UserCompanyId#";

  static const String CLIENT_AGREEMENT_URL =
      Server.BASE_URL + "/apps/client-agreement/-#CaseID#";

  static const GET_TASK_VERIFY_URL =
      Server.BASE_URL + "/api/users/gettaskerprofileverificationdata?UserId=";
}
