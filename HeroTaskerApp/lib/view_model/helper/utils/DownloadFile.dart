import 'dart:io';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';

class DownloadFile {
  Future<File> saveCSV(List<TaskPaymentsModel> listTaskPaymentModel) async {
    try {
      Directory directory;
      if (Platform.isIOS) {
        directory = await getApplicationDocumentsDirectory();
      } else {
        directory = await getApplicationDocumentsDirectory();
      }
      final folder =
          await new Directory('${directory.path}/csv').create(recursive: true);
      var formatter = new DateFormat('dd-MMM-yyyy');
      String formattedDate = formatter.format(DateTime.now());
      final file = new File('${folder.path}/' + formattedDate + '.csv');

      //  create csv file
      var data =
          'Invoice Id,Task Id,Task Title,Payment Method,Payable Amount,Payment Amount,Net Total Amount,Date,\n';
      for (var model in listTaskPaymentModel) {
        data += (model.id.toString() + ',');
        data += (model.taskId.toString() + ',');
        data += (model.taskTitle.toString() + ',');
        data += (model.paymentMethod.toString() + ',');
        data += (model.payableAmount.toString() + ',');
        data += (model.netTotalAmount.toString() + ',');
        data += (model.creationDate.toString() + ',\n');
      }
      await file.writeAsString(data, mode: FileMode.write, flush: true);
      return file;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
