import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/data/model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/data/model/auth/role/PostUserComRoleAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/main.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/auth/loginWith/auth_base.dart';
import 'package:aitl/view/auth/role/worker/worker_page.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/role_dialog.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/picker/DatePickerView.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:aitl/view/widgets/views/TCView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/LoginApiMgr.dart';
import 'package:aitl/view_model/helper/auth/RegAPIMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../widgets/input/mobile/InputTitleBoxfWithCountryCode.dart';
import 'email/check_email_page.dart';

class RegScreen extends StatefulWidget {
  final bool isOTPComeThrough;
  String firstName = "";
  String lastName = "";
  String emailAddress = "";
  String phoneNumber = "";

  RegScreen(
      {this.firstName,
      this.lastName,
      this.emailAddress,
      this.phoneNumber,
      this.isOTPComeThrough = false});

  @override
  State createState() => _RegScreenState();
}

enum genderEnum { male, female }

class _RegScreenState extends BaseAuth<RegScreen> with APIStateListener {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusPwd = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  bool isobscureText = true;

  String dob = "";

  String dobDD = "";

  String dobMM = "";

  String dobYY = "";

  genderEnum _gender; // = genderEnum.male;

  bool isStep2 = false;
  bool isLoading = false;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          //checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          //checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  callCheckUserEmailAPI(LoginAPIModel model, bool isRoute) async {
    try {
      await APIViewModel().req<PostEmailOtpAPIModel>(
        context: context,
        url: APIAuthCfg.POSTEMAILOTP_POST_URL,
        reqType: ReqType.Post,
        param: {
          "Email": _email.text.trim(),
          "Status": "101",
        },
        callback: (model2) async {
          if (mounted) {
            if (model2 != null) {
              if (model2.success) {
                await APIViewModel().req<SendUserEmailOtpAPIModel>(
                    context: context,
                    url: APIAuthCfg.SENDUSEREMAILOTP_GET_URL.replaceAll(
                        "#otpId#", model2.responseData.userOTP.id.toString()),
                    reqType: ReqType.Get,
                    callback: (model3) async {
                      if (mounted) {
                        if (model3 != null) {
                          if (isRoute) {
                            if (model3.success) {
                              Get.off(() => CheckEmailPage(
                                  email: _email.text.trim(),
                                  userOTP: model3.responseData.userOTP));
                            } else {
                              final err =
                                  model3.messages.getSenduseremailotplogin[0];
                              showToast(context: context, msg: err, which: 0);
                            }
                          }
                        }
                      }
                    });
              } else {
                if (isRoute) {
                  final err = model2.messages.postUserotp[0];
                  showToast(context: context, msg: err, which: 0);
                }
              }
            }
          }
        },
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;
    return fullNumber;
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    _apiStateProvider.unsubscribe(this);
    _apiStateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    getMobCode();
    try {
      _fname.text = widget.firstName;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _lname.text = widget.lastName;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _email.text = widget.emailAddress;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      _mobile.text = widget.phoneNumber;
    } catch (e) {
      myLog(e.toString());
    }
  }

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, _email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, _pwd)) {
      return false;
    }
    return true;
  }

  /*validate2() {
    if (!UserProfileVal().isDOBOK(context, dob)) {
      return false;
    }
    return true;
  }*/

  refreshStep1() {
    setState(() {
      isStep2 = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: (widget.isOTPComeThrough) ? true : false,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          //titleSpacing: 0,
          title: (isStep2)
              ? UIHelper().drawAppbarTitle(title: "Create an account")
              : GestureDetector(
                  onTap: () {
                    Get.offAll(() => LoginLandingScreen());
                  },
                  child: Container(
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        FittedBox(
                            fit: BoxFit.cover,
                            child: Icon(Icons.arrow_back_ios,
                                size: MyTheme.fontSize)),
                        SizedBox(width: 5),
                        Txt(
                            txt: "Sign in",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        //shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                      ],
                    ),
                  ),
                ),
          centerTitle: false,
          leading: (isStep2)
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      isStep2 = false;
                    });
                  },
                  icon: Icon(Icons.arrow_back_ios, size: MyTheme.fontSize),
                )
              : null,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (!isStep2) ? drawRegView1() : drawRegView2(),
        ),
      ),
    );
  }

  drawRegView1() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: SingleChildScrollView(
            child: Column(
          children: [
            Container(
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  //SizedBox(height: 30),
                  Txt(
                    txt: "Let's Get Started",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                  //SizedBox(height: 10),
                  Txt(
                    txt: "Create an account to get started",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.left,
                    fontWeight: FontWeight.w300,
                    txtLineSpace: 1.4,
                    isBold: false,
                  ),
                  SizedBox(height: MyTheme.fontSize),
                  drawInputBox(
                    context: context,
                    title: "First Name",
                    input: _fname,
                    ph: "",
                    kbType: TextInputType.name,
                    inputAction: TextInputAction.next,
                    focusNode: focusFname,
                    focusNodeNext: focusLname,
                    len: 20,
                  ),
                  SizedBox(height: 20),
                  drawInputBox(
                    context: context,
                    title: "Last Name",
                    input: _lname,
                    ph: "",
                    kbType: TextInputType.name,
                    inputAction: TextInputAction.next,
                    focusNode: focusLname,
                    focusNodeNext: focusEmail,
                    len: 20,
                  ),
                  SizedBox(height: 20),
                  drawInputBox(
                    context: context,
                    title: "Email",
                    input: _email,
                    ph: "",
                    kbType: TextInputType.emailAddress,
                    inputAction: TextInputAction.next,
                    focusNode: focusEmail,
                    focusNodeNext:
                        widget.phoneNumber == null ? focusMobile : focusPwd,
                    len: 50,
                  ),
                  SizedBox(height: 20),
                  widget.phoneNumber == null
                      ? drawInputBoxWithCountryCode(
                          context: context,
                          title: "Phone Number",
                          input: _mobile,
                          ph: "",
                          kbType: TextInputType.phone,
                          inputAction: TextInputAction.next,
                          focusNode: focusMobile,
                          focusNodeNext: focusPwd,
                          len: 15,
                          isWhiteBG: false,
                          countryCode: countryCode,
                          countryName: countryName,
                          getCountryCode: (value) {
                            countryCode = value.toString();
                            print("Country Code Clik = " + countryCode);
                            PrefMgr.shared
                                .setPrefStr("countryName", value.code);
                            PrefMgr.shared
                                .setPrefStr("countryCode", value.toString());
                          })
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Txt(
                                txt: "Mobile Number",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w400,
                                isBold: false),
                            SizedBox(height: 10),
                            TxtBox(txt: widget.phoneNumber),
                          ],
                        ),
                  /*  Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Phone Number",
                        input: _mobile,
                        ph: "0898215324232",
                        kbType: TextInputType.phone,
                        len: 20,
                      ),
                    ),*/
                  SizedBox(height: 20),
                  drawInputBox(
                    context: context,
                    title: "Password",
                    input: _pwd,
                    ph: "",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.done,
                    focusNode: focusPwd,
                    len: 20,
                    isPwd: true,
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            TCView(screenName: 'Continue'),
            SizedBox(height: 30),
            MMBtn(
                txt: "Continue",
                height: getHP(context, 7),
                width: getW(context),
                radius: 8,
                callback: () {
                  if (validate1()) {
                    isStep2 = true;
                    getMobCode();
                    setState(() {});
                  }
                }),
            SizedBox(height: 20),
            /*
            Align(
              alignment: Alignment.center,
              child: Txt(
                txt: "Or continue with",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 5, right: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  /*AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    icon: "ic_fb",
                    txt: "Continue with Facebook",
                    callback: () {
                      loginWithFB(this.runtimeType);
                    },
                  ),
                  SizedBox(height: 10),*/
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    icon: "ic_google",
                    txt: "Continue with Google",
                    callback: () {
                      loginWithGoogle(this.runtimeType);
                    },
                  ),
                  SizedBox(height: 10),
                  Platform.isIOS
                      ? AuthHelper().signInWithApple(
                          context: context,
                          height: getHP(context, MyTheme.btnHpa),
                          width: getW(context),
                          callback: () async {
                            if (await SignInWithApple.isAvailable()) {
                              signInWithApple(this.runtimeType);
                            }
                          })
                      : SizedBox(),
                  SizedBox(height: 20),
                ],
              ),
            ),*/
          ],
        )),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // drawCompSwitch(),
                SizedBox(height: 10),
                DatePickerView(
                  cap: "Select Date of Birth",
                  dt: (dob == '') ? 'Select Date' : dob,
                  txtColor: Colors.black,
                  initialDate: dateDOBlast,
                  firstDate: dateDOBfirst,
                  lastDate: dateDOBlast,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                          final dobArr = dob.toString().split('-');
                          this.dobDD = dobArr[0];
                          this.dobMM = dobArr[1];
                          this.dobYY = dobArr[2];
                        } catch (e) {
                          myLog(e.toString());
                        }
                      });
                    }
                  },
                ),
                SizedBox(height: 40),
                drawGender(),
                SizedBox(height: 40),
                MMBtn(
                    txt: "Get Started",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {
                      //if (validate2()) {
                      setState(() {
                        isLoading = true;
                      });
                      RegAPIMgr().wsRegAPI(
                          context: context,
                          email: _email.text.trim(),
                          pwd: _pwd.text.trim(),
                          fname: _fname.text.trim(),
                          lname: _lname.text.trim(),
                          mobile: getFullPhoneNumber(
                                  countryCodeTxt: countryCode,
                                  number: _mobile.text.toString())
                              .trim(),
                          countryCode: countryCode,
                          dob: dob,
                          dobDD: dobDD,
                          dobMM: dobMM,
                          dobYY: dobYY,
                          callback: (model) async {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  //  recall login to get cookie
                                  LoginAPIMgr().wsLoginAPI(
                                      context: context,
                                      email: _email.text.trim(),
                                      pwd: _pwd.text.trim(),
                                      callback: (model2) async {
                                        if (model2 != null && mounted) {
                                          try {
                                            setState(() {
                                              isLoading = false;
                                            });
                                            if (model2.success) {
                                              openRoleDialog(
                                                  context: context,
                                                  callback:
                                                      (communityId) async {
                                                    final user = model2
                                                        .responseData.user;
                                                    await APIViewModel().req<
                                                            PostUserComRoleAPIModel>(
                                                        context: context,
                                                        url: APIAuthCfg
                                                            .POST_ROLETYPE_URL,
                                                        reqType: ReqType.Post,
                                                        param: {
                                                          "Email": user.email,
                                                          "Password": "",
                                                          "ConfirmPassword": "",
                                                          "IsCouncilTask": "",
                                                          "UserCompanyCouncilId":
                                                              "",
                                                          "FirstName":
                                                              user.firstName,
                                                          "LastName":
                                                              user.lastName,
                                                          "MobileNumber":
                                                              user.mobileNumber,
                                                          "Address":
                                                              user.address ??
                                                                  '',
                                                          "Latitude": "",
                                                          "Longitude": "",
                                                          "CommunityId":
                                                              communityId,
                                                          "Persist": false,
                                                          "CheckSignUpMobileNumber":
                                                              true,
                                                          "CountryCode": "880",
                                                          "Status": "101",
                                                          "OTPCode": "",
                                                          "BirthDay": "",
                                                          "BirthMonth": "",
                                                          "BirthYear": "",
                                                          "isTermsConditions":
                                                              false,
                                                          "dialCode": "",
                                                          "Id": user.id
                                                        },
                                                        callback:
                                                            (model3) async {
                                                          if (mounted &&
                                                              model3 != null) {
                                                            if (model3
                                                                .success) {
                                                              await DBMgr.shared
                                                                  .setUserProfile(
                                                                      user: model2
                                                                          .responseData
                                                                          .user);
                                                              await userData
                                                                  .setUserModel();
                                                              if (communityId ==
                                                                  11) {
                                                                await callCheckUserEmailAPI(
                                                                    model2,
                                                                    false);
                                                                //  if 11 = worker
                                                                Get.to(() => WorkerRolePage(
                                                                    userModel: model2
                                                                        .responseData
                                                                        .user));
                                                              } else {
                                                                if (model
                                                                    .responseData
                                                                    .user
                                                                    .isMobileNumberVerified) {
                                                                  Get.offAll(() =>
                                                                      DashboardPage());
                                                                } else {
                                                                  if (!Server
                                                                      .isOtp) {
                                                                    Get.offAll(() =>
                                                                        DashboardPage());
                                                                  } else {
                                                                    callCheckUserEmailAPI(
                                                                        model2,
                                                                        true);
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        });
                                                  });
                                            } else {
                                              try {
                                                if (mounted) {
                                                  final err = model2
                                                      .errorMessages.login[0]
                                                      .toString();
                                                  showToast(
                                                      context: context,
                                                      msg: err);
                                                }
                                              } catch (e) {
                                                myLog(e.toString());
                                              }
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        }
                                      });
                                } else {
                                  try {
                                    setState(() {
                                      isLoading = false;
                                    });
                                    if (mounted) {
                                      final err = model
                                          .errorMessages.register[0]
                                          .toString();
                                      showToast(context: context, msg: err);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          });
                    }
                    //}
                    ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "Gender",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            fontWeight: FontWeight.w400,
            txtAlign: TextAlign.start,
          ),
          //SizedBox(height: 10),
          Theme(
            data: MyTheme.radioThemeData,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Male",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Female",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
/*
  drawCompSwitch() {
    return Container(
      //color: MyTheme.brandColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 50),
                      child: Txt(
                        txt: "Do you have an adviser to assist you with your case? If not we can help you find one.",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: true,
                        //txtLineSpace: 1.5,
                      ),
                    ),
                  ),
                  //SizedBox(width: 10),
                  SwitchView(
                    onTxt: "Yes",
                    offTxt: "No",
                    value: _isSwitch,
                    onChanged: (value) {
                      _isSwitch = value;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            (_isSwitch)
                ? Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 30),
                        Txt(txt: "Company Name", txtColor: Colors.black, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: true),
                        SizedBox(height: 20),
                        InputBox(
                          ctrl: _compName,
                          lableTxt: "Birilliant Company",
                          kbType: TextInputType.text,
                          len: 100,
                          isPwd: false,
                        ),
                      ],
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }*/
}
