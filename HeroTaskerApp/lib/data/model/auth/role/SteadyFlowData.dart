import '../UserModel.dart';

class SteadyFlowData {
  int userId;
  UserModel user;
  String whatsMostImportantInTheOportunitiesYouAreLookingFor;
  String whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari;
  String whereDoYouWantToWork;
  String whatsYourHighestLevelOfEducation;
  String whatDoesYourWorkSituationLookLikeRightNow;
  String whichOfTheFollowinDoYouUse;
  String howDoYouKeepTrackOfYourBudgetOrFinancialPlan;
  String inTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities;
  dynamic whatIamlookingfor;
  dynamic languages;
  dynamic qualifications;
  dynamic experiences;
  dynamic goAround;
  dynamic headline;
  dynamic briefBio;
  int id;

  SteadyFlowData(
      {this.userId,
      this.user,
      this.whatsMostImportantInTheOportunitiesYouAreLookingFor,
      this.whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari,
      this.whereDoYouWantToWork,
      this.whatsYourHighestLevelOfEducation,
      this.whatDoesYourWorkSituationLookLikeRightNow,
      this.whichOfTheFollowinDoYouUse,
      this.howDoYouKeepTrackOfYourBudgetOrFinancialPlan,
      this.inTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities,
      this.whatIamlookingfor,
      this.languages,
      this.qualifications,
      this.experiences,
      this.goAround,
      this.headline,
      this.briefBio,
      this.id});

  SteadyFlowData.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    whatsMostImportantInTheOportunitiesYouAreLookingFor =
        json['WhatsMostImportantInTheOportunitiesYouAreLookingFor'];
    whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari =
        json['WhatDoYouWantToDoWithExtraCashYouEarnThroughShohokari'];
    whereDoYouWantToWork = json['WhereDoYouWantToWork'];
    whatsYourHighestLevelOfEducation = json['WhatsYourHighestLevelOfEducation'];
    whatDoesYourWorkSituationLookLikeRightNow =
        json['WhatDoesYourWorkSituationLookLikeRightNow'];
    whichOfTheFollowinDoYouUse = json['WhichOfTheFollowinDoYouUse'];
    howDoYouKeepTrackOfYourBudgetOrFinancialPlan =
        json['HowDoYouKeepTrackOfYourBudgetOrFinancialPlan'];
    inTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities =
        json['InTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities'];
    whatIamlookingfor = json['WhatIamlookingfor'];
    languages = json['Languages'];
    qualifications = json['Qualifications'];
    experiences = json['Experiences'];
    goAround = json['GoAround'];
    headline = json['Headline'];
    briefBio = json['BriefBio'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['WhatsMostImportantInTheOportunitiesYouAreLookingFor'] =
        this.whatsMostImportantInTheOportunitiesYouAreLookingFor;
    data['WhatDoYouWantToDoWithExtraCashYouEarnThroughShohokari'] =
        this.whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari;
    data['WhereDoYouWantToWork'] = this.whereDoYouWantToWork;
    data['WhatsYourHighestLevelOfEducation'] =
        this.whatsYourHighestLevelOfEducation;
    data['WhatDoesYourWorkSituationLookLikeRightNow'] =
        this.whatDoesYourWorkSituationLookLikeRightNow;
    data['WhichOfTheFollowinDoYouUse'] = this.whichOfTheFollowinDoYouUse;
    data['HowDoYouKeepTrackOfYourBudgetOrFinancialPlan'] =
        this.howDoYouKeepTrackOfYourBudgetOrFinancialPlan;
    data['InTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities'] =
        this.inTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities;
    data['WhatIamlookingfor'] = this.whatIamlookingfor;
    data['Languages'] = this.languages;
    data['Qualifications'] = this.qualifications;
    data['Experiences'] = this.experiences;
    data['GoAround'] = this.goAround;
    data['Headline'] = this.headline;
    data['BriefBio'] = this.briefBio;
    data['Id'] = this.id;
    return data;
  }
}
