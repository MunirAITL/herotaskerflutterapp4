import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

import '../../../main.dart';

enum eCap {
  All,
  Word,
  Sentence,
  None,
}

InputBoxHT({
  ctrl,
  context,
  lableTxt,
  kbType,
  inputAction,
  focusNode,
  focusNodeNext,
  len,
  minLines,
  maxLines,
  isPwd = false,
  autofocus = false,
  ecap = eCap.None,
  prefixIco,
  txtAlign = TextAlign.start,
  Function(String) onChange,
  double fontSize,
}) {
  return TextField(
    controller: ctrl,
    focusNode: focusNode,
    autofocus: autofocus,
    keyboardType: kbType,
    minLines: minLines,
    maxLines: maxLines,
    onChanged:(v) { onChange(v);},
    textInputAction: inputAction,
    textCapitalization: kbType != TextInputType.emailAddress
        ? TextCapitalization.sentences
        : TextCapitalization.none,
    onEditingComplete: () {
      // Move the focus to the next node explicitly.
      if (focusNode != null) {
        focusNode.unfocus();
      } else {
        FocusScope.of(context).requestFocus(new FocusNode());
      }
      if (focusNodeNext != null) {
        FocusScope.of(context).requestFocus(focusNodeNext);
      } else {
        FocusScope.of(context).requestFocus(new FocusNode());
      }
    },
    inputFormatters: (kbType == TextInputType.phone)
        ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
        : (kbType == TextInputType.emailAddress)
            ? [
                FilteringTextInputFormatter.allow(RegExp(
                    "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
              ]
            : (ecap != eCap.None)
                ? [
                    (ecap == eCap.All)
                        ? AllUpperCaseTextFormatter()
                        : (ecap == eCap.Sentence)
                            ? FirstUpperCaseTextFormatter()
                            : WordsUpperCaseTextFormatter()
                  ]
                : null,
    obscureText: isPwd,
    maxLength: len,
    autocorrect: false,
    enableSuggestions: false,
    textAlign: txtAlign,
    style: TextStyle(
      color: Colors.black,
      fontSize: fontSize ?? MyTheme.fontSize,
      height: MyTheme.txtLineSpace,
    ),
    decoration: new InputDecoration(
      //isDense: true,
      counter: Offstage(),
      //contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
      prefixIcon: (prefixIco != null)
          ? Padding(
              padding: const EdgeInsets.only(left: 20, top: 10, bottom: 10),
              child: prefixIco,
            )
          : null,
      counterText: "",
      filled: true,
      fillColor: Colors.white,
      hintText: lableTxt,
      hintStyle: new TextStyle(
        color: MyTheme.lgreyColor,
        fontSize: fontSize ?? MyTheme.fontSize,
        height: MyTheme.txtLineSpace,
      ),
      labelStyle: new TextStyle(
        color: Colors.black,
        fontSize: fontSize ?? MyTheme.fontSize,
        height: MyTheme.txtLineSpace,
      ),
      contentPadding: EdgeInsets.only(
          left: (prefixIco != null) ? 0 : 10,
          right: (prefixIco != null) ? 50 : 10,
          top: 10,
          bottom: 10),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: MyTheme.lgreyColor),
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.black, width: 1),
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: MyTheme.lgreyColor, width: 1),
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
    ),
  );
}

class AllUpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.allInCaps);
  }
}

class FirstUpperCaseTextFormatter extends TextInputFormatter {
  /*@override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var newText = toBeginningOfSentenceCase(newValue.text.inCaps);
    return newValue.copyWith(text: newText);
  }*/
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.capitalizeFirstofEachDot);
  }
}

class WordsUpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.capitalizeFirstofEach2);
  }
}

extension CapExtension on String {
  String get capitalizeFirstofEach2 =>
      this.split(" ").map((str) => str.inCaps).join(" ");
}

extension SentenceExtension on String {
  String get capitalizeFirstofEachDot =>
      this.split(". ").map((str) => str.inCaps).join(". ");
}
