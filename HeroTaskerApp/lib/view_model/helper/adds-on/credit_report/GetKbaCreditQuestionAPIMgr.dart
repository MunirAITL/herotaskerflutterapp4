import 'dart:developer';

import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/data/model/ads-on/credit_report/getKbaQuestionAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class GetKbaCreditQuestionApiMgr with Mixin {
  static final GetKbaCreditQuestionApiMgr _shared =
      GetKbaCreditQuestionApiMgr._internal();

  factory GetKbaCreditQuestionApiMgr() {
    return _shared;
  }

  GetKbaCreditQuestionApiMgr._internal();

  getKbaQuestionList({
    BuildContext context,
    String validationIdentifier,
    Function(GetKbaQuestionAPIModel) callback,
  }) async {
    var params = {"ValidationIdentifier": "$validationIdentifier"};
    print("getQuestion bank is = $validationIdentifier");

    try {
      await NetworkMgr()
          .req<GetKbaQuestionAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: params,
        url: APICreditReportCfg.GET_KBA_QUESTIONS_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log("GET_KBA_QUESTIONS_URL  ERROR = " + e.toString());
    }
  }
}
