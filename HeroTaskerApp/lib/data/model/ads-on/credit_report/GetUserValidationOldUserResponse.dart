class GetUserValidationOldUser {
  ValidateNewUser validateNewUser;

  GetUserValidationOldUser({this.validateNewUser});

  GetUserValidationOldUser.fromJson(Map<String, dynamic> json) {
    validateNewUser = json['ValidateNewUser'] != null
        ? new ValidateNewUser.fromJson(json['ValidateNewUser'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.validateNewUser != null) {
      data['ValidateNewUser'] = this.validateNewUser.toJson();
    }
    return data;
  }
}

class ValidateNewUser {
  ValidateNewUserResult validateNewUserResult;

  ValidateNewUser({this.validateNewUserResult});

  ValidateNewUser.fromJson(Map<String, dynamic> json) {
    validateNewUserResult = json['ValidateNewUserResult'] != null
        ? new ValidateNewUserResult.fromJson(json['ValidateNewUserResult'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.validateNewUserResult != null) {
      data['ValidateNewUserResult'] = this.validateNewUserResult.toJson();
    }
    return data;
  }
}

class ValidateNewUserResult {
  Null message;
  Null status;
  Null failureReason;
  int identityValidationOutcome;
  String validationIdentifier;

  ValidateNewUserResult(
      {this.message,
      this.status,
      this.failureReason,
      this.identityValidationOutcome,
      this.validationIdentifier});

  ValidateNewUserResult.fromJson(Map<String, dynamic> json) {
    message = json['Message'];
    status = json['Status'];
    failureReason = json['FailureReason'];
    identityValidationOutcome = json['IdentityValidationOutcome'];
    validationIdentifier = json['ValidationIdentifier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this.message;
    data['Status'] = this.status;
    data['FailureReason'] = this.failureReason;
    data['IdentityValidationOutcome'] = this.identityValidationOutcome;
    data['ValidationIdentifier'] = this.validationIdentifier;
    return data;
  }
}
