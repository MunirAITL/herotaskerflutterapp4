class TaskPaymentsModel {
  int userId;
  dynamic status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int taskId;
  String paymentType;
  String paymentMethod;
  int employeeID;
  int employeePaymentGateWayId;
  double payableAmount;
  double dueAmount;
  double paymentAmount;
  int taskBiddingId;
  String description;
  String acAccountNo;
  String accountStatus;
  String transactionType;
  String taskTitle;
  String taskTitleUrl;
  String profileImageUrl;
  String profileOwnerName;
  String ownerProfileUrl;
  double shohokariAmount;
  double serviceFeeAmount;
  double discountAmount;
  double netTotalAmount;
  int userPromotionId;
  String collectionPaymentType;
  String paymentIntentId;
  String clientSecret;
  int id;

  TaskPaymentsModel(
      {this.userId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.taskId,
      this.paymentType,
      this.paymentMethod,
      this.employeeID,
      this.employeePaymentGateWayId,
      this.payableAmount,
      this.dueAmount,
      this.paymentAmount,
      this.taskBiddingId,
      this.description,
      this.acAccountNo,
      this.accountStatus,
      this.transactionType,
      this.taskTitle,
      this.taskTitleUrl,
      this.profileImageUrl,
      this.profileOwnerName,
      this.ownerProfileUrl,
      this.shohokariAmount,
      this.serviceFeeAmount,
      this.discountAmount,
      this.netTotalAmount,
      this.userPromotionId,
      this.collectionPaymentType,
      this.paymentIntentId,
      this.clientSecret,
      this.id});

  TaskPaymentsModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    status = json['Status'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    paymentType = json['PaymentType'] ?? '';
    paymentMethod = json['PaymentMethod'] ?? '';
    employeeID = json['EmployeeID'] ?? 0;
    employeePaymentGateWayId = json['EmployeePaymentGateWayId'] ?? 0;
    payableAmount = json['PayableAmount'] ?? 0.0;
    dueAmount = json['DueAmount'] ?? 0.0;
    paymentAmount = json['PaymentAmount'] ?? 0.0;
    taskBiddingId = json['TaskBiddingId'] ?? 0;
    description = json['Description'] ?? '';
    acAccountNo = json['AcAccountNo'] ?? '';
    accountStatus = json['AccountStatus'] ?? '';
    transactionType = json['TransactionType'] ?? '';
    taskTitle = json['TaskTitle'] ?? '';
    taskTitleUrl = json['TaskTitleUrl'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    profileOwnerName = json['ProfileOwnerName'] ?? '';
    ownerProfileUrl = json['OwnerProfileUrl'] ?? '';
    shohokariAmount = json['ShohokariAmount'] ?? 0.0;
    serviceFeeAmount = json['ServiceFeeAmount'] ?? 0.0;
    discountAmount = json['DiscountAmount'] ?? 0.0;
    netTotalAmount = json['NetTotalAmount'] ?? 0.0;
    userPromotionId = json['UserPromotionId'] ?? 0;
    collectionPaymentType = json['CollectionPaymentType'] ?? '';
    paymentIntentId = json['PaymentIntentId'] ?? '';
    clientSecret = json['ClientSecret'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['TaskId'] = this.taskId;
    data['PaymentType'] = this.paymentType;
    data['PaymentMethod'] = this.paymentMethod;
    data['EmployeeID'] = this.employeeID;
    data['EmployeePaymentGateWayId'] = this.employeePaymentGateWayId;
    data['PayableAmount'] = this.payableAmount;
    data['DueAmount'] = this.dueAmount;
    data['PaymentAmount'] = this.paymentAmount;
    data['TaskBiddingId'] = this.taskBiddingId;
    data['Description'] = this.description;
    data['AcAccountNo'] = this.acAccountNo;
    data['AccountStatus'] = this.accountStatus;
    data['TransactionType'] = this.transactionType;
    data['TaskTitle'] = this.taskTitle;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['ShohokariAmount'] = this.shohokariAmount;
    data['ServiceFeeAmount'] = this.serviceFeeAmount;
    data['DiscountAmount'] = this.discountAmount;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['UserPromotionId'] = this.userPromotionId;
    data['CollectionPaymentType'] = this.collectionPaymentType;
    data['PaymentIntentId'] = this.paymentIntentId;
    data['ClientSecret'] = this.clientSecret;
    data['Id'] = this.id;
    return data;
  }
}
