import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'reviews_base.dart';
import 'package:bloc_rest_api/bloc_rest_api.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ReviewsPage extends StatefulWidget {
  const ReviewsPage({Key key}) : super(key: key);
  @override
  State createState() => _ReviewsPageState();
}

class _ReviewsPageState extends BaseReviewsStatefull<ReviewsPage>
    with APIStateListener {
  List<UserRatingsModel> listUserRating = [];

  bool isLoading = false;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.user_rating &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRating =
                (model as UserRatingAPIModel).responseData.userRatings;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  Future<void> refreshData() async {
    onLazyLoadAPI();
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });

      try {
        await APIViewModel().req<UserRatingAPIModel>(
          context: context,
          apiState: APIState(APIType.user_rating, this.runtimeType, null),
          url: APIProfileCFg.USER_RATING_GET_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get,
        );
      } catch (e) {}
    }

    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    listUserRating = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    //refreshData();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      context
          .read<RequestCubit<UserRatingAPIModel>>()
          .request(NetworkMgr.shared.req<UserRatingAPIModel, Null>(
            context: context,
            url: APIProfileCFg.USER_RATING_GET_URL,
            param: {"UserId": userData.userModel.id},
            reqType: ReqType.Get,
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Reviews'),
          centerTitle: false,
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return //(listUserRating.length == 0)
        //? drawNF()
        BlocConsumer<RequestCubit<UserRatingAPIModel>,
            RequestState<UserRatingAPIModel>>(
      listener: (context, state) {
        if (state.status == RequestStatus.failure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(state.errorMessage)),
          );
        }
      },
      builder: (context, state) {
        switch (state.status) {
          case RequestStatus.empty:
          case RequestStatus.failure:
            return drawNF();
          case RequestStatus.loading:
            return Center(child: CircularProgressIndicator());
          case RequestStatus.success:
            return Container(
              child: state.model.responseData.userRatings.length > 0
                  ? drawUserRatingList(state.model.responseData.userRatings)
                  : drawNF(),
            );

          default:
            return Container();
        }
      },
    );
  }
}
