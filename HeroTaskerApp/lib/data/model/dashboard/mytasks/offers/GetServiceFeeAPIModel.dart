class GetServiceFeeAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetServiceFeeAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetServiceFeeAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  TaskBidding taskBidding;

  ResponseData({this.taskBidding});

  ResponseData.fromJson(Map<String, dynamic> json) {
    taskBidding = json['TaskBidding'] != null
        ? new TaskBidding.fromJson(json['TaskBidding'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskBidding != null) {
      data['TaskBidding'] = this.taskBidding.toJson();
    }
    return data;
  }
}

class TaskBidding {
  double serviceFeeAmount;
  double shohokariAmount;

  TaskBidding({this.serviceFeeAmount, this.shohokariAmount});

  TaskBidding.fromJson(Map<String, dynamic> json) {
    serviceFeeAmount = json['ServiceFeeAmount'] ?? 0;
    shohokariAmount = json['ShohokariAmount'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ServiceFeeAmount'] = this.serviceFeeAmount;
    data['ShohokariAmount'] = this.shohokariAmount;
    return data;
  }
}
