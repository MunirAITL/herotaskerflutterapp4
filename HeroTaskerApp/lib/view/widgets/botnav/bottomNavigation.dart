import 'dart:developer';

import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/main.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';
import '../../../data/app_data/AppData.dart';
import 'tabItem.dart';
import 'package:badges/badges.dart';

class BottomNavigation extends StatefulWidget {
  final BuildContext context;
  final ValueChanged<int> onSelectTab;
  final botNavController;
  final List<TabItem> tabs;
  final bool isHelpTut;
  BottomNavigation({
    @required this.context,
    @required this.onSelectTab,
    @required this.botNavController,
    @required this.tabs,
    @required this.isHelpTut,
  });

  @override
  State createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation>
    with Mixin, StateListener {
  int curTab = 0;

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) async {
    try {
      if (state == ObserverState.STATE_BOTNAV) {
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    curTab = widget.botNavController.index.value;

    return BottomAppBar(
      //color: Colors.white,
      shape: CircularNotchedRectangle(),
      //notchMargin: 4,
      clipBehavior: Clip.antiAlias,
      child: BottomNavigationBar(
        //selectedLabelStyle: TextStyle(fontSize: 14),
        showUnselectedLabels: true,
        showSelectedLabels: true,
        selectedItemColor: MyTheme.greenNewColor,
        currentIndex: curTab,
        //unselectedLabelStyle: TextStyle(fontSize: 14),
        elevation: 2,
        unselectedItemColor: Colors.black,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.shifting,
        items: widget.tabs
            .map(
              (e) => _buildItem(
                index: e.getIndex(),
                icon: e.icon,
                tabName: e.tabName,
              ),
            )
            .toList(),
        onTap: (index) => widget.onSelectTab(
          index,
        ),
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, AssetImage icon, String tabName}) {
    final totalMsg = appData.taskNotificationCountAndChatUnreadCountData != null
        ? appData
            .taskNotificationCountAndChatUnreadCountData.numberOfUnReadMessage
        : 0;
    final totalNoti =
        appData.taskNotificationCountAndChatUnreadCountData != null
            ? appData.taskNotificationCountAndChatUnreadCountData
                .numberOfUnReadNotification
            : 0;

    int totalBadge = 0;
    //if (index == 1 && totalTask > 0) totalBadge = totalTask;
    if (index == 3 && totalMsg > 0) totalBadge = totalMsg;
    if (index == 4 && totalNoti > 0) totalBadge = totalNoti;

    //widget.totalNoti = 0;
    if (widget.botNavController.isShowHelpDialogExtraHand.value) curTab = 2;
    return BottomNavigationBarItem(
        backgroundColor: MyTheme.appbarColor,
        icon: (widget.isHelpTut && curTab == index)
            ? Stack(clipBehavior: Clip.none, children: <Widget>[
                (index == 3 || index == 4)
                    ? Badge(
                        badgeColor: MyTheme.redColor,
                        showBadge: (totalBadge > 0) ? true : false,
                        //position: BadgePosition.topStart(start: -12),
                        badgeContent: Text(
                          totalBadge.toString(),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: isIPAD || isTablet ? 15 : 10),
                        ),
                        child: (index == 4 && totalBadge > 0)
                            ? null
                            : ImageIcon(
                                icon,
                                color: _tabColor(index: index),
                              ),
                      )
                    : ImageIcon(
                        icon,
                        color: _tabColor(index: index),
                      ),
                new Positioned(
                  top: (widget.botNavController.isShowHelpDialogExtraHand.value)
                      ? -getHP(widget.context, 29)
                      : -getHP(widget.context, 9),
                  right: -10,
                  child: Image.asset(
                    "assets/images/icons/hand_" +
                        ((widget.botNavController.isShowHelpDialogExtraHand
                                .value)
                            ? 'up'
                            : 'down') +
                        ".png",
                    width: 60,
                    height: 70,
                  ),
                )
              ])
            : Badge(
                badgeColor: MyTheme.redColor,
                showBadge: (totalBadge > 0) ? true : false,
                //position: BadgePosition.topStart(start: -2),
                badgeContent: Text(
                  totalBadge.toString(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: isIPAD || isTablet ? 15 : 10),
                ),
                child: (index == 4 && totalBadge > 0)
                    ? null
                    : ImageIcon(
                        icon,
                        color: _tabColor(index: index),
                      ),
              ),
        // ignore: deprecated_member_use
        label:
            tabName /* Txt(
          txt: tabName,
          txtColor: _tabColor(index: index),
          txtSize: MyTheme.txtSize - .5,
          txtAlign: TextAlign.center,
          isBold: (curTab == index) ? true : false),*/
        );
  }

  Color _tabColor({int index}) {
    return curTab == index ? MyTheme.greenNewColor : Colors.black;
  }
}
