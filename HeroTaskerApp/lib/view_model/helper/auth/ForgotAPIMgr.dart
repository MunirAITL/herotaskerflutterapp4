import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/mixin.dart';

import '../../../data/model/auth/ForgotAPIModel.dart';
import '../../../data/network/NetworkMgr.dart';

class ForgotAPIMgr with Mixin {
  static final ForgotAPIMgr _shared = ForgotAPIMgr._internal();

  factory ForgotAPIMgr() {
    return _shared;
  }

  ForgotAPIMgr._internal();

  wsForgotAPI({
    BuildContext context,
    String email,
    Function(ForgotAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<ForgotAPIModel, Null>(
        context: context,
        url: APIAuthCfg.FORGOT_URL,
        param: {'email': email},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
