import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTask3Controller extends GetxController {
  var totalAmounts = 0.0.obs;
  calculation({
    bool isSwitchFixedPrice,
    double estBudget,
    double priceHr,
    double totalHr,
  }) {
    totalAmounts = 0.0.obs;
    if (isSwitchFixedPrice) {
      if (estBudget>0) {
        try {
          totalAmounts.value = estBudget;
        } catch (e) {
          totalAmounts = 0.0.obs;
        }
      }
    } else {
      try {
        if (priceHr>0 && totalHr>0) {
          totalAmounts.value=
              priceHr * totalHr;
        }
      } catch (e) {
        totalAmounts = 0.0.obs;
      }
    }
  }
}
