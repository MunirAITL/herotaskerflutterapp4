import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ConfirmOferController extends GetxController {
  var listOffers = [
    {
      "icon": Icons.account_box_outlined,
      "title": "Upload a profile picture",
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    },
    {
      "icon": Icons.payment,
      "title": "Please provide your payment information",
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    },
    {
      "icon": Icons.calendar_today_outlined,
      "title": "Provide a date of birth",
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    },
    {
      "icon": Icons.calendar_view_day_outlined,
      "title": "Please upload your E-ID",
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    }
  ].obs;
}
