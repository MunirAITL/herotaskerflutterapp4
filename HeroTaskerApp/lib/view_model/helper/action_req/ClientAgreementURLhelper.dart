import 'package:aitl/config/server/APIActionReqCfg.dart';

class ClientAgreementURLHelper {
  static getUrl({String caseID}) {
    var url = APIActionReqCfg.CLIENT_AGREEMENT_URL;
    url = url.replaceAll("#CaseID#", caseID.toString());

    return url;
  }
}
