import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import '../../../main.dart';

showDeactivateProfileDialog(
    {BuildContext context,
    TextEditingController email,
    Function callback}) async {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      //customHeader: Icon(Icons.info, size: 50),
      body: Center(
        child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
                child: TextField(
                  controller: email,
                  keyboardType: TextInputType.emailAddress,
                  minLines: 3,
                  maxLines: 5,
                  maxLength: 255,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: MyTheme.fontSize,
                  ),
                  decoration: InputDecoration(
                    isDense: true,
                    counterText: "",
                    labelText: 'Reason?',
                    labelStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: MyTheme.fontSize,
                    ),
                    hintMaxLines: 1,
                    enabledBorder: const OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.black, width: .5),
                    ),
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.blue)),
                  ),
                ),
              ),
              Positioned(
                top: isIPAD || isTablet ? -70 : -50,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.warning,
                        color: MyTheme.redColor,
                        size: isIPAD || isTablet ? 60 : 40)),
              )
            ]),
      ),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      buttonsTextStyle: TextStyle(fontSize: MyTheme.fontSize),
      btnOkText: "Deactivate",
      btnOkColor: MyTheme.redColor,
      btnOkOnPress: () {
        //Navigator.pop(context);
        callback(email.text.trim());
      },
      btnCancelColor: Colors.grey,
      btnCancelOnPress: () {
        //Navigator.pop(context);
      })
    ..show();
}
