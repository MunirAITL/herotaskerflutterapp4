import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AlertDialogAccountAndAlerts extends StatelessWidget {
  String titleTxt;
  String bodyTxt1;
  String bodyTxt2;
  String bodyTxt3;

  AlertDialogAccountAndAlerts(
      {this.titleTxt, this.bodyTxt1, this.bodyTxt2, this.bodyTxt3});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 300,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                      txt: "$titleTxt",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(
                    height: 15,
                  ),
                  Txt(
                      txt: "• $bodyTxt1",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(
                    height: 15,
                  ),
                  Txt(
                      txt: "• $bodyTxt2",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(
                    height: 15,
                  ),
                  Txt(
                      txt: "• $bodyTxt3",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Btn(
                    txt: "Ok",
                    txtColor: Colors.white,
                    bgColor: MyTheme.redColor,
                    //width: 100,
                    //height: 50,
                    callback: () {
                      Get.back();
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
