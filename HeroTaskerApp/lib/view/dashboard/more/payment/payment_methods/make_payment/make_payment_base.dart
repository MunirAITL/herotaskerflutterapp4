import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseMakePaymentStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final taskCtrl = Get.put(TaskCtrl());
}
