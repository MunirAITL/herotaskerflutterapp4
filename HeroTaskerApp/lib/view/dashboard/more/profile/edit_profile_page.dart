import 'package:aitl/config/cfg/AddrCfg.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMArrowBtn.dart';
import 'package:aitl/view/widgets/dialog/DeactivateProfileDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/directions.dart';
import 'package:intl/intl.dart';
import '../../../../main.dart';
import 'edit_profile_base.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key key}) : super(key: key);
  @override
  State createState() => _EditProfilePageState();
}

class _EditProfilePageState extends BaseEditProfileStatefull<EditProfilePage>
    with APIStateListener {
  //  deactivate reason
  final deactivateReason = TextEditingController();

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.reg2 && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(user: model.responseData.user);
              await userData.setUserModel();
              showToast(
                  context: context,
                  msg: 'Profile updated successfully.',
                  which: 1);
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            try {
              if (mounted) {
                final err = model.messages.postUser[0].toString();
                showToast(context: context, msg: err);
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      }
      if (apiState.type == APIType.portfolio &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final userPortfulioModel =
                (model as UserPortFolioAPIModel).responseData.userPortfulio;
            listUrl = userPortfulioModel.portfulioItems.split('|');
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.about_skills &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            aboutModel = (model as AboutAPIModel).responseData.about;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final MediaUploadFilesModel mediaModel =
                (model as MediaUploadFilesAPIModel).responseData.images[0];
            if (!listUrl.contains(mediaModel.url)) {
              listUrl.add(mediaModel.url);
            }
            isPortfolioUploaded = true;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.portfolio_upload &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {}
  }

  bool validate() {
    if (!UserProfileVal().isFNameOK(context, fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, mobile)) {
      return false;
    } else if (!UserProfileVal().isDOBOK(context, dob)) {
      return false;
    } else if (address == "" || address == AddrCfg.AddrHint) {
      showToast(context: context, msg: "Please pick your location");
      return false;
    } else {
      return true;
    }
  }

  wsUpdateProfile() {
    try {
      APIViewModel().req<RegProfileAPIModel>(
          context: context,
          apiState: APIState(APIType.reg2, this.runtimeType, null),
          url: APIAuthCfg.REG_PROFILE_PUT_URL,
          reqType: ReqType.Put,
          param: {
            "Address": address.replaceAll(AddrCfg.AddrHint, "").trim(),
            "BriefBio": bio.text.trim(),
            "CommunityId": userData.communityId,
            "DateofBirth": dob,
            "Email": email.text.trim(),
            "FirstName": fname.text.trim(),
            "Cohort": "",
            "Headline": headline.text.trim(),
            "Id": userData.userModel.id,
            "LastName": lname.text.trim(),
            "Latitude": cord.lat,
            "Longitude": cord.lng,
            "MobileNumber": mobile.text.trim(),
            "DateCreatedLocal": userData.userModel.dateCreatedLocal,
          });
    } catch (e) {}
  }

  wsUploadPortfolio() async {
    try {
      if (listUrl.length > 0) {
        final portfulioItemsUrls = listUrl.join('|');
        await APIViewModel().req<UserPortFolioAPIModel>(
          context: context,
          apiState: APIState(APIType.portfolio_upload, this.runtimeType, null),
          url: APIProfileCFg.USER_PORTFOLIO_UPLOAD_URL,
          param: {
            "DocumentUrl": "",
            "Id": 0,
            "PortfulioItemsUrls": portfulioItemsUrls,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Post,
        );
      }
    } catch (e) {}
  }

  wsGetAboutSkills() async {
    try {
      await APIViewModel().req<AboutAPIModel>(
        context: context,
        apiState: APIState(APIType.about_skills, this.runtimeType, null),
        url: APIProfileCFg.ABOUT_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    //
    fname.dispose();
    lname.dispose();
    headline.dispose();
    bio.dispose();
    email.dispose();
    mobile.dispose();
    deactivateReason.dispose();

    dob = null;
    cord = null;
    address = null;
    aboutModel = null;
    listUrl = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    getMobCode();
    onUpdateInfo();
    try {
      await APIViewModel().req<UserPortFolioAPIModel>(
        context: context,
        apiState: APIState(APIType.portfolio, this.runtimeType, null),
        url: APIProfileCFg.USER_PORTFOLIO_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
      wsGetAboutSkills();
    } catch (e) {}
  }

  onUpdateInfo() {
    try {
      final user = userData.userModel;
      fname.text = user.firstName;
      lname.text = user.lastName;
      address = user.address;
      cord = Location(lat: 0, lng: 0);
      headline.text = user.headline;
      bio.text = user.briefBio;
      email.text = user.email;
      mobile.text = Common.getPhoneNumber(user.mobileNumber);
      dob = user.dateofBirth;
      final f1 = new DateFormat('dd-MMM-yyyy');
      final f2 = new DateFormat(DateFun.getDOBFormat());
      final d = f1.parse(dob);
      dob = f2.format(d);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, size: MyTheme.fontSize),
              onPressed: () {
                if (isPortfolioUploaded) {
                  confirmDialog(
                      context: context,
                      title: "Unsaved changes",
                      msg: "Do you want to discard the changes?",
                      callbackYes: () {
                        Get.back(result: true);
                      },
                      callbackNo: () {});
                } else {
                  Get.back();
                }
              }),
          title: UIHelper().drawAppbarTitle(title: 'Edit profile'),
          centerTitle: false,
          actions: [
            TextButton(
                onPressed: () async {
                  if (validate()) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    await wsUpdateProfile();
                    await wsUploadPortfolio();
                    isPortfolioUploaded = false;
                  }
                },
                child: Txt(
                  txt: "Save",
                  txtColor: MyTheme.appbarTxtColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ))
          ],
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      color: Colors.white,
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          drawGenInfo(),
          drawPvtInfo(),
          drawAdditionalInfo(),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
            child: MMArrowBtn(
              txt: "Save",
              icon: null,
              height: getHP(context, MyTheme.btnHpa),
              width: MediaQuery.of(context).size.width,
              bgColor: MyTheme.brandColor,
              callback: () async {
                if (validate()) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  await wsUpdateProfile();
                  await wsUploadPortfolio();
                  isPortfolioUploaded = false;
                }
              },
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 40, left: 20, right: 20, bottom: 20),
            child: MMArrowBtn(
              txt: "Deactivate My Account",
              icon: null,
              height: getHP(context, MyTheme.btnHpa),
              width: MediaQuery.of(context).size.width,
              bgColor: MyTheme.redColor,
              callback: () async {
                showDeactivateProfileDialog(
                  context: context,
                  email: deactivateReason,
                  callback: (String reason) {
                    if (reason != null) {
                      APIViewModel().req<DeactivateProfileAPIModel>(
                          context: context,
                          url: APIProfileCFg.DEACTIVATE_PROFILE_URL,
                          param: {
                            'id': userData.userModel.id,
                            'CancelReason': reason
                          },
                          reqType: ReqType.Post,
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  try {
                                    //  signout
                                    CookieMgr().delCookiee();
                                    DBMgr.shared.delTable("User");
                                    StateProvider().notify(
                                        ObserverState.STATE_LOGOUT, null);
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                } else {
                                  try {
                                    if (mounted) {
                                      final err =
                                          model.messages.delete[0].toString();
                                      showToast(
                                        context: context,
                                        msg: err,
                                      );
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          });
                    }
                  },
                );
              },
            ),
          ),
          drawPortFolio(this.runtimeType),
          drawAboutSkills(),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}
