import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/material.dart';

import 'InputBox.dart';
import '../txt/Txt.dart';

drawInputBox({
  @required BuildContext context,
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required TextInputAction inputAction,
  @required FocusNode focusNode,
  FocusNode focusNodeNext,
  @required int len,
  String ph,
  Color txtColor,
  bool isPwd = false,
  bool autofocus = false,
  FontWeight fontWeight = FontWeight.w400,
}) {
  if (txtColor == null) txtColor = Colors.black;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Txt(
          txt: title,
          txtColor: txtColor,
          txtSize: MyTheme.txtSize - .2,
          txtAlign: TextAlign.start,
          fontWeight: fontWeight,
          isBold: false),
      SizedBox(height: 10),
      InputBox(
        context: context,
        ctrl: input,
        lableTxt: (ph == null) ? title : ph,
        kbType: kbType,
        inputAction: inputAction,
        focusNode: focusNode,
        focusNodeNext: focusNodeNext,
        len: len,
        isPwd: isPwd,
        autofocus: autofocus,
      ),
    ],
  );
}
