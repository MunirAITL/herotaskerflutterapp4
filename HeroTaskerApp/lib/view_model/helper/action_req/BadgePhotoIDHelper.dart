import 'package:aitl/data/app_data/UserData.dart';

class BadgePhotoIDHelper {
  getParam({String refrenceUrl}) {
    return {
      "Title": "NationalIDCard",
      "Type": "NationalIDCard",
      "Description": "",
      "ReferenceId": "",
      "ReferenceType": "NationalIDCard",
      "RefrenceNumber": "",
      "RefrenceUrl": refrenceUrl,
      "Refrence": "",
      "AccountNumber": "",
      "AccountName": "",
      "SwiftCode": "",
      "IsVerified": 0,
      "VerificationCode": "",
      "UserCompanyId": userData.userModel.userCompanyID,
      "UserId": userData.userModel.id,
    };
  }
}
