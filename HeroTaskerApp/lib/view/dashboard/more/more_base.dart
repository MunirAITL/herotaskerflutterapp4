import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/db/db_page.dart';
import 'package:aitl/view/dashboard/more/help/help_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_history_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/payment_methods_page.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/dashboard/more/profile/tasker_score_page.dart';
import 'package:aitl/view/dashboard/more/reviews/reviews_page.dart';
import 'package:aitl/view/dashboard/more/settings/settings_page.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

import '../../../data/app_data/AppData.dart';
import 'discover/DiscoverPage.dart';

abstract class BaseMoreStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final List<Map<String, dynamic>> listMore = [
    {
      "title": "Dashboard",
      "icon": "assets/images/more/dashboard_ico.png",
      "route": () => DashboardMore()
    },
    {
      "title": "Profile",
      "icon": "assets/images/more/profile_ico.png",
      "route": () => ProfilePage()
    },
    /*{
      "title": "Tasker Score",
      "icon": "assets/images/more/tasker_score_ico.png",
      "route": () => TaskerScorePage()
    },*/
    {
      "title": "Payment History",
      "icon": "assets/images/more/payment_his_ico.png",
      "route": () => PaymentHistoryPage()
    },
    {
      "title": "Payment Methods",
      "icon": "assets/images/more/payment_method_ico.png",
      "route": () => PaymentMethodsPage()
    },
    {
      "title": "Reviews",
      "icon": "assets/images/more/reviews_ico.png",
      "route": () => ReviewsPage()
    },
    {
      "title": "Notifications",
      "icon": "assets/images/more/noti_ico.png",
      "route": () => NotiPage()
    },
    {
      "title": "Task Alerts Setting",
      "icon": "assets/images/more/task_alerts_settings_ico.png",
      "route": () => TaskAlertPage()
    },
    {
      "title": "Settings",
      "icon": "assets/images/more/settings_ico.png",
      "route": () => SettingsPage()
    },
    {
      "title": "Discover",
      "icon": "assets/images/more/discover_ico.png",
      "route": () => DiscoverPage()
    },
    {
      "title": "Help",
      "icon": "assets/images/more/help_ico.png",
      "route": () => HelpPage()
    },
    {
      "title": "Logout",
      "icon": "assets/images/more/logout_ico.png",
      "route": null
    },
  ];

  drawNotiBadge() {
    return appData.taskNotificationCountAndChatUnreadCountData != null
        ? appData.taskNotificationCountAndChatUnreadCountData
                    .numberOfUnReadNotification >
                0
            ? Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Container(
                  decoration: BoxDecoration(
                    color: MyTheme.redColor,
                    shape: BoxShape.circle,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(3),
                    child: Txt(
                        txt: appData.taskNotificationCountAndChatUnreadCountData
                            .numberOfUnReadNotification
                            .toString(),
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize - .5,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ),
              )
            : SizedBox()
        : SizedBox();
  }
}
