import 'UserModel.dart';

class LoginAPIModel {
  ErrorMessages errorMessages;
  Messages messages;
  bool success;
  ResponseData responseData;

  LoginAPIModel(
      {this.errorMessages, this.messages, this.success, this.responseData});

  LoginAPIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toMap();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toMap();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toMap();
    }
    return data;
  }
}

class ErrorMessages {
  List<dynamic> login;
  ErrorMessages({this.login});
  factory ErrorMessages.fromJson(Map<String, dynamic> j) {
    return ErrorMessages(
      login: j['login'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'login': login,
      };
}

class Messages {
  List<dynamic> login;
  Messages({this.login});
  factory Messages.fromJson(Map<String, dynamic> j) {
    return Messages(
      login: j['login'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'login': login,
      };
}

class ResponseData {
  UserModel user;
  String accessToken;
  String refreshToken;
  //List<dynamic> user;
  ResponseData({this.user, this.accessToken, this.refreshToken});
  factory ResponseData.fromJson(Map<String, dynamic> j) {
    return ResponseData(
        user: UserModel.fromJson(j['User']) ?? [],
        accessToken: j['AccessToken'],
        refreshToken: j['RefreshToken']);
  }

  Map<String, dynamic> toMap() => {
        'User': user.toJson(),
        'AccessToken': accessToken,
        'RefreshToken': refreshToken,
      };
}
