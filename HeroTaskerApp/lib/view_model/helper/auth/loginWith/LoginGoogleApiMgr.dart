import 'package:aitl/config/server/APILoginWithCfg.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class LoginGoogleAPIMgr with Mixin {
  static final LoginGoogleAPIMgr _shared = LoginGoogleAPIMgr._internal();

  factory LoginGoogleAPIMgr() {
    return _shared;
  }

  LoginGoogleAPIMgr._internal();

  wsLoginGoogleAPI({
    BuildContext context,
    var params,
    Function(LoginAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginAPIModel, Null>(
        context: context,
        url: APILoginWithCfg.GOOGLE_LOGIN_URL,
        param: params,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("Error login with google= " + e.toString());
    }
  }
}
