import 'dart:developer';
import 'dart:io';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppShareCfg.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/details/TaskFundedAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/pubnub/PubnubMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:get/get.dart';
import 'taskdetails_base.dart';

class TaskDetailsPage extends StatefulWidget {
  final TaskModel taskModel;
  final String description, title, body;
  const TaskDetailsPage({
    Key key,
    @required this.taskModel,
    this.description,
    this.title,
    this.body,
  }) : super(key: key);
  @override
  State createState() => _TaskDetailsPageState();
}

class _TaskDetailsPageState extends BaseTaskDetailsStatefull<TaskDetailsPage>
    with APIStateListener, StateListener {
  List<TaskBiddingModel> listTaskBiddings;
  List<GetPicModel> listGetPicModel;
  List<TimelinePostModel> listTimelinePostsModel = [];
  List<UserRatingsModel> listUserRatings = [];

  final TextEditingController textController = TextEditingController();
  final ScrollController scrollController = new ScrollController();
  String comments = '';
  bool isScrolling = true;

  PubnubMgr pubnubMgr;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TASKDETAILS_GET_TIMELINE) {
        wsGetTimelineByApp();
      } else if (state == ObserverState.STATE_RELOAD_TASKDETAILS) {
        onReloadAPI();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_timeline_by_app &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listTimelinePostsModel =
                (model as GetTimeLineByAppAPIModel).responseData.timelinePosts;
            if (!isScrolling) {
              Future.delayed(const Duration(seconds: 1), () {
                scrollController
                    .jumpTo(scrollController.position.maxScrollExtent);
              });
            }
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.task_biddings &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listTaskBiddings =
                (model as TaskBiddingsAPIModel).responseData.taskBiddings;
            print(listTaskBiddings);

            //for (var taskBidding in listTaskBidding2) {
            //if (isTaskBiddingStatusOK(taskCtrl, taskBidding)) {
            // if (model2.userId == userData.userModel.id || !isPoster) {
            // listTaskBiddings.add(taskBidding);
            //  }
            // }
            // }

            //if(listTaskBiddings == null) listTaskBiddings = [];

            // for (var taskBidding in listTaskBidding2) {
            //   if (isTaskBiddingStatusOK(taskCtrl, taskBidding)) {
            //     //if (model2.userId == userData.userModel.id || !isPoster) {
            //     listTaskBiddings.add(taskBidding);
            //     //}
            //   }
            // }

            try {
              if (taskCtrl.getTaskModel().workerNumber > 1) {
                for (final taskBidding in listTaskBiddings) {
                  if (taskBidding.status ==
                      TaskStatusCfg()
                          .getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED)) {
                    isPaymentDue = true;
                    break;
                  }
                }
              }
            } catch (e) {}
            //myLog(listTaskBidding);
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.user_rating &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRatings =
                (model as UserRatingAPIModel).responseData.userRatings;
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.get_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listGetPicModel =
                (model as GetPicAPIModel).responseData.taskPictures;
            listGetPicModel =
                (listGetPicModel.length == 0) ? null : listGetPicModel;
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostTimeline((model as MediaUploadFilesAPIModel));
          }
        }
      } else if (apiState.type == APIType.post_timeline &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            await wsGetTimelineByApp();
            final tModal = (model as TimelinePostAPIModel).responseData.post;
            final Map<String, dynamic> data = new Map<String, dynamic>();
            data['AdditionalData'] = tModal.additionalAttributeValue;
            data['CanDelete'] = tModal.canDelete;
            data['CommentText'] = tModal.message;
            data['DateCreated'] = tModal.dateCreated;
            data['DateCreatedUtc'] = tModal.dateCreatedUtc;
            data['EntityId'] = tModal.ownerId;
            data['EntityName'] = tModal.ownerName;
            data['Id'] = tModal.id;
            data['IsSpam'] = false;
            data['LikeCount'] = tModal.totalLikes;
            data['LikeStatus'] = tModal.likeStatus;
            data['User'] = (userData.userModel).toJson();
            final commentModel = UserCommentPublicModelList.fromJson(data);
            try {
              final user = new UserModel(
                  id: userData.userModel.id, name: userData.userModel.name);
              final chatModel =
                  new ChatModel(commentModel: commentModel, user: user);
              pubnubMgr.postMessage(
                  chatModel: chatModel, receiverId: commentModel.user.id);
            } catch (e) {}
            setState(() {
              Future.delayed(const Duration(seconds: 1), () {
                scrollController
                    .jumpTo(scrollController.position.maxScrollExtent);
              });
            });
            final timelineId =
                (model as TimelinePostAPIModel).responseData.post.id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TIMELINE_EMAI_NOTI_GET_URL
                  .replaceAll("#timelineId#", timelineId.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      } else if (apiState.type == APIType.del_task &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              taskCtrl.setTaskModel(null);
              Get.back(result: true);
            } catch (e) {}
          }
        }
      } else if (apiState.type == APIType.task_bidding_del &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: true);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  onDelTaskClicked() {
    try {
      APIViewModel().req<DelTaskAPIModel>(
        context: context,
        apiState: APIState(APIType.del_task, this.runtimeType, null),
        url: APIPostTaskCfg.DEL_TASK_URL
            .replaceAll("#taskId#", taskCtrl.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  wsTaskFunded() async {
    try {
      await APIViewModel().req<TaskFundedAPIModel>(
          context: context,
          url: APIMyTasksCfg.GET_TASK_FUNDED_URL
              .replaceAll("#taskId#", taskCtrl.getTaskModel().id.toString()),
          isLoading: true,
          reqType: ReqType.Get,
          callback: (model) {
            if (mounted && model != null) {
              if (model.success) {}
            }
          });
    } catch (e) {}
  }

  wsWithdrawTaskbidding(TaskBiddingModel model) async {
    try {
      await APIViewModel().req<CommonAPIModel>(
        context: context,
        apiState: APIState(APIType.task_bidding_del, this.runtimeType, null),
        url: APIMyTasksCfg.DEL_TASKBIDDING_URL
            .replaceAll("#taskBiddingId#", model.id.toString()),
        isLoading: true,
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  wsPostTimeline(MediaUploadFilesAPIModel model) async {
    try {
      //final jsonRes = json.encode(model.jsonRes);
      var res = '';
      try {
        res = model.jsonRes.toString();
      } catch (e) {}
      await APIViewModel().req<TimelinePostAPIModel>(
        context: context,
        apiState: APIState(APIType.post_timeline, this.runtimeType, null),
        url: APITimelineCfg.TIMELINE_POST_URL,
        isLoading: false,
        reqType: ReqType.Post,
        param: {
          "AdditionalAttributeValue": res ?? '',
          "Checkin": "",
          "FromLat": 0.0,
          "FromLng": 0.0,
          "IsPrivate": false,
          "Message": comments,
          "OwnerId": userData.userModel.id,
          "PostTypeName": "status", //(url == null) ? "status" : "picture",
          "TaskId": taskCtrl.getTaskModel().id,
        },
      );
    } catch (e) {}
  }

  getTaskAPI() async {
    try {
      await APIViewModel().req<GetTaskAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASK_URL
            .replaceAll("#taskId#", taskCtrl.getTaskModel().id.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              final taskModel = model.responseData.task;
              if (taskModel != null) {
                await taskCtrl.setTaskModel(taskModel);
              }
            }
          }
        },
      );
    } catch (e) {}
  }

  wsGetTimelineByApp() async {
    try {
      await APIViewModel().req<GetTimeLineByAppAPIModel>(
        context: context,
        apiState: APIState(APIType.get_timeline_by_app, this.runtimeType, null),
        url: APITimelineCfg.GET_TIMELINE_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": 8,
          "CustomerId": 0,
          "IsPrivate": false,
          "Page": 1,
          "TaskId": taskCtrl.getTaskModel().id,
          "timeLineId": 0,
        },
      );
    } catch (e) {}
  }

  wsTaskBiddingAPI() async {
    try {
      final param = {
        "Count": 50,
        "IsAll": true,
        "Page": 0,
        "TaskId": taskCtrl.getTaskModel().id,
        "userId": userData.userModel.id,
      };
      log(param.toString());
      await APIViewModel().req<TaskBiddingsAPIModel>(
        context: context,
        apiState: APIState(APIType.task_biddings, this.runtimeType, null),
        url: APIMyTasksCfg.GET_TASKBIDDING_URL
            .replaceAll("/#taskBiddingId#", ""),
        reqType: ReqType.Get,
        param: param,
        isLoading: false,
      );
    } catch (e) {}
  }

  wsGetPicAPI() async {
    try {
      await APIViewModel().req<GetPicAPIModel>(
        context: context,
        apiState: APIState(APIType.get_pic, this.runtimeType, null),
        reqType: ReqType.Get,
        isLoading: false,
        url: APIPostTaskCfg.GET_PIC_URL.replaceAll(
          "#taskId#",
          taskCtrl.getTaskModel().id.toString(),
        ),
      );
    } catch (e) {}
  }

  wsGetRatingAPI() async {
    try {
      await APIViewModel().req<UserRatingAPIModel>(
        context: context,
        apiState: APIState(APIType.user_rating, this.runtimeType, null),
        url: APIMyTasksCfg.GET_USERRATING_BYTASKID_URL
            .replaceAll("#taskId#", taskCtrl.getTaskModel().id.toString()),
        reqType: ReqType.Get,
        isLoading: false,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    indexTopBtn.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    comments = null;
    listTaskBiddings = null;
    listGetPicModel = null;
    listUserRatings = null;
    listTimelinePostsModel = null;
    textController.dispose();
    scrollController.dispose();
    try {
      taskCtrl.dispose();
    } catch (e) {}
    try {
      pubnubMgr.destroy();
    } catch (e) {}
    NetworkMgr().dispose();
    super.dispose();
  }

  Future<void> onReloadAPI() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        await getTaskAPI();
        if (mounted) {
          await wsGetTimelineByApp();
        }
        //if(mounted) {
        //await wsTaskFunded();
        //}
        if (mounted) {
          await wsTaskBiddingAPI();
        }
        if (mounted) {
          await wsGetPicAPI();
        }
        if (mounted) {
          await wsGetRatingAPI();
        }
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      }
    } catch (e) {}
  }

  appInit() async {
    indexTopBtn = ValueNotifier<int>(0);

    try {
      taskCtrl.setTaskModel(widget.taskModel);
    } catch (e) {}
    if (taskCtrl.getTaskModel().userId == userData.userModel.id) {
      isPoster = true;
      //this.
      setIsPoster(isPoster);
    } else {
      isPoster = false;
      setIsPoster(isPoster);
    }

    myLog(taskCtrl.getTaskModel().id);

    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    var channelId = taskCtrl.getTaskModel().id;
    pubnubMgr = PubnubMgr();
    await pubnubMgr.initPubNub(
        channelId: channelId,
        callback: (envelope) async {
          myLog(envelope.payload);
          isScrolling = false;
          await wsGetTimelineByApp();
        });
    onReloadAPI();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Task details'),
          centerTitle: false,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () async {
                final msg = AppShareCfg.SERVER_SHARE_PUBLIC_URL +
                    userData.userModel.userProfileUrl;
                final FlutterShareMe flutterShareMe = FlutterShareMe();
                await flutterShareMe.shareToSystem(msg: msg);
              },
            ),
            getPopupMenuButton(),
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(0),
              child: (isLoading)
                  ? AppbarBotProgBar(
                      backgroundColor: MyTheme.appbarProgColor,
                    )
                  : Container(
                      height: .1,
                      color: Colors.transparent,
                    )),
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
        child: NotificationListener(
      onNotification: (scrollNotification) {
        if (scrollNotification is ScrollStartNotification) {
          isScrolling = true;
        } else if (scrollNotification is ScrollEndNotification) {
          isScrolling = false;
        }
        return true;
      },
      child: RefreshIndicator(
        color: Colors.white,
        backgroundColor: MyTheme.brandColor,
        onRefresh: () => onReloadAPI(),
        child: ListView(
          controller: scrollController,
          shrinkWrap: true,
          //primary: true,
          children: [
            SizedBox(height: 20),
            drawTopStatus(),
            drawTitle(),
            drawPostedBy(),
            drawLocation(),
            drawDueDate(),
            drawPriceBox(listTaskBiddings),
            drawProjectSummary(),
            drawDetailsView(listGetPicModel),
            drawPrivateMsg(listTaskBiddings),
            drawReviews(listUserRatings, listTaskBiddings),
            drawOffers(listTaskBiddings, false),
            drawQuestions(
                listTimelinePostsModel: listTimelinePostsModel,
                textController: textController,
                cls: this.runtimeType,
                callbackCam: (File file, String txt) async {
                  comments = txt.trim();
                  textController.clear();
                  if (file != null) {
                    await APIViewModel().upload(
                      context: context,
                      apiState: APIState(
                          APIType.media_upload_file, this.runtimeType, null),
                      file: file,
                    );
                  } else {
                    wsPostTimeline(null);
                  }
                }),
            SizedBox(height: 50),
          ],
        ),
      ),
    ));
  }
}
