import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NofiTaskAlert extends StatefulWidget {
  final NotiModel notiModel;
  final String description;
  const NofiTaskAlert(
      {Key key, @required this.notiModel, @required this.description})
      : super(key: key);

  @override
  _NofiTaskAlertState createState() => _NofiTaskAlertState();
}

class _NofiTaskAlertState extends State<NofiTaskAlert> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Task alerts'),
          centerTitle: false,
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 20),
                Txt(
                    txt: widget.notiModel.eventName ?? '',
                    txtColor: MyTheme.hotdipPink,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 20),
                Txt(
                    txt: widget.notiModel.message ?? '',
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 20),
                Container(
                  width: getWP(context, 50),
                  height: getWP(context, 50),
                  child: CircleAvatar(
                    radius: 50,
                    backgroundColor: Colors.transparent,
                    backgroundImage: new CachedNetworkImageProvider(
                      MyNetworkImage.checkUrl((widget.notiModel != null)
                          ? widget.notiModel.initiatorImageUrl
                          : ServerUrls.MISSING_IMG),
                    ),
                  ),
                ),
                SizedBox(height: 40),
                MMBtn(
                  txt: "Close",
                  height: getHP(context, MyTheme.btnHpa),
                  width: getWP(context, 30),
                  callback: () {
                    Get.back();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
