import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/hreflogin/LoginAccExistsByEmailApiModel.dart';
import 'package:aitl/data/model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/data/model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/auth/loginWith/auth_base.dart';
import 'package:aitl/view/auth/otp/OtpByMobilePage.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/href_login_dialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/LoginApiMgr.dart';
import 'package:aitl/view_model/helper/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../../main.dart';
import 'check_email_page.dart';

class SigninPage extends StatefulWidget {
  final hrefLoginData;
  const SigninPage({Key key, this.hrefLoginData}) : super(key: key);

  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends BaseAuth<SigninPage> with APIStateListener {
  final email = TextEditingController();
  final focusEmail = FocusNode();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          //checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          //checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  callCheckUserEmailAPI() async {
    try {
      await APIViewModel().req<LoginAccExistsByEmailApiModel>(
          context: context,
          url: APIAuthCfg.LOGINACCEXISTSBYEMAIL_POST_URL,
          reqType: ReqType.Post,
          param: {"Email": email.text.trim()},
          callback: (model1) async {
            if (mounted) {
              if (model1 != null) {
                if (model1.success) {
                  await APIViewModel().req<PostEmailOtpAPIModel>(
                    context: context,
                    url: APIAuthCfg.POSTEMAILOTP_POST_URL,
                    reqType: ReqType.Post,
                    param: {
                      "Email": email.text.trim(),
                      "Status": "101",
                    },
                    callback: (model2) async {
                      if (mounted) {
                        if (model2 != null) {
                          if (model2.success) {
                            await APIViewModel().req<SendUserEmailOtpAPIModel>(
                                context: context,
                                url: APIAuthCfg.SENDUSEREMAILOTP_GET_URL
                                    .replaceAll(
                                        "#otpId#",
                                        model2.responseData.userOTP.id
                                            .toString()),
                                reqType: ReqType.Get,
                                callback: (model3) async {
                                  if (mounted) {
                                    if (model3 != null) {
                                      if (model3.success) {
                                        Get.to(() => CheckEmailPage(
                                            userOTP:
                                                model3.responseData.userOTP,
                                            email: email.text.trim()));
                                      } else {
                                        final err = model1.messages.login[0];
                                        showToast(
                                            context: context,
                                            msg: err,
                                            which: 0);
                                      }
                                    }
                                  }
                                });
                          } else {
                            final err = model1.messages.login[0];
                            showToast(context: context, msg: err, which: 0);
                          }
                        }
                      }
                    },
                  );
                } else {
                  Get.dialog(HrefLoginDialog(
                      context: context,
                      callback: () {
                        Get.off(
                            () => RegScreen(emailAddress: email.text.trim()));
                      }));
                }
              }
            }
          });
    } catch (e) {}
  }

  callOtpMobileAPI() async {
    var countryCode = "+44";
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    Sms2APIMgr().wsLoginMobileOtpPostAPI(
      context: context,
      countryCode: countryCode,
      mobile: email.text.trim(),
      callback: (model) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              Sms2APIMgr().wsSendOtpNotiAPI(
                  context: context,
                  otpId: model.responseData.userOTP.id,
                  callback: (model) {
                    if (model != null && mounted) {
                      try {
                        if (model.success) {
                          Get.to(() => OtpByMobilePage(
                              mobile: model.responseData.userOTP.mobileNumber));
                        } else {
                          try {
                            if (mounted) {
                              final err =
                                  model.messages.postUserotp[0].toString();
                              showToast(context: context, msg: err);
                            }
                          } catch (e) {
                            myLog(e.toString());
                          }
                        }
                      } catch (e) {
                        myLog(e.toString());
                      }
                    }
                  });
            } else {
              try {
                if (mounted) {
                  final err = model.messages.postUserotp[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    //testLogin();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          //titleSpacing: 0,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              width: getW(context),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    "assets/images/welcome/login_logo.png",
                    fit: BoxFit.cover,
                    width: getWP(context, 60),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Txt(
                      txt:
                          "Enter the email or mobile associated with your account.",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize + .1,
                      txtAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      isBold: false,
                    ),
                  ),
                  SizedBox(height: 30),
                  Txt(
                    txt: "Email / Mobile:",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    fontWeight: FontWeight.w400,
                    isBold: false,
                  ),
                  TextField(
                      controller: email,
                      focusNode: focusEmail,
                      autofocus: true,
                      maxLength: 50,
                      autocorrect: false,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: MyTheme.fontSize,
                        height: MyTheme.txtLineSpace,
                      ),
                      keyboardType: TextInputType.emailAddress,
                      decoration: new InputDecoration(
                        isDense: true,
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                        counterText: '',
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: .5),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: .5),
                        ),
                        border: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.white, width: .5),
                        ),
                      )),
                  SizedBox(height: 30),
                  Align(
                    alignment: Alignment.center,
                    child: MMBtn(
                        txt: "Next",
                        txtColor: Colors.white,
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          //if (!Server.isOtp) {
                          //testLogin();
                          //} else {

                          if (email.text.trim().isEmpty) {
                            showToast(
                                context: context,
                                msg: "Invalid email or mobile entered");
                          } else if (email.text.contains("@")) {
                            if (RegExp(UserProfileVal.EMAIL_REG)
                                .hasMatch(email.text.trim())) {
                              callCheckUserEmailAPI();
                            } else {
                              showToast(
                                  context: context,
                                  msg: "Invalid email address entered");
                            }
                          } else {
                            final str = email.text.trim();
                            try {
                              if (Common.isNumeric(str) &&
                                  str.length > UserProfileVal.PHONE_LIMIT) {
                                callOtpMobileAPI();
                              } else {
                                showToast(
                                    context: context,
                                    msg: "Invalid mobile number entered");
                              }
                            } catch (e) {
                              showToast(
                                  context: context,
                                  msg: "Invalid mobile number entered");
                            }
                          }
                        }),
                  ),
                  /*SizedBox(height: 20),
                  Align(
                    alignment: Alignment.center,
                    child: Txt(
                      txt: "Or continue with",
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize + .3,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 20, left: 5, right: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        /*AuthHelper().drawGoogleFBLoginButtons(
                          context: context,
                          height: getHP(context, MyTheme.btnHpa),
                          width: getW(context),
                          icon: "ic_fb",
                          txt: "Continue with Facebook",
                          callback: () {
                            loginWithFB(this.runtimeType);
                          },
                        ),
                        SizedBox(height: 10),*/
                        AuthHelper().drawGoogleFBLoginButtons(
                          context: context,
                          height: getHP(context, MyTheme.btnHpa),
                          width: getW(context),
                          icon: "ic_google",
                          txt: "Continue with Google",
                          callback: () {
                            loginWithGoogle(this.runtimeType);
                          },
                        ),
                        SizedBox(height: 10),
                        Platform.isIOS
                            ? AuthHelper().signInWithApple(
                                context: context,
                                height: getHP(context, MyTheme.btnHpa),
                                width: getW(context),
                                callback: () async {
                                  if (await SignInWithApple.isAvailable()) {
                                    signInWithApple(this.runtimeType);
                                  }
                                })
                            : SizedBox(),
                      ],
                    ),
                  ),*/
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  testLogin() {
    LoginAPIMgr().wsLoginAPI(
      context: context,
      email: email.text =
          "munir@aitl.net", //"rc.cust.16@cool.fr.nf", // "anisur5001@yopmail.com", //"rc.cust.16@cool.fr.nf"; //LIVE
      pwd: "123",
      callback: (model) async {
        if (model != null && mounted) {
          if (model.success) {
            await DBMgr.shared.setUserProfile(user: model.responseData.user);
            await userData.setUserModel();
            Get.offAll(() => DashboardPage());
          } else {
            try {
              if (mounted) {
                final err = model.errorMessages.login[0].toString();
                showToast(context: context, msg: err);
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      },
    );
  }
}
