import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/messages/chat_page.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:badges/badges.dart';
import '../../../data/model/dashboard/timeline/GetTaskListGroupChatAPIModel.dart';
import '../../../main.dart';

abstract class BasePrivateMsgStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TimeLineHelper {
  final taskCtrl = Get.put(TaskCtrl());

  bool isLoading = false;

  refreshData();

  drawSearchbar(TextEditingController searchText, Function(String) onChange) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextField(
        controller: searchText,
        autofocus: true,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.search,
        onChanged: (value) => onChange(value),
        autocorrect: false,
        textCapitalization: TextCapitalization.sentences,
        style: TextStyle(
          color: MyTheme.appbarTxtColor,
          fontSize: MyTheme.fontSize,
        ),
        decoration: InputDecoration(
          counter: Offstage(),
          prefixIcon: Icon(
            Icons.search,
            color: MyTheme.appbarTxtColor,
          ),
          suffixIcon: IconButton(
              onPressed: () => onChange(""),
              icon: Icon(
                Icons.close,
                color: MyTheme.appbarTxtColor,
              )),
          hintText: "Search by title",
          hintStyle: new TextStyle(
            color: Colors.black45,
            fontSize: MyTheme.fontSize,
            //height: MyTheme.txtLineSpace,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }

  drawAppbarNavBar(Function callback) {
    final xtraH = isIPAD || isTablet ? 20 : 0;
    return PreferredSize(
      preferredSize: Size.fromHeight(AppConfig.private_msg_height + xtraH),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Get.to(() => ChatPage(isSupport: true));
              },
              child: Card(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Card(
                          elevation: 2,
                          child: Image.asset(
                            'assets/images/logo/logo.png',
                            width: 40,
                            height: 40,
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        flex: 5,
                        child: Txt(
                            txt: "Contact with Herotasker Support Team",
                            txtColor: MyTheme.gray5Color,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : Container(
                    height: .5,
                    color: Colors.grey.shade400.withOpacity(.5),
                  )
          ],
        ),
      ),
    );
  }

  drawItem(CaseGroupMessageData caseGroupMessageData) {
    final priceTxt = (caseGroupMessageData.isFixedPrice)
        ? caseGroupMessageData.fixedBudgetAmount.round().toString()
        : caseGroupMessageData.totalHours.round().toString() + '/hr';
    //
    var status = caseGroupMessageData.status;
    try {
      if (int.parse(status) > 0) {
        status = TaskStatusCfg().getSatus(int.parse(status));
      }
    } catch (e) {
      status = caseGroupMessageData.status;
    }

    return GestureDetector(
      onTap: () async {
        await APIViewModel().req<GetTaskAPIModel>(
          context: context,
          url: APIMyTasksCfg.GET_TASK_URL
              .replaceAll("#taskId#", caseGroupMessageData.id.toString()),
          reqType: ReqType.Get,
          callback: (model) async {
            if (model != null && mounted) {
              if (model.success) {
                final taskModel = model.responseData.task;
                if (taskModel != null) {
                  await taskCtrl.setTaskModel(taskModel);
                  await Get.to(() => ChatPage()).then((value) {
                    if (mounted) {
                      if (mounted) {
                        Future.delayed(Duration(seconds: 1), () {
                          if (mounted) taskCtrl.setTaskModel(null);
                        });
                      }
                    }
                  });
                }
              }
            }
          },
        );
      },
      child: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: getWP(context, 12),
                    height: getWP(context, 12),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: MyNetworkImage.loadProfileImage(
                            caseGroupMessageData.ownerImageUrl),
                        fit: BoxFit.cover,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: caseGroupMessageData.name,
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .1,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w600,
                            isBold: false),
                        SizedBox(height: 5),
                        Txt(
                            txt: caseGroupMessageData.title,
                            /*+
                                "has been " +
                                status.toLowerCase(),*/
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: false),
                        caseGroupMessageData.preferedLocation != ''
                            ? Padding(
                                padding: const EdgeInsets.only(top: 5),
                                child: Row(
                                  children: [
                                    Icon(Icons.location_on_outlined,
                                        color: Colors.grey, size: 15),
                                    SizedBox(width: 2),
                                    Flexible(
                                      child: Txt(
                                          txt: caseGroupMessageData
                                              .preferedLocation,
                                          txtColor: MyTheme.gray5Color,
                                          txtSize: MyTheme.txtSize - .5,
                                          txtAlign: TextAlign.start,
                                          fontWeight: FontWeight.w400,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                              )
                            : SizedBox(),
                        Padding(
                          padding: const EdgeInsets.only(top: 2),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Icon(Icons.lock_clock,
                                  color: Colors.grey, size: 15),
                              SizedBox(width: 2),
                              Flexible(
                                child: Txt(
                                    txt: DateFun.getTimeAgoTxt(
                                        caseGroupMessageData
                                            .lastMessageDateTime),
                                    txtColor: MyTheme.gray5Color,
                                    txtSize: MyTheme.txtSize - .5,
                                    txtAlign: TextAlign.end,
                                    fontWeight: FontWeight.w400,
                                    isBold: false),
                              ),
                              SizedBox(width: 10),
                              Image.asset(
                                "assets/images/icons/" +
                                    (caseGroupMessageData.isRead
                                        ? "check1_icon.png"
                                        : "check2_icon.png"),
                                color: caseGroupMessageData.isRead
                                    ? Colors.grey
                                    : Colors.green,
                                width: 20,
                                height: 20,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(width: 15),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      /*Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: status,
                              style: TextStyle(
                                  color: MyTheme.gray5Color,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14),
                            ),
                            /*TextSpan(
                              text: offerInfo,
                              style: TextStyle(
                                  color: MyTheme.gray5Color,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 12),
                            ),*/
                          ],
                        ),
                      ),
                      SizedBox(height: 10),*/
                      /*Container(
                        //color: Colors.black,
                        child: drawPrice(
                          price: priceTxt,
                          txtSize: 22,
                          txtColor: Colors.black,
                        ),
                      ),*/

                      /*Badge(
                          badgeColor: Colors.red,
                          showBadge: true, //(totalBadge > 0) ? true : false,
                          //position: BadgePosition.topStart(start: -2),
                          badgeContent: Text(
                            "2",
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          )),*/
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  SizedBox(width: getWP(context, 12)),
                  Expanded(
                    child: Container(
                      color: MyTheme.gray4Color,
                      height: .3,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  drawNF() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          //shrinkWrap: true,
          children: [
            Container(
              width: getWP(context, 70),
              height: getWP(context, 50),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_msg.png"),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt:
                      "You haven't got any messages yet - assign a task or get assigned to chat privately!",
                  txtColor: MyTheme.mycasesNFBtnColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
            SizedBox(height: 10),
            Btn(
                txt: "Refresh",
                txtColor: Colors.white,
                bgColor: MyTheme.brandColor,
                txtSize: 1.8,
                radius: 0,
                callback: () {
                  refreshData();
                }),
          ],
        ),
      ),
    );
  }
}
