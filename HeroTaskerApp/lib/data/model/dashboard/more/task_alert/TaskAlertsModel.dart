class TaskAlertKeywordsModel {
  String creationDate;
  int distance;
  int id;
  bool isOnline;
  String keyword;
  String location;
  double latitude;
  double longitude;
  int status;
  int userId;

  TaskAlertKeywordsModel(
      {this.creationDate,
      this.distance,
      this.id,
      this.isOnline,
      this.keyword,
      this.latitude,
      this.location,
      this.longitude,
      this.status,
      this.userId});

  TaskAlertKeywordsModel.fromJson(Map<String, dynamic> json) {
    creationDate = json['CreationDate'] ?? '';
    distance = json['Distance'] ?? 0;
    id = json['Id'] ?? 0;
    isOnline = json['IsOnline'] ?? false;
    keyword = json['Keyword'] ?? '';
    location = json['Location'] ?? '';
    latitude = json['Latitude'] ?? 0.0;
    longitude = json['Longitude'] ?? 0.0;
    status = json['Status'] ?? 0;
    userId = json['UserId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CreationDate'] = this.creationDate;
    data['Distance'] = this.distance;
    data['Id'] = this.id;
    data['IsOnline'] = this.isOnline;
    data['Keyword'] = this.keyword;
    data['Location'] = this.location;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['Status'] = this.status;
    data['UserId'] = this.userId;
    return data;
  }
}
