import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/main.dart';
import 'package:aitl/view/widgets/picker/country_code_picker/CountryCodePicker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/Mixin.dart';

InputBoxWithCountryCode({
  context,
  ctrl,
  lableTxt,
  kbType,
  inputAction,
  focusNode,
  focusNodeNext,
  len,
  isPwd,
  countryCode,
  countryName,
  getCountryCode,
  autofocus = false,
  isWhiteBG = false,
  radius = 5,
}) {
  double width = MediaQuery.of(context).size.width;
  double height = MediaQuery.of(context).size.height;
  return Container(
    decoration: BoxDecoration(
      color: (isWhiteBG) ? Colors.white : null,
      border: Border.all(color: Colors.grey, width: 1),
      borderRadius: BorderRadius.all(Radius.circular(radius)),
    ),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Color(0xFFDBD5D5),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(radius),
              bottomLeft: Radius.circular(radius),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(0),
            child: CountryCodePicker(
              dialogSize: Size(width * 70 / 100, height * 70 / 100),
              padding: EdgeInsets.all(isIPAD || isTablet ? 10 : 2),
              showFlagDialog: true,
              showDropDownButton: true,
              textStyle: TextStyle(color: Colors.black, fontSize: 17),
              backgroundColor: Colors.transparent,
              dialogBackgroundColor: Colors.white,
              dialogTextStyle: TextStyle(color: Colors.black),
              searchDecoration: InputDecoration(
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                hintText: 'Search',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: MyTheme.fontSize,
                ),
                labelStyle: TextStyle(
                  color: Colors.black,
                  fontSize: MyTheme.fontSize,
                ),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                  size: 20,
                ),
                //contentPadding: EdgeInsets.only(left: 20, right: 20),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(0),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black54),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(0),
                  ),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(0),
                  ),
                ),
              ),
              searchStyle: TextStyle(color: Colors.black),
              initialSelection: countryName,
              showOnlyCountryWhenClosed: false,
              hideMainText: false,
              showFlagMain: true,
              showFlag: true,
              hideSearch: false,
              showCountryOnly: false,
              alignLeft: false,
              closeIcon: Icon(
                Icons.close,
                color: Colors.grey,
              ),
              onChanged: getCountryCode,
              favorite: ["GB", "IE"],
              //flagWidth: 20,
            ),
          ),
        ),
        //SizedBox(width: 10),
        Expanded(
          flex: 5,
          child: TextField(
            controller: ctrl,
            focusNode: focusNode,
            style: TextStyle(color: Colors.black, fontSize: MyTheme.fontSize),
            maxLength: len,
            obscureText: false,
            keyboardType: TextInputType.phone,
            textInputAction: inputAction,
            onEditingComplete: () {
              // Move the focus to the next node explicitly.
              if (focusNode != null) {
                focusNode.unfocus();
              } else {
                FocusScope.of(context).requestFocus(new FocusNode());
              }
              if (focusNodeNext != null) {
                FocusScope.of(context).requestFocus(focusNodeNext);
              } else {
                FocusScope.of(context).requestFocus(new FocusNode());
              }
            },
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            decoration: InputDecoration(
              //isDense: true,
              contentPadding: EdgeInsets.symmetric(
                  horizontal: 20, vertical: isIPAD || isTablet ? 20 : 0),
              counterText: "",
              fillColor: Colors.grey,
              focusColor: Colors.black,
              hoverColor: Colors.black,
              labelStyle:
                  TextStyle(color: Colors.grey, fontSize: MyTheme.fontSize),
              hintText: lableTxt,
              hintStyle:
                  TextStyle(color: Colors.grey, fontSize: MyTheme.fontSize),
              // contentPadding: EdgeInsets.all(15.0),
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
          ),
        ),
      ],
    ),
  );
}
