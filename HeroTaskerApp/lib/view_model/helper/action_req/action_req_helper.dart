import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/action_req/TaskerProfileVerificationData.dart';
import 'package:aitl/view/widgets/btn/BtnOutlineBadge.dart';
import 'package:flutter/material.dart';

enum eVerifyType {
  MOBILE,
  EMAIL,
  EID,
  PROFILE_PIC,
  TASK_ALERTS,
  PORTFOLIO,
  SKILLS,
  PAYMENT_METHOD,
}

class ActionReqHelper {
  drawActionRequiredItems(
      {TaskerProfileVerificationData taskVerifyModel,
      Function(eVerifyType) callbackType}) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: Container(
            width: double.infinity,
            child: Column(
              children: [
                taskVerifyModel.mobileVerificationId == 0
                    ? Card(
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: Image.asset(
                                "assets/images/ar/ar_mobile.png",
                                width: 25,
                                height: 25,
                              )),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Mobile verification",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                              ),
                              Flexible(
                                  flex: 2,
                                  child: BtnOutlineBadge(
                                      txt: "Verify",
                                      txtColor: Color(0xFF263F5D),
                                      borderColor: Color(0xFF263F5D),
                                      callback: () {
                                        callbackType(eVerifyType.MOBILE);
                                      }))
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                taskVerifyModel.emailVerificationId == 0
                    ? Card(
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: Image.asset(
                                "assets/images/ar/ar_email.png",
                                width: 25,
                                height: 25,
                              )),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Email verification",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                              ),
                              Flexible(
                                  flex: 2,
                                  child: BtnOutlineBadge(
                                      txt: "Verify",
                                      txtColor: Color(0xFF263F5D),
                                      borderColor: Color(0xFF263F5D),
                                      callback: () {
                                        callbackType(eVerifyType.EMAIL);
                                      }))
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                taskVerifyModel.eIDVerificationId == 0
                    ? Card(
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: Image.asset(
                                "assets/images/ar/ar_eid.png",
                                width: 25,
                                height: 25,
                              )),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "E-ID verification",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                              ),
                              Flexible(
                                  flex: 2,
                                  child: BtnOutlineBadge(
                                      txt: "Verify",
                                      txtColor: Color(0xFF263F5D),
                                      borderColor: Color(0xFF263F5D),
                                      callback: () {
                                        callbackType(eVerifyType.EID);
                                      }))
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                taskVerifyModel.userProfilePictureId == 0
                    ? Card(
                        elevation: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                  child: Image.asset(
                                "assets/images/ar/ar_pic.png",
                                width: 25,
                                height: 25,
                              )),
                              Expanded(
                                flex: 6,
                                child: Text(
                                  "Profile Picture",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                ),
                              ),
                              Flexible(
                                  flex: 2,
                                  child: BtnOutlineBadge(
                                      txt: "Upload",
                                      txtColor: Color(0xFF263F5D),
                                      borderColor: Color(0xFF263F5D),
                                      callback: () {
                                        callbackType(eVerifyType.PROFILE_PIC);
                                      }))
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                userData.isWorker
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          taskVerifyModel.taskAlertId == 0
                              ? Card(
                                  elevation: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, bottom: 5, left: 10, right: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            child: Image.asset(
                                          "assets/images/ar/ar_alerts.png",
                                          width: 25,
                                          height: 25,
                                        )),
                                        Expanded(
                                          flex: 6,
                                          child: Text(
                                            "Task Alert Setting",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15),
                                          ),
                                        ),
                                        Flexible(
                                            flex: 2,
                                            child: BtnOutlineBadge(
                                                txt: "Add",
                                                txtColor: Color(0xFF263F5D),
                                                borderColor: Color(0xFF263F5D),
                                                callback: () {
                                                  callbackType(
                                                      eVerifyType.TASK_ALERTS);
                                                }))
                                      ],
                                    ),
                                  ),
                                )
                              : SizedBox(),
                          taskVerifyModel.userPortfulioId == 0
                              ? Card(
                                  elevation: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, bottom: 5, left: 10, right: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            child: Image.asset(
                                          "assets/images/ar/ar_portfolio.png",
                                          width: 25,
                                          height: 25,
                                        )),
                                        Expanded(
                                          flex: 6,
                                          child: Text(
                                            "Portfolio",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15),
                                          ),
                                        ),
                                        Flexible(
                                            flex: 2,
                                            child: BtnOutlineBadge(
                                                txt: "Add",
                                                txtColor: Color(0xFF263F5D),
                                                borderColor: Color(0xFF263F5D),
                                                callback: () {
                                                  callbackType(
                                                      eVerifyType.PORTFOLIO);
                                                }))
                                      ],
                                    ),
                                  ),
                                )
                              : SizedBox(),
                          taskVerifyModel.userSkillId == 0
                              ? Card(
                                  elevation: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, bottom: 5, left: 10, right: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            child: Image.asset(
                                          "assets/images/ar/ar_skills.png",
                                          width: 25,
                                          height: 25,
                                        )),
                                        Expanded(
                                          flex: 6,
                                          child: Text(
                                            "Skills",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15),
                                          ),
                                        ),
                                        Flexible(
                                            flex: 2,
                                            child: BtnOutlineBadge(
                                                txt: "Add",
                                                txtColor: Color(0xFF263F5D),
                                                borderColor: Color(0xFF263F5D),
                                                callback: () {
                                                  callbackType(
                                                      eVerifyType.SKILLS);
                                                }))
                                      ],
                                    ),
                                  ),
                                )
                              : SizedBox(),
                          taskVerifyModel.paymentMethodVerificationId == 0
                              ? Card(
                                  elevation: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, bottom: 5, left: 10, right: 10),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                            child: Image.asset(
                                          "assets/images/ar/ar_payment.png",
                                          width: 25,
                                          height: 25,
                                        )),
                                        Expanded(
                                          flex: 6,
                                          child: Text(
                                            "Payment method",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15),
                                          ),
                                        ),
                                        Flexible(
                                            flex: 2,
                                            child: BtnOutlineBadge(
                                                txt: "Add",
                                                txtColor: Color(0xFF263F5D),
                                                borderColor: Color(0xFF263F5D),
                                                callback: () {
                                                  callbackType(eVerifyType
                                                      .PAYMENT_METHOD);
                                                }))
                                      ],
                                    ),
                                  ),
                                )
                              : SizedBox(),
                        ],
                      )
                    : SizedBox(),
              ],
            )),
      ),
    );
  }

  drawListTile(bool isVerified, Widget wid, Function() callback) {
    return Card(
      elevation: 2,
      child: ListTile(
        minLeadingWidth: 0,
        title: wid,
        trailing: isVerified
            ? BtnOutlineBadge(
                txt: "✓ Verified",
                txtColor: Color(0xFF00C938),
                borderColor: Color(0xFF263F5D),
                callback: () {})
            : BtnOutlineBadge(
                txt: "Verify",
                txtColor: Color(0xFF263F5D),
                borderColor: Color(0xFF263F5D),
                callback: () async {
                  callback();
                }),
      ),
    );
  }
}
