import 'package:aitl/config/server/APIActionReqCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';

class CaseReviewHelper {
  getUrl({pageStart = 0, pageCount = 50, status}) {
    var url = APIActionReqCfg.CASE_REVIEW;
    url = url.replaceAll("#UserId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#UserCompanyInfoId#", userData.userModel.userCompanyID.toString());
    url = url.replaceAll("#ClientAgreementStatus#", "Sent");
    return url;
  }
}
