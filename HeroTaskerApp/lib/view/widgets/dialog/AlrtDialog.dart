import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

import '../../../main.dart';

showAlrtDialog({
  BuildContext context,
  String msg,
  String title,
}) {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      //showCloseIcon: true,
      //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      //customHeader: Icon(Icons.info, size: 50),
      body: Center(
        child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  (title != null)
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 20),
                          child: Text(
                            title,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: MyTheme.fontSize,
                                fontWeight: FontWeight.w500),
                          ),
                        )
                      : SizedBox(),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Text(
                      msg,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black, fontSize: MyTheme.fontSize - 2),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
              Positioned(
                top: isIPAD || isTablet ? -70 : -50,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.warning,
                        color: MyTheme.redColor,
                        size: isIPAD || isTablet ? 60 : 40)),
              )
            ]),
      ),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      buttonsTextStyle: TextStyle(fontSize: MyTheme.fontSize),
      btnOkText: "Ok",
      btnOkColor: MyTheme.redColor,
      btnOkOnPress: () {
        //Navigator.pop(context);
        //callback(email.text.trim());
      })
    ..show();
}
