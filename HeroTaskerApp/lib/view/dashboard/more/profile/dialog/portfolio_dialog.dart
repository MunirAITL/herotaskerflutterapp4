import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMArrowBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PortFolioDialog extends StatefulWidget {
  const PortFolioDialog({Key key}) : super(key: key);

  @override
  State<PortFolioDialog> createState() => _PortFolioDialogState();
}

class _PortFolioDialogState extends State<PortFolioDialog> with Mixin {
  //  portfolio upload file list
  List<String> listUrl = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initPage();
  }

  initPage() async {
    try {
      await APIViewModel().req<UserPortFolioAPIModel>(
          context: context,
          url: APIProfileCFg.USER_PORTFOLIO_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get,
          callback: (model) {
            if (model != null && mounted) {
              if (model.success) {
                final userPortfulioModel = model.responseData.userPortfulio;
                listUrl = userPortfulioModel.portfulioItems.split('|');
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    final len = listUrl.length;
    return SingleChildScrollView(
      child: Container(
        height: getH(context),
        width: getW(context),
        child: Card(
          elevation: 10,
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: MyTheme.appbarColor,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    children: [
                      Flexible(
                          child: IconButton(
                              onPressed: () {
                                Get.back();
                              },
                              icon: Icon(Icons.arrow_back_ios,
                                  color: Colors.black))),
                      Txt(
                          txt: "Portfolio",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Container(
                  child: GridView.count(
                    shrinkWrap: true,
                    primary: false,
                    crossAxisCount: 3,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    padding: EdgeInsets.all(10.0),
                    children: List.generate(
                      len + ((len >= AppConfig.totalUploadLimit) ? 0 : 1),
                      (index) {
                        return Card(
                          child: (index < len)
                              ? Stack(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image:
                                              MyNetworkImage.loadProfileImage(
                                                  listUrl[index]),
                                          fit: BoxFit.cover,
                                        ),
                                        shape: BoxShape.rectangle,
                                      ),
                                    ),
                                    Positioned(
                                      right: -5,
                                      top: -5,
                                      child: Container(
                                        width: 30,
                                        height: 30,
                                        child: MaterialButton(
                                          onPressed: () {
                                            confirmDialog(
                                                context: context,
                                                title: "Portfolio Item Delete",
                                                msg:
                                                    "Are you sure, you want to delete this portfolio?",
                                                callbackYes: () {
                                                  listUrl
                                                      .remove(listUrl[index]);
                                                  setState(() {});
                                                },
                                                callbackNo: () {});
                                          },
                                          color: MyTheme.gray1Color,
                                          child: Icon(Icons.close,
                                              color: Colors.black),
                                          padding: EdgeInsets.all(0),
                                          shape: CircleBorder(),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : GestureDetector(
                                  onTap: () {
                                    CamPicker().showCamDialog(
                                      context: context,
                                      isRear: false,
                                      callback: (File path) {
                                        if (path != null) {
                                          APIViewModel().upload(
                                              context: context,
                                              file: path,
                                              callback: (model) {
                                                if (model != null && mounted) {
                                                  if (model.success) {
                                                    try {
                                                      final MediaUploadFilesModel
                                                          mediaModel = model
                                                              .responseData
                                                              .images[0];
                                                      if (!listUrl.contains(
                                                          mediaModel.url)) {
                                                        listUrl.add(
                                                            mediaModel.url);
                                                      }

                                                      setState(() {});
                                                    } catch (e) {}
                                                  }
                                                }
                                              });
                                        }
                                      },
                                    );
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyTheme.gray1Color,
                                    ),
                                    child: Icon(
                                      Icons.add,
                                      size: 100,
                                      color: MyTheme.gray3Color,
                                    ),
                                  ),
                                ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
                child: MMArrowBtn(
                  txt: "Save",
                  icon: null,
                  height: getHP(context, MyTheme.btnHpa),
                  width: MediaQuery.of(context).size.width,
                  bgColor: MyTheme.brandColor,
                  callback: () async {
                    if (listUrl.length > 0) {
                      final portfulioItemsUrls = listUrl.join('|');
                      await APIViewModel().req<UserPortFolioAPIModel>(
                          context: context,
                          url: APIProfileCFg.USER_PORTFOLIO_UPLOAD_URL,
                          param: {
                            "DocumentUrl": "",
                            "Id": 0,
                            "PortfulioItemsUrls": portfulioItemsUrls,
                            "UserId": userData.userModel.id,
                          },
                          reqType: ReqType.Post,
                          callback: (model) {
                            if (model != null && mounted) {
                              if (model.success) {
                                showToast(
                                    context: context,
                                    msg: "Portfolio uploaded successfully.",
                                    which: 1);
                                Future.delayed(
                                    Duration(seconds: 3), (() => Get.back()));
                              }
                            }
                          });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
