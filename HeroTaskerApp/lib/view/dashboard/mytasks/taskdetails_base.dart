import 'dart:io';
import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/view/dashboard/mytasks/funds/task_payment_page.dart';
import 'package:aitl/view/widgets/btn/BtnRWidget.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view_model/helper/pubnub/PubnubMgr.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/messages/chat_page.dart';
import 'package:aitl/view/dashboard/messages/comments/offers_comments_page.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/dashboard/more/resolutions/res_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/rec_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/req_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/review_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/accept_offer_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/update_offer_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/view_offer_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/mapview/TaskLocationMap.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/generic/enum_gen.dart';
import 'package:aitl/view_model/helper/utils/ProfileHelper.dart';
import 'package:aitl/view_model/helper/utils/TaskDetailsHelper.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:badges/badges.dart';
import 'offers/confirm_your_offer/confirm_your_offer_page.dart';

/*

customer/poster = mesuk2
tasker = mesuk1
Task.userId == login.id --> poster

*/
enum enumMenu {
  Edit,
  Share_task,
  Copy,
  Report,
  Cancel_task,
}

abstract class BaseTaskDetailsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TaskDetailsHelper, TimeLineHelper {
  final taskCtrl = Get.put(TaskCtrl());
  bool isPoster = true;
  bool isLoading = false;
  bool isPaymentDue = false;
  ValueNotifier<int> indexTopBtn;

  //this.isPoster = taskCtrl.getTaskModel().userId === userData.userModel.id;
  setIsPoster(bool isPoster) {
    this.isPoster = isPoster;
  }

  var listTopBtn = [
    "Active",
    "Assigned",
    "Paid",
  ];

  onReloadAPI();
  wsGetTimelineByApp();
  wsWithdrawTaskbidding(TaskBiddingModel model);
  onDelTaskClicked();

  /*drawTestStripePayment(List<TaskBiddingModel> taskBidding) {
    return MMBtn(
      txt: "Test Stripe",
      height: 50,
      width: 100,
      callback: () {
        if (taskBidding != null && taskBidding.length > 0)
          onReleasePayment(taskBidding[0]);
        else
          onReleasePayment(null);
      },
    );
  }*/

  getPopupMenuButton() {
    if (!isPoster &&
        taskCtrl.getStatusCode() != TaskStatusCfg.TASK_STATUS_ACTIVE) {
      return PopupMenuButton<String>(
        offset: Offset(-10.0, kToolbarHeight),
        onSelected: handleMenuClick,
        color: Colors.white,
        itemBuilder: (BuildContext context) {
          return {EnumGen.getEnum2Str(enumMenu.Copy)}.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(
                choice,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            );
          }).toList();
        },
      );
    } else if (!isPoster) {
      return PopupMenuButton<String>(
        offset: Offset(-10.0, kToolbarHeight),
        onSelected: handleMenuClick,
        color: Colors.white,
        itemBuilder: (BuildContext context) {
          return {
            EnumGen.getEnum2Str(enumMenu.Edit),
            EnumGen.getEnum2Str(enumMenu.Cancel_task),
            EnumGen.getEnum2Str(enumMenu.Copy),
          }.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(
                choice,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            );
          }).toList();
        },
      );
    } else {
      return PopupMenuButton<String>(
        offset: Offset(-10.0, kToolbarHeight),
        onSelected: handleMenuClick,
        color: Colors.white,
        itemBuilder: (BuildContext context) {
          return {
            EnumGen.getEnum2Str(enumMenu.Copy),
            EnumGen.getEnum2Str(enumMenu.Report),
          }.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(
                choice,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            );
          }).toList();
        },
      );
    }
  }

  void handleMenuClick(String str) {
    str = str.replaceAll(" ", "_");
    enumMenu f =
        enumMenu.values.firstWhere((e) => e.toString().split('.').last == str);
    switch (f) {
      case enumMenu.Edit:
        myLog("edit");
        onEditTask(false);
        break;
      case enumMenu.Cancel_task:
        myLog("cancel task");
        confirmDialog(
            context: context,
            title: taskCtrl.getTaskModel().title,
            msg: "Are you sure you want to cancel your task?",
            callbackYes: () {
              onDelTaskClicked();
            },
            callbackNo: () {});
        break;
      case enumMenu.Copy:
        myLog("copy");
        onEditTask(true);
        break;
      case enumMenu.Report:
        myLog("report");
        Get.to(
          () => ResPage(
            from: ResCfg.RESOULUTION_TYPE_TASK_SPAMREPORT,
            userId: userData.userModel.id,
          ),
        );
        break;
      case enumMenu.Share_task:
        myLog("share_task");
        break;
      default:
        break;
    }
  }

  onEditTask(bool isCopyTask) {
    Get.to(
      () => AddTask1Screen(
        index: null,
        userModel: userData.userModel,
        taskModel: taskCtrl.getTaskModel(),
        isCopyTask: isCopyTask,
      ),
    ).then(
      (pageNo) {
        if (isCopyTask) {
          Get.back(result: "STATE_RELOAD_TAB_FIND_WORK");
        } else {
          //getRefreshData();
          //obsUpdateTabs(pageNo);
          if (pageNo != null) {
            if (pageNo == 4)
              Get.back();
            else {
              onReloadAPI();
              obsUpdateTabs(pageNo);
            }
          } else {
            onReloadAPI();
            obsUpdateTabs(pageNo);
          }
        }
      },
    );
  }

  onReviewClicked(TaskBiddingModel taskBidding) {
    Get.to(() => ReviewPage(taskBiddingModel: taskBidding)).then(
      (value) {
        onReloadAPI();
      },
    );
  }

  onReleasePaymnet(TaskBiddingModel taskBidding) {
    Get.to(() =>
            FundPaymentPage(taskBidding: taskBidding, isReleasePayment: true))
        .then((value) {
      onReloadAPI();
    });
  }

  onTaskPayment(TaskBiddingModel taskBidding) {
    Get.to(() => TaskPaymentPage(taskBidding: taskBidding));
  }

  onRequestPayment(TaskBiddingModel taskBidding) {
    Get.to(
      () => ReqPaymentPage(
        taskBidding: taskBidding,
      ),
    ).then(
      (value) {
        onReloadAPI();
      },
    );
  }

  drawTopBtn(int index) {
    final w = (getWP(context, 100) / 3) - 20;
    return ValueListenableBuilder(
      valueListenable: indexTopBtn,
      builder: (context, value, child) => Container(
        width: w,
        child: Btn(
            txt: listTopBtn[index],
            txtColor: (value == index)
                ? MyTheme.greenAlertWidgetText
                : MyTheme.gray3Color,
            bgColor: (value == index)
                ? MyTheme.greenAlertWidgetBackground
                : Colors.white,
            radius: 20,
            callback: () {
              indexTopBtn.value = index;
            }),
      ),
    );
  }

  drawTopStatus() {
    var stateSelected = 0;
    try {
      switch (taskCtrl.getStatusCode()) {
        case TaskStatusCfg.TASK_STATUS_ACTIVE:
          stateSelected = 0;
          break;
        case TaskStatusCfg.TASK_STATUS_ACCEPTED:
          stateSelected = 1;
          break;
        case TaskStatusCfg.TASK_STATUS_PAYMENTED:
          stateSelected = 2;
          break;
        case TaskStatusCfg.TASK_STATUS_CANCELLED:
          stateSelected = 0;
          listTopBtn = ["Cancelled", "", ""];
          break;
        default:
      }
    } catch (e) {}

    return (stateSelected == 2)
        ? Container(
            decoration: BoxDecoration(
              color: MyTheme.greenAlertWidgetBackground,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                for (String statusStr in listTopBtn)
                  Container(
                    width: getW(context) / (listTopBtn.length + 1),
                    child: Txt(
                        txt: statusStr,
                        txtColor: MyTheme.greenAlertWidgetText,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
              ],
            ),
          )
        : (stateSelected == 1)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: MyTheme.greenAlertWidgetBackground,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (int i = 0; i < listTopBtn.length - 1; i++)
                          Container(
                            width: getW(context) / (listTopBtn.length + 1),
                            child: Txt(
                                txt: listTopBtn[i],
                                txtColor: MyTheme.greenAlertWidgetText,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                      ],
                    ),
                  ),
                  Txt(
                      txt: listTopBtn[listTopBtn.length - 1],
                      txtColor: MyTheme.gray3Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false)
                ],
              )
            : Padding(
                padding: EdgeInsets.only(
                    left: (listTopBtn.length == 1) ? 40 : 20,
                    right: (listTopBtn.length == 1) ? 40 : 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyTheme.greenAlertWidgetBackground,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Txt(
                            txt: listTopBtn[0],
                            txtColor: MyTheme.greenAlertWidgetText,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (int i = 1; i < listTopBtn.length; i++)
                          Container(
                            width: getW(context) / (listTopBtn.length + 1),
                            child: Txt(
                                txt: listTopBtn[i],
                                txtColor: MyTheme.gray3Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                      ],
                    ),
                  ],
                ),
              );
  }

  drawTitle() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Txt(
            txt: taskCtrl.getTaskModel().title,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize + .5,
            txtAlign: TextAlign.start,
            fontWeight: FontWeight.w500,
            isBold: false),
      ),
    );
  }

  drawPostedBy() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: getWP(context, 15),
              height: getWP(context, 15),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: MyNetworkImage.loadProfileImage(
                      taskCtrl.getTaskModel().ownerImageUrl),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Txt(
                      txt: "POSTED BY",
                      txtColor: Colors.black54,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Get.to(() => ProfilePage(
                                profileUserID: taskCtrl
                                    .getTaskModel()
                                    .userId)).then((value) {
                              onReloadAPI();
                            });
                          },
                          child: Txt(
                              txt: taskCtrl.getTaskModel().ownerName,
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ),
                      Txt(
                          txt: DateFun.getTimeAgoTxt(
                              taskCtrl.getTaskModel().creationDate),
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Container(color: Colors.grey, height: .5),
                  ),
                ])),
          ],
        ),
      ),
    );
  }

  drawLocation() {
    var preferedLocation = taskCtrl.getTaskModel().preferedLocation;
    try {
      preferedLocation =
          (preferedLocation.isEmpty) ? "Remote" : preferedLocation;
    } catch (e) {
      preferedLocation = "";
    }

    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: ListTile(
          leading: Icon(
            Icons.location_on_outlined,
            color: MyTheme.gray3Color,
            size: 30,
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "LOCATION",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 5),
              Row(
                children: [
                  Expanded(
                    child: Txt(
                        txt: preferedLocation,
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isOverflow: true,
                        isBold: false),
                  ),
                  (preferedLocation.isNotEmpty &&
                          !taskCtrl.getTaskModel().isInPersonOrOnline)
                      ? GestureDetector(
                          onTap: () {
                            Get.to(() => TaskLocationMap());
                          },
                          child: Txt(
                              txt: "View map",
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      : SizedBox()
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Container(color: Colors.grey, height: .5),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawDueDate() {
    bool isReshedule = false;
    if (isPoster) {
      if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACTIVE ||
          taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_DRAFT) {
        isReshedule = true;
      }
    }

    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: ListTile(
          leading: Icon(
            Icons.calendar_today_outlined,
            color: MyTheme.gray3Color,
            size: 30,
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "DUE DATE",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 5),
              Row(
                children: [
                  taskCtrl.getTaskModel().deliveryDate != null
                      ? Expanded(
                          child: Txt(
                              txt: DateFun.getFormatedDate(
                                  mDate: DateFormat("dd-MMM-yyyy").parse(
                                      taskCtrl.getTaskModel().deliveryDate),
                                  format: "dd/MMM/yyyy"),
                              txtColor: MyTheme.gray4Color,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      : SizedBox(),
                  (isReshedule)
                      ? GestureDetector(
                          onTap: () {
                            onEditTask(false);
                          },
                          child: Txt(
                              txt: "Reschedule",
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      : SizedBox()
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawPriceBox(List<TaskBiddingModel> listTaskBidding) {
    if (listTaskBidding == null) return SizedBox();
    //if (taskCtrl.isTaskModel()) return SizedBox();

    if (taskCtrl.getTaskModel().fixedBudgetAmount == null)
      taskCtrl.getTaskModel().fixedBudgetAmount = 0;

    var priceApproxHrTxt = "";
    var priceTxt = "0";
    /*if (taskCtrl.isTaskModel()) {
      if (!taskCtrl.getTaskModel().isFixedPrice) {
        priceApproxHrTxt =
            "Approx. " + taskCtrl.getTaskModel().totalHours.toString() + " hrs";
        priceTxt = (taskCtrl.getTaskModel().totalHours *
                taskCtrl.getTaskModel().fixedBudgetAmount)
            .round()
            .toString();
      }
    }*/
    try {
      if (!taskCtrl.getTaskModel().isFixedPrice) {
        try {
          priceApproxHrTxt = "Approx. " +
              taskCtrl.getTaskModel().totalHours.toString() +
              " hrs";
          priceTxt = (taskCtrl.getTaskModel().isFixedPrice)
              ? taskCtrl.getTaskModel().fixedBudgetAmount.round().toString()
              : taskCtrl.getTaskModel().hourlyRate.round().toString() + '/hr';
        } catch (e) {}
      } else {
        try {
          priceTxt =
              taskCtrl.getTaskModel().fixedBudgetAmount.round().toString();
        } catch (e) {}
      }
    } catch (e) {}

    var perTaskerTxt = "";
    if (taskCtrl.isTaskModel()) {
      if (taskCtrl.getTaskModel().workerNumber > 1) {
        perTaskerTxt = " per Tasker";
      }
    }

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.grey.shade500, width: .5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: new BoxDecoration(
                  color: MyTheme.gray2Color,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0),
                  )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/images/svg/ic_card.svg',
                    ),
                    SizedBox(width: 10),
                    Txt(
                        txt: "TASK PRICE",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .5,
                        txtAlign: TextAlign.start,
                        isBold: false)
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            (priceApproxHrTxt != '')
                ? Txt(
                    txt: priceApproxHrTxt,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.start,
                    isBold: false)
                : SizedBox(),
            SizedBox(height: 20),
            drawPrice(
              price: priceTxt,
              txtSize: 45,
              txtColor: Colors.black,
            ),
            //SizedBox(height: 20),
            (perTaskerTxt != '')
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Txt(
                        txt: perTaskerTxt,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .3,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  )
                : SizedBox(),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Container(color: Colors.grey, height: .5),
            ),
            _drawTaskPriceBtn(listTaskBidding),
          ],
        ),
      ),
    );
  }

  _drawTaskPriceBtn(List<TaskBiddingModel> taskBiddingList) {
    TaskBiddingModel _taskBiddingModel;
    //var biddingCount =
    try {
      for (var taskBidding in taskBiddingList) {
        if (taskBidding.status ==
                TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED) &&
            taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACCEPTED &&
            taskCtrl.getTaskModel().userId == userData.userModel.id) {
          _taskBiddingModel = taskBidding;
        } else if (taskBidding.userId == userData.userModel.id) {
          _taskBiddingModel = taskBidding;
          break;
        }
      }
      //  taskBiddingUserId = listTaskBidding[0].userId;
    } catch (e) {}

// For only active/cancelled/ paid status , it will get all users for active status
    if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACTIVE ||
        taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_CANCELLED ||
        taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_PAYMENTED) {
      return _drawTaskPriceBtn_AllUserForActiveStatus(_taskBiddingModel);
    }
    // Poster and Tasker Assgined tasked
    else if ((taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACCEPTED &&
            taskCtrl.getTaskModel().userId == userData.userModel.id) ||
        (_taskBiddingModel != null &&
            taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACCEPTED &&
            _taskBiddingModel.status ==
                TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED) &&
            _taskBiddingModel.userId == userData.userModel.id)) {
      return _drawTaskPriceBtn_UserForAssignedStatus(_taskBiddingModel);
    }
    // All others user view for assigned tasks
    else if (taskCtrl.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_ACCEPTED) //  && _taskBiddingModel == null
    {
      return _drawTaskPriceBtn_AllOthersUserForAssignedStatus(
          _taskBiddingModel);
    }
    // else  if (taskCtrl.getStatusCode() ==
    //       TaskStatusCfg.TASK_STATUS_ACCEPTED) {
    //     btnTxt1 = "ASSIGNED";
    //     isAccepted = true;
    //   }

    // else if (taskBiddingUserId ==  &&
    //     taskCtrl.getStatusCode() ==
    //         TaskStatusCfg.TASK_STATUS_ACCEPTED) {
    //   return _drawTaskPriceBtn_Taskbidding(listTaskBidding);
    // }

    //  else if (taskCtrl.getStatusCode() ==
    //         TaskStatusCfg.TASK_STATUS_ACTIVE &&
    //     taskCtrl.getTaskModel().userId != userData.userModel.id &&
    //     _taskBiddingModel == null) {
    //   //if(taskCtrl.getTaskModel().id == userData.userModel.id && listTaskBidding != null && listTaskBidding.length > 0){

    //   //final radius = 20;
    //   taskDetController.isShowPrivateMessage.value = false;
    //   String btnTxt1 = "";
    //   bool isPaid = false;
    //   bool isAccepted = false;
    //   bool isCancelled = false;
    //   bool isMakeOffer = false;
    // if (taskCtrl.getStatusCode() ==
    //     TaskStatusCfg.TASK_STATUS_ACTIVE) {
    //   //final totalBidsNumber = taskCtrl.getTaskModel().totalBidsNumber;
    //   //if (totalBidsNumber > 0) {
    //   //btnTxt1 = "View all offers";
    //   // } else {
    //   btnTxt1 = "Make an offer";
    //   isMakeOffer = true;
    //   // }
    // }
    // else if (taskCtrl.getStatusCode() ==
    //     TaskStatusCfg.TASK_STATUS_PAYMENTED) {
    //   btnTxt1 = "PAID";
    //   isPaid = true;
    // }

    //   else if (taskCtrl.getStatusCode() ==
    //       TaskStatusCfg.TASK_STATUS_CANCELLED) {
    //     btnTxt1 = "CANCELLED";
    //     isCancelled = true;
    //   }
    //   return Column(
    //     children: [
    //       Padding(
    //         padding:
    //             const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
    //         child: MMBtn(
    //           txt: btnTxt1,
    //           bgColor: (isPaid || isAccepted || isCancelled)
    //               ? MyTheme.gray4Color
    //               : null,
    //           width: getW(context),
    //           height: getHP(context, MyTheme.btnHpa),
    //           radius: 20,
    //           callback: () async {
    //             if (isMakeOffer) {
    //               var taskBidding = TaskBiddingModel();
    //               try {
    //                 taskBidding = listTaskBidding[0];
    //               } catch (e) {}

    //               // await APIViewModel().req<PublicProfileAPIModel>(
    //               //     context: context,
    //               //     url: APIProfileCFg.PUBLIC_USER_GET_URL.replaceAll(
    //               //         "#userId#", userData.userModel.id.toString()),
    //               //     reqType: ReqType.Get,
    //               //     callback: (model) {
    //               //       if (mounted && model != null) {
    //               //         if (model.success) {
    //               //           final usr = model.responseData.user;
    //               //           if (model.responseData.user != null &&
    //               //               usr.communityId != "2") {
    //               //             Get.to(() => ConfirmYourOfferPage(
    //               //                 //  bank info, profile image nic popup
    //               //                 taskBiddingModel: taskBidding)).then(
    //               //               (value) {
    //               //                 getRefreshData();
    //               //               },
    //               //             );
    //               //           } else {
    //               //             //  normal offer page
    //               //             Get.to(() =>
    //               //                     UpdateOfferPage(taskBidding: taskBidding))
    //               //                 .then(
    //               //               (value) {
    //               //                 getRefreshData();
    //               //               },
    //               //             );
    //               //           }
    //               //         }
    //               //       } else {
    //               //         Get.to(() =>
    //               //             UpdateOfferPage(taskBidding: taskBidding)).then(
    //               //           (value) {
    //               //             getRefreshData();
    //               //           },
    //               //         );
    //               //       }
    //               //     });
    //             }
    //           },
    //         ),
    //       ),
    //     ],
    //   );
    // }

    else {
      return SizedBox();
    }
  }

  _drawTaskPriceBtn_AllUserForActiveStatus(TaskBiddingModel taskBiddingModel) {
    if (isPaymentDue) {
      return SizedBox();
    }
    // if (listTaskBidding == null || listTaskBidding.length == 0) {
    //   return SizedBox();
    // }

    switch (taskCtrl.getStatusCode()) {
      case TaskStatusCfg.TASK_STATUS_ACTIVE:
        // start If condition
        if (isPoster &&
            taskCtrl.getTaskModel().userId == userData.userModel.id &&
            taskCtrl.getTaskModel().totalBidsNumber > 0) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: MMBtn(
                  txt: "Review Offers",
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 20,
                  callback: () async {
                    Get.to(() => ViewOfferPage(taskBidding: null)).then(
                      (value) {
                        onReloadAPI();
                      },
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 20, right: 20, bottom: 10),
                child: _drawTaskPriceInfo(
                    "You will be prompted to confirm the release of payment"),
              ),
            ],
          );
        } else if ((taskCtrl.getStatusCode() ==
                    TaskStatusCfg.TASK_STATUS_ACTIVE &&
                taskCtrl.getTaskModel().userId != userData.userModel.id) ||
            taskCtrl.getTaskModel().status == "ASSIGNED" &&
                taskCtrl.getTaskModel().userId != userData.userModel.id &&
                taskCtrl.getTaskModel().workerNumber -
                        taskCtrl.getTaskModel().totalAcceptedNumber >
                    0) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 20),
                child: MMBtn(
                  txt: taskBiddingModel == null
                      ? "Make an offer"
                      : "Update an offer",
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 20,
                  callback: () async {
                    // update for make offer coding
                    if (userData.userModel.communityId != "2") {
                      Get.to(() => ConfirmYourOfferPage(
                          //  bank info, profile image nic popup
                          taskBiddingModel: taskBiddingModel)).then(
                        (value) {
                          onReloadAPI();
                        },
                      );
                    } else {
                      //  normal offer page
                      Get.to(() =>
                          UpdateOfferPage(taskBidding: taskBiddingModel)).then(
                        (value) {
                          onReloadAPI();
                        },
                      );
                    }
                  },
                ),
              ),
            ],
          );
        } else {
          return SizedBox();
        }
        break;
      case TaskStatusCfg.TASK_STATUS_PAYMENTED:
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 20, bottom: 20),
              child: MMBtn(
                txt: "PAID",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                bgColor: MyTheme.gray4Color,
                txtColor: Colors.white,
                radius: 5,
                callback: () async {},
              ),
            ),
          ],
        );
        break;
      case TaskStatusCfg.TASK_STATUS_CANCELLED:
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: MMBtn(
                txt: "CANCELLED",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                bgColor: MyTheme.gray4Color,
                txtColor: Colors.white,
                radius: 20,
                callback: () async {},
              ),
            ),
          ],
        );
        break;
      default:
        return SizedBox();
    }
  }

  _drawTaskPriceBtn_UserForAssignedStatus(TaskBiddingModel taskBiddingModel) {
    if (isPaymentDue) {
      return SizedBox();
    }
    // if (listTaskBidding == null || listTaskBidding.length == 0) {
    //   return SizedBox();
    // }

    Widget widMakePayment = SizedBox();
    Widget widReleasePayment = SizedBox();
    Widget widReceivedPayment = SizedBox();

    switch (taskCtrl.getStatusCode()) {
      case TaskStatusCfg.TASK_STATUS_ACCEPTED:
        if (taskBiddingModel != null &&
            taskBiddingModel.paymentStatus != "FUNDED_REQUESTPAYMENT" &&
            taskBiddingModel.paymentStatus != "FUNDED") {
          widMakePayment = Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: MMBtn(
                  txt: "Request payment",
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 20,
                  callback: () async {
                    onRequestPayment(taskBiddingModel);
                  },
                ),
              ),
            ],
          );
        }
        if (isPoster &&
            taskCtrl.getTaskModel().userId == userData.userModel.id &&
            taskBiddingModel != null &&
            (taskBiddingModel.paymentStatus == "FUNDED_REQUESTPAYMENT" ||
                taskBiddingModel.paymentStatus == "FUNDED")) {
          widReleasePayment = Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: MMBtn(
                  txt: "Release payment",
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 20,
                  callback: () async {
                    onTaskPayment(taskBiddingModel);
                  },
                ),
              ),
            ],
          );
        }
        if (isPoster == false &&
            taskBiddingModel.paymentStatus != "FUNDED_REQUESTPAYMENT" &&
            taskBiddingModel.paymentStatus != "FUNDED") {
          widReceivedPayment = Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 10),
                child: MMBtn(
                  txt: "Received Payment",
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 20,
                  callback: () async {
                    onTaskPayment(taskBiddingModel);
                  },
                ),
              ),
              /*Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 20, right: 20, bottom: 10),
                child: _drawTaskPriceInfo(
                    "You will be prompted to confirm the release of payment"),
              ),*/
            ],
          );
        }
        return Column(
            children: [widMakePayment, widReleasePayment, widReceivedPayment]);
        break;
      default:
        return SizedBox();
    }
  }

  _drawTaskPriceBtn_AllOthersUserForAssignedStatus(
      TaskBiddingModel taskBiddingModel) {
    if (isPaymentDue) {
      return SizedBox();
    }
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: MMBtn(
            txt: "ASSIGNED",
            width: getW(context),
            height: getHP(context, MyTheme.btnHpa),
            bgColor: MyTheme.gray4Color,
            txtColor: Colors.white,
            radius: 20,
            callback: () async {},
          ),
        ),
      ],
    );
  }

  /*_drawTaskPriceBtn_Taskbidding(List<TaskBiddingModel> listTaskBidding) {
    bool isTaskBiddingAccepted = false;
    if (TaskStatusCfg().getSatusCode(listTaskBidding[0].status) ==
        TaskStatusCfg.TASK_STATUS_ACCEPTED) {
      isTaskBiddingAccepted = true;
    }

    if (TaskStatusCfg().getSatusCode(listTaskBidding[0].status) ==
        TaskStatusCfg.TASK_STATUS_ACTIVE) {
      taskCtrl.isShowPrivateMessage.value = false;
    } else {
      taskCtrl.isShowPrivateMessage.value = true;
    }

    //
    if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACCEPTED &&
        isTaskBiddingAccepted) {
      String btnTxt1 = "Request payment";
      String btnTxt2 = "Receive payment";

      bool isFuneded = false;
      try {
        if (listTaskBidding[0].paymentStatus ==
                TaskStatusCfg.STATUS_PAYMENT_METHOD_FUNDED ||
            listTaskBidding[0].paymentStatus ==
                TaskStatusCfg.STATUS_PAYMENT_METHOD_FUNDED_REQUESTPAYMENT) {
          btnTxt1 = "Funded";
          btnTxt2 = "Request release payment";
          isFuneded = true;
        }
      } catch (e) {}

      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: MMBtn(
              txt: btnTxt1,
              bgColor: (isFuneded) ? MyTheme.gray4Color : null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (!isFuneded) {
                  if (btnTxt1 == "Request payment") {
                    Get.to(
                      () => ReqPaymentPage(
                        taskBidding: listTaskBidding[0],
                      ),
                    ).then(
                      (value) {
                        onReloadAPI();
                      },
                    );
                  }
                }
              },
            ),
          ),
          btnTxt2 != null
              ? Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 20, bottom: 20),
                  child: MMBtn(
                    txt: btnTxt2,
                    width: getW(context),
                    height: getHP(context, MyTheme.btnHpa),
                    radius: 20,
                    callback: () async {
                      if (btnTxt2 == "Receive payment") {
                        Get.to(
                          () => RecPaymentPage(
                            taskBidding: listTaskBidding[0],
                          ),
                        ).then(
                          (value) {
                            onReloadAPI();
                          },
                        );
                      } else if (btnTxt2 == "Request release payment") {
                        Get.to(
                          () => ReqPaymentPage(
                            taskBidding: listTaskBidding[0],
                          ),
                        ).then(
                          (value) {
                            onReloadAPI();
                          },
                        );
                      }
                    },
                  ),
                )
              : SizedBox(height: 10),
        ],
      );
    } else if (taskCtrl.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_PAYMENTED) {
      return Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        child: MMBtn(
          txt: "PAID",
          bgColor: MyTheme.gray4Color,
          width: getW(context),
          height: getHP(context, MyTheme.btnHpa),
          radius: 5,
          callback: () async {},
        ),
      );
    } else if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACCEPTED &&
        listTaskBidding[0].status ==
            TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACTIVE)) {
      String btnTxt1 = "Make an offer";
      bool isPaid = false;
      bool isAccepted = false;
      if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_PAYMENTED) {
        btnTxt1 = "PAID";
        isPaid = true;
      } else if (taskCtrl.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_ACCEPTED) {
        btnTxt1 = "ASSIGNED";
        isAccepted = true;
      }

      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: MMBtn(
              txt: btnTxt1,
              bgColor: (isPaid || isAccepted) ? MyTheme.gray4Color : null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (!isPaid && !isAccepted) {}
              },
            ),
          ),
          SizedBox(height: 10),
        ],
      );
    } else if (listTaskBidding[0].id > 0) {
      return Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            child: MMBtn(
              txt: "Update offer",
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (userData.communityId != 2) {
                  Get.to(() => ConfirmYourOfferPage(
                      taskBiddingModel: listTaskBidding[0])).then(
                    (value) {
                      onReloadAPI();
                    },
                  );
                } else {
                  Get.to(() => UpdateOfferPage(taskBidding: listTaskBidding[0]))
                      .then((value) {
                    onReloadAPI();
                  });
                }
              },
            ),
          ),
        ],
      );
    } else {
      return SizedBox();
    }
  }*/

  _drawIncreasePrice() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RawMaterialButton(
            onPressed: () {},
            elevation: 2.0,
            fillColor: MyTheme.brandColor,
            child: Icon(
              Icons.add,
              size: 20,
              color: Colors.white,
            ),
            //padding: EdgeInsets.all(5.0),
            shape: CircleBorder(),
          ),
          //SizedBox(width: 10),
          Flexible(
            flex: 2,
            child: Txt(
                txt: "Increase price",
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          )
        ],
      ),
    );
  }

  _drawTaskPriceInfo(String txt) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
      child: Txt(
          txt: txt,
          txtColor: MyTheme.gray4Color,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.center,
          isBold: false),
    );
  }

  drawReviews(List<UserRatingsModel> listUserRatings,
      List<TaskBiddingModel> listTaskBidding) {
    if (listUserRatings.length == 0) return SizedBox();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.all(20),
            child: Txt(
                txt: "REVIEWS",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          ProfileHelper().drawTaskDetailsUserRatingView(
              w: getW(context),
              h: getH(context),
              model: listTaskBidding[0],
              listUserRating: listUserRatings,
              callback: () {
                if (listTaskBidding != null) {
                  if (listTaskBidding.length > 0) {
                    onReviewClicked(listTaskBidding[0]);
                  }
                }
              }),
          SizedBox(height: 20),
          drawLine(),
        ],
      ),
    );
  }

  drawPrivateMsg(List<TaskBiddingModel> taskBiddingList) {
    if (taskCtrl.getStatusCode() != TaskStatusCfg.TASK_STATUS_ACCEPTED &&
        taskCtrl.getStatusCode() != TaskStatusCfg.TASK_STATUS_PAYMENTED) {
      return SizedBox();
    }

    TaskBiddingModel _taskBiddingModel = null;
    //var biddingCount =
    try {
      for (var taskBidding in taskBiddingList) {
        if (taskBidding.status ==
                TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED) ||
            taskBidding.status ==
                TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_PAYMENTED)) {
          if (taskCtrl.getTaskModel().userId == userData.userModel.id) {
            _taskBiddingModel = taskBidding;
          } else if (taskBidding.userId == userData.userModel.id) {
            _taskBiddingModel = taskBidding;
            break;
          }
        }
      }
      //  taskBiddingUserId = listTaskBidding[0].userId;
    } catch (e) {}

    if (_taskBiddingModel == null) {
      return SizedBox();
    }
    if ((taskCtrl.getTaskModel().userId == userData.userModel.id) ||
        (_taskBiddingModel != null &&
            _taskBiddingModel.userId == userData.userModel.id)) {
      return Container(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Column(
            children: [
              /*Txt(
                txt: "",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),*/
              BtnRWidget(
                txt: "Open private messages",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                bgColor: MyTheme.whatappBtnChatColor,
                radius: 20,
                lWid: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Icon(Icons.chat_outlined, color: Colors.white),
                ),
                rWid:
                    SizedBox() /*Badge(
                  badgeColor: Colors.white,
                  showBadge: true, //(totalBadge > 0) ? true : false,
                  //position: BadgePosition.topStart(start: -2),
                  badgeContent: Text(
                    "0",
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ))*/
                ,
                callback: () async {
                  Get.to(() => ChatPage());
                },
              ),
            ],
          ),
        ),
      );
    } else {
      return SizedBox();
    }
  }

  drawDetailsView(List<GetPicModel> listGetPicModel) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "DETAILS",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            Txt(
                txt: taskCtrl.getTaskModel().description,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            (listGetPicModel != null)
                ? Container(
                    width: getW(context),
                    height:
                        getHP(context, (listGetPicModel.length > 3) ? 32 : 20),
                    child: GridView.count(
                      crossAxisCount: 3,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      primary: false,
                      padding: EdgeInsets.all(10.0),
                      children: List.generate(
                        listGetPicModel.length,
                        (index) {
                          var model = listGetPicModel[index];
                          return GestureDetector(
                            onTap: () {
                              Get.to(() => PicFullView(
                                    title: "Attachment",
                                    url: model.thumbnailPath,
                                  ));
                            },
                            child: Card(
                                child: Stack(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          model.thumbnailPath),
                                      fit: BoxFit.cover,
                                    ),
                                    shape: BoxShape.rectangle,
                                  ),
                                ),
                                /*Positioned(
                              right: -5,
                              top: -5,
                              child: Container(
                                width: 30,
                                height: 30,
                                child: MaterialButton(
                                  onPressed: () {
                                    Get.dialog(
                                      ConfirmDialog(
                                          callback: () {
                                            TaskPicViewModel().fetchDataDelPic(
                                              context: context,
                                               apiState: APIState(APIType.del_pic, this.runtimeType, null),
                                              index: index,
                                              mediaId: model.id,
                                            );
                                          },
                                          title: "Confirmation",
                                          msg:
                                              "Are you sure, you want to remove this attachment?"),
                                    );
                                  },
                                  color: MyTheme.gray1Color,
                                  child: Text("X",
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.black)),
                                  padding: EdgeInsets.all(0),
                                  shape: CircleBorder(),
                                ),
                              ),
                            ),*/
                              ],
                            )),
                          );
                        },
                      ),
                    ),
                  )
                : SizedBox(),
            drawLine(),
          ],
        ),
      ),
    );
  }

  drawProjectSummary() {
    final workerNumber = taskCtrl.getTaskModel().workerNumber;
    if (workerNumber == null) return SizedBox();
    if (workerNumber <= 1) return SizedBox();

    final int project_layout_spots_count =
        workerNumber - taskCtrl.getTaskModel().totalAcceptedNumber;
    final int project_layout_assigned_count =
        taskCtrl.getTaskModel().totalAcceptedNumber;
    final int project_layout_offers_count =
        taskCtrl.getTaskModel().totalBidsNumber;

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "TASK SUMMARY",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _drawProjSumBox("Spots Left", project_layout_spots_count),
                  _drawProjSumBox("Assigned", project_layout_assigned_count),
                  _drawProjSumBox("Offers", project_layout_offers_count),
                ],
              ),
            ),
            /*Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: MMBtn(
                txt: "Finalise project",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 20,
                callback: () async {},
              ),
            ),*/
          ],
        ),
      ),
    );
  }

  _drawProjSumBox(String title, int val) {
    return Flexible(
      child: Column(
        children: [
          Txt(
              txt: val.toString(),
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize + 2,
              txtAlign: TextAlign.center,
              isBold: false),
          Txt(
              txt: title,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawOffers(List<TaskBiddingModel> listTaskBidding, bool isHide) {
    if (listTaskBidding == null) return SizedBox();
    //if (listTaskBidding.length == 0) return SizedBox();
    //if (listTaskBidding.length == 0 &&
    //taskCtrl.getStatusCode() != TaskStatusCfg.TASK_STATUS_ACTIVE) {
    if (isPaymentDue &&
        taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACTIVE) {
      return SizedBox();
    } else {
      /*List<TaskBiddingModel> list = [];
      for (final v in listTaskBidding) {
        if (v.userId != userData.userModel.id) {
          list.add(v);
        }
      }
      listTaskBidding = list;*/

      //final totalBidsNumber = taskCtrl.getTaskModel().totalBidsNumber ?? 0;
      //var btnTxt1 = "Make an offer";
      //if (totalBidsNumber > 0) {
      //btnTxt1 = "View all offers";
      //} else {}

      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: (!isHide) ? 20 : 0),
            (!isHide)
                ? Padding(
                    padding: EdgeInsets.all((!isHide) ? 20 : 0),
                    child: Txt(
                        txt:
                            "OFFERS(" + listTaskBidding.length.toString() + ")",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  )
                : SizedBox(),
            (listTaskBidding.length == 0 && !isHide)
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Image.asset(
                              "assets/images/icons/ic_waiting_offers.png",
                              fit: BoxFit.cover,
                              width: getW(context) / 3.5,
                              height: getW(context) / 5,
                            ),
                          ),
                          SizedBox(height: 5),
                          Txt(
                              txt: "Waiting for offers",
                              txtColor: MyTheme.gray4Color,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
            /*(!isHide && listTaskBidding != null && listTaskBidding.length > 8)
                ? Container(
                    width: getW(context),
                    child: BtnOutline(
                      txt: btnTxt1,
                      width: getW(context),
                      height: getHP(context, MyTheme.btnHpa),
                      txtColor: MyTheme.brandColor,
                      borderColor: Colors.grey,
                      radius: 20,
                      callback: () async {

                      },
                    ),
                  )
                : SizedBox(),*/
            _drawOfferItems(listTaskBidding, isHide),
            SizedBox(height: (!isHide) ? 20 : 0),
            drawLine(),
          ],
        ),
      );
    }
  }

  Widget _drawOfferItems(List<TaskBiddingModel> listTaskBidding, bool isHide) {
    List<Widget> list = [];

    for (var model in listTaskBidding) {
      //final map = _getTextViewPriceOffered(model);

      dynamic map;

      if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACTIVE ||
          taskCtrl.getTaskModel().status == model.status) {
        map = _getTextViewPriceOffered(model);
      } else if (taskCtrl.getTaskModel().status == model.status &&
          taskCtrl.getTaskModel().workerNumber ==
              taskCtrl.getTaskModel().totalAcceptedNumber &&
          (model.userId == userData.userModel.id ||
              model.taskOwnerId == userData.userModel.id)) {
        map = _getTextViewPriceOffered(model);
      } else if (taskCtrl.getStatusCode() != TaskStatusCfg.TASK_STATUS_ACTIVE &&
          taskCtrl.getTaskModel().workerNumber >
              taskCtrl.getTaskModel().totalAcceptedNumber) {
        map = _getTextViewPriceOffered(model);
      } else {
        //map = _getTextViewPriceOffered(model);
      }

      if (map != null) {
        list.add(
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ListTile(
                    leading: GestureDetector(
                        onTap: () {
                          Get.to(() => ProfilePage(profileUserID: model.userId))
                              .then((value) => onReloadAPI());
                        },
                        child: CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.transparent,
                          backgroundImage: MyNetworkImage.loadProfileImage(
                              model.ownerImageUrl),
                        )),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          //flex: 3,
                          child: Container(
                            //width: getWP(context, 60),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Get.to(() => ProfilePage(
                                            profileUserID: model.userId))
                                        .then((value) => onReloadAPI());
                                  },
                                  child: Txt(
                                      txt: model.ownerName,
                                      txtColor: MyTheme.brandColor,
                                      txtSize: MyTheme.txtSize - .2,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    UIHelper().getStarRatingView(
                                      w: getW(context),
                                      h: getH(context),
                                      rate: model.averageRating.round(),
                                      reviewTxt1: '(',
                                      reviews: model.ratingCount,
                                      reviewTxt2: ')',
                                      align: MainAxisAlignment.start,
                                    ),
                                    UIHelper().getCompletionText(
                                      pa: model.taskCompletionRate.round(),
                                      align: MainAxisAlignment.start,
                                      txtColor: MyTheme.gray4Color,
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        (map['btnTxt'] != '')
                            ? Flexible(
                                //flex: 2,
                                child: Container(
                                  //width: getWP(context, 40),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Txt(
                                        txt: getCurSign() + map['price'],
                                        style: TextStyle(
                                            color: MyTheme.gray5Color,
                                            fontSize: 22),
                                        txtAlign: TextAlign.center,
                                        txtColor: null,
                                        txtSize: null,
                                        isBold: null,
                                      ),
                                      SizedBox(height: 10),
                                      GestureDetector(
                                        onTap: () async {
                                          if (map['confirmTxt'] != null) {
                                            confirmDialog(
                                                context: context,
                                                title: taskCtrl
                                                        .getTaskModel()
                                                        .title ??
                                                    '',
                                                msg: map['confirmTxt'],
                                                callbackYes: () {
                                                  Function.apply(
                                                      map['callback'], []);
                                                },
                                                callbackNo: () {});
                                          } else {
                                            if (map['callback'] != null) {
                                              Function.apply(
                                                  map['callback'], []);
                                            } else if (map['route'] != null) {
                                              await Get.to(map['route'])
                                                  .then((value) {
                                                if (value != null) {
                                                  StateProvider().notify(
                                                      ObserverState
                                                          .STATE_RELOAD_TASKDETAILS_GET_TIMELINE,
                                                      null);
                                                  Get.back();
                                                }
                                              });
                                            }
                                          }
                                        },
                                        child: Container(
                                          width: ((map['btnTxt'])
                                                      .toString()
                                                      .length *
                                                  15)
                                              .toDouble(),
                                          decoration: map['bgColor'] != null
                                              ? BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  color: map['bgColor'],
                                                )
                                              : BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                      color:
                                                          map['borderColor'])),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8),
                                            child: Text(
                                              map['btnTxt'],
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: map['txtColor'],
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : SizedBox()
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        (!isHide)
                            ? SizedBox(width: getWP(context, 15))
                            : SizedBox(),
                        Flexible(
                          child: Container(
                            color: MyTheme.gray1Color,
                            width: double.infinity,
                            //height: (!isHide) ? null : getHP(context, 5),
                            child: (model.coverLetter.length <
                                    (AppConfig.textExpandableSize + 100))
                                ? Txt(
                                    txt: model.coverLetter.toString().trim(),
                                    txtColor: MyTheme.gray5Color,
                                    txtSize: MyTheme.txtSize - .3,
                                    txtAlign: TextAlign.start,
                                    isBold: false)
                                : UIHelper().expandableTxt(model.coverLetter,
                                    MyTheme.gray5Color, Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                  (!isHide)
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(width: getWP(context, 15)),
                              Expanded(
                                child: Txt(
                                    txt: DateFun.getTimeAgoTxt(
                                        model.creationDate),
                                    txtColor: MyTheme.redColor,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ),
                              (TaskStatusCfg().getSatusCode(model.status) ==
                                      TaskStatusCfg.TASK_STATUS_ACTIVE)
                                  ? TextButton.icon(
                                      icon: Icon(Icons.reply,
                                          color: MyTheme.brandColor, size: 16),
                                      onPressed: () {
                                        Get.to(OffersCommentsPage(
                                          taskBiddingsModel: model,
                                        )).then((value) {
                                          onReloadAPI();
                                        });
                                      },
                                      label: Text(
                                        "Reply",
                                        style: TextStyle(
                                            color: MyTheme.brandColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )
                                  : SizedBox(),
                              Expanded(child: SizedBox()),
                              GestureDetector(
                                onTapDown: (TapDownDetails details) {
                                  onReportClicked(
                                      context,
                                      details.globalPosition,
                                      model.userId,
                                      ResCfg
                                          .RESOULUTION_TYPE_TASK_OFFERCOMMENT_SPAMREPORT);
                                },
                                child: Icon(
                                  Icons.more_horiz,
                                  color: MyTheme.gray4Color,
                                ),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  (!isHide)
                      ? Txt(
                          txt: "", // textViewReplyComments
                          txtColor: MyTheme.hotdipPink,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false)
                      : SizedBox(),
                ],
              ),
            ),
          ),
        );
      }
    }
    return Column(children: list);
  }

  //_getTextViewPriceOffered( List<TaskBiddingModel> listTaskBidding, TaskBiddingModel model) {
  _getTextViewPriceOffered(TaskBiddingModel model) {
    //view\ViewOffersAdapter.java -> buttonViewOffer
    String price = "";
    String btnTxt = "";
    Color txtColor = MyTheme.gray5Color;
    Color bgColor;
    Color borderColor;
    bool isEnable = true;
    String confirmTxt;
    var route;
    var callback;

    if (model.userId == userData.userModel.id) {
      switch (model.status) {
        case "ACTIVE":
          price = model.fixedbiddigAmount.round().toString();
          btnTxt = "Withdraw";
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          confirmTxt = "Are you sure you want to withdraw your offer?";
          isEnable = true;
          callback = () => wsWithdrawTaskbidding(model);
          break;
        case "ASSIGNED":
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          btnTxt = "Assigned";
          //set condi
          isEnable = false;
          /*if (model.isTaskOwner &&
              model.status ==
                  TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED))
            isEnable = true;
          else
            isEnable = false;
          if (isEnable) {
            txtColor = MyTheme.brandColor;
            borderColor = MyTheme.gray4Color;
            callback = () => onReleasePayment(model);
          } else {*/
          txtColor = Colors.white;
          bgColor = MyTheme.gray4Color;
          borderColor = MyTheme.gray4Color;
          //}
          break;
        case "PAID":
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          btnTxt = (!model.isReview) ? "Review" : "Paid";
          txtColor = (!model.isReview) ? MyTheme.brandColor : Colors.white;
          borderColor = (!model.isReview) ? MyTheme.gray4Color : null;
          bgColor = (!model.isReview) ? null : MyTheme.gray4Color;
          isEnable = (!model.isReview) ? true : false;
          callback = (!model.isReview) ? () => onReviewClicked(model) : null;
          break;
        default:
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          break;
      }
    } else if (model.taskOwnerId == userData.userModel.id) {
      txtColor = MyTheme.brandColor;
      borderColor = MyTheme.gray4Color;
      if (model.discountAmount > 0) {
        price = model.fixedbiddigAmount.round().toString();
      } else {
        price = model.netTotalAmount.round().toString();
      }
      if (model.status == "ACTIVE") {
        btnTxt = "Accept";
        txtColor = MyTheme.brandColor;
        borderColor = MyTheme.gray4Color;
        route = () => AcceptOfferPage(taskBidding: model);
      } else if (model.status == "ASSIGNED" &&
          model.status == "FUNDED" &&
          model.status == "FUNDED_REQUESTPAYMENT") {
        btnTxt = "Release";
        txtColor = MyTheme.brandColor;
        borderColor = MyTheme.gray4Color;
      } else if (model.status == "ASSIGNED") {
        btnTxt = "Assigned";
        if (model.isTaskOwner)
          //if(!isPoster)
          isEnable = true;
        else
          isEnable = false;
        if (isEnable) {
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          callback = () => onTaskPayment(model);
        } else {
          txtColor = Colors.white;
          bgColor = MyTheme.gray4Color;
          borderColor = MyTheme.gray4Color;
        }
      } else if (model.status == "PAID") {
        btnTxt = (!model.isReview) ? "Review" : "Paid";
        txtColor = (!model.isReview) ? MyTheme.brandColor : Colors.white;
        borderColor = (!model.isReview) ? MyTheme.gray4Color : null;
        bgColor = (!model.isReview) ? null : MyTheme.gray4Color;
        isEnable = (!model.isReview) ? true : false;
        callback = (!model.isReview) ? () => onReviewClicked(model) : null;
      } else {
        btnTxt = "";
        price = "";
      }
    }

    return {
      "btnTxt": btnTxt,
      'bgColor': bgColor,
      'txtColor': txtColor,
      'borderColor': borderColor,
      'isEnable': isEnable,
      "price": price,
      "confirmTxt": confirmTxt,
      "callback": callback,
      "route": route,
    };
  }

  drawQuestions(
      {List<TimelinePostModel> listTimelinePostsModel,
      TextEditingController textController,
      cls,
      Function(File, String) callbackCam}) {
    if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_PAYMENTED ||
        taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACCEPTED) {
      return SizedBox();
    }

    if (listTimelinePostsModel == null) return SizedBox();

    String picUrl = "";
    try {
      picUrl = userData
          .userModel.profileImageUrl; //taskCtrl.getTaskModel().ownerImageUrl;
    } catch (e) {}
    int totalQ = listTimelinePostsModel.length;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, bottom: 20, top: 20),
            child: Txt(
                txt: "COMMENTS(" + totalQ.toString() + ")",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          (taskCtrl.getStatusCode() != TaskStatusCfg.TASK_STATUS_CANCELLED)
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: drawTimelineHeading(),
                    ),
                    SizedBox(height: 10),
                    Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: drawLine()),
                    SizedBox(height: 20),
                    drawMessageBox(
                      context: context,
                      picUrl: picUrl,
                      hintText: 'Ask ' +
                          taskCtrl.getTaskModel().ownerName +
                          " a question",
                      textController: textController,
                      callbackCam: (File file, String txt) {
                        if (file == null && txt.length == 0) {
                        } else {
                          txt = Common.isNotAllowdPhoneNumber(txt);
                          txt = Common.isNotAllowdEmailAddress(txt);
                          callbackCam(file, txt);
                        }
                      },
                    ),
                    SizedBox(height: 10),
                    Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: drawLine()),
                  ],
                )
              : SizedBox(),
          drawQ(
            context: context,
            taskCtrl: taskCtrl,
            listTimelinePostsModel: listTimelinePostsModel,
            isReply: true,
            isPoster: isPoster,
            isReverse: true,
            callback: () async {
              wsGetTimelineByApp();
            },
          ),
        ],
      ),
    );
  }

  /*_getTextViewPriceOffered(TaskBiddingModel model) {
    //view\ViewOffersAdapter.java -> buttonViewOffer
    String price = "";
    String btnTxt = "";
    Color txtColor = MyTheme.gray5Color;
    Color bgColor;
    Color borderColor;
    bool isEnable = true;
    String confirmTxt;
    var route;
    var callback;

    if (model.userId == userData.userModel.id) {
      switch (model.status) {
        case "ACTIVE":
          price = model.fixedbiddigAmount.round().toString();
          btnTxt = "Withdraw";
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          confirmTxt = "Are you sure you want to withdraw your offer?";
          isEnable = true;
          callback = () => wsWithdrawTaskbidding(model);
          break;
        case "ASSIGNED":
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          btnTxt = "Assigned";
          /*if (model.isTaskOwner &&
              model.status ==
                  TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED))
            isEnable = true;
          else
            isEnable = false;
          if (isEnable) {
            txtColor = MyTheme.brandColor;
            borderColor = MyTheme.gray4Color;
            callback = () => onReleasePayment(model);
          } else {*/
          txtColor = Colors.white;
          bgColor = MyTheme.gray4Color;
          borderColor = MyTheme.gray4Color;
          //}
          break;
        case "PAID":
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          btnTxt = (!model.isReview) ? "Review" : "Paid";
          txtColor = (!model.isReview) ? MyTheme.brandColor : Colors.white;
          borderColor = (!model.isReview) ? MyTheme.gray4Color : null;
          bgColor = (!model.isReview) ? null : MyTheme.gray4Color;
          isEnable = (!model.isReview) ? true : false;
          callback = (!model.isReview) ? () => onReviewClicked(model) : null;
          break;
        default:
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          break;
      }
    } else if (model.taskOwnerId == userData.userModel.id) {
      txtColor = MyTheme.brandColor;
      borderColor = MyTheme.gray4Color;
      if (model.discountAmount > 0) {
        price = model.fixedbiddigAmount.round().toString();
      } else {
        price = model.netTotalAmount.round().toString();
      }
      if (model.status == "ACTIVE") {
        btnTxt = "Accept";
        txtColor = MyTheme.brandColor;
        borderColor = MyTheme.gray4Color;
        route = () => AcceptOfferPage(taskBidding: model);
      } else if (model.status == "ASSIGNED" &&
          model.status == "FUNDED" &&
          model.status == "FUNDED_REQUESTPAYMENT") {
        btnTxt = "Release";
        txtColor = MyTheme.brandColor;
        borderColor = MyTheme.gray4Color;
      } else if (model.status == "ASSIGNED") {
        btnTxt = "Assigned";
        if (model.isTaskOwner)
          isEnable = true;
        else
          isEnable = false;
        if (isEnable) {
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          callback = () => onTaskPayment(model);
        } else {
          txtColor = Colors.white;
          bgColor = MyTheme.gray4Color;
          borderColor = MyTheme.gray4Color;
        }
      } else if (model.status == "PAID") {
        btnTxt = (!model.isReview) ? "Review" : "Paid";
        txtColor = (!model.isReview) ? MyTheme.brandColor : Colors.white;
        borderColor = (!model.isReview) ? MyTheme.gray4Color : null;
        bgColor = (!model.isReview) ? null : MyTheme.gray4Color;
        isEnable = (!model.isReview) ? true : false;
        callback = (!model.isReview) ? () => onReviewClicked(model) : null;
      } else {
        btnTxt = "";
        price = "";
      }
    }

    return {
      "btnTxt": btnTxt,
      'bgColor': bgColor,
      'txtColor': txtColor,
      'borderColor': borderColor,
      'isEnable': isEnable,
      "price": price,
      "confirmTxt": confirmTxt,
      "callback": callback,
      "route": route,
    };
  }*/
}
