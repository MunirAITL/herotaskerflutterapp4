import 'package:aitl/config/server/Server.dart';

class APIPayCfg {
  static const String PAYMENTCONFIRMATION_PUT_URL =
      Server.BASE_URL + "/api/taskbidding/put/paymentconfirmation";

  static const USERPRICE_GET_URL = Server.BASE_URL + "/api/userprice/get";
  static const USERPRICE_POST_URL = Server.BASE_URL + "/api/userprice/post";
  static const USERPRICE_PUT_URL = Server.BASE_URL + "/api/userprice/put";
}
