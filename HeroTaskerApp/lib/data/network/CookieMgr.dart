import 'dart:io';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:path_provider/path_provider.dart';

class CookieMgr {
  static Map<String, String> headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
  };

  Future<CookieJar> getCookiee() async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    // return PersistCookieJar(dir: tempPath);
    return PersistCookieJar(storage: FileStorage(tempPath));
  }

  delCookiee() async {
    Directory tempDir = await getApplicationDocumentsDirectory();
    String tempPath = tempDir.path;
    PersistCookieJar(storage: FileStorage(tempPath)).deleteAll();
  }

  delCookie() async {
    try {
      /*final cj = await getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cj.saveFromResponse(Uri.parse(Server.BASE_URL), listCookies);
*/
    } catch (e) {
      print(e.toString());
    }
  }
}
