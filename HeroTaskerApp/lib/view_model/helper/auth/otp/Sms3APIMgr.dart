import 'dart:io';

import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/data/model/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/MobileUserOTPModel.dart';
import 'package:aitl/data/model/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class Sms3APIMgr with Mixin {
  static final Sms3APIMgr _shared = Sms3APIMgr._internal();

  factory Sms3APIMgr() {
    return _shared;
  }

  Sms3APIMgr._internal();

  wsLoginMobileOtpPutAPI({
    BuildContext context,
    String otpCode,
    var userID,
    String mobile,
    Function(MobileUserOtpPutAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<MobileUserOtpPutAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_MOBILE_OTP_PUT_URL,
        reqType: ReqType.Put,
        param: getParam(mobileNumber: mobile, otpCode: otpCode, userId: userID),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  getParam({
    String mobileNumber,
    String otpCode = '',
    var userId = "",
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
      "UserId": userId,
      "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
      "Persist": true,
    };
  }

  wsLoginMobileFBPostAPI({
    BuildContext context,
    MobileUserOTPModel mobileUserOTPModel,
    String otpCode,
    Function(LoginRegOtpFBAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginRegOtpFBAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_REG_OTP_FB_URL,
        param: getParam(
          mobileNumber: mobileUserOTPModel.mobileNumber,
          otpCode: otpCode,
        ),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  getFBParam({
    String mobileNumber,
    String otpCode,
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
      "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
      "Persist": true,
    };
  }
}
