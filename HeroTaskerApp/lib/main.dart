import 'dart:io';

import 'package:aitl/config/cfg/StripeCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/welcome/welcome_screen.dart';
import 'package:aitl/view_model/helper/apns/InitializeAwesomeNotifications.dart';
import 'package:aitl/view_model/helper/apns/NotificationData.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:bloc_rest_api/bloc_rest_api.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:mypkg/controller/PinCert.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart'
    as getTransitions;
import 'package:responsive_framework/responsive_framework.dart';

import 'config/cfg/AppConfig.dart';
import 'config/cfg/AppDefine.dart';
import 'config/server/Server.dart';
import 'config/theme/theme_types/app_themes.dart';
import 'data/app_data/UserDevice.dart';
import 'data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'data/network/NetworkMgr.dart';
import 'view/dashboard/adds-on/eid/doc_scan/camera/cam_page.dart';
import 'view/dashboard/more/noti/noti_page.dart';

var isIPAD = false;
var isTablet = false;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initApp();
  runApp(MyApp());
}

initApp() async {
  await Firebase.initializeApp();
  await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  isIPAD = await AppConfig().isIpad();

  //  stripe start
  StripeCfg();
  Stripe.publishableKey = StripeCfg.PUBLISH_KEY;
  //Stripe.merchantIdentifier = StripeCfg.MERCHANT_ID;
  //Stripe.urlScheme = 'flutterstripe';
  //Stripe.androidPayMode: (Server.isTest) ? 'test' : 'live'
  //await Stripe.instance.applySettings();

  //  stripe end

  //  device settings
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    print("mail::FlutterErrorDetails" + details.toString());
  };

  EasyLoading.instance
    ..displayDuration = const Duration(seconds: 3)
    ..loadingStyle = EasyLoadingStyle.custom
    ..textColor = Colors.white
    ..textStyle = TextStyle(
        fontFamily: "Fieldwork",
        fontSize: MyTheme.fontSize,
        color: Colors.white)
    ..backgroundColor = MyTheme.greenNewColor
    ..indicatorColor = Colors.white
    ..maskColor = Colors.black
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..maskType = EasyLoadingMaskType.clear
    ..userInteractions = false;

  //notification int
  await InitializeAwesomeNotifications.initAwesome();

  HttpOverrides.global = MyHttpOverrides();

  //  get device info
  await userDevice.setUserDevice();
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: MyTheme.brandColor));

    AwesomeNotifications()
        .actionStream
        .listen((ReceivedNotification receivedNotification) async {
      try {
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          if (receivedNotification.id == 0) {
            //  TEST PUSH NOTIFICATION

          } else {
            //  EVENTNAME PUSH NOTIFICATION BY NOTIFICATION EVENT LISTNER
            if (receivedNotification.payload != null) {
              final payload = receivedNotification.payload;
              final notiModel = NotiModel.fromJson({
                'UserId': int.parse(payload['UserId']),
                'EntityId': int.parse(payload['EntityId']),
                'EntityName': payload['EntityName'],
                'InitiatorId': int.parse(payload['InitiatorId']),
                'InitiatorDisplayName': payload['InitiatorName'],
                'NotificationEventId':
                    int.parse(payload['NotificationEventId']),
                'Description': payload['Description'],
                'Message': payload['Message'],
                'Id': int.parse(payload['Id']),
                'WebUrl': payload['WebUrl'],
                'TaskId': receivedNotification.id,
              });
              Get.to(() => NotiPage(notiModel: notiModel));
            }
          }
        }
      } catch (e) {
        print(e.toString());
      }
    });

    //Future.delayed(Duration(seconds: 5), () {});
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    FirebaseMessaging.instance.getToken().then((value) async => {
          print("apns Token = ${value.toString()}"),
          await PrefMgr.shared.setPrefStr("fcmTokenKey", value.toString())
        });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      try {
        if (message.data != null) {
          if (message.data["EntityName"] == "TestPushNotitification") {
            return AwesomeNotifications().createNotification(
                content: NotificationContent(
                    id: 0,
                    channelKey: 'basic_channel',
                    title: message.data["Message"] ?? '',
                    body: message.data["Description"] ?? ''));
          } else {
            notiCreator(message);
          }
        }
      } catch (e) {}
      // debugPrint("Notification generation getting error ${message.data}");
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        // for single model
        BlocProvider(
          create: (context) => RequestCubit<UserRatingAPIModel>(
              fromMap: (json) => UserRatingAPIModel.fromJson(json)),
        ),
        BlocProvider(
          create: (context) => RequestCubit<NotiAPIModel>(
              fromMap: (json) => NotiAPIModel.fromJson(json)),
        ),
        /*BlocProvider(
          create: (context) => HydratedRequestCubit<APIModel>(
            fromMap: (json) => APIModel.fromJson(json),
            toMap: (model) => model.toJson(),
          ),
        ),
        // for list of posts simply update type and fromMap method
        BlocProvider(
          create: (context) => RequestCubit<List<APIModel>>(
            fromMap: (json) =>
                List<APIModel>.from(json.map((x) => APIModel.fromJson(x))),
          ),
        ),*/
      ],
      child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          smartManagement: SmartManagement.full,
          enableLog: Server.isTest,
          defaultTransition: getTransitions.Transition.fade,
          title: AppDefine.APP_NAME,
          theme: MyTheme.themeData,
          //darkTheme: AppThemes.dark,
          //theme: AppThemes.dark,
          //themeMode: ThemeMode.system,
          builder: (context, widget) {
            // do your initialization here
            widget = EasyLoading.init()(context, widget);
            widget = ResponsiveWrapper.builder(
              BouncingScrollWrapper.builder(context, widget),
              maxWidth: 1200,
              minWidth: 480,
              defaultScale: true,
              breakpoints: [
                ResponsiveBreakpoint.resize(600, name: MOBILE),
                ResponsiveBreakpoint.autoScale(800, name: TABLET),
                ResponsiveBreakpoint.autoScale(1200, name: DESKTOP)
              ],
            );
            return widget;
          },
          home: WelcomeScreen()),
    );
    //home: Test());
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  notiCreator(message);
}

notiCreator(message) async {
  try {
    debugPrint("notification Message data get ${message.data}");

    NotificationData notiModel = new NotificationData.fromJson(message.data);
    if (notiModel.initiatorName != null) {
      if (userData.userModel == null) {
        userData.userModel = await DBMgr.shared.getUserProfile();
      }
      InitializeAwesomeNotifications.generateNotification(
          notiModelData: notiModel);
    } else {
      print(
          "APNS ************************** notiModel.initiatorName NOT FOUND");
    }
  } catch (e) {
    debugPrint("Notification generation getting error $e");
  }
}
