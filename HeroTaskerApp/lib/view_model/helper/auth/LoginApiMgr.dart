import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class LoginAPIMgr with Mixin {
  static final LoginAPIMgr _shared = LoginAPIMgr._internal();

  factory LoginAPIMgr() {
    return _shared;
  }

  LoginAPIMgr._internal();

  wsLoginAPI({
    BuildContext context,
    String email,
    String pwd,
    Function(LoginAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_URL,
        param: getParam(email: email, pwd: pwd),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  getParam({
    String email,
    String pwd,
    bool persist = false,
    bool checkSignUpMobileNumber = false,
    String countryCode = "880",
    String status = "101",
    String oTPCode = "",
    String birthDay = "",
    String birthMonth = "",
    String birthYear = "",
    String userCompanyId = "0",
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": "880",
      "Status": "101",
      "OTPCode": "",
      "BirthDay": "",
      "BirthMonth": "",
      "BirthYear": "",
      "UserCompanyId": "0"
    };
  }
}
