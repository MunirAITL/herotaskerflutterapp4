import 'package:aitl/config/server/Server.dart';

class DocScanCfg {
  static const identificationStatus = {
    101: "waiting for an agent's review",
    102: "",
    103: "",
    104: "",
    105: ""
  };

  static String getIdenficationStatus(int statusId) {
    try {
      return identificationStatus[statusId];
    } catch (e) {
      return statusId.toString();
    }
  }
}
