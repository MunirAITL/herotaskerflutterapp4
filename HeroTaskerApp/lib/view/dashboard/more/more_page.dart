import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/app_data/AppData.dart';
import '../../../main.dart';
import '../../widgets/images/MyNetworkImage.dart';
import 'more_base.dart';
import 'package:package_info_plus/package_info_plus.dart';

enum eMoreEvent {
  app_tut1,
  signout,
}

class MorePage extends StatefulWidget {
  @override
  State createState() => _MorePageState();
}

class _MorePageState extends BaseMoreStatefull<MorePage> with StateListener {
  PackageInfo packageInfo;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_USERPROFILE) {
        if (mounted) {
          setState(() {});
        }
      } else if (state == ObserverState.STATE_BADGE_NOTIFICATION_COUNT &&
          data == 4) {
        if (mounted) {
          Future.delayed(Duration(seconds: 1), () {
            if (mounted) {
              StateProvider().notify(ObserverState.STATE_BOTNAV, null);
              setState(() {});
            }
          });
        }
      } else if (state == ObserverState.STATE_RELOAD_TAB_MORE &&
          data == this.runtimeType) {
        if (mounted) {
          final taskCtrl = Get.put(TaskCtrl());
          taskCtrl.setTaskModel(null);
          StateProvider().notify(ObserverState.STATE_BOTNAV, null);
          setState(() {});
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  appInit() async {
    try {
      try {
        _stateProvider = new StateProvider();
        _stateProvider.subscribe(this);
      } catch (e) {}
      packageInfo = await PackageInfo.fromPlatform();
    } catch (e) {}
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        /*appBar: AppBar(
          backgroundColor: MyTheme.bgColor, //MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: 0,
          automaticallyImplyLeading: false,
          //title: UIHelper().drawAppbarTitle(title: 'More'),
          centerTitle: false,
          /*bottom: PreferredSize(
            preferredSize: new Size(getW(context), getHP(context, 9)),
            child: Container(
              child: Image.asset("assets/images/welcome/ht_banner.png"),
            ),
          ),*/
        ),*/
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 20),
            Container(
              width: getWP(context, 18),
              height: getWP(context, 18),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: MyNetworkImage.loadProfileImage(
                      userData.userModel.profileImageUrl),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(height: 5),
            Txt(
                txt: userData.userModel.name,
                txtColor: MyTheme.moreTxtColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 20),
            ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: listMore.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> mapMore = listMore[index];

                return GestureDetector(
                  onTap: () async {
                    try {
                      final taskCtrl = Get.put(TaskCtrl());
                      taskCtrl.setTaskModel(null);
                      taskCtrl.dispose();
                    } catch (e) {}
                    if (mapMore['route'] != null) {
                      Get.to(mapMore['route']).then((value) {
                        if (value != null) {
                          if (value['moreEvent'] == eMoreEvent.app_tut1) {
                            StateProvider().notify(
                                ObserverState.STATE_OPEN_HELP_DIALOG, null);
                          }
                        } else {
                          if (mapMore['title'] == 'Notifications') {
                            appData.taskNotificationCountAndChatUnreadCountData
                                .numberOfUnReadNotification = 0;
                            StateProvider().notify(
                                ObserverState.STATE_BADGE_NOTIFICATION_COUNT,
                                4);
                          }
                        }
                      });
                    } else {
                      confirmDialog(
                          context: context,
                          title: "Log out",
                          msg: "Are you sure you want to log out?",
                          callbackYes: () {
                            StateProvider()
                                .notify(ObserverState.STATE_LOGOUT, null);
                          },
                          callbackNo: () {});
                    }
                  },
                  child: Container(
                    color: Colors.transparent,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: 15),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Image.asset(
                                mapMore['icon'],
                                width: 25,
                                height: 25,
                                fit: BoxFit.cover,
                              ),
                              SizedBox(width: 20),
                              Expanded(
                                child: Txt(
                                    txt: mapMore['title'].toString(),
                                    txtColor: MyTheme.gray5Color,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ),
                              mapMore['title'] == 'Notifications'
                                  ? drawNotiBadge()
                                  : SizedBox(),
                              Icon(
                                Icons.arrow_forward_ios,
                                color: MyTheme.gray3Color,
                                size: 20,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 30),
                        drawLine(),
                        SizedBox(height: 15),
                      ],
                    ),
                  ),
                );
              },
            ),
            SizedBox(height: 20),
            packageInfo != null
                ? Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 30),
                    child: Container(
                      width: getW(context),
                      //color: Colors.green,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Txt(
                              txt: "Version",
                              txtColor: Colors.black87,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 7),
                          Txt(
                              txt: packageInfo.version,
                              txtColor: Colors.grey,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ],
                      ),
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
