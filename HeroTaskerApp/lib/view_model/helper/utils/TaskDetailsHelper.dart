import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';

class TaskDetailsHelper {
  //  kindly set the condition by removing extra un necessary condition for taskbidding here...
  isTaskBiddingStatusOK(TaskCtrl taskCtrl, TaskBiddingModel taskBidding) {
    final task = taskCtrl.getTaskModel();
    if (task.status == taskBidding.status ||
        (((task.workerNumber - task.totalAcceptedNumber) > 0) &&
            task.workerNumber > 1 &&
            TaskStatusCfg().getSatusCode(taskBidding.status) ==
                TaskStatusCfg.TASK_STATUS_ACTIVE) ||
        (task.workerNumber > 1 &&
                taskCtrl.getStatus() == TaskStatusCfg.TASK_STATUS_PAYMENTED) &&
            TaskStatusCfg().getSatusCode(taskBidding.status) ==
                TaskStatusCfg.TASK_STATUS_ACCEPTED) {
      return true;
    } else
      return false;
  }
}
