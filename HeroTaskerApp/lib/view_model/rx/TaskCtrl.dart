import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:get/get.dart';

class TaskCtrl extends GetxController {
  var _taskModel = TaskModel().obs;
  var isShowPrivateMessage = false.obs;
  //var taskImages = List().obs;
  setTaskModel(TaskModel model) {
    _taskModel = model.obs;
  }

  bool isTaskModel() {
    return _taskModel.value != null ? true : false;
  }

  TaskModel getTaskModel() {
    return _taskModel.value ?? TaskModel();
  }

  getStatus() {
    return TaskStatusCfg().getSatus(getTaskModel().status);
  }

  getStatusCode() {
    print(getTaskModel().status);
    return TaskStatusCfg().getSatusCode(getTaskModel().status);
  }

  // getIsPoster() {
  //  if (taskCtrl.getTaskModel().userId == userData.userModel.id) {
  //     isPoster = true;
  //   }
  // }

  isExists() => _taskModel == null ? false : true;
}
