class CreateorUpdatePaymentAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  CreateorUpdatePaymentAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  CreateorUpdatePaymentAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  TaskPaymentInfo taskPaymentInfo;
  ResponseData({this.taskPaymentInfo});
  ResponseData.fromJson(Map<String, dynamic> json) {
    taskPaymentInfo = json['taskPaymentInfo'] != null
        ? new TaskPaymentInfo.fromJson(json['taskPaymentInfo'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskPaymentInfo != null) {
      data['taskPaymentInfo'] = this.taskPaymentInfo.toJson();
    }
    return data;
  }
}

class TaskPaymentInfo {
  int userId;
  String status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int taskId;
  String paymentType;
  String paymentMethod;
  int employeeID;
  int employeePaymentGateWayId;
  double payableAmount;
  double dueAmount;
  double paymentAmount;
  int taskBiddingId;
  String description;
  String acAccountNo;
  String accountStatus;
  String transactionType;
  String taskTitle;
  String taskTitleUrl;
  String profileImageUrl;
  String profileOwnerName;
  String ownerProfileUrl;
  double shohokariAmount;
  double serviceFeeAmount;
  double discountAmount;
  double netTotalAmount;
  int userPromotionId;
  int collectionPaymentType;
  String paymentIntentId;
  String clientSecret;
  int id;

  TaskPaymentInfo(
      {this.userId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.taskId,
      this.paymentType,
      this.paymentMethod,
      this.employeeID,
      this.employeePaymentGateWayId,
      this.payableAmount,
      this.dueAmount,
      this.paymentAmount,
      this.taskBiddingId,
      this.description,
      this.acAccountNo,
      this.accountStatus,
      this.transactionType,
      this.taskTitle,
      this.taskTitleUrl,
      this.profileImageUrl,
      this.profileOwnerName,
      this.ownerProfileUrl,
      this.shohokariAmount,
      this.serviceFeeAmount,
      this.discountAmount,
      this.netTotalAmount,
      this.userPromotionId,
      this.collectionPaymentType,
      this.paymentIntentId,
      this.clientSecret,
      this.id});

  TaskPaymentInfo.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    taskId = json['TaskId'];
    paymentType = json['PaymentType'];
    paymentMethod = json['PaymentMethod'];
    employeeID = json['EmployeeID'];
    employeePaymentGateWayId = json['EmployeePaymentGateWayId'];
    payableAmount = json['PayableAmount'];
    dueAmount = json['DueAmount'];
    paymentAmount = json['PaymentAmount'];
    taskBiddingId = json['TaskBiddingId'];
    description = json['Description'];
    acAccountNo = json['AcAccountNo'];
    accountStatus = json['AccountStatus'];
    transactionType = json['TransactionType'];
    taskTitle = json['TaskTitle'];
    taskTitleUrl = json['TaskTitleUrl'];
    profileImageUrl = json['ProfileImageUrl'];
    profileOwnerName = json['ProfileOwnerName'];
    ownerProfileUrl = json['OwnerProfileUrl'];
    shohokariAmount = json['ShohokariAmount'];
    serviceFeeAmount = json['ServiceFeeAmount'];
    discountAmount = json['DiscountAmount'];
    netTotalAmount = json['NetTotalAmount'];
    userPromotionId = json['UserPromotionId'];
    collectionPaymentType = json['CollectionPaymentType'];
    paymentIntentId = json['PaymentIntentId'];
    clientSecret = json['ClientSecret'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['TaskId'] = this.taskId;
    data['PaymentType'] = this.paymentType;
    data['PaymentMethod'] = this.paymentMethod;
    data['EmployeeID'] = this.employeeID;
    data['EmployeePaymentGateWayId'] = this.employeePaymentGateWayId;
    data['PayableAmount'] = this.payableAmount;
    data['DueAmount'] = this.dueAmount;
    data['PaymentAmount'] = this.paymentAmount;
    data['TaskBiddingId'] = this.taskBiddingId;
    data['Description'] = this.description;
    data['AcAccountNo'] = this.acAccountNo;
    data['AccountStatus'] = this.accountStatus;
    data['TransactionType'] = this.transactionType;
    data['TaskTitle'] = this.taskTitle;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['ShohokariAmount'] = this.shohokariAmount;
    data['ServiceFeeAmount'] = this.serviceFeeAmount;
    data['DiscountAmount'] = this.discountAmount;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['UserPromotionId'] = this.userPromotionId;
    data['CollectionPaymentType'] = this.collectionPaymentType;
    data['PaymentIntentId'] = this.paymentIntentId;
    data['ClientSecret'] = this.clientSecret;
    data['Id'] = this.id;
    return data;
  }
}
