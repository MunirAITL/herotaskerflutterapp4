import 'dart:convert';
import 'dart:developer';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/cfg/StripeCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:path/path.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:fluttertoast/fluttertoast.dart';

//  https://dashboard.stripe.com/test/dashboard
//  Stripe Password = Hndxfg123456
import 'package:http/http.dart' as http;

import '../../../data/model/dashboard/stripe/CreateorUpdatePaymentAPIModel.dart';
//import 'package:flutter_credit_card/fluttefr_credit_card.dart';

class StripeMgr with Mixin {
  /*Future<Map<String, dynamic>> fetchPaymentIntentClientSecret() async {
    final url = Uri.parse('https://api.stripe.com/v1/create-payment-intent');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({
        'email': "a@a.com",
        'currency': 'usd',
        'items': [
          {'id': 'id'}
        ],
        'request_three_d_secure': 'any',
      }),
    );
    return json.decode(response.body);
  }*/

  Future<PaymentIntent> makePayment({
    BuildContext context,
    TaskPaymentInfo taskPaymentInfo,
  }) async {
    //final _saveCard = true;
    // 1. fetch Intent Client Secret from backend
    //final clientSecret = await fetchPaymentIntentClientSecret();

    try {
      //final paymentIntent = await createPaymentIntent(amounts: amount);

      //final clientSecret = await fetchPaymentIntentClientSecret();

      // create some billingdetails
      final billingDetails = BillingDetails(
        email: userData.userModel.email,
        phone: userData.userModel.mobileNumber,
        address: Address(
          city: "",
          country: AppDefine.COUNTRY_FLAG.toUpperCase(),
          line1: userData.userModel.address,
          line2: '',
          state: '',
          postalCode: '',
        ),
      ); // mocked data for tests

      final paymentData = PaymentMethodParams.card(
          paymentMethodData: PaymentMethodData(billingDetails: billingDetails)
          //billingDetails: billingDetails,
          //setupFutureUsage:
          // _saveCard == true ? PaymentIntentsFutureUsage.OffSession : null,
          );

      final paymentBody = {
        'amount': (taskPaymentInfo.paymentAmount * 100).round().toString(),
        'currency': AppDefine.CUR_ABBR,
        'payment_method_types[]': 'card'
      };

      //final paymentMethod =
      //await Stripe.instance.createPaymentMethod(paymentData, paymentBody);

      final payment = await Stripe.instance.confirmPayment(
          taskPaymentInfo.clientSecret, paymentData, paymentBody);

      myLog(payment);
      return payment;

      //final paymentIntentSecret = await getPaymentIntentSecret(taskPaymentInfo);

      //final confirmPayment = await Stripe.instance
      //.confirmPayment(paymentIntentSecret['client_secret'], paymentData);

      /*final confirmPayment = await Stripe.instance.confirmPayment(
        paymentIntent['client_secret'],
        PaymentMethodParams.card(
          billingDetails: billingDetails,
          setupFutureUsage:
              _saveCard == true ? PaymentIntentsFutureUsage.OffSession : null,
        ),
      );*/

    } catch (e) {
      print(e.toString());
      //showSnake("Error!", e.toString());
      return null;
      //rethrow;
    }
    /*try {
      final user = userData.userModel;
      BillingAddress billingAddress = BillingAddress(
          line1: user.address,
          line2: "",
          city: "",
          state: "",
          postalCode: "",
          name: user.name);
      PrefilledInformation prefilledInformation =
          PrefilledInformation(billingAddress: billingAddress);

      StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest(
              prefilledInformation: prefilledInformation))
          .then((paymentMethod) async {
        if (paymentMethod != null) {
          EasyLoading.show(status: "Payment Processing...");
          final paymentIntentRes = await createPaymentIntent(amounts: amount);
          StripePayment.confirmPaymentIntent(
            PaymentIntent(
              clientSecret: paymentIntentRes['client_secret'],
              paymentMethodId: paymentMethod.id,
            ),
          ).then((paymentIntent) {
            print(paymentIntent);
            StripePayment.authenticatePaymentIntent(
                    clientSecret: paymentIntentRes['client_secret'])
                .then((authPaymentIntent) {
              print(authPaymentIntent);
              EasyLoading.dismiss();
              callback(paymentMethod, paymentIntent, authPaymentIntent);
            }).catchError((e) {
              EasyLoading.dismiss();
              showToast(
                context: context,
                msg: 'Card declined. Please try again.',
              );
              //showAlert(context: context,msg: "authenticatePaymentIntent\n\n" + e.toString());
            });
          }).catchError((e) {
            EasyLoading.dismiss();
            showToast(
              context: context,
              msg: 'Card declined. Please try again.',
            );
            //showAlert(context: context,msg: "confirmPaymentIntent\n\n" + e.toString());
          });
        }
      }).catchError((e) {
        print(e.toString());
        EasyLoading.dismiss();
        //showAlert(context: context,msg: "paymentRequestWithCardForm\n\n" + e.toString());
        showToast(
          context: context,
          msg: 'Your card payment has been cancelled.',
        );
      });
      /*createCharges(
            tokenId: paymentMethod.id,
            amounts: amount,
            description: description);*/
      //}).catchError(setError);

      /*StripePayment.createTokenWithCard(
        CreditCard(
          number: "",
          expMonth: 02,
          expYear: 22,
          //cvc: details["cvv"],
        ),
      ).then((token) {
        createCharge(
            tokenId: token.tokenId, amounts: amount, description: description);
      }).catchError(setError);*/
    } catch (e) {
      EasyLoading.dismiss();
    }*/
  }

  /*Future<Map<String, dynamic>> createPaymentIntent({double amounts = 0}) async {
    try {
      Map<String, dynamic> body = {
        'amount': amounts.round().toString(),
        'currency': AppDefine.CUR_ABBR,
        'payment_method_types[]': 'card'
      };
      var respose = await http.post(
          Uri.parse('https://api.stripe.com/v1/payment_intents'),
          body: body,
          headers: {
            'Authorization': 'Bearer ${StripeCfg.SECRET_KEY}',
            'Content-Type': 'application/x-www-form-urlencoded'
          });
      return jsonDecode(respose.body);
    } catch (e) {
      showSnake("Error!", e.toString());
      //showAlert(context: context,
      //msg: "https://api.stripe.com/v1/payment_intents\n\n" + e.toString());
    }
    return null;
  }

  Future<Map<String, dynamic>> createCharges(
      {String tokenId, double amounts = 0, String description = ''}) async {
    try {
      EasyLoading.show(status: "Payment Processing...");
      if (tokenId != null && amounts > 0) {
        Map<String, dynamic> body = {
          'amount': amounts.round().toString(),
          'currency': AppDefine.CUR_ABBR,
          'source': tokenId,
          'description': description
        };
        var response = await http.post(
            Uri.parse('https://api.stripe.com/v1/charges'),
            body: body,
            headers: {
              'Authorization': 'Bearer ${StripeCfg.SECRET_KEY}',
              'Content-Type': 'application/x-www-form-urlencoded'
            });
        EasyLoading.dismiss();
        return jsonDecode(response.body);
      }
    } catch (e) {
      showSnake("Error!", e.toString());
      //showAlert(context: context,msg: "https://api.stripe.com/v1/charges\n\n" + e.toString());
    }
    EasyLoading.dismiss();
    return null;
  }*/

  /*Future<Map<String, dynamic>> getPaymentIntentSecret(
      TaskPaymentInfo taskPaymentInfo) async {
    try {
      final amounts = 0.3;
      //taskPaymentInfo.paymentAmount;
      final url = 'https://api.stripe.com/v1/payment_intents';
      //final body = "{'amount':"+amounts.toStringAsFixed(2) +",'currency':'"+AppDefine.CUR_ABBR+"','payment_method_types[]': 'card'}";
      final body = {
        'amount': (amounts * 100).round().toString(),
        'currency': AppDefine.CUR_ABBR,
        'payment_method_types[]': 'card'
      };
      print(body);
      var respose = await http.post(Uri.parse(url), body: body, headers: {
        'Authorization': 'Bearer ${taskPaymentInfo.paymentIntentId}',
        'Content-Type': 'application/x-www-form-urlencoded'
      });
      return jsonDecode(respose.body);
    } catch (e) {
      showSnake("Error!", e.toString());
      //showAlert(context: context,
      //msg: "https://api.stripe.com/v1/payment_intents\n\n" + e.toString());
    }
    return null;
  }*/

  /*Future<Map<String, dynamic>> createPaymentIntent({double amounts = 0}) async {
    try {
      Map<String, dynamic> body = {
        'amount': amounts.round().toString(),
        'currency': AppDefine.CUR_ABBR,
        'payment_method_types[]': 'card'
      };
      var respose = await http.post(
          Uri.parse('https://api.stripe.com/v1/payment_intents'),
          body: body,
          headers: {
            'Authorization': 'Bearer ${StripeCfg.SECRET_KEY}',
            'Content-Type': 'application/x-www-form-urlencoded'
          });
      return jsonDecode(respose.body);
    } catch (e) {
      showSnake("Error!", e.toString());
      //showAlert(context: context,
      //msg: "https://api.stripe.com/v1/payment_intents\n\n" + e.toString());
    }
    return null;
  }

  Future<Map<String, dynamic>> fetchPaymentIntentClientSecret() async {
    final url = Uri.parse('$kApiUrl/payment_intents');
    final response = await http.post(
      url,
      headers: {
        'Authorization': 'Bearer ${StripeCfg.SECRET_KEY}',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: json.encode({
        'email': userData.userModel.email,
        'amount': "10",
        'currency': AppDefine.CUR_ABBR,
        'payment_method_types[]': 'card'
      }),
    );
    return json.decode(response.body);
  }*/

  //  **************************  Stripe API

  /*Future<Map<String, dynamic>> callNoWebhookPayEndpointMethodId({
    bool useStripeSdk,
    String paymentMethodId,
    String currency,
    List<Map<String, dynamic>> items,
  }) async {
    final url = Uri.parse('https://api.stripe.com/v1/pay-without-webhooks');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({
        'useStripeSdk': useStripeSdk,
        'paymentMethodId': paymentMethodId,
        'currency': currency,
        'items': items
      }),
    );
    return json.decode(response.body);
  }*/
}
