import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter/material.dart';

import '../../../widgets/txt/Txt.dart';
import 'package:get/get.dart';

abstract class WorkerBase<T extends StatefulWidget> extends State<T>
    with Mixin {
  var list1 = [
    "Flexible schedule",
    "High pay",
    "Benefits",
    "Working towards my career goals",
  ];
  var list1Index = [false, false, false, false, false].obs;

  var list2 = [
    "I need to pay the bills",
    "I want some extra spending money",
    "I'm trying to build my savings",
    "I'm saving for retirement",
    "I'm saving for something specific, like a house or my kid's college fund.",
    "I want to pay down debt",
  ];
  var list2Index = [false, false, false, false, false, false].obs;

  var list3 = [
    "From Home",
    "In a retail store or restaurant",
    "In an Office or Business Establishment",
    "From My Car",
    "In Someone Else's Home",
    "Outdoors",
  ];
  var list3Index = [false, false, false, false, false, false].obs;

  var list4 = [
    "High school diploma",
    "Some college",
    "2-year degree or trade school",
    "4-year degree",
    "Post-graduate degree",
  ];
  var list4Index = 0.obs;

  var list5 = [
    "I work full time",
    "I work part time",
    "I do freelance or contract work",
    "I've got a side hustle",
    "I do one-time gigs (like help people move)",
  ];
  var list5Index = [false, false, false, false, false].obs;

  var list6 = [
    "Bank Account",
    "Prepaid Debit Card",
    "Paypal Account",
    "Cash App",
  ];
  var list6Index = [false, false, false, false].obs;

  var list7 = [
    "From my phone",
    "On my computer",
    "On paper",
    "Through conversation with others",
    "It's all in my head",
    "I live life on the fly. I don't have a plan"
  ];
  var list7Index = [false, false, false, false, false, false].obs;

  drawStep1() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt:
                  "What's most important in the opportunities you're looking for?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list1.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Checkbox(
                          //checkColor: Colors.white, // color of tick Mark
                          //activeColor: MyTheme.redColor,
                          value: list1Index[i],
                          onChanged: (v) => list1Index[i] = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list1[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));

  drawStep2() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt:
                  "What do you wnat to do with extra cash you earn through Herotasker?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list2.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Checkbox(
                          value: list2Index[i],
                          onChanged: (v) => list2Index[i] = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list2[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));

  drawStep3() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt: "Where do you want to work?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list3.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Checkbox(
                          value: list3Index[i],
                          onChanged: (v) => list3Index[i] = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list3[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));

  drawStep4() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt: "What's your highest level of education?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list4.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Radio(
                          value: i,
                          groupValue: list4Index.value,
                          onChanged: (v) => list4Index.value = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list4[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));

  drawStep5() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt: "What does your work situation look like right now?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list5.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Checkbox(
                          value: list5Index[i],
                          onChanged: (v) => list5Index[i] = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list5[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));

  drawStep6() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt: "Which of the following do you use?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list6.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Checkbox(
                          value: list6Index[i],
                          onChanged: (v) => list6Index[i] = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list6[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));

  drawStep7() => Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Txt(
              txt: "How do you keep track of your budget or financial plan?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          SizedBox(height: 10),
          for (int i = 0; i < list7.length; i++)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                        child: Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.red,
                        toggleableActiveColor: Colors.red.shade800,
                      ),
                      child: Checkbox(
                          value: list7Index[i],
                          onChanged: (v) => list7Index[i] = v),
                    )),
                    Expanded(
                        flex: 3,
                        child: Txt(
                          txt: list7[i],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        )),
                  ],
                )
              ],
            )
        ],
      ));
}
