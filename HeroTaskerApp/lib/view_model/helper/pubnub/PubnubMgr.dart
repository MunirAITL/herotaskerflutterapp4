import 'package:aitl/config/cfg/PubnubCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:pubnub/pubnub.dart';

class PubnubMgr {
  PubNub pubnub;
  Channel channel;

  initPubNub({Function(Envelope) callback, int channelId}) async {
    print("init pub nub call");
    pubnub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            secretKey: PubNubCfg.secretKey,
            uuid: UUID(userData.userModel.id.toString())));

    // Subscribe to a channel
    //var subscription = pubnub.subscribe(channels: {'test'});
    print("channel ID =  " + channelId.toString());
    var subscription = pubnub.subscribe(channels: {channelId.toString()});
    subscription.messages.listen((envelope) async {
      print(envelope.payload);
      //await subscription.dispose();
      callback(envelope);
    });

    //await Future.delayed(Duration(seconds: 3));

    // Publish a message
    //await pubnub.publish('test', {'message': 'My message BY flutter app1!'});

    // Channel abstraction for easier usage
    //var channel = pubnub.channel('test');

    //await channel.publish({'message': 'Another message BY flutter app2'});

    // Work with channel History API
    //var history = channel.messages();
    //var count = await history.count();

    //print('Messages on test channel: $count');

    /*
    print('sender id : ${userData.userModel.id}');
    subscription.messages.listen((envelope) {
      print('sent a message full : ${envelope.payload}');
      callback(envelope);
    });*/
  }

  void postMessage({int receiverId, ChatModel chatModel}) async {
    channel = pubnub.channel(receiverId.toString());
    print('sent a message to : ${receiverId.toString()}');
    await channel.publish({
      'data': chatModel.toJson(),
      'user': userData.userModel.id,
    });
  }

  getHistory({int receiverId}) async {
    channel = pubnub.channel(receiverId.toString());
    var history =
        channel.history(chunkSize: 100, order: ChannelHistoryOrder.descending);
    //await history.more();
    print(history.messages.length); // 50
    // await history.more();
    //print(history.messages.length); // 100
  }

  destroy() {
    try {
      pubnub = null;
      channel = null;
    } catch (e) {}
  }
}
