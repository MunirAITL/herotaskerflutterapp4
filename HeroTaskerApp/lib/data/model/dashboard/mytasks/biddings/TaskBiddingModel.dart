import '../../../auth/UserModel.dart';

class TaskBiddingModel {
  double averageRating;
  String coverLetter;
  String creationDate;
  String deliveryDate;
  String deliveryTime;
  String description;
  double discountAmount;
  double fixedbiddigAmount;
  double hourlyBiddingAmount;
  String imageServerUrl;
  bool isInPersonOrOnline;
  bool isReview;
  bool isReviewByShohoKari;
  bool isReviewByPoster;
  bool isTaskOwner;
  double netTotalAmount;
  String ownerImageUrl;
  String ownerName;
  dynamic paymentStatus;
  int ratingCount;
  String referenceId;
  String referenceType;
  String remarks;
  int serviceFeeAmount;
  int shohokariAmount;
  dynamic status;
  double taskCompletionRate;
  int taskId;
  int taskOwnerId;
  int id;
  String thumbnailPath;
  double totalHour;
  double totalHourPerWeek;
  String updatedDate;
  UserModel user;
  int userId;
  int userPromotionId;
  int versionNumber;

  TaskBiddingModel(
      {this.averageRating,
      this.coverLetter,
      this.creationDate,
      this.deliveryDate,
      this.deliveryTime,
      this.description,
      this.discountAmount,
      this.fixedbiddigAmount,
      this.hourlyBiddingAmount,
      this.imageServerUrl,
      this.isInPersonOrOnline,
      this.isReview,
      this.isReviewByShohoKari,
      this.isReviewByPoster,
      this.isTaskOwner,
      this.netTotalAmount,
      this.ownerImageUrl,
      this.ownerName,
      this.paymentStatus,
      this.ratingCount,
      this.referenceId,
      this.referenceType,
      this.remarks,
      this.serviceFeeAmount,
      this.shohokariAmount,
      this.status,
      this.taskCompletionRate,
      this.taskId,
      this.taskOwnerId,
      this.id,
      this.thumbnailPath,
      this.totalHour,
      this.totalHourPerWeek,
      this.updatedDate,
      this.user,
      this.userId,
      this.userPromotionId,
      this.versionNumber});

  TaskBiddingModel.fromJson(Map<String, dynamic> json) {
    averageRating = json['AverageRating'] ?? 0.0;
    coverLetter = json['CoverLetter'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    deliveryDate = json['DeliveryDate'] ?? '';
    deliveryTime = json['DeliveryTime'] ?? '';
    description = json['Description'] ?? '';
    discountAmount = json['DiscountAmount'] ?? 0.0;
    fixedbiddigAmount = json['FixedbiddigAmount'] ?? 0.0;
    hourlyBiddingAmount = json['HourlyBiddingAmount'] ?? 0.0;
    imageServerUrl = json['ImageServerUrl'] ?? '';
    isInPersonOrOnline = json['IsInPersonOrOnline'] ?? false;
    isReview = json['IsReview'] ?? false;
    isReviewByShohoKari = json['IsReviewByShohoKari'] ?? false;
    isReviewByPoster = json['IsReviewByPoster'] ?? false;
    isTaskOwner = json['IsTaskOwner'] ?? false;
    netTotalAmount = json['NetTotalAmount'] ?? 0.0;
    ownerImageUrl = json['OwnerImageUrl'] ?? '';
    ownerName = json['OwnerName'] ?? '';
    paymentStatus = json['PaymentStatus'] ?? 0;
    ratingCount = json['RatingCount'] ?? 0;
    referenceId = json['ReferenceId'] ?? '';
    referenceType = json['ReferenceType'] ?? '';
    remarks = json['Remarks'] ?? '';
    serviceFeeAmount = json['ServiceFeeAmount'] ?? 0;
    shohokariAmount = json['ShohokariAmount'] ?? 0;
    status = json['Status'] ?? 0;
    taskCompletionRate = json['TaskCompletionRate'] ?? 0.0;
    taskId = json['TaskId'] ?? 0;
    taskOwnerId = json['TaskOwnerId'] ?? 0;
    id = json['Id'] ?? 0;
    thumbnailPath = json['ThumbnailPath'] ?? '';
    totalHour = json['TotalHour'] ?? 0.0;
    totalHourPerWeek = json['TotalHourPerWeek'] ?? 0.0;
    updatedDate = json['UpdatedDate'] ?? '';
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
    userId = json['UserId'] ?? 0;
    userPromotionId = json['UserPromotionId'] ?? 0;
    versionNumber = json['VersionNumber'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AverageRating'] = this.averageRating;
    data['CoverLetter'] = this.coverLetter;
    data['CreationDate'] = this.creationDate;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['Description'] = this.description;
    data['DiscountAmount'] = this.discountAmount;
    data['FixedbiddigAmount'] = this.fixedbiddigAmount;
    data['HourlyBiddingAmount'] = this.hourlyBiddingAmount;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['IsReview'] = this.isReview;
    data['IsReviewByShohoKari'] = this.isReviewByShohoKari;
    data['IsReviewByPoster'] = this.isReviewByPoster;
    data['IsTaskOwner'] = this.isTaskOwner;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerName'] = this.ownerName;
    data['PaymentStatus'] = this.paymentStatus;
    data['RatingCount'] = this.ratingCount;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['Remarks'] = this.remarks;
    data['ServiceFeeAmount'] = this.serviceFeeAmount;
    data['ShohokariAmount'] = this.shohokariAmount;
    data['Status'] = this.status;
    data['TaskCompletionRate'] = this.taskCompletionRate;
    data['TaskId'] = this.taskId;
    data['TaskOwnerId'] = this.taskOwnerId;
    data['Id'] = this.id;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['TotalHour'] = this.totalHour;
    data['TotalHourPerWeek'] = this.totalHourPerWeek;
    data['UpdatedDate'] = this.updatedDate;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    data['UserId'] = this.userId;
    data['UserPromotionId'] = this.userPromotionId;
    data['VersionNumber'] = this.versionNumber;
    return data;
  }
}
