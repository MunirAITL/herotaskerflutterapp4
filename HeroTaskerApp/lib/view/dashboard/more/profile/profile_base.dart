import 'dart:io';

import 'package:aitl/config/cfg/AppShareCfg.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgesModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortfulioModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/main.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/profile/edit_profile_page.dart';
import 'package:aitl/view/dashboard/more/resolutions/res_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/ProfileHelper.dart';
import 'package:aitl/view_model/helper/utils/TaskHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:aitl/view_model/rx/ProfileController.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../config/server/APIMyTasksCfg.dart';
import '../../../../data/model/ads-on/action_req/UserBadgeModel.dart';
import '../../../../data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import '../../../../data/model/dashboard/mytasks/biddings/TaskBiddingAPIModel.dart';
import '../../../../data/network/NetworkMgr.dart';
import '../../mytasks/taskdetails_page.dart';
import 'badge_screen.dart';

abstract class BaseProfileStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  var taskCtrl = Get.put(TaskCtrl());
  final profileController = Get.put(ProfileController());

  //  expandable bg

  bool isLoading = false;
  bool isPublicUser = true;
  int posterSwitchValue = 0;
  int profileUserID;

  List<dynamic> listItems = [];
  List<UserBadgeModel> listUserBadgeModel = [];
  List<UserBadgeModel> listUserBadgeModelShowList = [];
  UserBadgeModel userBadgeModel;
  UserModel userModel;

  final menuItems = [
    {'index': 0, 'icon': 'assets/images/ico/star_ico.png', 'title': 'Badges'},
    {
      'index': 1,
      'icon': 'assets/images/ico/setting_ico.png',
      'title': 'Settings'
    },
    {'index': 2, 'icon': 'assets/images/ico/logout_ico.png', 'title': 'Logout'},
  ];

  refreshData();

  drawHeader(UserModel userModel, cls) {
    //if (userModel == null) return Container(color: Colors.white);
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            elevation: 0,
            backgroundColor: MyTheme.bgColor,
            iconTheme: IconThemeData(
                color: (innerBoxIsScrolled)
                    ? Colors.black
                    : Colors.black //change your color here
                ),
            title: UIHelper().drawAppbarTitle(title: 'Profile'),
            centerTitle: false,
            actions: [
              !isPublicUser
                  ? IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        Get.to(() => EditProfilePage()).then((value) {
                          userModel = userModel;
                          refreshData();
                        });
                      })
                  : SizedBox(),
              SizedBox(width: 30),
              IconButton(
                icon: Icon(Icons.share),
                onPressed: () async {
                  final msg = AppShareCfg.SERVER_SHARE_PUBLIC_URL +
                      userModel.userProfileUrl;
                  final FlutterShareMe flutterShareMe = FlutterShareMe();
                  await flutterShareMe.shareToSystem(msg: msg);
                },
              ),
              SizedBox(width: 10),
            ],

            expandedHeight: getHP(context, isPublicUser ? 50 : 40),
            floating: false,
            pinned: true,
            snap: false,
            forceElevated: true,
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.parallax,
              centerTitle: true,
              background: drawUserProfileData(userModel, cls),
            ),
            //background:
            bottom: PreferredSize(
                preferredSize: Size.fromHeight(1),
                child: (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : Container(
                        height: 1,
                        color: Colors.transparent,
                      )),
          ),
        ];
      },
      body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout()),
    );
  }

  drawUserProfileData(UserModel userModel, cls) {
    if (userModel == null) return SizedBox();
    final address = Common.removeTag(userModel.address);
    return Container(
      //height: getHP(context, !isPublicUser ? h1 : h2),
      decoration: MyNetworkImage.isValidUrl(userModel.coverImageUrl)
          ? BoxDecoration(
              color: Colors.black,
              image: DecorationImage(
                image: MyNetworkImage.loadProfileImage(userModel.coverImageUrl),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.7), BlendMode.dstATop),
                fit: BoxFit.cover,
              ),
            )
          : BoxDecoration(color: MyTheme.grayColor.withOpacity(.9)),
      child: ListView(
        primary: false,
        children: [
          Container(
            width: double.infinity,
            child: Column(
              //shrinkWrap: true,
              //primary: false,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                !isPublicUser
                    ? Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                          onTap: () {
                            CamPicker().showCamDialog(
                              context: context,
                              isRear: true,
                              callback: (File path) {
                                if (path != null) {
                                  APIViewModel().upload(
                                    context: context,
                                    apiState: APIState(
                                        APIType.media_profile_cover_image,
                                        cls,
                                        null),
                                    file: path,
                                  );
                                }
                              },
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(top: 50, right: 20),
                            child: Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: MyTheme.gray1Color),
                                child: Icon(
                                  Icons.camera_alt_outlined,
                                  size: 20,
                                  color: Colors.pink,
                                )),
                          ),
                        ),
                      )
                    : SizedBox(),
                SizedBox(height: !isPublicUser ? 30 : 50),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Stack(
                      //alignment: Alignment.center,
                      children: [
                        Container(
                          width: getWP(context, 20),
                          height: getWP(context, 20),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: MyNetworkImage.loadProfileImage(
                                  userModel.profileImageUrl),
                              fit: BoxFit.cover,
                            ),
                            shape: BoxShape.circle,
                          ),
                        ),
                        !isPublicUser
                            ? Positioned(
                                right: 0,
                                //left: 20,
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  child: MaterialButton(
                                    onPressed: () {
                                      CamPicker().showCamDialog(
                                        context: context,
                                        isRear: false,
                                        callback: (File path) {
                                          if (path != null) {
                                            APIViewModel().upload(
                                              context: context,
                                              apiState: APIState(
                                                  APIType.media_profile_image,
                                                  cls,
                                                  null),
                                              file: path,
                                            );
                                          }
                                        },
                                      );
                                    },
                                    color: MyTheme.gray1Color,
                                    child: Icon(
                                      Icons.camera_alt_outlined,
                                      size: 20,
                                      color: Colors.pink,
                                    ),
                                    padding: EdgeInsets.all(0),
                                    shape: CircleBorder(),
                                  ),
                                ),
                              )
                            : SizedBox(),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 20),
                      child: Txt(
                          txt: userModel.name,
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                    /*Padding(
                      padding: const EdgeInsets.only(top: 7),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          drawCircle(
                            context: context,
                            color: !isPublicUser
                                ? MyTheme.onlineColor
                                : (userModel.isOnline)
                                    ? MyTheme.onlineColor
                                    : MyTheme.offlineColor,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Txt(
                                txt: !isPublicUser
                                    ? 'Online'
                                    : (userModel.isOnline)
                                        ? 'Online'
                                        : 'Last online ' +
                                            DateFun.getTimeAgoTxt(
                                                userModel.dateCreatedLocal),
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 7),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Image.asset(
                                "assets/images/icons/map_pin_icon.png",
                                width: 20,
                                height: 20,
                                color: Colors.white,
                              ),
                            ),
                            (address.length > 0)
                                ? Flexible(
                                    //flex: 3,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: Txt(
                                          txt: address ?? '',
                                          txtColor: Colors.white,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          maxLines: 2,
                                          isBold: false),
                                    ),
                                  )
                                : SizedBox(),
                          ]),
                    ),*/

                    userModel.dateCreatedLocal != ''
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Txt(
                                txt: "Joined in " +
                                    DateFun.getDate(
                                        userModel.dateCreatedLocal, "MMM yyyy"),
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          )
                        : SizedBox(),
                    userModel.isElectronicIDVerified
                        ? Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                    radius: 8,
                                    backgroundColor: Colors.green,
                                    child: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 12,
                                    )),
                                SizedBox(width: 5),
                                Txt(
                                  maxLines: 1,
                                  txt: "E-ID VERIFIED",
                                  txtColor: Colors.green,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: false,
                                ),
                              ],
                            ),
                          )
                        : SizedBox(),
                    isPublicUser
                        ? Column(
                            children: [
                              SizedBox(height: 20),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 60, right: 60),
                                child: MMBtn(
                                  txt: "Request a quote",
                                  txtColor: Colors.white,
                                  bgColor: MyTheme.redColor,
                                  height: getHP(context, MyTheme.btnHpa - 1),
                                  width: getW(context),
                                  radius: 10,
                                  callback: () {
                                    Get.to(
                                      () => AddTask1Screen(
                                        index: null,
                                        userModel: userModel,
                                      ),
                                    ).then((pageNo) {
                                      obsUpdateTabs(pageNo);
                                    });
                                  },
                                ),
                              ),
                              SizedBox(height: 20),
                              GestureDetector(
                                onTap: () {
                                  Get.to(() => ResPage(
                                        from: ResCfg.RESOULUTION_TYPE_USER,
                                        userId: userModel.id,
                                      ));
                                },
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.flag_outlined,
                                        color: Colors.white,
                                      ),
                                      SizedBox(width: 10),
                                      Flexible(
                                        child: Txt(
                                            txt: "Report this member",
                                            txtColor: Colors.white,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.center,
                                            isBold: false),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        : SizedBox(),
                  ],
                ),

                //ProfileHelper().getStarRatingView(rate: 4, reviews: 5),
                //SizedBox(height: 5),
                //ProfileHelper().getCompletionText(pa: 50),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //  lISTVIEW  start here...

  drawBasicInfo() {
    return userModel != null
        ? Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Txt(
                    txt: "Basic Information :",
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                  userModel != null
                      ? userModel.id != profileUserID
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                userModel.email != null &&
                                        userModel.email.isNotEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Row(
                                          children: [
                                            Icon(Icons.email_outlined,
                                                size: 20, color: Colors.grey),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Txt(
                                                txt: "${userModel.email}",
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false,
                                                maxLines: 5,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                userModel.mobileNumber != null &&
                                        userModel.mobileNumber.isNotEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 10),
                                        child: Row(
                                          children: [
                                            Icon(Icons.phone_android_outlined,
                                                size: 20, color: Colors.grey),
                                            SizedBox(width: 10),
                                            Expanded(
                                              child: Txt(
                                                txt:
                                                    "${userModel.mobileNumber}",
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false,
                                                maxLines: 5,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                              ],
                            )
                          : SizedBox()
                      : SizedBox(),
                  userModel.address != null && userModel.address.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/icons/home_icon.png",
                                width: 20,
                                color: Colors.grey,
                              ),
                              SizedBox(width: 10),
                              Expanded(
                                child: Txt(
                                  txt: "${userModel.address}",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: false,
                                  maxLines: 5,
                                ),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  userModel.countryofResidency != null &&
                          userModel.countryofResidency.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/icons/glob_ic.png",
                                width: 20,
                                color: Colors.grey,
                              ),
                              SizedBox(width: 10),
                              Txt(
                                txt: "${userModel.countryofResidency}",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  userModel.dateofBirth != null &&
                          userModel.dateofBirth.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/icons/date_icon.png",
                                width: 20,
                                color: Colors.grey,
                              ),
                              SizedBox(width: 10),
                              Txt(
                                txt: "${userModel.dateofBirth}",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                  userModel.maritalStatus != null &&
                          userModel.maritalStatus.isNotEmpty
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            children: [
                              Image.asset(
                                "assets/images/icons/gender_icon.png",
                                width: 20,
                                color: Colors.grey,
                              ),
                              SizedBox(width: 10),
                              Txt(
                                txt: "${userModel.maritalStatus}",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            ),
          )
        : SizedBox();
  }

  drawBadges() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(10.0),
          // margin: EdgeInsets.all(8.0),
          color: Colors.grey[200],
          child: Row(
            // crossAxisAlignment: CrossAxisAlignment.s,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                txt: "Badges",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true,
              ),
              userModel != null
                  ? userModel.id != profileUserID
                      ? Container(
                          decoration: BoxDecoration(
                              color: MyTheme.bgColor2,
                              shape: BoxShape.rectangle,
                              border: Border.all(
                                width: 1,
                              )),
                          child: GestureDetector(
                              onTap: () {
                                Get.to(() => BadgeScreen());
                              },
                              child: Icon(
                                Icons.add,
                                color: Colors.black,
                                size: 25,
                              )),
                        )
                      : SizedBox()
                  : SizedBox(),
              /*MMBtn(
                  txt: "Add More",
                  txtColor: Colors.white,
                  width: getWP(context, 22),
                  height: getHP(context, 5),
                  callback: () {
                    Get.to(() => BadgeScreen());
                  }),*/
            ],
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: listUserBadgeModelShowList.length,
          itemBuilder: (context, index) {
            var item;
            debugPrint("badge type  ${listUserBadgeModelShowList[index].type}");

            if (listUserBadgeModelShowList[index].type == "Mobile") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "Email") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "Passport") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "Facebook") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "CustomerPrivacy") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "NationalIDCard") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "ElectricId") {
              item = listItems[index];
            }
            return (item == null)
                ? SizedBox()
                : Card(
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: ListTile(
                        // leading: Image.asset(item['icon']),
                        title: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                width: getWP(context, 6),
                                height: getWP(context, 6),
                                child: Image.asset(item['icon'])),
                            SizedBox(width: 10),
                            Expanded(
                              child: Txt(
                                  txt: item["title"],
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isOverflow: true,
                                  isBold: false),
                            ),
                          ],
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: Txt(
                                  txt: item["title"] == "Customer Privacy"
                                      ? item["desc"] +
                                          " at ${DateFormat('dd-MMM-yyyy').format(DateTime.parse(listUserBadgeModelShowList[index].creationDate.toString()))}"
                                      : item["desc"],
                                  txtColor: Colors.grey,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  //txtLineSpace: 1.4,
                                  isBold: false),
                            ),
                            Container(
                              color: Colors.grey[200],
                              height: 1,
                              width: getW(context),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
          },
        ),
      ],
    );
  }

  drawUserSwitchView() {
    return Center(
      child: ToggleSwitch(
        isBorderColor: true,
        minWidth: getWP(context, 35),
        minHeight: getHP(context, MyTheme.switchBtnHpa),
        initialLabelIndex: posterSwitchValue,
        cornerRadius: 50.0,
        fontSize: 20,
        activeBgColor: MyTheme.heroTheamColors,
        activeFgColor: Colors.white,
        inactiveBgColor: Colors.white,
        inactiveFgColor: MyTheme.heroTheamColors,
        labels: ['As a Hero', 'As a Client'],
        //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
        onToggle: (index) {
          posterSwitchValue = index;
          setState(() {});
        },
      ),
    );
  }

  drawUserRatingAndSummaryView(
      UserModel userModel,
      List<UserRatingSummaryDataModel> listUserRatingSummary,
      List<UserRatingsModel> listUserRating) {
    if (userModel == null) return SizedBox();
    if (listUserRatingSummary == null) return SizedBox();

    int rate = 0;
    int completionRate = 0;
    int reviews = 0;
    int completedTasks = 0;
    //if (!isPublicUser) {
    if (listUserRatingSummary.length > 0) {
      if (posterSwitchValue == 1) {
        rate = listUserRatingSummary[0].posterAverageRating.toInt();
        completionRate = listUserRatingSummary[0].posterCompletionRate;
        reviews = listUserRatingSummary[0].posterRatingCount;
        completedTasks = listUserRatingSummary[0].completedPosterTaskCount;
      } else {
        rate = listUserRatingSummary[0].taskerAverageRating.toInt();
        completionRate = listUserRatingSummary[0].taskerCompletionRate;
        reviews = listUserRatingSummary[0].taskerRatingCount;
        completedTasks = listUserRatingSummary[0].completedTaskerTaskCount;
      }
    }

    if (reviews == 0) {
      if (posterSwitchValue == 0) {
        var lookslikeStr = !isPublicUser
            ? "Looks like you haven\'t created any reviews just yet"
            : (userModel != null
                ? userModel.name
                : "" + " hasn\'t received any reviews just yet");

        return Txt(
            txt: lookslikeStr,
            txtColor: MyTheme.gray5Color,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false);
      } else {
        final lookslikeStr = !isPublicUser
            ? "Looks like you haven\'t received any reviews just yet"
            : (userModel != null
                ? userModel.name
                : "" + " hasn\'t received any reviews just yet");
        return Txt(
            txt: lookslikeStr,
            txtColor: MyTheme.gray5Color,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false);
      }
    } else {
      //} else {
      //rate = profileController.aveargeRatingAsTasker.value;
      //completionRate = profileController.completionRateAsTasker.value;
      //reviews = profileController.countAsTasker.value;
      //}

      return Container(
        //color: Colors.black,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            UIHelper().getStarRatingView(
              w: getW(context),
              h: getH(context),
              rate: rate,
              reviews: reviews,
            ),
            //SizedBox(height: 5),
            UIHelper().getCompletionText(
              pa: completionRate,
              callbackInfo: () {
                if (posterSwitchValue == 0) {
                  showAlert(
                      context: context,
                      msg:
                          "Completion rate is the percentage of tasks assigned to the 'Hero' which were successfully completed");
                } else {
                  showAlert(
                      context: context,
                      msg:
                          "Completion rate is the percentage of tasks assigned by the 'Client' which were successfully completed");
                }
              },
            ),
            Txt(
                txt: completedTasks.toString() +
                    " completed task" +
                    (completedTasks > 1 ? "s" : ''),
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            ProfileHelper().drawProfileUserRatingView(
                getW(context),
                getH(context),
                listUserRating,
                (posterSwitchValue == 0) ? false : true, (ratingModel) {
              go2TaskDetailsPage(ratingModel.taskBiddingId);
            }),
          ],
        ),
      );
    }
  }

  go2TaskDetailsPage(int taskBiddingId) async {
    try {
      await APIViewModel().req<TaskBiddingAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASKBIDDING_URL
            .replaceAll("#taskBiddingId#", taskBiddingId.toString()),
        reqType: ReqType.Get,
        callback: (model2) async {
          if (model2 != null && mounted) {
            if (model2.success) {
              await APIViewModel().req<GetTaskAPIModel>(
                context: context,
                url: APIMyTasksCfg.GET_TASK_URL.replaceAll("#taskId#",
                    model2.responseData.taskBidding.taskId.toString()),
                reqType: ReqType.Get,
                callback: (model) async {
                  if (model != null && mounted) {
                    if (model.success) {
                      final taskModel = model.responseData.task;
                      if (taskModel != null) {
                        final taskModel2 = taskCtrl
                            .getTaskModel(); //  temporary store old task object
                        await taskCtrl.setTaskModel(taskModel);
                        await Get.to(() => TaskDetailsPage(
                            taskModel: model.responseData.task)).then((value) {
                          if (mounted) {
                            Future.delayed(Duration(seconds: 1), () {
                              if (mounted) {
                                taskCtrl.setTaskModel(
                                    taskModel2); //  re- store old task object back
                                if (value == "STATE_RELOAD_TAB_FIND_WORK") {
                                  StateProvider().notify(
                                      ObserverState.STATE_RELOAD_TAB, 2);
                                }
                              }
                            });
                          }
                        });
                      }
                    }
                  }
                },
              );
            }
          }
        },
      );
    } catch (e) {}
  }

  /*drawBadgesView(List<UserBadgesModel> listUserBadge) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "BADGES",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            for (UserBadgesModel badgeModel in listUserBadge)
              drawBadgeList(badgeModel, isPublicUser, profileController),
          ],
        ),
      ),
    );
  }*/

  drawBadgeView(List<UserBadgesModel> listUserBadge) {
    return Container();
  }

  drawBadgeList(UserBadgesModel badgeModel, bool isPoster,
      ProfileController profileController) {
    try {
      if (!badgeModel.isVerified) return SizedBox();
      if (badgeModel.title == "") return SizedBox();

      var title = (badgeModel.title != null && !isPoster)
          ? badgeModel.title
          : TaskHelper().getBadgeTypeTxt(badgeModel.type);
      var activationDate = (badgeModel.verificationCode != null && !isPoster)
          ? DateFun.getTimeAgoTxt(badgeModel.creationDate)
          : "";
      //var refCode = "";
      bool isBadgeStatus101 = (badgeModel.status == 101) ? true : false;
      profileController.setBadgeValue(isBadgeStatus101.obs);
      return (isBadgeStatus101)
          ? Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 30,
                          height: 30,
                          child: TaskHelper().getBadgeTypeSvg(badgeModel.type),
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: title,
                                    txtColor: MyTheme.gray5Color,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                (activationDate != '')
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Txt(
                                            txt: activationDate,
                                            txtColor: MyTheme.gray4Color,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                      )
                                    : SizedBox()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          : SizedBox();
    } catch (e) {
      return SizedBox();
    }
  }

  drawPortFolioView(UserPortfulioModel userPortfulioModel) {
    if (userPortfulioModel == null) return SizedBox();
    final listUrl = userPortfulioModel.portfulioItems.split("|");
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "PORTFOLIO",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 20),
            Container(
              height: getHP(context, 30),
              child: ListView.separated(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.horizontal,
                itemCount: listUrl.length,
                separatorBuilder: (BuildContext context, int index) =>
                    Container(width: 10),
                itemBuilder: (context, index) {
                  return new Container(
                    width: getWP(context, listUrl.length == 1 ? 90 : 85),
                    child: GestureDetector(
                      onTap: () {
                        Get.to(() => PicFullView(
                              title: "Attachment",
                              url: listUrl[index],
                            ));
                      },
                      child:
                          MyNetworkImage().loadCacheImage(url: listUrl[index]),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  /*drawMoreButtonView() {
    return Obx(
      () => Container(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
          child: Column(
            children: [
              (!profileController.isBadgeStatus101.value)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Txt(
                          txt:
                              "Where are the ID Badges? None have been applied yet.",
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    )
                  : SizedBox(),
              SizedBox(height: 20),
              Container(
                width: getWP(context, 100),
                height: getHP(context, MyTheme.btnHpa),
                child: BtnOutline(
                    width: null,
                    height: null,
                    txt: "Learn More",
                    radius: 30,
                    txtColor: MyTheme.brandColor,
                    borderColor: MyTheme.gray3Color,
                    callback: () {
                      Get.to(() => WebScreen(
                            //'http://192.168.1.100/mm/'
                            url: ServerUrls.BADGE_SUPPORT_URL,
                            title: 'Learn More',
                          ));
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }*/

  drawAboutSkillsView(AboutModel aboutModel) {
    if (aboutModel == null) return SizedBox();
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "SKILLS",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 20),
            _drawSkills("Transportation", aboutModel.goAround ?? ''),
            _drawSkills("Languages", aboutModel.languages ?? ''),
            _drawSkills("Education", aboutModel.qualifications ?? ''),
            _drawSkills("Work", aboutModel.experiences ?? ''),
            _drawSkills("Specialities", aboutModel.whatIamlookingfor ?? ''),
            SizedBox(height: 20),
            Txt(
                txt: aboutModel.remarks,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }

  _drawSkills(String title, String str) {
    final _controller = ScrollController();
    if (str == '') return SizedBox();
    final arr = str.split("|").reversed.toList();
    //
    bool isShowLRArrow = false;
    int h = 0;
    int txtLen = 0;
    for (var t in arr) txtLen += t.length;
    final txtFontSize = txtLen * 15;
    h += txtFontSize;
    if (h > getWP(context, 90)) {
      isShowLRArrow = true;
    }
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Txt(
                      txt: title,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                (isShowLRArrow)
                    ? Row(
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.grey,
                            ),
                            onPressed: () {
                              var pos = _controller.position.pixels - 200;
                              if (pos < 0) pos = 0;
                              _controller.animateTo(pos,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.fastOutSlowIn);
                            },
                          ),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios,
                                color: Colors.grey),
                            onPressed: () {
                              final pos = _controller.position.pixels + 200;
                              _controller.animateTo(pos,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.fastOutSlowIn);
                            },
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 20,
              ),
              child: Container(
                width: getW(context),
                height: isIPAD || isTablet ? 80 : 50,
                child: ListView.builder(
                  controller: _controller,
                  primary: false,
                  shrinkWrap: true,
                  itemCount: arr.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Center(
                      child: Container(
                        margin: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                            color: MyTheme.gray2Color,
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(color: Colors.grey, width: .5)),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 2, bottom: 2, left: 15, right: 15),
                          child: Txt(
                              txt: arr[index],
                              txtColor: MyTheme.blueColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            //(arr.length == 0) ? SizedBox(height: 10) : SizedBox(),
            Container(
              color: Colors.grey,
              height: .5,
            ),
          ],
        ),
      ),
    );
  }
}
