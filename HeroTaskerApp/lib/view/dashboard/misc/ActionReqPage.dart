import 'dart:io';

import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIActionReqCfg.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/data/model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/data/model/dashboard/action_req/GetTaskVerifyAPIModel.dart';
import 'package:aitl/data/model/dashboard/action_req/TaskerProfileVerificationData.dart';
import 'package:aitl/data/model/dashboard/more/profile/EntityPropertyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/OtpByEmailPage.dart';
import 'package:aitl/view/auth/otp/OtpByMobilePage.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/selfie_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/receive_payment/add_bank_acc_page.dart';
import 'package:aitl/view/dashboard/more/profile/dialog/portfolio_dialog.dart';
import 'package:aitl/view/dashboard/more/profile/dialog/skills_dialog.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_page.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/action_req/action_req_helper.dart';
import 'package:aitl/view_model/helper/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ActionReqPage extends StatefulWidget {
  TaskerProfileVerificationData taskVerifyModel;
  ActionReqPage({Key key, @required this.taskVerifyModel}) : super(key: key);
  @override
  State<ActionReqPage> createState() => _ActionReqPageState();
}

class _ActionReqPageState extends State<ActionReqPage> with Mixin {
  Widget widActionRequired;

  drawActionRequiredItems(TaskerProfileVerificationData model) {
    widActionRequired = ActionReqHelper().drawActionRequiredItems(
      taskVerifyModel: model,
      callbackType: (eVerifyType type) {
        switch (type) {
          case eVerifyType.MOBILE:
            Sms2APIMgr().wsLoginMobileOtpPostAPI(
                context: context,
                countryCode: AppDefine.COUNTRY_DIALCODE,
                mobile: userData.userModel.mobileNumber,
                callback: (model) {
                  if (model != null && mounted) {
                    try {
                      if (model.success) {
                        Sms2APIMgr().wsSendOtpNotiAPI(
                            context: context,
                            otpId: model.responseData.userOTP.id,
                            callback: (model) {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    Get.to(() => OtpByMobilePage(
                                        mobile: userData.userModel.mobileNumber,
                                        isBack: true)).then((value) {
                                      refreshData();
                                    });
                                  }
                                } catch (e) {}
                              }
                            });
                      }
                    } catch (e) {}
                  }
                });
            break;
          case eVerifyType.EMAIL:
            APIViewModel().req<PostEmailOtpAPIModel>(
                context: context,
                url: APIAuthCfg.POSTEMAILOTP_POST_URL,
                reqType: ReqType.Post,
                param: {
                  "Email": userData.userModel.email,
                  "Status": "101",
                },
                callback: (model2) async {
                  if (mounted) {
                    if (model2 != null) {
                      if (model2.success) {
                        await APIViewModel().req<SendUserEmailOtpAPIModel>(
                            context: context,
                            url: APIAuthCfg.SENDUSEREMAILOTP_GET_URL.replaceAll(
                                "#otpId#",
                                model2.responseData.userOTP.id.toString()),
                            reqType: ReqType.Get,
                            callback: (model3) async {
                              if (mounted) {
                                if (model3 != null) {
                                  if (model3.success) {
                                    Get.to(() => OtpByEmailPage(
                                        email: userData.userModel.email,
                                        isBack: true)).then((value) {
                                      refreshData();
                                    });
                                  }
                                }
                              }
                            });
                      }
                    }
                  }
                });
            break;
          case eVerifyType.EID:
            Get.to(() => SelfiePage()).then((value) {
              refreshData();
            });
            break;
          case eVerifyType.PAYMENT_METHOD:
            Get.to(() => AddBankAccountPage()).then((value) {
              refreshData();
            });
            break;
          case eVerifyType.PROFILE_PIC:
            CamPicker().showCamDialog(
              context: context,
              isRear: false,
              callback: (File path) {
                if (path != null) {
                  APIViewModel()
                      .upload(
                          context: context,
                          file: path,
                          callback: (model) async {
                            if (mounted && model != null) {
                              if (model.success) {
                                try {
                                  final uploadFileModelProfile =
                                      model.responseData.images[0];
                                  await APIViewModel().req<
                                          EntityPropertyAPIModel>(
                                      context: context,
                                      url: APIProfileCFg
                                          .ENTITY_PROPERTY_POST_URL,
                                      reqType: ReqType.Post,
                                      param: {
                                        "EntityId": userData.userModel.id,
                                        "EntityName": "User",
                                        "PropertyName": "DefaultPictureId",
                                        "Value": uploadFileModelProfile.id,
                                      },
                                      callback: (model2) async {
                                        if (mounted && model2 != null) {
                                          if (model2.success) {
                                            await APIViewModel()
                                                .req<PublicProfileAPIModel>(
                                                    context: context,
                                                    url: APIProfileCFg
                                                        .PUBLIC_USER_GET_URL
                                                        .replaceAll(
                                                            "#userId#",
                                                            userData
                                                                .userModel.id
                                                                .toString()),
                                                    reqType: ReqType.Get,
                                                    callback: (model3) {
                                                      if (mounted &&
                                                          model3 != null) {
                                                        if (model3.success) {
                                                          userData.userModel =
                                                              model3
                                                                  .responseData
                                                                  .user;
                                                          showToast(
                                                              context: context,
                                                              msg:
                                                                  "Profile image uploaded successfully.",
                                                              which: 1);
                                                          refreshData();
                                                        }
                                                      }
                                                    });
                                          }
                                        }
                                      });
                                } catch (e) {}
                              }
                            }
                          })
                      .then((value) => refreshData());
                }
              },
            );
            break;
          case eVerifyType.TASK_ALERTS:
            Get.to(() => TaskAlertPage()).then((value) => refreshData());
            break;
          case eVerifyType.PORTFOLIO:
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      contentPadding: EdgeInsets.all(0),
                      content: PortFolioDialog(),
                    )).then((value) => refreshData());
            break;
          case eVerifyType.SKILLS:
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      contentPadding: EdgeInsets.all(0),
                      content: SkillsDialog(),
                    )).then((value) => refreshData());
            break;
          default:
        }
      },
    );
  }

  refreshData() async {
    try {
      await APIViewModel().req<GetTaskVerifyAPIModel>(
          context: context,
          url: APIActionReqCfg.GET_TASK_VERIFY_URL +
              userData.userModel.id.toString(),
          reqType: ReqType.Get,
          callback: (model) {
            if (mounted && model != null) {
              if (model.success) {
                try {
                  final taskVerifyModel =
                      model.responseData.taskerProfileVerificationData[0];
                  drawActionRequiredItems(taskVerifyModel);
                } catch (e) {}
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    widActionRequired = null;
    super.dispose();
  }

  initPage() async {
    try {
      await drawActionRequiredItems(widget.taskVerifyModel);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          centerTitle: false,
          elevation: 1,
          title: UIHelper().drawAppbarTitle(title: "Action Required"),
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return widActionRequired == null
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
                primary: true,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30, left: 30, right: 20, bottom: 20),
                        child: Txt(
                            txt:
                                "A green tick shows that the verification is currently active.",
                            txtColor: MyTheme.gray5Color,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: false),
                      ),
                      (widActionRequired != null)
                          ? Container(child: widActionRequired)
                          : SizedBox(),
                    ])));
  }
}
