import 'package:aitl/mixin.dart';
import 'package:aitl/view_model/helper/utils/APIHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';

import '../../config/server/APIBadgeCfg.dart';
import '../../data/app_data/AppData.dart';
import '../../data/app_data/UserData.dart';
import '../../data/model/misc/GetBadgeCounterAPIModel.dart';
import '../../data/network/NetworkMgr.dart';
import '../../view_model/api/api_view_model.dart';

abstract class BaseDashboard<T extends StatefulWidget> extends State<T>
    with Mixin, UIHelper, WidgetsBindingObserver {
  drawLayout();

  wsUserDevice({String eventType = ''}) async {
    try {
      await APIHelper().wsUserDevice(
          context: context, eventType: eventType, callback: (model) {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    //WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        onResumed();
        break;
      case AppLifecycleState.inactive:
        onPaused();
        break;
      case AppLifecycleState.paused:
        onInactive();
        break;
      case AppLifecycleState.detached:
        onDetached();
        break;
    }
  }

  void onResumed();
  void onPaused();
  void onInactive();
  void onDetached();
}
