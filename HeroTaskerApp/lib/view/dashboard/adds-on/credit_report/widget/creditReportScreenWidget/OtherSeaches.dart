import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditDashBoardReport.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

import '../../CreditDashboardTabController.dart';

class OtherSeaches extends StatefulWidget {
  @override
  State<OtherSeaches> createState() => _OtherSeachesState();
}

class _OtherSeachesState extends State<OtherSeaches> with Mixin {
  CreditDashBoardReport creditDashBoardReport;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;

    return Container(
      child: Column(
        children: [
          //SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              width: getW(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Txt(
                        txt: "COMPANY",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "PURPOSE",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "DATE",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(child: SizedBox(width: getWP(context, 5))),
                ],
              ),
            ),
          ),
          drawAccountTypeItems(
              ["CurrentAddressOtherSearches", "PreviousAddressOtherSearches"],
              "Searches on your current address"),
        ],
      ),
    );
  }

  drawAccountTypeItems(List<String> listAccountType, String title) {
    if (creditDashBoardReport == null) return SizedBox();
    return Container(
      child: Column(
        children: [
          Container(
            width: getW(context),
            decoration: BoxDecoration(color: Colors.grey),
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: creditDashBoardReport.cRSearchRecordList.length,
            itemBuilder: (context, index) {
              final model = creditDashBoardReport.cRSearchRecordList[index];

              bool isReturn = false;
              if (!listAccountType.contains(model.searchHistoryType)) {
                isReturn = true;
              }

              return (isReturn)
                  ? SizedBox()
                  : Container(
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black87),
                          borderRadius: BorderRadius.all(
                            Radius.circular(7),
                          )),
                      child: ExpansionTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Txt(
                                  txt: model.company ?? '',
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: model.searchPurpose ?? '',
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: DateFun.getDate(
                                      model.creationDate, "dd-MMM-yyyy"),
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ],
                        ),
                        iconColor: Colors.black,
                        collapsedIconColor: Colors.black,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Column(
                                children: [],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }

  userInfoItem({String title, String value}) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Txt(
              txt: "$title: ",
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.start,
              isBold: true),
          Expanded(
              child: Txt(
                  txt: "$value",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false))
        ],
      ),
    );
  }
}
