class CreditDashBoardReport {
  List<CRSearchRecordList> cRSearchRecordList;
  CRElectoralRoll cRElectoralRoll;
  CRUserManage cRUserManage;
  CRAccountHolderDetail cRAccountHolderDetail;
  List<CRAddressLinkDetailList> cRAddressLinkDetailList;
  List<CRBankAccountList> cRBankAccountList;
  List cRFinancialConnectionList;
  List<CRJudgmentList> cRJudgmentList;
  List<CRMonthlyStatusDataList> cRMonthlyStatusDataList;
  List<CRMonthlyDataList> cRMonthlyDataList;
  Report report;

  CreditDashBoardReport(
      {this.cRSearchRecordList,
      this.cRElectoralRoll,
      this.cRUserManage,
      this.cRAccountHolderDetail,
      this.cRAddressLinkDetailList,
      this.cRBankAccountList,
      this.cRFinancialConnectionList,
      this.cRJudgmentList,
      this.cRMonthlyStatusDataList,
      this.cRMonthlyDataList,
      this.report});

  CreditDashBoardReport.fromJson(Map<String, dynamic> json) {
    if (json['CRSearchRecordList'] != null) {
      cRSearchRecordList = [];
      json['CRSearchRecordList'].forEach((v) {
        cRSearchRecordList.add(new CRSearchRecordList.fromJson(v));
      });
    }
    cRElectoralRoll = json['CRElectoralRoll'] != null
        ? new CRElectoralRoll.fromJson(json['CRElectoralRoll'])
        : null;
    cRUserManage = json['CRUserManage'] != null
        ? new CRUserManage.fromJson(json['CRUserManage'])
        : null;
    cRAccountHolderDetail = json['CRAccountHolderDetail'] != null
        ? new CRAccountHolderDetail.fromJson(json['CRAccountHolderDetail'])
        : null;
    if (json['CRAddressLinkDetailList'] != null) {
      cRAddressLinkDetailList = [];
      json['CRAddressLinkDetailList'].forEach((v) {
        cRAddressLinkDetailList.add(new CRAddressLinkDetailList.fromJson(v));
      });
    }
    if (json['CRBankAccountList'] != null) {
      cRBankAccountList = [];
      json['CRBankAccountList'].forEach((v) {
        cRBankAccountList.add(new CRBankAccountList.fromJson(v));
      });
    }
    if (json['CRFinancialConnectionList'] != null) {
      cRFinancialConnectionList = [];
      json['CRFinancialConnectionList'].forEach((v) {
        cRFinancialConnectionList.add(v.fromJson(v));
      });
    }
    if (json['CRJudgmentList'] != null) {
      cRJudgmentList = [];
      json['CRJudgmentList'].forEach((v) {
        cRJudgmentList.add(new CRJudgmentList.fromJson(v));
      });
    }
    if (json['CRMonthlyStatusDataList'] != null) {
      cRMonthlyStatusDataList = [];
      json['CRMonthlyStatusDataList'].forEach((v) {
        cRMonthlyStatusDataList.add(new CRMonthlyStatusDataList.fromJson(v));
      });
    }
    if (json['CRMonthlyDataList'] != null) {
      cRMonthlyDataList = [];
      json['CRMonthlyDataList'].forEach((v) {
        cRMonthlyDataList.add(new CRMonthlyDataList.fromJson(v));
      });
    }
    report =
        json['Report'] != null ? new Report.fromJson(json['Report']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.cRSearchRecordList != null) {
      data['CRSearchRecordList'] =
          this.cRSearchRecordList.map((v) => v.toJson()).toList();
    }
    if (this.cRElectoralRoll != null) {
      data['CRElectoralRoll'] = this.cRElectoralRoll.toJson();
    }
    if (this.cRUserManage != null) {
      data['CRUserManage'] = this.cRUserManage.toJson();
    }
    if (this.cRAccountHolderDetail != null) {
      data['CRAccountHolderDetail'] = this.cRAccountHolderDetail.toJson();
    }
    if (this.cRAddressLinkDetailList != null) {
      data['CRAddressLinkDetailList'] =
          this.cRAddressLinkDetailList.map((v) => v.toJson()).toList();
    }
    if (this.cRBankAccountList != null) {
      data['CRBankAccountList'] =
          this.cRBankAccountList.map((v) => v.toJson()).toList();
    }
    if (this.cRFinancialConnectionList != null) {
      data['CRFinancialConnectionList'] =
          this.cRFinancialConnectionList.map((v) => v.toJson()).toList();
    }
    if (this.cRJudgmentList != null) {
      data['CRJudgmentList'] =
          this.cRJudgmentList.map((v) => v.toJson()).toList();
    }
    if (this.cRMonthlyStatusDataList != null) {
      data['CRMonthlyStatusDataList'] =
          this.cRMonthlyStatusDataList.map((v) => v.toJson()).toList();
    }
    if (this.cRMonthlyDataList != null) {
      data['CRMonthlyDataList'] =
          this.cRMonthlyDataList.map((v) => v.toJson()).toList();
    }
    if (this.report != null) {
      data['Report'] = this.report.toJson();
    }
    return data;
  }
}

class CRSearchRecordList {
  int status;
  String creationDate;
  int userId;
  int userCompanyId;
  String searchHistoryType;
  Null address;
  Null applicationType;
  Null company;
  String dateOfBirth;
  Null name;
  String searchDate;
  Null searchPurpose;
  Null searchReference;
  Null unit;
  Null remarks;
  int id;

  CRSearchRecordList(
      {this.status,
      this.creationDate,
      this.userId,
      this.userCompanyId,
      this.searchHistoryType,
      this.address,
      this.applicationType,
      this.company,
      this.dateOfBirth,
      this.name,
      this.searchDate,
      this.searchPurpose,
      this.searchReference,
      this.unit,
      this.remarks,
      this.id});

  CRSearchRecordList.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    searchHistoryType = json['SearchHistoryType'];
    address = json['Address'];
    applicationType = json['ApplicationType'];
    company = json['Company'];
    dateOfBirth = json['DateOfBirth'];
    name = json['Name'];
    searchDate = json['SearchDate'];
    searchPurpose = json['SearchPurpose'];
    searchReference = json['SearchReference'];
    unit = json['Unit'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['SearchHistoryType'] = this.searchHistoryType;
    data['Address'] = this.address;
    data['ApplicationType'] = this.applicationType;
    data['Company'] = this.company;
    data['DateOfBirth'] = this.dateOfBirth;
    data['Name'] = this.name;
    data['SearchDate'] = this.searchDate;
    data['SearchPurpose'] = this.searchPurpose;
    data['SearchReference'] = this.searchReference;
    data['Unit'] = this.unit;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRElectoralRoll {
  var status;
  String creationDate;
  var userId;
  var userCompanyId;
  var address;
  String currentStatus;
  String electoralRollId;
  String endDate;
  String name;
  String notices;
  String startDate;
  var remarks;
  var id;

  CRElectoralRoll(
      {this.status,
      this.creationDate,
      this.userId,
      this.userCompanyId,
      this.address,
      this.currentStatus,
      this.electoralRollId,
      this.endDate,
      this.name,
      this.notices,
      this.startDate,
      this.remarks,
      this.id});

  CRElectoralRoll.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    address = json['Address'];
    currentStatus = json['CurrentStatus'];
    electoralRollId = json['ElectoralRollId'];
    endDate = json['EndDate'];
    name = json['Name'];
    notices = json['Notices'];
    startDate = json['StartDate'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Address'] = this.address;
    data['CurrentStatus'] = this.currentStatus;
    data['ElectoralRollId'] = this.electoralRollId;
    data['EndDate'] = this.endDate;
    data['Name'] = this.name;
    data['Notices'] = this.notices;
    data['StartDate'] = this.startDate;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRUserManage {
  var status;
  String creationDate;
  String updatedDate;
  var userId;
  var userCompanyId;
  String userIdentifier;
  String remarks;
  String reportId;
  String searchReference;
  var ratingValue;
  var score;
  var id;

  CRUserManage(
      {this.status,
      this.creationDate,
      this.updatedDate,
      this.userId,
      this.userCompanyId,
      this.userIdentifier,
      this.remarks,
      this.reportId,
      this.searchReference,
      this.ratingValue,
      this.score,
      this.id});

  CRUserManage.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    userIdentifier = json['UserIdentifier'];
    remarks = json['Remarks'];
    reportId = json['ReportId'];
    searchReference = json['SearchReference'];
    ratingValue = json['RatingValue'];
    score = json['Score'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['UserIdentifier'] = this.userIdentifier;
    data['Remarks'] = this.remarks;
    data['ReportId'] = this.reportId;
    data['SearchReference'] = this.searchReference;
    data['RatingValue'] = this.ratingValue;
    data['Score'] = this.score;
    data['Id'] = this.id;
    return data;
  }
}

class CRAccountHolderDetail {
  var userId;
  var status;
  String creationDate;
  var userCompanyId;
  String userIdentifier;
  var accountHolderId;
  String accountNumber;
  String address;
  String dateOfBirth;
  String name;
  String startDate;
  String dateReportedCreated;
  var accountHolderDetailStatus;
  var searchReference;
  var remarks;
  var id;

  CRAccountHolderDetail(
      {this.userId,
      this.status,
      this.creationDate,
      this.userCompanyId,
      this.userIdentifier,
      this.accountHolderId,
      this.accountNumber,
      this.address,
      this.dateOfBirth,
      this.name,
      this.startDate,
      this.dateReportedCreated,
      this.accountHolderDetailStatus,
      this.searchReference,
      this.remarks,
      this.id});

  CRAccountHolderDetail.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    userCompanyId = json['UserCompanyId'];
    userIdentifier = json['UserIdentifier'];
    accountHolderId = json['AccountHolderId'];
    accountNumber = json['AccountNumber'];
    address = json['Address'];
    dateOfBirth = json['DateOfBirth'];
    name = json['Name'];
    startDate = json['StartDate'];
    dateReportedCreated = json['DateReportedCreated'];
    accountHolderDetailStatus = json['AccountHolderDetailStatus'];
    searchReference = json['SearchReference'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserCompanyId'] = this.userCompanyId;
    data['UserIdentifier'] = this.userIdentifier;
    data['AccountHolderId'] = this.accountHolderId;
    data['AccountNumber'] = this.accountNumber;
    data['Address'] = this.address;
    data['DateOfBirth'] = this.dateOfBirth;
    data['Name'] = this.name;
    data['StartDate'] = this.startDate;
    data['DateReportedCreated'] = this.dateReportedCreated;
    data['AccountHolderDetailStatus'] = this.accountHolderDetailStatus;
    data['SearchReference'] = this.searchReference;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRAddressLinkDetailList {
  var status;
  String creationDate;
  var userId;
  var userCompanyId;
  String addressFrom;
  String addressLinkId;
  var addressTo;
  var earliestConfirmationOn;
  var lastConfirmationOn;
  var mostRecentSource;
  var notices;
  var remarks;
  var id;

  CRAddressLinkDetailList(
      {this.status,
      this.creationDate,
      this.userId,
      this.userCompanyId,
      this.addressFrom,
      this.addressLinkId,
      this.addressTo,
      this.earliestConfirmationOn,
      this.lastConfirmationOn,
      this.mostRecentSource,
      this.notices,
      this.remarks,
      this.id});

  CRAddressLinkDetailList.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    addressFrom = json['AddressFrom'];
    addressLinkId = json['AddressLinkId'];
    addressTo = json['AddressTo'];
    earliestConfirmationOn = json['EarliestConfirmationOn'];
    lastConfirmationOn = json['LastConfirmationOn'];
    mostRecentSource = json['MostRecentSource'];
    notices = json['Notices'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['AddressFrom'] = this.addressFrom;
    data['AddressLinkId'] = this.addressLinkId;
    data['AddressTo'] = this.addressTo;
    data['EarliestConfirmationOn'] = this.earliestConfirmationOn;
    data['LastConfirmationOn'] = this.lastConfirmationOn;
    data['MostRecentSource'] = this.mostRecentSource;
    data['Notices'] = this.notices;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRBankAccountList {
  var userId;
  var status;
  String creationDate;
  var userCompanyId;
  String name;
  var address;
  String accountAddress;
  String userIdentifier;
  String dateOfBirth;
  var accountEndDate;
  String accountId;
  String accountNumber;
  String accountStartDate;
  String accountType;
  String accountTypeName;
  var balance;
  String currencyCode;
  var defaultBalance;
  var defaultDate;
  String lenderName;
  String memberPortId;
  var minimumPayment;
  var openingBalance;
  String paymentStartDate;
  String promotionalRate;
  var regularPaymentAmount;
  String repaymentFrequency;
  String statusSubjectiveLevel;
  String updatedDate;
  String cRBankAccountStatus;
  String remarks;
  var id;

  CRBankAccountList(
      {this.userId,
      this.status,
      this.creationDate,
      this.userCompanyId,
      this.name,
      this.address,
      this.accountAddress,
      this.userIdentifier,
      this.dateOfBirth,
      this.accountEndDate,
      this.accountId,
      this.accountNumber,
      this.accountStartDate,
      this.accountType,
      this.balance,
      this.currencyCode,
      this.defaultBalance,
      this.defaultDate,
      this.lenderName,
      this.memberPortId,
      this.minimumPayment,
      this.openingBalance,
      this.paymentStartDate,
      this.promotionalRate,
      this.regularPaymentAmount,
      this.repaymentFrequency,
      this.statusSubjectiveLevel,
      this.updatedDate,
      this.cRBankAccountStatus,
      this.remarks,
      this.id});

  CRBankAccountList.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    userCompanyId = json['UserCompanyId'];
    name = json['Name'];
    address = json['Address'];
    accountAddress = json['AccountAddress'];
    userIdentifier = json['UserIdentifier'];
    dateOfBirth = json['DateOfBirth'];
    accountEndDate = json['AccountEndDate'];
    accountId = json['AccountId'];
    accountNumber = json['AccountNumber'];
    accountStartDate = json['AccountStartDate'];
    accountType = json['AccountType'];
    accountTypeName = json['AccountTypeName'];
    balance = json['Balance'];
    currencyCode = json['CurrencyCode'];
    defaultBalance = json['DefaultBalance'];
    defaultDate = json['DefaultDate'];
    lenderName = json['LenderName'];
    memberPortId = json['MemberPortId'];
    minimumPayment = json['MinimumPayment'];
    openingBalance = json['OpeningBalance'];
    paymentStartDate = json['PaymentStartDate'];
    promotionalRate = json['PromotionalRate'];
    regularPaymentAmount = json['RegularPaymentAmount'];
    repaymentFrequency = json['RepaymentFrequency'];
    statusSubjectiveLevel = json['StatusSubjectiveLevel'];
    updatedDate = json['UpdatedDate'];
    cRBankAccountStatus = json['CRBankAccountStatus'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserCompanyId'] = this.userCompanyId;
    data['Name'] = this.name;
    data['Address'] = this.address;
    data['AccountAddress'] = this.accountAddress;
    data['UserIdentifier'] = this.userIdentifier;
    data['DateOfBirth'] = this.dateOfBirth;
    data['AccountEndDate'] = this.accountEndDate;
    data['AccountId'] = this.accountId;
    data['AccountNumber'] = this.accountNumber;
    data['AccountStartDate'] = this.accountStartDate;
    data['AccountType'] = this.accountType;
    data['AccountTypeName'] = this.accountTypeName;
    data['Balance'] = this.balance;
    data['CurrencyCode'] = this.currencyCode;
    data['DefaultBalance'] = this.defaultBalance;
    data['DefaultDate'] = this.defaultDate;
    data['LenderName'] = this.lenderName;
    data['MemberPortId'] = this.memberPortId;
    data['MinimumPayment'] = this.minimumPayment;
    data['OpeningBalance'] = this.openingBalance;
    data['PaymentStartDate'] = this.paymentStartDate;
    data['PromotionalRate'] = this.promotionalRate;
    data['RegularPaymentAmount'] = this.regularPaymentAmount;
    data['RepaymentFrequency'] = this.repaymentFrequency;
    data['StatusSubjectiveLevel'] = this.statusSubjectiveLevel;
    data['UpdatedDate'] = this.updatedDate;
    data['CRBankAccountStatus'] = this.cRBankAccountStatus;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRJudgmentList {
  var status;
  String creationDate;
  var userId;
  var userCompanyId;
  String address;
  var amount;
  String caseNumber;
  String casePerId;
  String courtName;
  String courtType;
  String currencyCode;
  String judgmentDate;
  String name;
  String notices;
  var satisfiedDate;
  String judgmentStatus;
  var remarks;
  var id;

  CRJudgmentList(
      {this.status,
      this.creationDate,
      this.userId,
      this.userCompanyId,
      this.address,
      this.amount,
      this.caseNumber,
      this.casePerId,
      this.courtName,
      this.courtType,
      this.currencyCode,
      this.judgmentDate,
      this.name,
      this.notices,
      this.satisfiedDate,
      this.judgmentStatus,
      this.remarks,
      this.id});

  CRJudgmentList.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    address = json['Address'];
    amount = json['Amount'];
    caseNumber = json['CaseNumber'];
    casePerId = json['CasePerId'];
    courtName = json['CourtName'];
    courtType = json['CourtType'];
    currencyCode = json['CurrencyCode'];
    judgmentDate = json['JudgmentDate'];
    name = json['Name'];
    notices = json['Notices'];
    satisfiedDate = json['SatisfiedDate'];
    judgmentStatus = json['JudgmentStatus'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Address'] = this.address;
    data['Amount'] = this.amount;
    data['CaseNumber'] = this.caseNumber;
    data['CasePerId'] = this.casePerId;
    data['CourtName'] = this.courtName;
    data['CourtType'] = this.courtType;
    data['CurrencyCode'] = this.currencyCode;
    data['JudgmentDate'] = this.judgmentDate;
    data['Name'] = this.name;
    data['Notices'] = this.notices;
    data['SatisfiedDate'] = this.satisfiedDate;
    data['JudgmentStatus'] = this.judgmentStatus;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRMonthlyStatusDataList {
  var status;
  String creationDate;
  var userId;
  var userCompanyId;
  String accountStatus;
  String month;
  String paymentStatus;
  String paymentStatusDescription;
  String year;
  var remarks;
  var id;

  CRMonthlyStatusDataList(
      {this.status,
      this.creationDate,
      this.userId,
      this.userCompanyId,
      this.accountStatus,
      this.month,
      this.paymentStatus,
      this.paymentStatusDescription,
      this.year,
      this.remarks,
      this.id});

  CRMonthlyStatusDataList.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    accountStatus = json['AccountStatus'];
    month = json['Month'];
    paymentStatus = json['PaymentStatus'];
    paymentStatusDescription = json['PaymentStatusDescription'];
    year = json['Year'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['AccountStatus'] = this.accountStatus;
    data['Month'] = this.month;
    data['PaymentStatus'] = this.paymentStatus;
    data['PaymentStatusDescription'] = this.paymentStatusDescription;
    data['Year'] = this.year;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class CRMonthlyDataList {
  var status;
  String creationDate;
  var userId;
  var userCompanyId;
  String month;
  String monthlyValue;
  String year;
  String type;
  var remarks;
  var id;

  CRMonthlyDataList(
      {this.status,
      this.creationDate,
      this.userId,
      this.userCompanyId,
      this.month,
      this.monthlyValue,
      this.year,
      this.type,
      this.remarks,
      this.id});

  CRMonthlyDataList.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    creationDate = json['CreationDate'];
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    month = json['Month'];
    monthlyValue = json['MonthlyValue'];
    year = json['Year'];
    type = json['Type'];
    remarks = json['Remarks'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Month'] = this.month;
    data['MonthlyValue'] = this.monthlyValue;
    data['Year'] = this.year;
    data['Type'] = this.type;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}

class Report {
  var getReportResult;

  Report({this.getReportResult});

  Report.fromJson(Map<String, dynamic> json) {
    getReportResult = json['GetReportResult'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['GetReportResult'] = this.getReportResult;
    return data;
  }
}
