import 'dart:math';

import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/view/dashboard/more/db/db_base.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/apns/NotificationData.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

class InitializeAwesomeNotifications {
  static initAwesome() {
    AwesomeNotifications().initialize(
        // set the icon to null if you want to use the default app icon
        null,
        [
          NotificationChannel(
              channelKey: 'basic_channel',
              channelName: 'notifications-channel',
              channelDescription: 'Herotasker notification centre',
              defaultColor: MyTheme.redColor,
              ledColor: Colors.white)
        ]);

    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      debugPrint("Notitication is $isAllowed");
      if (!isAllowed) {
        AwesomeNotifications().requestPermissionToSendNotifications();
      }
    });
  }

  static generateNotification({NotificationData notiModelData}) {
    var id = 0;
    try {
      final webUrlArr = notiModelData.webUrl.toString().split('-');
      id = int.parse(webUrlArr[webUrlArr.length - 1]);
    } catch (e) {}
    if (id > 0) {
      final notiModel = NotiModel.fromJson({
        'UserId': int.parse(notiModelData.userId),
        'EntityId': int.parse(notiModelData.entityId),
        'EntityName': notiModelData.entityName,
        'InitiatorId': int.parse(notiModelData.initiatorId),
        'InitiatorDisplayName': notiModelData.initiatorName ?? '',
        'NotificationEventId': int.parse(notiModelData.notificationEventId),
        'Description': notiModelData.description,
        'Message': notiModelData.message,
        'Id': int.parse(notiModelData.id),
        'WebUrl': notiModelData.webUrl,
        'TaskId': id,
      });
      final map = BaseDashboardMoreStatefull().getNotiEvents(notiModel);
      var msg = '';
      msg = map['description'] + ' ' + notiModel.message;
      msg = msg.trim();
      if (msg == "") return SizedBox();
      if (map['isTaskDetails'] as bool) {
        msg = "A new task has been posted matching your skills: [" + msg + "]";
      }

      final payloadMap = Map<String, String>();
      for (final v in notiModel.toJson().entries) {
        payloadMap[v.key.toString()] = v.value.toString();
      }

      AwesomeNotifications().createNotification(
          content: NotificationContent(
        id: id, //new Random().nextInt(130),
        payload: payloadMap,
        channelKey: 'basic_channel',
        title: notiModelData.message,
        body: msg,
        //payload: notiModel.toJson(),
      ));
    }
  }
}
