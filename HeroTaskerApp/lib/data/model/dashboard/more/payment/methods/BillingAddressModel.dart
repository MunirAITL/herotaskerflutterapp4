import '../../../../auth/UserModel.dart';

class BillingAddressModel {
  String address1;
  String address2;
  String city;
  String country;
  String creationDate;
  int id;
  bool isDefault;
  bool isVerified;
  String state;
  int status;
  String updatedDate;
  int userId;
  String zip;
  UserModel user;

  BillingAddressModel({
    this.address1,
    this.address2,
    this.city,
    this.country,
    this.creationDate,
    this.id,
    this.isDefault,
    this.isVerified,
    this.state,
    this.status,
    this.updatedDate,
    this.userId,
    this.zip,
    this.user,
  });

  BillingAddressModel.fromJson(Map<String, dynamic> json) {
    address1 = json['Address1'] ?? '';
    address2 = json['Address2'] ?? '';
    city = json['City'] ?? '';
    country = json['Country'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    id = json['Id'] ?? 0;
    isDefault = json['IsDefault'] ?? false;
    isVerified = json['IsVerified'] ?? false;
    state = json['State'] ?? '';
    status = json['Status'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    userId = json['UserId'] ?? 0;
    zip = json['Zip'] ?? '';
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Address1'] = this.address1;
    data['Address2'] = this.address2;
    data['City'] = this.city;
    data['Country'] = this.country;
    data['CreationDate'] = this.creationDate;
    data['Id'] = this.id;
    data['IsDefault'] = this.isDefault;
    data['IsVerified'] = this.isVerified;
    data['State'] = this.state;
    data['Status'] = this.status;
    data['UpdatedDate'] = this.updatedDate;
    data['UserId'] = this.userId;
    data['Zip'] = this.zip;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
