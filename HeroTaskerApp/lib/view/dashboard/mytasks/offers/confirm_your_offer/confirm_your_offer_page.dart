import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIActionReqCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/action_req/GetTaskVerifyAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/selfie_page.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/action_req/BadgeAPIMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/ConfirmOfferController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'confirm_your_offer_base.dart';

class ConfirmYourOfferPage extends StatefulWidget {
  final TaskBiddingModel taskBiddingModel;
  const ConfirmYourOfferPage({Key key, this.taskBiddingModel})
      : super(key: key);

  @override
  State createState() => _ConfirmYourOfferPageState();
}

class _ConfirmYourOfferPageState
    extends BaseConfirmYourOfferStatefull<ConfirmYourOfferPage> {
  final confirmOfferController = Get.put(ConfirmOferController());

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      await APIViewModel().req<GetTaskVerifyAPIModel>(
          context: context,
          url: APIActionReqCfg.GET_TASK_VERIFY_URL +
              userData.userModel.id.toString(),
          reqType: ReqType.Get,
          callback: (model) {
            if (mounted && model != null) {
              if (model.success) {
                try {
                  taskVerifyModel =
                      model.responseData.taskerProfileVerificationData[0];
                  setState(() {});
                } catch (e) {}
              }
            }
          });
    } catch (e) {}
  }

  verifyPending(bool isBtnClicked) async {
    if (isBtnClicked) {
      wsUserProfile(
        userId: userData.userModel.id,
        callback: (model) {
          bool isYes = false;
          confirmDialog(
            context: context,
            title: "Pending Verification!",
            msg:
                "Your information is pending verification. Do you want to contact us?",
            callbackYes: () async {
              try {
                final userModel = model.responseData.user;
                if (userModel.communityId == "2") {
                  if (widget.taskBiddingModel != null) {
                    Get.to(() => ConfirmYourOfferPage(
                        taskBiddingModel: widget.taskBiddingModel));
                  } else {
                    await APIViewModel().req<TaskBiddingsAPIModel>(
                        context: context,
                        url: APIMyTasksCfg.GET_TASKBIDDING_URL
                            .replaceAll("/#taskBiddingId#", ""),
                        reqType: ReqType.Get,
                        param: {
                          "Count": 1,
                          "IsAll": true,
                          "Page": 0,
                          "TaskId": 0,
                          "userId": userData.userModel.id,
                        },
                        callback: (model2) {
                          if (model2 != null) {
                            if (model2.success &&
                                model2.responseData.taskBiddings.length > 0) {
                              Get.to(() => ConfirmYourOfferPage(
                                  taskBiddingModel:
                                      model2.responseData.taskBiddings[0]));
                            } else {
                              Get.back();
                            }
                          } else {
                            Get.back();
                          }
                        });
                  }
                } else {
                  Get.to(() => ResolutionScreen());
                }
              } catch (e) {
                Get.to(() => ResolutionScreen());
              }
            },
          );
        },
      );
    } else {
      Get.to(() => ResolutionScreen());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.close, color: Colors.black)),
          title: UIHelper().drawAppbarTitle(title: 'Confirm offer'),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Continue",
            callback: () async {
              verifyPending(true);
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    isAlreadySubmitted = true;
    return Container(
      child: ListView.builder(
          itemCount: confirmOfferController.listOffers.length,
          itemBuilder: (context, i) {
            final map = confirmOfferController.listOffers[i].obs;
            bool status = false;
            switch (i) {
              case 0:
                try {
                  status =
                      taskVerifyModel.userProfilePictureId > 0 ? true : false;
                } catch (e) {}
                break;
              case 1:
                try {
                  status = taskVerifyModel.paymentMethodVerificationId > 0
                      ? true
                      : false;
                } catch (e) {}
                break;
              case 2:
                try {
                  status = userData.userModel.dateofBirth == null
                      ? false
                      : userData.userModel.dateofBirth != ''
                          ? true
                          : false;
                } catch (e) {}
                break;
              case 3:
                try {
                  status = taskVerifyModel.eIDVerificationId > 0 ? true : false;
                } catch (e) {}
                break;
              default:
            }
            return Obx(() => Container(
                  child: ListTile(
                    onTap: () {
                      switch (i) {
                        case 0:
                          uploadProfilePicClicked(this.runtimeType);
                          break;
                        case 1:
                          paymentInfoClicked(this.runtimeType);
                          break;
                        case 2:
                          dobClicked(this.runtimeType);
                          break;
                        case 3:
                          Get.to(() => SelfiePage()).then((value) => appInit());
                          //uploadNIDClicked(this.runtimeType);
                          break;
                        default:
                      }
                    },
                    leading: Icon(map["icon"], color: MyTheme.hotdipPink),
                    title: Txt(
                        txt: map['title'],
                        txtColor: MyTheme.hotdipPink,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: status ? map['status_icon'] : SizedBox(),
                  ),
                ));
          }),
    );
  }

  drawNF() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  "assets/images/icons/ic_waiting_offers.png",
                  fit: BoxFit.cover,
                  width: getW(context) / 3.5,
                  height: getW(context) / 5,
                ),
              ),
              SizedBox(height: 10),
              Txt(
                  txt: "You have not yet received any Offer for this Task",
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
