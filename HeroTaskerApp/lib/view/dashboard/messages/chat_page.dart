import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/pubnub/PubnubMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../data/model/auth/UserModel.dart';
import 'chat_base.dart';

class ChatPage extends StatefulWidget {
  final bool isSupport;
  final bool isPublicChat;
  final String description;

  const ChatPage({
    Key key,
    this.isSupport,
    this.isPublicChat,
    this.description,
  }) : super(key: key);
  @override
  State createState() => _ChatPageState();
}

class _ChatPageState extends BaseChatStatefull<ChatPage> with APIStateListener {
  final TextEditingController textController = TextEditingController();
  ScrollController scrollController = new ScrollController();

  List<TimelinePostModel> listTimeLineModel = [];

  PubnubMgr pubnubMgr;

  //Timer timer;
  //static const int callTimelineSec = 10;
  //  page stuff start here
  bool isPageDone = false;
  bool isScrolling = false;
  bool isPosting = false;
  int pageStart = 1;
  int pageCount = AppConfig.page_limit;
  String msgTmp = '';

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_timeline_by_app &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final listTimeLineModel2 = (model as GetTimeLineByAppAPIModel)
                  .responseData
                  .timelinePosts;
              if (listTimeLineModel2.length > 0 &&
                  listTimeLineModel2.length != listTimeLineModel.length) {
                listTimeLineModel = listTimeLineModel2;
                setState(() {
                  if (listTimeLineModel.length < pageCount) {
                    pageStart = 1;
                    isPageDone = true;
                  }
                  if (!isScrolling) {
                    Future.delayed(const Duration(seconds: 1), () {
                      scrollController
                          .jumpTo(scrollController.position.maxScrollExtent);
                    });
                  }
                });
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostTimeline((model as MediaUploadFilesAPIModel));
          }
        }
      } else if (apiState.type == APIType.post_timeline &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final postModel = (model as TimelinePostAPIModel).responseData.post;
            final timelineId = postModel.id;

            try {
              final user = new UserModel(
                  id: userData.userModel.id, name: userData.userModel.name);
              final chatModel =
                  new ChatModel(timelinePostModel: postModel, user: user);
              pubnubMgr.postMessage(
                  chatModel: chatModel, receiverId: postModel.ownerId);
            } catch (e) {}

            wsGetTimelineByAPI();
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TIMELINE_EMAI_NOTI_GET_URL
                  .replaceAll("#timelineId#", timelineId.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  wsPostTimeline(MediaUploadFilesAPIModel model) async {
    try {
      //final jsonRes = json.encode(model.jsonRes);
      isPosting = true;
      var res = '';
      try {
        res = model.jsonRes.toString();
      } catch (e) {}
      final param = {
        "AdditionalAttributeValue": res ?? '',
        "Checkin": "",
        "FromLat": 0.0,
        "FromLng": 0.0,
        "IsPrivate": (widget.isSupport != null)
            ? false
            : (widget.isPublicChat == null)
                ? true
                : false,
        "Message": msgTmp,
        "OwnerId": userData.userModel.id,
        "PostTypeName": "status", //(url == null) ? "status" : "picture",
        "TaskId": (widget.isSupport != null) ? 0 : taskCtrl.getTaskModel().id,
      };
      myLog(json.encode(param));
      msgTmp = '';
      await APIViewModel().req<TimelinePostAPIModel>(
        context: context,
        apiState: APIState(APIType.post_timeline, this.runtimeType, null),
        url: APITimelineCfg.TIMELINE_POST_URL,
        isLoading: false,
        reqType: ReqType.Post,
        param: param,
      );
      isPosting = false;
    } catch (e) {}
  }

  wsGetTimelineByAPI() async {
    try {
      // userId, taskId. timeLineId, IsPrivate,Page,Count
      isLoading.value = true;
      final param = {
        "Count": (widget.isPublicChat == null) ? pageCount : 100,
        "CustomerId": (widget.isSupport != null) ? userData.userModel.id : 0,
        "IsPrivate": (widget.isSupport != null)
            ? false
            : (widget.isPublicChat == null)
                ? true
                : false,
        "Page": pageStart,
        "TaskId": (widget.isSupport != null) ? 0 : taskCtrl.getTaskModel().id,
        "timeLineId": 0,
      };
      myLog(param);
      await APIViewModel().req<GetTimeLineByAppAPIModel>(
        context: context,
        apiState: APIState(APIType.get_timeline_by_app, this.runtimeType, null),
        url: APITimelineCfg.GET_TIMELINE_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: param,
      );
      isLoading.value = false;
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimeLineModel = null;
    textController.dispose();
    scrollController.dispose();
    scrollController = null;

    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      pubnubMgr.destroy();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      try {
        _apiStateProvider = new APIStateProvider();
        _apiStateProvider.subscribe(this);
      } catch (e) {}

      await wsGetTimelineByAPI();

      var channelId = 0;
      if (widget.isPublicChat == null) {
        //  PRIVATE CHAT
        /*if (taskCtrl.getTaskModel().userId !=
                        userData.userModel.id) {
                      try {
                        channelId = model.responseData.taskBiddings[0].userId;
                      } catch (e) {}
                    } else {
                      channelId = taskCtrl.getTaskModel().userId;
                    }*/
        channelId = userData.userModel.id;
      } else {
        /*if (taskCtrl.getTaskModel().userId !=
                        userData.userModel.id) {
                      channelId = model.responseData.taskBiddings[0].taskId;
                    } else {
                      channelId = taskCtrl.getTaskModel().id;
                    }*/
        channelId = widget.isSupport != null
            ? userData.userModel.id
            : taskCtrl.getTaskModel().id;
      }
      pubnubMgr = PubnubMgr();
      await pubnubMgr.initPubNub(
          channelId: channelId,
          callback: (envelope) async {
            myLog(envelope.payload);
            if (!isPosting) {
              pageStart = 1;
              await wsGetTimelineByAPI();
            }
          });

      /*try {
        await APIViewModel().req<TaskBiddingsAPIModel>(
            context: context,
            url: APIMyTasksCfg.GET_TASKBIDDING_URL
                .replaceAll("/#taskBiddingId#", ""),
            reqType: ReqType.Get,
            param: {
              "Count": 1,
              "IsAll": true,
              "Page": 0,
              "TaskId": taskCtrl.getTaskModel().id,
              "userId": userData.userModel.id,
            },
            callback: (model) async {
              if (mounted && model != null) {
                if (model.success) {
                  var channelId = 0;
                  if (widget.isPublicChat == null) {
                    //  PRIVATE CHAT
                    /*if (taskCtrl.getTaskModel().userId !=
                        userData.userModel.id) {
                      try {
                        channelId = model.responseData.taskBiddings[0].userId;
                      } catch (e) {}
                    } else {
                      channelId = taskCtrl.getTaskModel().userId;
                    }*/
                    channelId = userData.userModel.id;
                  } else {
                    /*if (taskCtrl.getTaskModel().userId !=
                        userData.userModel.id) {
                      channelId = model.responseData.taskBiddings[0].taskId;
                    } else {
                      channelId = taskCtrl.getTaskModel().id;
                    }*/
                    channelId = taskCtrl.getTaskModel().id;
                  }
                  pubnubMgr = PubnubMgr();
                  await pubnubMgr.initPubNub(
                      channelId: channelId,
                      callback: (envelope) async {
                        myLog(envelope.payload);
                        if (!isPosting) {
                          pageStart = 1;
                          await wsGetTimelineByAPI();
                        }
                      });
                }
              }
            });
      } catch (e) {}*/

      //await pubnubMgr.getHistory(receiverId: 114453);
    } catch (e) {
      myLog(e.toString());
    }
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            isScrolling = true;
            if (!isPageDone && msgTmp == '') {
              pageStart++;
              wsGetTimelineByAPI();
            }
          } else if (scrollNotification is ScrollEndNotification) {
            isScrolling = false;
          }
          return true;
        },
        child: ListView.builder(
          controller: scrollController,
          //padding: new EdgeInsets.all(8.0),
          reverse: false,
          //shrinkWrap: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            return buildSingleMessage(listTimeLineModel, index, false);
          },
        ),
      ),
    );
  }

  Widget buildQList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            isScrolling = true;
            if (!isPageDone && msgTmp == '') {
              pageStart++;
              wsGetTimelineByAPI();
            }
          } else if (scrollNotification is ScrollEndNotification) {
            isScrolling = false;
          }
          return true;
        },
        child: ListView.builder(
          controller: scrollController,
          padding: new EdgeInsets.all(8.0),
          reverse: false,
          //shrinkWrap: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            final list = listTimeLineModel[index].userCommentPublicModelList;
            if (list != null) if (list.length > 0)
              return Column(
                children: [
                  buildSingleMessage(listTimeLineModel, index, true),
                  Padding(
                    padding: const EdgeInsets.only(left: 50),
                    child: buildQMessage(list[list.length - 1], index),
                  ),
                ],
              );
            else
              return SizedBox();
            else
              return SizedBox();
          },
        ),
      ),
    );
  }

  _chatTextArea() {
    if (taskCtrl == null) return SizedBox();
    if (taskCtrl.getStatusCode() == TaskStatusCfg.TASK_STATUS_PAYMENTED &&
        widget.isSupport == null) {
      return Container(
          color: MyTheme.hotdipPink.withAlpha(60),
          width: getW(context),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt: "Task completed. Private messages closed",
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ));
    } else if (taskCtrl.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_CANCELLED) {
      return Container(
          color: MyTheme.hotdipPink.withAlpha(60),
          width: getW(context),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt: "Task cancelled. Private messages closed",
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ));
    } else {
      TimelinePostModel timelineModel;
      try {
        timelineModel = listTimeLineModel[index2];
      } catch (e) {}
      return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: drawMessageBox(
          context: context,
          picUrl: null,
          hintText: 'Type a message',
          iconSize: 25,
          iconLeft: Icon(
            Icons.add,
            color: Colors.green,
          ),
          iconRight: Icon(
            Icons.send,
            color: Colors.white,
          ),
          textController: textController,
          callbackCam: (File file, String txt) async {
            //textController.clear();
            msgTmp = txt.trim();
            msgTmp = Common.isNotAllowdPhoneNumber(msgTmp);
            msgTmp = Common.isNotAllowdEmailAddress(msgTmp);
            //final str = isNotAllowdEmailAddress(msgTmp);
            //print(str);
            //return;

            if (file != null) {
              await APIViewModel().upload(
                context: context,
                apiState:
                    APIState(APIType.media_upload_file, this.runtimeType, null),
                file: file,
              );
            } else {
              FocusScope.of(context).requestFocus(FocusNode());
              if (msgTmp.isNotEmpty) {
                //Add the message to the list
                if (timelineModel != null) {
                  String formattedDate =
                      DateFormat('dd-MMM-yyyy').format(DateTime.now());
                  final TimelinePostModel timeLinePostModel =
                      TimelinePostModel();
                  timeLinePostModel.ownerId =
                      userData.userModel.id; //timelineModel.ownerId;
                  timeLinePostModel.ownerImageUrl = userData
                      .userModel.profileImageUrl; //timelineModel.ownerImageUrl;
                  timeLinePostModel.additionalAttributeValue = '';
                  timeLinePostModel.message = msgTmp;
                  timeLinePostModel.dateCreatedUtc = formattedDate;
                  listTimeLineModel.add(timeLinePostModel);
                  scrollDown(scrollController,
                      getH(context) * AppConfig.chatScrollHeight);
                }
                textController.clear();
                setState(() {});
                Future.delayed(Duration(seconds: 1), () async {
                  pageStart = 1;
                  wsPostTimeline(null);
                });
              }
            }
          },
        ),
      );
    }
  }

  isNotAllowdEmailAddress(String str) {
    final repStr = '*';
    final listSkipWord = [
      "at the rate",
      "(at the rate)",
      "attherate",
      "@",
      "dotcom",
      "dotnet",
      "mail",
      "gmail",
      "yahoo",
      "yopmail",
      "msn",
      "outlook",
      "proton",
      "aol",
      "zoho",
      "icloud",
      "gmx",
      "tutanota",
      "inbox",
      "startmail",
      "thexyz",
      "dot ",
      " com",
      ".com",
      ".net",
      " one ",
      " two ",
      " three ",
      " four ",
      " five ",
      " six ",
      " seven ",
      " eight ",
      " nine ",
      " ten ",
      " eleven ",
      " twelve ",
      " thirteen ",
      " fourteen ",
      " fifteen ",
      " sixteen ",
      " seventeen ",
      " eighteen ",
      " nineteen ",
      " twenty ",
      " thirty ",
      " fourty ",
      " fifty ",
      " sixty ",
      " seventy ",
      " eighty ",
      " ninty ",
      " hundred ",
      " i ",
      "ii",
      " iii ",
      " iv ",
      " v ",
      " vi ",
      " vii ",
      " viii ",
      " ix ",
      " x "
    ];
    //  EMAIL PARSER  **********************************************
    try {
      //  checking by email address
      final mailPattern = r"\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b";
      final regEx = RegExp(mailPattern, multiLine: true);
      final obtainedMail =
          regEx.allMatches(str).map((m) => m.group(0)).join(' ');
      print(obtainedMail);
      final star = List.filled(obtainedMail.length, repStr).join();
      if (obtainedMail.isNotEmpty)
        return str.replaceAll(obtainedMail, star).trim();
    } catch (e) {}
    try {
      //if (listSkipWord.contains(str)) {
      //str = str.replaceAll("*", str);
      //}

      var ss = str.toLowerCase().trim();
      for (String s in listSkipWord) {
        //bool isFound = false;
        String regex = r'[~!@#$%^&*()_+`{}|<>?;:./,=\-\[\]\(\)]';
        final s2 = ss.replaceAll(RegExp(regex, unicode: true), '').trim();
        if (s2.toLowerCase().contains(s.toLowerCase())) {
          ss = s2.replaceAll(s.toLowerCase().trim(), "*");
        }
        print(ss);
        /*for (final m in listSkipWord) {
          if (m.contains(s2)) {
            //ss += List.filled(s2.length, repStr).join();
            ss += ss.replaceAll("*", m);
            isFound = true;
            break;
          }
        }
        if (!isFound) ss += (s + " ");*/
      }
      if (ss.isNotEmpty) return ss.trim();
    } catch (e) {}
    return str.trim();
  }

  isNotAllowdPhoneNumber(String str) {
    //  PHONE PARSER  **********************************************
    final num = str.replaceAll(RegExp(r'(\D+)'), '');
    if (num.length > 2) {
      //final star = List.filled(num.length, "*").join();
      return str.replaceAll(RegExp(r'[0-9]'), "*").trim();
    } else {
      return str;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(
              title: widget.isPublicChat != null
                  ? 'Questions'
                  : 'Private messages'),
          centerTitle: false,
          bottom: drawAppbarNavBar(
              isSupport: widget.isSupport, isPublicChat: widget.isPublicChat),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          widget.isPublicChat != null ? buildQList() : buildMessageList(),
          //Divider(height: 1.0),
          _chatTextArea(),
        ],
      ),
    );
  }
}
