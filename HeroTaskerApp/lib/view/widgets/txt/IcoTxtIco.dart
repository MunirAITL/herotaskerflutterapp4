import 'package:aitl/main.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

class IcoTxtIco extends StatelessWidget with Mixin {
  final IconData leftIcon;
  final IconData rightIcon;
  final String txt;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height;
  double topbotHeight;
  Color iconColor;
  Color txtColor;
  Color bgColor;
  double borderWidth;

  IcoTxtIco({
    Key key,
    @required this.txt,
    @required this.leftIcon,
    @required this.rightIcon,
    this.iconColor = Colors.grey,
    this.txtColor = Colors.black87,
    this.txtAlign = TextAlign.center,
    this.leftIconSize,
    this.rightIconSize,
    this.height = 10,
    this.topbotHeight = 0,
    this.borderWidth = .5,
    this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (leftIconSize == null) {
      if (isIPAD || isTablet) {
        leftIconSize = 40;
      } else {
        leftIconSize = 20;
      }
    }
    if (rightIconSize == null) {
      if (isIPAD || isTablet) {
        rightIconSize = 50;
      } else {
        rightIconSize = 30;
      }
    }

    return Container(
      //height: getHP(context, MyTheme.btnHpa),
      decoration: borderWidth > 0
          ? BoxDecoration(
              color: this.bgColor,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.grey,
                width: borderWidth,
              ),
            )
          : BoxDecoration(
              color: this.bgColor,
              borderRadius: BorderRadius.circular(10),
            ),
      alignment: Alignment.center,
      child: Container(
        //color: (topbotHeight > 0) ? Colors.white : null,
        child: Padding(
          padding: EdgeInsets.only(
              left: (leftIcon != null) ? 10 : 0,
              right: 0,
              top: topbotHeight,
              bottom: topbotHeight),
          child: Row(
            //dense: true,
            //contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
            children: [
              (leftIcon != null)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Icon(
                        leftIcon,
                        color: iconColor,
                        size: leftIconSize,
                      ),
                    )
                  : SizedBox(),
              Expanded(
                child: Txt(
                  txt: txt,
                  txtColor: (txt.toLowerCase().contains("date"))
                      ? Colors.grey
                      : Colors.black,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
              ),
              (rightIcon != null)
                  ? Icon(
                      rightIcon,
                      color: iconColor,
                      size: rightIconSize,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
