import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';

import '../../../main.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;

  TCView({@required this.screenName});

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text:
                      "By clicking you confirm that you accept the Herotasker ",
                  style: TextStyle(
                    height: MyTheme.txtLineSpace,
                    color: Colors.black,
                    fontSize: MyTheme.fontSize,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Terms and Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            decoration: TextDecoration.underline,
                            decorationThickness: 2,
                            color: MyTheme.brandColor,
                            fontSize: MyTheme.fontSize,
                            fontWeight: FontWeight.w500),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Privacy",
                                url: ServerUrls.TC_URL,
                              ),
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
