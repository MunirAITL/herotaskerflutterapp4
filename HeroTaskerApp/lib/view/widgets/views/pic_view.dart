import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';
import '../../../main.dart';

class PicFullView extends StatelessWidget with Mixin {
  final String url;
  final String title;
  const PicFullView({Key key, @required this.url, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, size: MyTheme.fontSize),
            onPressed: () {
              Get.back();
            },
          ),
          title: UIHelper().drawAppbarTitle(title: title),
          centerTitle: false,
        ),
        body: drawLayout(context),
      ),
    );
  }

  drawLayout(context) {
    return Container(
      width: getW(context),
      height: getH(context),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: CachedNetworkImageProvider(
            url,
          ),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
