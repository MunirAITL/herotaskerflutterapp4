import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/noti/noti_settings_page.dart';
import 'package:aitl/view/dashboard/more/profile/edit_profile_page.dart';
import 'package:aitl/view/dashboard/more/settings/change_pwd_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'myprice_settings_page.dart';

class SettingsPage extends StatelessWidget with Mixin, UIHelper {
  SettingsPage({Key key}) : super(key: key);

  final List<Map<String, dynamic>> listMore = [
    {
      "title": "Edit account",
      "icon": "assets/images/more/ico/edit_acc_ico.png",
      "route": () => EditProfilePage()
    },
    {
      "title": "Notification settings",
      "icon": "assets/images/more/ico/noti_set_ico.png",
      "route": () => NotiSettingsPage()
    },
    {
      "title": "Change password",
      "icon": "assets/images/more/ico/change_pwd_ico.png",
      "route": () => ChangePwdScreen()
    },
    {
      "title": "My price settings",
      "icon": "assets/images/more/ico/my_prce_set_ico.png",
      "route": () => MyPriceSettingsPage()
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: 'Settings'),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: ListView.builder(
          itemCount: listMore.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> mapMore = listMore[index];
            return GestureDetector(
              onTap: () async {
                if (mapMore['route'] != null) {
                  Get.to(mapMore['route']);
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.asset(
                            mapMore['icon'],
                            width: 25,
                            height: 25,
                            fit: BoxFit.cover,
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: Txt(
                                txt: mapMore['title'].toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: MyTheme.gray3Color,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    drawLine(),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
