import 'UserRatingsModel.dart';
import 'package:equatable/equatable.dart';

class UserRatingAPIModel extends Equatable {
  bool success;
  ResponseData responseData;
  ErrorMessages errorMessages;
  Messages messages;

  UserRatingAPIModel({this.success, this.responseData});

  UserRatingAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }

  @override
  List<Object> get props => [success, responseData, errorMessages, messages];
}

class ResponseData {
  List<UserRatingsModel> userRatings;
  ResponseData({this.userRatings});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserRatings'] != null) {
      userRatings = [];
      json['UserRatings'].forEach((v) {
        try {
          userRatings.add(new UserRatingsModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userRatings != null) {
      data['UserRatings'] = this.userRatings.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}
