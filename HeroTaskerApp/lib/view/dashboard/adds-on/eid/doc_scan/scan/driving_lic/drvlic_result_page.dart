import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/ads-on/eid/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import '../result_base.dart';

class DrvLicResultPage extends StatefulWidget {
  final PostDocVerifyAPIModel model;
  const DrvLicResultPage({Key key, @required this.model}) : super(key: key);
  @override
  _DrvLicResultPageState createState() => _DrvLicResultPageState();
}

class _DrvLicResultPageState extends ResultBase<DrvLicResultPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title:
              UIHelper().drawAppbarTitle(title: "Identification of a person"),
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          centerTitle: false,
        ),
        body: drawLayout(eScanDocType.DL, widget.model),
      ),
    );
  }
}
