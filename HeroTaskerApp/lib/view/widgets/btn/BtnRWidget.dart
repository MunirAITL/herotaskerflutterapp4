import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

class BtnRWidget extends StatelessWidget with Mixin {
  final String txt;
  Color txtColor;
  Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Widget lWid;
  final Widget rWid;
  final Function callback;

  BtnRWidget({
    Key key,
    @required this.txt,
    @required this.width,
    @required this.height,
    this.radius = 20,
    @required this.callback,
    this.txtColor = Colors.white,
    this.bgColor,
    this.lWid,
    this.rWid,
  }) {
    if (bgColor == null) bgColor = MyTheme.btnColor;
    if (txtColor == null) txtColor = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        width: width,
        height: (height != null) ? height : getHP(context, 6),
        alignment: Alignment.center,
        //color: MyTheme.brownColor,
        decoration: new BoxDecoration(
          color: bgColor,
          borderRadius: new BorderRadius.circular(radius),
        ),
        child: Row(
          children: [
            lWid != null
                ? Flexible(
                    child: lWid,
                  )
                : SizedBox(),
            Expanded(
              flex: 7,
              child: Txt(
                txt: txt,
                txtColor: txtColor,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            rWid != null
                ? Flexible(
                    child: rWid,
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
