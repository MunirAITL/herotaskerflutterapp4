import 'dart:io';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIActionReqCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/action_req/GetTaskVerifyAPIModel.dart';
import 'package:aitl/data/model/dashboard/action_req/TaskerProfileVerificationData.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/misc/ActionReqPage.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/dashboard/post_task/all_cat/all_cat_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/action_req/action_req_helper.dart';
import 'package:aitl/view_model/helper/utils/PostTaskHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:launch_review/launch_review.dart';
import 'package:new_version/new_version.dart';
import '../../../config/app/status/TaskStatusCfg.dart';
import '../../../config/server/APIPostTaskCfg.dart';
import '../../../data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import '../../../data/model/dashboard/posttask/TaskModel.dart';
import '../../../data/network/ModelMgr.dart';
import '../../../data/network/NetworkMgr.dart';
import '../../../main.dart';
import '../../../view_model/api/api_view_model.dart';
import '../mytasks/mytasks_base.dart';

class PostTaskListPage extends StatefulWidget {
  @override
  State createState() => _PostTaskListPageState();
}

class _PostTaskListPageState extends BaseMyTaskStatefull<PostTaskListPage>
    with StateListener, APIStateListener {
  List<TaskModel> listTaskModel = [];
  TaskerProfileVerificationData taskVerifyModel;

  bool isLoading = true;
  var isShowActionReqBtn = false.obs;
  int taskLen = 2;

  @override
  void onDetached() {
    // TODO: implement onDetached
  }

  @override
  void onInactive() {
    // TODO: implement onInactive
  }

  @override
  void onPaused() {
    // TODO: implement onPaused
  }

  @override
  void onResumed() {
    // TODO: implement onResumed
  }

  checkNewerVersion() async {
    // Instantiate NewVersion manager object (Using GCP Console app as example)
    if (!Server.isTest) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (mounted) {
          final newVersion = NewVersion(
            iOSId: 'com.herotasker',
            androidId: 'com.herotasker',
          );
          newVersion.showAlertIfNecessary(context: context);
        }
      });
    }
  }

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        refreshData();
      } else if (state == ObserverState.STATE_RELOAD_TAB_NEW_TASK) {
        await refreshData();
        checkNewerVersion();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.taskinfo_search &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          final List<dynamic> locations = model.responseData.locations;
          if (locations != null) {
            try {
              listTaskModel.clear();
              int i = 0;
              for (TaskModel task in locations) {
                if (TaskStatusCfg().getSatusCode(task.status) ==
                        TaskStatusCfg.TASK_STATUS_ACTIVE ||
                    TaskStatusCfg().getSatusCode(task.status) ==
                        TaskStatusCfg.TASK_STATUS_ACCEPTED)
                  listTaskModel.add(task);
                i++;
                if (i > taskLen) break;
              }
              setState(() {});
            } catch (e) {
              myLog(e.toString());
            }
          } else {}
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  onMyTaskAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await APIViewModel().req<TaskInfoSearchAPIModel>(
        context: context,
        apiState: APIState(APIType.taskinfo_search, this.runtimeType, null),
        url: APIPostTaskCfg.TASKINFOBYSEARCH_POST_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": 50,
          "Distance": 500,
          "FromPrice": 20,
          "InPersonOrOnline": 0,
          "IsHideAssignTask": false,
          "Latitude": 0.0,
          "Location": "",
          "Longitude": 0.0,
          "Page": 0,
          "SearchText": '',
          "Status": TaskStatusCfg.TASK_STATUS_ALL,
          "ToPrice": 100000,
          "UserId": userData.userModel.id,
        },
      );
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  //  Action Required Start...

  Future<void> refreshData() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        await APIViewModel().req<GetTaskVerifyAPIModel>(
            context: context,
            url: APIActionReqCfg.GET_TASK_VERIFY_URL +
                userData.userModel.id.toString(),
            reqType: ReqType.Get,
            callback: (model) {
              if (mounted && model != null) {
                if (model.success) {
                  try {
                    taskVerifyModel =
                        model.responseData.taskerProfileVerificationData[0];
                    if (taskVerifyModel.eIDVerificationId > 0 &&
                        taskVerifyModel.emailVerificationId > 0 &&
                        taskVerifyModel.mobileVerificationId > 0 &&
                        taskVerifyModel.paymentMethodVerificationId > 0 &&
                        taskVerifyModel.taskAlertId > 0 &&
                        taskVerifyModel.userPortfulioId > 0 &&
                        taskVerifyModel.userProfilePictureId > 0 &&
                        taskVerifyModel.userSkillId > 0) {
                      isShowActionReqBtn.value = false;
                    } else {
                      isShowActionReqBtn.value = true;
                    }
                  } catch (e) {}
                }
              }
            });
        await onMyTaskAPI();
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    listTaskModel = null;
    taskVerifyModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    await refreshData();
    await checkNewerVersion();
  }

  @override
  Widget build(BuildContext context) {
    final xtraH = isIPAD || isTablet ? 20 : 0;

    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: 0,
          automaticallyImplyLeading: false,
          //title: UIHelper().drawAppbarTitle(title: 'Create a Task'),
          centerTitle: false,
          actions: <Widget>[],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(AppConfig.post_add_height + xtraH),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, top: 15, bottom: 20, right: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Txt(
                            txt: "Create a Task",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isOverflow: true,
                            isBold: true),
                      ),
                      Flexible(
                        child: GestureDetector(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Container(
                                  padding: EdgeInsets.all(2),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: MyTheme.redColor),
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                    size: MyTheme.fontSize + 3,
                                  )),
                            ),
                            onTap: () {
                              //openUrl(context, APIYoutubeCfg.HELP_YOUTUBE_URL);
                              Get.to(() => AddTask1Screen(
                                    userModel: userData.userModel,
                                    categoryPH:
                                        'E.g. Suggest a name for my new company etc.',
                                  ));
                            }),
                      )
                    ],
                  ),
                ),
                (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : Container(
                        height: .1,
                        color: Colors.transparent,
                      )
              ],
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawBtns(),
            //drawCreditReport(),
            drawRecentTasks(),
            drawTaskList(),
            Padding(
                padding: const EdgeInsets.only(bottom: 30, left: 20, right: 20),
                child: drawHireWorkButtons()),
          ],
        ),
      ),
    );
  }

  drawBtns() => Obx(() => isShowActionReqBtn.value
      ? Column(
          children: [
            SizedBox(height: 20),
            MMBtn(
                txt: "Action Required",
                ico: Icon(Icons.info_outline, color: Colors.white, size: 25),
                width: getWP(context, 90),
                radius: 10,
                height: getHP(context, 6.5),
                callback: () {
                  Get.to(() => ActionReqPage(taskVerifyModel: taskVerifyModel));
                }),
            SizedBox(height: 20),
          ],
        )
      : SizedBox());

  drawRecentTasks() {
    if (listTaskModel.length == 0) return SizedBox();
    return Container(
      child: Padding(
        padding:
            const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
              txt: "Recent Tasks",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.left,
              //fontWeight: FontWeight.w600,
              isBold: true,
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: .5),
                  borderRadius: BorderRadius.all(Radius.circular(
                          10) //                 <--- border radius here
                      )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ListView.builder(
                    primary: false,
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: listTaskModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawItem(listTaskModel[index], true);
                    },
                  ),

                  /*isMyCasesFound
                      ? OutlinedButton(
                          onPressed: () {
                            StateProvider().notify(
                                ObserverState.STATE_RELOAD_TAB_MY_TASKS, null);
                          },
                          child: Text(
                            'Show More',
                            style: TextStyle(
                                color: Color(0xFF28415F), fontSize: 12),
                          ),
                          style: OutlinedButton.styleFrom(
                            minimumSize: Size.zero, // <-- Add this
                            padding: EdgeInsets.all(5),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            side: BorderSide(width: .5, color: Colors.grey),
                          ),
                        )
                      : SizedBox(),*/
                ],
              ),
            ),
            listTaskModel.length >= taskLen
                ? GestureDetector(
                    onTap: () {
                      StateProvider().notify(ObserverState.STATE_RELOAD_TAB, 1);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          'Show More',
                          textAlign: TextAlign.end,
                          style:
                              TextStyle(color: Color(0xFF28415F), fontSize: 14),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  drawTaskList() {
    final w = getW(context) / (isIPAD || isTablet ? 5 : 4.5);
    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      //padding: const EdgeInsets.only(top: 40, bottom: 40),
      scrollDirection: Axis.vertical,
      itemCount: PostTaskHelper.listTask.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 0,
          mainAxisSpacing: 0,
          mainAxisExtent: w + (w * .6)),
      //childAspectRatio: width / (height / 1.6)),

      /*SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
        crossAxisCount: 3,
        crossAxisSpacing: 0,
        mainAxisSpacing: 0,
        height: getHP(context, isIPAD || isTablet ? 22 : 20),
      ),*/ //, childAspectRatio: getHP(context, .12)),
      itemBuilder: (context, index) {
        final map = PostTaskHelper.listTask[index];
        final icon = map["icon"];
        String title = map["title"];
        final isCat = map['isCat'];

        return GestureDetector(
          onTap: () async {
            if (!isCat) {
              final playstore_appId = map['playstore_appId'].toString();
              final appstore_appId = map['appstore_appId'].toString();
              if (playstore_appId.isNotEmpty || appstore_appId.isNotEmpty) {
                LaunchReview.launch(
                    androidAppId: playstore_appId, iOSAppId: appstore_appId);
              } else {
                Get.to(
                  () => AddTask1Screen(
                    index: index,
                    userModel: userData.userModel,
                  ),
                ).then((pageNo) {
                  obsUpdateTabs(pageNo);
                });
              }
            } else {
              Get.to(
                () => AllCatPage(),
              ).then((value) {
                if (value != null) {
                  Get.to(
                    () => AddTask1Screen(
                      index: null,
                      userModel: userData.userModel,
                      categoryPH: value,
                    ),
                  ).then((pageNo) {
                    obsUpdateTabs(pageNo);
                  });
                }
              });
            }
          },
          child: Container(
            //elevation: 0,
            color: MyTheme.bgColor,
            margin: new EdgeInsets.all(10),
            //color: Colors.black,
            //width: w,
            // height: w + 50,
            child: new Column(
              //mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: w - 15,
                  height: w - 15,
                  //color: Colors.black,
                  child: SvgPicture.asset(
                    icon,
                    //width: w,
                    //height: w,
                    //fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 5),
                Expanded(
                  flex: 2,
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: MyTheme.gray5Color, //MyTheme.iconTextColor,
                      fontSize:
                          MyTheme.fontSize - 5, //(title.length > 25) ? 11 : 14,
                    ),
                  ),
                ),
                //just for testing, will fill with image later
              ],
            ),
          ),
        );
      },
    );
  }

  drawHireWorkButtons() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Flexible(
            child: MMBtn(
              txt: "I want to work",
              //txtColor: Colors.white,
              //bgColor: accentColor,
              //width: getWP(context, 48),
              height: getHP(context, MyTheme.btnHpa + 1),
              radius: 20,
              callback: () {
                if (userData.communityId != 2) {
                  //Get.to(() => OffersCommentsPage(taskBiddingsModel: null));
                  StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
                      DashboardPage.TAB_FINDWORKS);
                } else {
                  StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
                      DashboardPage.TAB_FINDWORKS);
                }
              },
            ),
          ),
          SizedBox(width: 10),
          Flexible(
            child: MMBtn(
              txt: "I want to hire",
              //txtColor: Colors.white,
              //bgColor: accentColor,
              //width: getWP(context, 48),
              height: getHP(context, MyTheme.btnHpa + 1),
              radius: 20,
              callback: () {
                Get.to(() => AddTask1Screen(
                      userModel: userData.userModel,
                      categoryPH: 'E.g. Suggest a name for my new company etc.',
                    ));
              },
            ),
          ),
        ],
      ),
    );
  }

  /*drawCreditReport() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        //width: getW(context),
        child: MMBtnLeftIconDashBoard(
          imageFile: 'assets/images/adds-on/my_credit_icon.png',
          bgColor: HexColor.fromHex("#E4E6EB"),
          txt: "My credit report",
          height: getHP(context, 6),
          width: null, //getW(context),
          radius: 0,
          callback: () async {
            if (!Server.isOtp) {
              scanDocData.file_selfie = await ImageLib()
                  .getImageFileFromAssets(path: "eid_scan", img: "dada.png");
              Get.to(() => UploadingPage());
            } else {
              final androidAppId = "uk.co.mortgagemagic.client";
              final appstoreAppId = "1574687745";
              final appstoreAppName =
                  "mortgagemagic.clients"; //  Mortgage Magic
              if (Platform.isAndroid) {
                await LaunchApp.openApp(
                  androidPackageName: androidAppId,
                  iosUrlScheme: appstoreAppName + "://",
                  appStoreLink:
                      'itms-apps://itunes.apple.com/us/app/$appstoreAppName/id' +
                          appstoreAppId,
                );
              } else if (Platform.isIOS) {
                final platform =
                    const MethodChannel('flutter.native/herotasker');
                final param = {
                  'appstoreAppId': appstoreAppId,
                  'appstoreAppName': appstoreAppName,
                  'appName': 'Mortgage Magic'
                };
                await platform.invokeMethod('openMM3App', param);
              }
            }
          },
        ),
      ),
    );
  }*/

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return MyTheme.redColor;
  }
}
