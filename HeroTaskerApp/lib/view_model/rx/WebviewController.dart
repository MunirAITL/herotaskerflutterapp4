import 'package:get/get.dart';

class WebviewController extends GetxController {
  var progress = 0.obs;
  var isLoading = false.obs;

  //  others
  var appTitle = "".obs;
}
