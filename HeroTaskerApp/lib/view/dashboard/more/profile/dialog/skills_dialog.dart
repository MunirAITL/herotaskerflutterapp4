import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/profile/add_skills_page.dart';
import 'package:aitl/view/widgets/btn/MMArrowBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SkillsDialog extends StatefulWidget {
  const SkillsDialog({Key key}) : super(key: key);

  @override
  State<SkillsDialog> createState() => _SkillsDialogState();
}

class _SkillsDialogState extends State<SkillsDialog> with Mixin {
  //  portfolio upload file list
  AboutModel aboutModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    wsGetAboutSkills();
  }

  wsGetAboutSkills() async {
    try {
      await APIViewModel().req<AboutAPIModel>(
          context: context,
          url: APIProfileCFg.ABOUT_GET_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get,
          callback: (model) {
            if (model != null && mounted) {
              if (model.success) {
                aboutModel = model.responseData.about;
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: getH(context),
        width: getW(context),
        color: Colors.white,
        child: aboutModel != null
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: MyTheme.appbarColor,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Flexible(
                              child: IconButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  icon: Icon(Icons.arrow_back_ios,
                                      color: Colors.black))),
                          Txt(
                              txt: "Skills",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Card(
                    elevation: 0,
                    color: Colors.white,
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _drawSkillRow(
                              "Transportation", aboutModel.goAround ?? ''),
                          _drawSkillRow(
                              "Languages", aboutModel.languages ?? ''),
                          _drawSkillRow(
                              "Education", aboutModel.qualifications ?? ''),
                          _drawSkillRow("Work", aboutModel.experiences ?? ''),
                          _drawSkillRow("Specialities",
                              aboutModel.whatIamlookingfor ?? ''),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : SizedBox(),
      ),
    );
  }

  _drawSkillRow(String title, String skills) {
    return GestureDetector(
      onTap: () {
        var hint = "";
        switch (title) {
          case "Transportation":
            hint = "Select your means of transport?";
            break;
          case "Languages":
            hint = "What languages you're versed in?";
            break;
          case "Education":
            hint = "Where have you studied?";
            break;
          case "Work":
            hint = "Where have you previously worked?";
            break;
          case "Specialities":
            hint = "What tasks are you great at?";
            break;
          default:
        }
        Get.to(() => AddSkillsPage(
              aboutModel: aboutModel,
              skills: skills,
              title: title,
              hint: hint,
            )).then((value) => {wsGetAboutSkills()});
      },
      child: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(left: 10, bottom: 10),
          child: Column(
            children: [
              ListTile(
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Txt(
                        txt: title,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(width: 10),
                    Expanded(
                      child: Txt(
                          txt: skills.replaceAll("|", ", "),
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.end,
                          isOverflow: true,
                          isBold: false),
                    ),
                    SizedBox(width: 5),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: MyTheme.gray3Color,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
