import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/switchview/SwitchBar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/rx/FiltersController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

import '../../../view/widgets/slider/SliderThumbShape.dart';

class FiltersHelper {
  drawDistanceSlider(FiltersController filterController, Function callback) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                  txt: 'Distance',
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              Obx(
                () => Txt(
                    txt: filterController.distanceTxt.value,
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
          SfSliderTheme(
            data: SfSliderThemeData(
              activeTrackHeight: 2,
              inactiveTrackHeight: 1,
            ),
            child: SfSlider(
              thumbShape: SliderThumbShape(),
              activeColor: MyTheme.redColor.withOpacity(.5),
              inactiveColor: Colors.grey,
              value: filterController.distance.value,
              min: 0.0,
              max: 6.0,
              stepSize: 1,
              onChanged: (newValue) {
                double v = newValue;
                filterController.distance.value = v.round();
                filterController
                    .setDistanceTxt(filterController.distance.value);
                callback();
              },
            ),
          ),

          /*FlutterSlider(
            values: [filterController.distance.value.toDouble()],
            max: 6,
            min: 0,
            tooltip: FlutterSliderTooltip(
                textStyle: TextStyle(color: Colors.transparent),
                boxStyle: FlutterSliderTooltipBox(
                    decoration: BoxDecoration(color: Colors.transparent))),
            onDragging: (handlerIndex, lowerValue, upperValue) {
              filterController.distance.value = (lowerValue as num).toInt();
              filterController.setDistanceTxt(filterController.distance.value);
            },
            trackBar: FlutterSliderTrackBar(
              activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.grey.withOpacity(0.5)),
            ),
            handler: FlutterSliderHandler(
              decoration: BoxDecoration(),
              child: Icon(
                Icons.circle,
                size: 20,
                color: Colors.grey,
              ),
            ),
          )*/
        ],
      ),
    );
  }

  drawPriceSlider(FiltersController filterController, Function callback) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                  txt: 'Price',
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              Obx(
                () => Txt(
                    txt: filterController.priceTxt.value,
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
          SfRangeSliderTheme(
            data: SfRangeSliderThemeData(
              activeTrackHeight: 2,
              inactiveTrackHeight: 1,
              //thumbRadius: 15,
              //thumbStrokeWidth: 0,
            ),
            child: SfRangeSlider(
              thumbShape: SliderThumbShape(),
              activeColor: MyTheme.redColor.withOpacity(.5),
              inactiveColor: Colors.grey,
              interval: 1.0, //filterController.distance.value.toDouble(),
              values: SfRangeValues(filterController.minPrice.value.toDouble(),
                  filterController.maxPrice.value.toDouble()),
              min: 0.0,
              max: 9.0,
              stepSize: 1,
              minorTicksPerInterval: 1,
              onChanged: (newValue) {
                filterController.minPrice.value =
                    (newValue.start as num).toInt();
                filterController.maxPrice.value = (newValue.end as num).toInt();
                filterController.setPriceTxt(filterController.minPrice.value,
                    filterController.maxPrice.value);
                callback();
              },
            ),
          ),

          /*FlutterSlider(
            values: [
              filterController.minPrice.value.toDouble(),
              filterController.maxPrice.value.toDouble()
            ],
            max: 9,
            min: 0,
            rangeSlider: true,
            tooltip: FlutterSliderTooltip(
                textStyle: TextStyle(color: Colors.transparent),
                boxStyle: FlutterSliderTooltipBox(
                    decoration: BoxDecoration(color: Colors.transparent))),
            onDragging: (handlerIndex, lowerValue, upperValue) {
              filterController.minPrice.value = (lowerValue as num).toInt();
              filterController.maxPrice.value = (upperValue as num).toInt();
              filterController.setPriceTxt(filterController.minPrice.value,
                  filterController.maxPrice.value);
            },
            trackBar: FlutterSliderTrackBar(
              activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.grey.withOpacity(0.5)),
            ),
            handler: FlutterSliderHandler(
              decoration: BoxDecoration(),
              child: Icon(
                Icons.circle,
                size: 20,
                color: Colors.grey,
              ),
            ),
            rightHandler: FlutterSliderHandler(
              decoration: BoxDecoration(),
              child: Icon(
                Icons.circle,
                size: 20,
                color: Colors.grey,
              ),
            ),
          )*/
        ],
      ),
    );
  }

  drawAvailableTaskOnly(FiltersController filterController) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
              txt: 'Available tasks only',
              txtColor: MyTheme.gray5Color.withOpacity(.8),
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Txt(
                    txt: 'Hide tasks that are already assigned',
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Flexible(
                child: Container(
                  width: 80,
                  child: SwitchBar(
                    activeColor: MyTheme.redColor,
                    value: filterController.isAvailableTasksOnly.value,
                    onChanged: (value) {
                      filterController.isAvailableTasksOnly.value = value;
                    },
                  ),
                ),
              )
              /*SwitchBar(
                bgColor: Colors.grey.withOpacity(.5),
                thumbColor: Colors.grey.withOpacity(.9),
                thumbActiveColor: Colors.black,
                value: filterController.isAvailableTasksOnly.value,
                onChanged: (v) {
                  filterController.isAvailableTasksOnly.value = v;
                },
              )*/
            ],
          ),
        ],
      ),
    );
  }
}
