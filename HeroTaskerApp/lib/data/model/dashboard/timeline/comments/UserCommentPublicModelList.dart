import '../../../auth/UserModel.dart';

class UserCommentPublicModelList {
  String additionalData;
  bool canDelete;
  String commentText;
  String dateCreated;
  String dateCreatedUtc;
  int entityId;
  String entityName;
  int id;
  bool isSpam;
  int likeCount;
  int likeStatus;
  UserModel user;

  UserCommentPublicModelList(
      {this.additionalData,
      this.canDelete,
      this.commentText,
      this.dateCreated,
      this.dateCreatedUtc,
      this.entityId,
      this.entityName,
      this.id,
      this.isSpam,
      this.likeCount,
      this.likeStatus,
      this.user});

  UserCommentPublicModelList.fromJson(Map<String, dynamic> json) {
    additionalData = json['AdditionalData'] ?? '';
    canDelete = json['CanDelete'] ?? false;
    commentText = json['CommentText'] ?? '';
    dateCreated = json['DateCreated'] ?? '';
    dateCreatedUtc = json['DateCreatedUtc'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    id = json['Id'] ?? 0;
    isSpam = json['IsSpam'] ?? false;
    likeCount = json['LikeCount'] ?? 0;
    likeStatus = json['LikeStatus'] ?? 0;
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AdditionalData'] = this.additionalData;
    data['CanDelete'] = this.canDelete;
    data['CommentText'] = this.commentText;
    data['DateCreated'] = this.dateCreated;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['Id'] = this.id;
    data['IsSpam'] = this.isSpam;
    data['LikeCount'] = this.likeCount;
    data['LikeStatus'] = this.likeStatus;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
