import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';

class TaskerScorePage extends StatefulWidget {
  const TaskerScorePage({Key key}) : super(key: key);

  @override
  State<TaskerScorePage> createState() => _TaskerScorePageState();
}

class _TaskerScorePageState extends State<TaskerScorePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Tasker Score Rating"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _drawBadgeView(),
          _drawScoreView(),
          _drawHelpWorks(),
          _drawNextTierView(),
        ],
      ),
    );
  }

  _drawBadgeView() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(child: Icon(Icons.badge_outlined, size: 50)),
              SizedBox(width: 20),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Txt(
                            txt: "Bronze",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: true,
                          ),
                        ),
                        SizedBox(width: 5),
                        Flexible(
                          child: Txt(
                            txt: "20% service fee",
                            txtColor: Colors.grey,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 2),
                    Txt(
                      txt: "Based on earning",
                      txtColor: Colors.black54,
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Divider(color: Colors.grey, height: 5),
          SizedBox(height: 10),
        ],
      );

  _drawScoreView() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Txt(
            txt: "YOUR SCORES",
            txtColor: Colors.grey,
            txtSize: MyTheme.txtSize - .6,
            txtAlign: TextAlign.start,
            isBold: false,
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Your Earnings (last 30 days)",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .5,
            txtAlign: TextAlign.start,
            isBold: true,
          ),
          SizedBox(height: 10),
          Txt(
            txt: "Your earnings are " +
                AppDefine.CUR_SIGN +
                "400 away from Silver and lowering service fees.",
            txtColor: Colors.black54,
            txtSize: MyTheme.txtSize - .4,
            txtAlign: TextAlign.start,
            isBold: false,
          ),
          SizedBox(height: 10),
          Container(
            color: Colors.indigo.shade100.withOpacity(.5),
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Txt(
                txt: AppDefine.CUR_SIGN + "0",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .5,
                txtAlign: TextAlign.start,
                isBold: true,
              ),
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 5, right: 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                    child: Txt(
                  txt: AppDefine.CUR_SIGN + "0",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.start,
                  isBold: false,
                )),
                Flexible(
                    child: Txt(
                  txt: AppDefine.CUR_SIGN + "400",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.start,
                  isBold: false,
                )),
                Flexible(
                    child: Txt(
                  txt: AppDefine.CUR_SIGN + "1,500",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.start,
                  isBold: false,
                )),
                Flexible(
                    child: Txt(
                  txt: AppDefine.CUR_SIGN + "3,500+",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ))
              ],
            ),
          ),
        ],
      );

  _drawHelpWorks() => Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                    child: Icon(
                  Icons.help_outline,
                  color: MyTheme.brandColor,
                  size: 20,
                )),
                SizedBox(width: 10),
                Expanded(
                    child: Txt(
                  txt: "How do scores and tiers work?",
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.start,
                  isBold: true,
                ))
              ],
            ),
            SizedBox(height: 10),
            Divider(color: Colors.grey, height: 5),
            SizedBox(height: 10),
          ],
        ),
      );

  _drawNextTierView() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Txt(
            txt: "NEXT TIER BENEFITS",
            txtColor: Colors.grey,
            txtSize: MyTheme.txtSize - .6,
            txtAlign: TextAlign.start,
            isBold: false,
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                child: Txt(
                  txt: AppDefine.CUR_SIGN,
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
              ),
              SizedBox(width: 20),
              Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Txt(
                        txt: "Reach Silver for lower service fees!",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .5,
                        txtAlign: TextAlign.start,
                        isBold: true,
                      ),
                      SizedBox(height: 2),
                      Txt(
                        txt: "Pay less with a service fee of 15%",
                        txtColor: Colors.black54,
                        txtSize: MyTheme.txtSize - .5,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                    ],
                  ))
            ],
          )
        ],
      );
}
