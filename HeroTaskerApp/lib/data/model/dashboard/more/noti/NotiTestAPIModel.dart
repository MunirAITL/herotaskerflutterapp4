import '../../../auth/UserModel.dart';

class NotiTestAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  NotiTestAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  NotiTestAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> pushMessage;
  Messages({this.pushMessage});

  Messages.fromJson(Map<String, dynamic> json) {
    pushMessage = json['push_message'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['push_message'] = this.pushMessage;
    return data;
  }
}

class ResponseData {
  NotificationTestModel notification;
  ResponseData({this.notification});

  ResponseData.fromJson(Map<String, dynamic> json) {
    notification = json['notification'] != null
        ? new NotificationTestModel.fromJson(json['notification'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.notification != null) {
      data['notification'] = this.notification.toJson();
    }
    return data;
  }
}

class NotificationTestModel {
  int userId;
  UserModel user;
  bool isRead;
  String publishDateTime;
  String readDateTime;
  int entityId;
  String entityName;
  int notificationEventId;
  String notificationEvent;
  int initiatorId;
  String initiatorName;
  String message;
  String webUrl;
  String description;
  int id;

  NotificationTestModel(
      {this.userId,
      this.user,
      this.isRead,
      this.publishDateTime,
      this.readDateTime,
      this.entityId,
      this.entityName,
      this.notificationEventId,
      this.notificationEvent,
      this.initiatorId,
      this.initiatorName,
      this.message,
      this.webUrl,
      this.description,
      this.id});

  NotificationTestModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
    isRead = json['IsRead'] ?? false;
    publishDateTime = json['PublishDateTime'] ?? '';
    readDateTime = json['ReadDateTime'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    notificationEventId = json['NotificationEventId'] ?? 0;
    notificationEvent = json['NotificationEvent'] ?? '';
    initiatorId = json['InitiatorId'] ?? 0;
    initiatorName = json['InitiatorName'] ?? '';
    message = json['Message'] ?? '';
    webUrl = json['WebUrl'] ?? '';
    description = json['Description'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    data['IsRead'] = this.isRead;
    data['PublishDateTime'] = this.publishDateTime;
    data['ReadDateTime'] = this.readDateTime;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['NotificationEventId'] = this.notificationEventId;
    data['NotificationEvent'] = this.notificationEvent;
    data['InitiatorId'] = this.initiatorId;
    data['InitiatorName'] = this.initiatorName;
    data['Message'] = this.message;
    data['WebUrl'] = this.webUrl;
    data['Description'] = this.description;
    data['Id'] = this.id;
    return data;
  }
}
