import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/findworks/filters/filters_page.dart';
import 'package:aitl/view/dashboard/mytasks/taskdetails_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../main.dart';

abstract class BaseFindWorksStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  List<String> listTopBtn = [
    "In person & remotely",
    "500km, London",
    "Any price",
    "Open tasks",
  ];

  bool isLoading = false;

  refreshData();

  drawAppbarNavBar(Function callback) {
    final xtraH = isIPAD || isTablet ? 20 : 0;
    return PreferredSize(
      preferredSize: Size.fromHeight(AppConfig.findworks_height + xtraH),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*Container(
              color: MyTheme.appbarColor,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 12, bottom: 12),
                child: Center(
                  child: Txt(
                      txt: "To earn money select suitable job from below",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
            ),*/
            Container(
              //color: MyTheme.appbarDarkColor,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 5, right: 5, top: 10, bottom: 10),
                child: Container(
                  width: getW(context),
                  height: getHP(context, 5),
                  child: Center(
                    child: ListView.builder(
                      itemCount: listTopBtn.length,
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            Get.to(() => FilterPage()).then((value) async {
                              if (value != null) {
                                refreshData();
                              }
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 5, right: 5),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: MyTheme.redColor,
                              borderRadius: BorderRadius.circular(20),
                              //border:
                              //Border.all(color: Colors.grey, width: .5)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 5, bottom: 5, left: 10, right: 10),
                              child: Txt(
                                  txt: listTopBtn[index],
                                  txtColor: Colors.white,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),

              /*HorizontalBtns(
                listTopBtn: listTopBtn,
                context: context,
                bgColor: MyTheme.grayColor,
                bgColorHighlight: MyTheme.grayColor,
                txtColor: MyTheme.gray2Color,
                txtColorHighlight: MyTheme.gray2Color,
                txtSize: 1.5,
                fixedHeight: 4.5,
                callback: (v) {
                  Get.to(() => FilterPage()).then((value) async {
                    if (value != null) {
                      refreshData();
                    }
                  });
                },
              ),*/
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : Container(
                    height: .1,
                    color: Colors.transparent,
                  )
          ],
        ),
      ),
    );
  }

  onTopTabbarIndexChanged(int index, Function(Map<String, dynamic>) callback) {
    switch (index) {
      case 0:
        callback({'status': TaskStatusCfg.TASK_STATUS_ALL});
        break;
      case 1:
        callback({'status': TaskStatusCfg.TASK_STATUS_ACTIVE});
        break;
      case 2:
        callback({'status': TaskStatusCfg.TASK_STATUS_DRAFT});
        break;
      case 3:
        callback({'status': TaskStatusCfg.TASK_STATUS_ACCEPTED});
        break;
      case 4:
        callback({'status': TaskStatusCfg.TASK_STATUS_PENDINGTASK});
        break;
      case 5:
        callback({'status': TaskStatusCfg.TASK_STATUS_PAYMENTED});
        break;
      default:
    }
  }

  drawSearchbar(TextEditingController searchText, Function(String) onChange) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextField(
        controller: searchText,
        autofocus: true,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.search,
        onChanged: (value) => onChange(value),
        autocorrect: false,
        style: TextStyle(
          color: MyTheme.appbarTxtColor,
          fontSize: 17,
        ),
        decoration: InputDecoration(
          counter: Offstage(),
          prefixIcon: Icon(
            Icons.search,
            color: MyTheme.appbarTxtColor,
          ),
          suffixIcon: IconButton(
              onPressed: () => onChange(""),
              icon: Icon(
                Icons.close,
                color: MyTheme.appbarTxtColor,
              )),
          hintText: "Search by title",
          hintStyle: new TextStyle(
            color: Colors.black45,
            fontSize: MyTheme.fontSize,
            //height: MyTheme.txtLineSpace,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }

  go2PostTaskScreen(TaskModel taskModel) async {
    Get.to(
      () => AddTask1Screen(
        index: null,
        userModel: userData.userModel,
        taskModel: taskModel,
      ),
    ).then(
      (pageNo) {
        obsUpdateTabs(pageNo);
      },
    );
  }

  go2TaskDetailScreen(TaskModel taskModel) async {
    Get.to(() => TaskDetailsPage(
          taskModel: taskModel,
        )).then((value) {
      if (value == "STATE_RELOAD_TAB_FIND_WORK") {
        StateProvider().notify(ObserverState.STATE_RELOAD_TAB, 2);
      } else {
        refreshData();
      }
    });
  }

  drawItem({TaskModel taskModel, bool isFromMap = false}) {
    final priceTxt = (taskModel.isFixedPrice)
        ? taskModel.fixedBudgetAmount.round().toString()
        : taskModel.hourlyRate.round().toString() + '/hr';
    //
    var status = taskModel.status;
    try {
      if (int.parse(status) > 0) {
        status = TaskStatusCfg().getSatus(int.parse(status));
      }
    } catch (e) {
      status = taskModel.status;
    }
    //
    String preferedLocation = taskModel.preferedLocation;
    if (taskModel.isInPersonOrOnline) preferedLocation = "Remote";
    //
    Color statusColor = MyTheme.dGreenColor;
    Color ribbonColor = MyTheme.airBlueColor;
    if (status.toString().toLowerCase() == "active") {
      ribbonColor = MyTheme.airGreenColor;
    } else if (status.toString().toLowerCase() == "paid") {
      statusColor = MyTheme.gray4Color;
    }
    //

    String statusAddtionalInfo = "";
    if (status.toString().toLowerCase() == "active") {
      if (taskModel.totalBidsNumber > 0) {
        statusAddtionalInfo =
            " - " + taskModel.totalBidsNumber.toString() + " offer";
      } else {
        statusAddtionalInfo = "";
      }
    }
    //

    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: GestureDetector(
        onTap: () {
          if (taskModel.status.toString().toLowerCase() == "draft") {
            go2PostTaskScreen(taskModel);
          } else {
            go2TaskDetailScreen(taskModel);
          }
        },
        child: Card(
          /*decoration: BoxDecoration(
              color: (!isFromMap)
                  ? Colors.transparent
                  : Colors.white.withOpacity(.8),
              border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
          */
          //color: Colors.transparent,
          elevation: 1,
          margin: EdgeInsets.only(top: 2),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Container(color: ribbonColor),
                ),
                Container(
                  width: getWP(context, (!isFromMap) ? 98.5 : 68.5),
                  //alignment: Alignment.centerRight,
                  //color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Txt(
                                  txt: taskModel.title,
                                  txtColor: MyTheme.gray5Color,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ),
                            Container(
                              //color: Colors.black,
                              child: drawPrice(
                                price: priceTxt,
                                txtSize: 22,
                                txtColor: MyTheme.gray5Color,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.location_on_outlined,
                                        size: 20,
                                        color: MyTheme.gray4Color,
                                      ),
                                      SizedBox(width: 5),
                                      Expanded(
                                        child: Txt(
                                          txt: preferedLocation,
                                          txtColor: MyTheme.gray4Color,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                          isOverflow: true,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.calendar_today_outlined,
                                        size: 20,
                                        color: MyTheme.gray4Color,
                                      ),
                                      SizedBox(width: 5),
                                      Expanded(
                                        child: Txt(
                                          txt: DateFun.getFormatedDate(
                                              mDate: DateFormat("dd-MMM-yyyy")
                                                  .parse(
                                                      taskModel.deliveryDate),
                                              format: "EEE MMM dd, yyyy"),
                                          txtColor: MyTheme.gray4Color,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Flexible(
                                          child: Txt(
                                              txt: status,
                                              txtColor: statusColor,
                                              txtSize: MyTheme.txtSize - .6,
                                              txtAlign: TextAlign.start,
                                              isBold: false)),
                                      Flexible(
                                          child: Txt(
                                              txt: statusAddtionalInfo,
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .6,
                                              txtAlign: TextAlign.start,
                                              isBold: false)),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Container(
                              width: getWP(context, 10),
                              height: getWP(context, 10),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: MyNetworkImage.loadProfileImage(
                                      taskModel.ownerImageUrl),
                                  fit: BoxFit.cover,
                                ),
                                shape: BoxShape.circle,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawNF() {
    return Container(
      height: getH(context),
      color: MyTheme.gray1Color,
      child: SingleChildScrollView(
        child: Column(
          //shrinkWrap: true,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: getWP(context, 80),
              height: getWP(context, 80),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_browse.png"),
                  //fit: BoxFit.fitWidth,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt:
                      "We couldn't find any tasks that match - may be try a different filter or search word?",
                  txtColor: MyTheme.gray5Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
            SizedBox(height: 10),
            OutlinedButton(
              onPressed: () {
                Get.to(() => FilterPage()).then((value) async {
                  if (value != null) {
                    refreshData();
                  }
                });
              },
              style: ButtonStyle(
                side: MaterialStateProperty.all(BorderSide(
                    color: Colors.blue, style: BorderStyle.solid, width: 1)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0))),
              ),
              child: const Text("Change filters"),
            ),
          ],
        ),
      ),
    );
  }
}
