import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/main.dart';
import 'package:flutter/material.dart';

import 'MMArrowBtn.dart';

drawBottomBtn({
  @required context,
  @required String text,
  @required Function callback,
  Color bgColor,
  IconData icon,
}) {
  if (bgColor == null) {
    bgColor = MyTheme.bgColor;
  }
  double height = MediaQuery.of(context).size.height;
  var padding = MediaQuery.of(context).padding;
  double newheight = height - padding.top - padding.bottom;
  return SizedBox(
      height: newheight * MyTheme.btnHpa / 100,
      child: BottomAppBar(
        color: bgColor,
        child: Padding(
          padding: EdgeInsets.only(
              left: 20, right: 20, bottom: isIPAD || isTablet ? 10 : 5),
          child: MMArrowBtn(
            txt: text,
            icon: icon,
            height: newheight * MyTheme.btnHpa / 100,
            width: MediaQuery.of(context).size.width,
            callback: () {
              callback();
            },
          ),
        ),
        elevation: MyTheme.botbarElevation,
      ));
}
