import 'dart:convert';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/offers/UpdateOfferTaskBiddingAPIModel.dart';
import 'package:aitl/data/model/dashboard/stripe/CreateorUpdatePaymentAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/stripe/StripeMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';

import 'fund_payment_base.dart';

class FundPaymentPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  //final TaskPaymentInfo taskPaymentInfo;
  final bool isReleasePayment;
  const FundPaymentPage({
    Key key,
    @required this.taskBidding,
    this.isReleasePayment = false,
  }) : super(key: key);
  @override
  State createState() => _FundPaymentPageState();
}

class _FundPaymentPageState extends BasePaymentStatefull<FundPaymentPage>
    with Mixin, UIHelper {
  int switchIndex = 0;
  double price = 0;
  var isCash = false.obs;
  TaskPaymentInfo taskPaymentInfo;

  /*DropListModel ddTitle = DropListModel([
    OptionItem(id: 1, title: "Bkash Mobile Banking"),
    OptionItem(id: 2, title: "DBBL Mobile Banking"),
    OptionItem(id: 3, title: "BRAC VISA"),
    OptionItem(id: 4, title: "Dutch Bangla VISA"),
    OptionItem(id: 5, title: "City Bank Visa"),
    OptionItem(id: 6, title: "EBL Visa"),
    OptionItem(id: 7, title: "Southeast Bank Visa"),
    OptionItem(id: 8, title: "BRAC MASTER"),
    OptionItem(id: 9, title: "MASTER Dutch-Bangla"),
    OptionItem(id: 10, title: "City Master Card"),
    OptionItem(id: 11, title: "EBL Master Card"),
    OptionItem(id: 12, title: "Southeast Bank Master Card"),
    OptionItem(id: 13, title: "City Bank AMEX"),
    OptionItem(id: 14, title: "QCash"),
    OptionItem(id: 15, title: "DBBL Nexus"),
    OptionItem(id: 16, title: "Bank Asia IB"),
    OptionItem(id: 17, title: "AB Bank IB"),
    OptionItem(id: 18, title: "IBBL IB and Mobile Banking"),
    OptionItem(id: 19, title: "Mutual Trust Bank IB"),
    OptionItem(id: 20, title: "City Touch IB"),
  ]);

  OptionItem optTitle = OptionItem(id: null, title: "Select Payment Type");*/

  //  stripe
  CardFieldInputDetails _card;
  final theme = ThemeData.light().copyWith(
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      floatingLabelBehavior: FloatingLabelBehavior.always,
      contentPadding: EdgeInsets.all(12),
    ),
  );
//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  Future<void> _handlePayPress() async {
    if (taskPaymentInfo == null) {
      showToast(context: context, msg: "Missing payment intent details");
      return;
    }

    PaymentIntent paymentIntent;
    if (!isCash.value) {
      //  card
      if (_card == null) {
        showToast(context: context, msg: "Please enter valid card details");
        return;
      }
      paymentIntent = await StripeMgr()
          .makePayment(context: context, taskPaymentInfo: taskPaymentInfo);
      if (paymentIntent != null) {
        final param = {
          "UserId": userData.userModel.id,
          "Status": taskPaymentInfo.status,
          "CreationDate": DateTime.now().toString(),
          "UpdatedDate": DateTime.now().toString(),
          "VersionNumber": taskPaymentInfo.versionNumber,
          "TaskId": taskPaymentInfo.taskId,
          "PaymentType": taskPaymentInfo.paymentType ?? '',
          "PaymentMethod": taskPaymentInfo.paymentMethod ?? 'Bank',
          "EmployeeID": taskPaymentInfo.employeeID,
          "EmployeePaymentGateWayId":
              taskPaymentInfo.employeePaymentGateWayId ?? 0,
          "PayableAmount": taskPaymentInfo.payableAmount,
          "DueAmount": taskPaymentInfo.dueAmount ?? 0,
          "PaymentAmount": taskPaymentInfo.payableAmount,
          "TaskBiddingId": taskPaymentInfo.taskBiddingId,
          "Description": taskPaymentInfo.description,
          "AcAccountNo": "",
          "AccountStatus": "",
          "TransactionType": "",
          "TaskTitle": taskPaymentInfo.taskTitle,
          "TaskTitleUrl": taskPaymentInfo.taskTitleUrl,
          "ProfileImageUrl": userData.userModel.profileImageUrl,
          "ProfileOwnerName": taskPaymentInfo.profileOwnerName,
          "OwnerProfileUrl": taskPaymentInfo.profileImageUrl,
          "ShohokariAmount": taskPaymentInfo.shohokariAmount ?? 0,
          "ServiceFeeAmount": taskPaymentInfo.serviceFeeAmount ?? 0,
          "DiscountAmount": taskPaymentInfo.discountAmount ?? 0,
          "NetTotalAmount": taskPaymentInfo.netTotalAmount ?? 0,
          "UserPromotionId": 0,
          "CollectionPaymentType": "",
          "PaymentIntentId": paymentIntent.id,
          "ClientSecret": paymentIntent.clientSecret,
          "Id": taskPaymentInfo.id
        };
        myLog(param);
        await APIViewModel().req<CreateorUpdatePaymentAPIModel>(
            context: context,
            url: APIPaymentCfg
                .PostStripeValidationFromClientForTaskCompletion_URL,
            reqType: ReqType.Post,
            param: param,
            callback: (model) async {
              if (mounted) {
                if (model != null) {
                  if (model.success) {
                    //  email notification api call
                    await APIViewModel().req<CommonAPIModel>(
                        context: context,
                        url: APIEmailNotiCfg.TASK_EMAI_NOTI_GET_URL.replaceAll(
                            "#taskId#", widget.taskBidding.taskId.toString()),
                        reqType: ReqType.Get,
                        callback: (model2) {});
                    showToast(
                      context: context,
                      msg: 'Payment accepted.',
                      which: 1,
                    );
                    StateProvider()
                        .notify(ObserverState.STATE_RELOAD_TASKDETAILS, null);
                    Future.delayed(Duration(seconds: AppConfig.AlertDismisSec),
                        () {
                      Get.back(result: true);
                    });
                  } else {
                    onPaymentFailed(
                        "Payment failed!\nPlease try again later or contact us");
                  }
                } else {
                  onPaymentFailed(
                      "Payment failed!\nPlease try again later or contact us");
                }
              }
            });
      } else {
        onPaymentFailed(
            "Payment failed!\nPlease try again later or contact us");
        return;
      }
    } else {
      //  cash
      final param = {
        "UserId": widget.taskBidding.userId,
        "Status": 102,
        "CreationDate": DateTime.now().toString(),
        "UpdatedDate": DateTime.now().toString(),
        "VersionNumber": 1,
        "TaskId": taskPaymentInfo.taskId,
        "CoverLetter": taskPaymentInfo.taskTitle,
        "Description": taskPaymentInfo.description,
        "DeliveryDate": DateTime.now().toString(),
        "DeliveryTime": "",
        "FixedbiddigAmount": taskPaymentInfo.payableAmount,
        "HourlyBiddingAmount": 0,
        "TotalHourPerWeek": 0,
        "TotalHour": 0,
        "OwnerName": taskPaymentInfo.profileOwnerName,
        "ThumbnailPath": taskPaymentInfo.profileImageUrl,
        "ReferenceType": "Admin",
        "ReferenceId": "114454",
        "Remarks": "",
        "ImageServerUrl": "https://herotasker.com/api",
        "OwnerImageUrl": taskPaymentInfo.profileImageUrl,
        "OwnerProfileUrl": taskPaymentInfo.ownerProfileUrl,
        "TaskCompletionRate": 0,
        "AverageRating": 0,
        "RatingCount": 0,
        "IsTaskOwner": true,
        "TaskOwnerId": taskCtrl.getTaskModel().userId,
        "IsReview": false,
        "IsReviewByPoster": false,
        "IsReviewByShohoKari": false,
        "ReferenceBiddingUserId": null,
        "ReferenceBiddingUserType": null,
        "PaymentStatus": 204,
        "DiscountAmount": 0,
        "NetTotalAmount": taskPaymentInfo.payableAmount,
        "UserPromotionId": 0,
        "IsInPersonOrOnline": false,
        "TotalComments": 0,
        "TaskPaymentPaymentMethod": null,
        "TaskPaymentAccountStatus": null,
        "TaskPaymentId": 0,
        "TaskTitle": null,
        "TaskTitleUrl": null,
        "CommissionSetupId": null,
        "Id": widget.taskBidding.id
      };
      await APIViewModel().req<UpdateOfferTaskBiddingAPIModel>(
          context: context,
          url: APIMyTasksCfg.PUT_TASKBIDDING_URL,
          reqType: ReqType.Put,
          param: param,
          callback: (model) async {
            if (model != null) {
              if (mounted && model.success) {
                //  email notification api call
                await APIViewModel().req<CommonAPIModel>(
                    context: context,
                    url: APIEmailNotiCfg.TASKBIDDING_EMAI_NOTI_GET_URL
                        .replaceAll("#taskBiddingId#",
                            widget.taskBidding.taskId.toString()),
                    reqType: ReqType.Get,
                    callback: (model2) {});
                showToast(
                  context: context,
                  msg: 'Order accepted.',
                  which: 1,
                );
                StateProvider()
                    .notify(ObserverState.STATE_RELOAD_TASKDETAILS, null);
                Future.delayed(Duration(seconds: AppConfig.AlertDismisSec), () {
                  Get.back(result: true);
                });
              } else {
                //onPaymentFailed();
              }
            } else {
              //onPaymentFailed();
            }
          });
    }

    /* } else {
     
    }*/
  }

  onPaymentFailed(String msg) {
    confirmDialog(
        context: context,
        yesBtnTxt: "Contact",
        noBtnTxt: "Cancel",
        title: msg,
        callbackYes: () {
          Get.to(() => ResolutionScreen());
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() async {
    try {
      await APIViewModel().req<CreateorUpdatePaymentAPIModel>(
          context: context,
          url: APIPaymentCfg.GetSecretKeyWithDetails_URL.replaceAll(
              "#id#", widget.taskBidding.id.toString()),
          reqType: ReqType.Get,
          callback: (model) async {
            if (model != null && mounted) {
              if (model.success) {
                taskPaymentInfo = model.responseData.taskPaymentInfo;
                if (taskPaymentInfo.id == 0) {
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    onPaymentFailed(
                        "Failed!\nPlease try again later or contact us");
                  });
                } else {}
              }
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(
              title: (widget.isReleasePayment)
                  ? "Release payment"
                  : "Offer accept"),
          centerTitle: false,
        ),
        bottomNavigationBar: Obx(() => drawBottomBtn(
            context: context,
            text: (widget.isReleasePayment)
                ? isCash.value
                    ? "Cash"
                    : "Release payment"
                : isCash.value
                    ? "Cash"
                    : "Accept & add funds",
            callback: () async {
              _handlePayPress();
              /*callback: (PaymentMethod paymentMethod,
                      PaymentIntentResult paymentIntent,
                      PaymentIntentResult authPaymentIntent*)*/ //{
            })),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          payOffer(),
        ],
      ),
    );
  }

  payOffer() {
    return Padding(
        padding: const EdgeInsets.all(20),
        child: Obx(
          () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: MyTheme.picEmboseCircleDeco,
                alignment: Alignment.center,
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                      MyNetworkImage.checkUrl(
                          widget.taskBidding.ownerImageUrl)),
                ),
              ),
              SizedBox(height: 10),
              Center(
                child: Txt(
                    txt: widget.taskBidding.ownerName,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ),
              SizedBox(height: 20),
              Txt(
                  txt: widget.taskBidding.coverLetter,
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 20),
              drawLine(colr: MyTheme.gray3Color),
              /*
          @munir->switch view
          ...ListTile.divideTiles(context: context, tiles: [
            SwitchListTile.adaptive(
              title: Text('Show postal code field'),
              value: postalCodeEnabled,
              onChanged: (v) => setState(() => postalCodeEnabled = v),
            ),
          ]),*/

              Txt(
                  txt: "Payment Method",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 10),

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Flexible(
                      child: Theme(
                    data: Theme.of(context).copyWith(
                      unselectedWidgetColor: Colors.red,
                      toggleableActiveColor: Colors.red.shade800,
                    ),
                    child: Radio(
                        value: false,
                        groupValue: isCash.value,
                        onChanged: (v) {
                          isCash.value = v;
                        }),
                  )),
                  Expanded(
                      flex: 7,
                      child: Theme(
                        data: theme,
                        child: Container(
                          //height: 150,
                          alignment: Alignment.center,
                          //padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          color: theme
                              .scaffoldBackgroundColor, //theme.scaffoldBackgroundColor,
                          child: CardField(
                            onCardChanged: (card) {
                              setState(() {
                                _card = card;
                              });
                            },
                            autofocus: true,
                            enablePostalCode: true,
                            style: TextStyle(fontFamily: 'OtomanopeeOne'),
                            decoration: InputDecoration(
                              labelText: theme.inputDecorationTheme
                                          .floatingLabelBehavior ==
                                      FloatingLabelBehavior.always
                                  ? userData.userModel.name
                                  : null,
                            ),
                          ),
                        ),
                      )),
                ],
              ),

              (!taskCtrl.getTaskModel().isInPersonOrOnline &&
                      taskCtrl.getTaskModel().userId == userData.userModel.id &&
                      taskCtrl.getStatusCode() !=
                          TaskStatusCfg.TASK_STATUS_ACCEPTED)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Flexible(
                                child: Theme(
                              data: Theme.of(context).copyWith(
                                unselectedWidgetColor: Colors.red,
                                toggleableActiveColor: Colors.red.shade800,
                              ),
                              child: Radio(
                                  value: true,
                                  groupValue: isCash.value,
                                  onChanged: (v) {
                                    isCash.value = v;
                                  }),
                            )),
                            Expanded(
                                flex: 7,
                                child: Material(
                                    elevation: 2,
                                    //height: 150,
                                    //alignment: Alignment.center,
                                    //padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                    color: Colors.grey[
                                        200], //theme.scaffoldBackgroundColor,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Txt(
                                              txt: "Cash",
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .2,
                                              txtAlign: TextAlign.start,
                                              fontWeight: FontWeight.w500,
                                              isBold: false),
                                          SizedBox(height: 10),
                                          Txt(
                                              txt:
                                                  "Select if you want to directly pay cash to the tasker. They will confirm the received payment for your task.",
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .4,
                                              txtAlign: TextAlign.start,
                                              isBold: false),
                                        ],
                                      ),
                                    )))
                          ]),
                    )
                  : SizedBox(),
              SizedBox(height: 20),
              drawPriceBox(),
              //SizedBox(height: 20),
              //drawPayBox(),
              (widget.isReleasePayment) ? drawReleaseFundBox() : SizedBox(),
              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:
                          "To get this task completed, you'll need to add funds to the task. Don't worry! Your money is securely held until the task is completed and you'll be able to release the funds. ",
                      style: TextStyle(
                        height: MyTheme.txtLineSpace,
                        color: Colors.black54,
                        fontSize: MyTheme.fontSize - .6,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'View terms',
                            style: TextStyle(
                                height: MyTheme.txtLineSpace,
                                decoration: TextDecoration.underline,
                                decorationThickness: 2,
                                color: MyTheme.brandColor,
                                fontSize: MyTheme.fontSize - .6,
                                fontWeight: FontWeight.w500),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                // navigate to desired screen
                                Get.to(
                                  () => WebScreen(
                                    title: "Payment Terms",
                                    url: ServerUrls.PAYMENT_TC_URL,
                                  ),
                                ).then((value) {
                                  //callback(route);
                                });
                              })
                      ]),
                ),
              ),
            ],
          ),
        ));
  }

  drawPriceBox() {
    price = widget.taskBidding.netTotalAmount;
    if (price < 1) {
      price = widget.taskBidding.fixedbiddigAmount;
    }
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Txt(
                txt: "Task price",
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          (widget.isReleasePayment)
              ? Flexible(
                  child: Row(
                    children: [
                      Icon(Icons.lock, size: 14, color: Colors.black),
                      SizedBox(width: 5),
                      Flexible(
                        child: Txt(
                            txt: "PAYMENT SECURED",
                            txtColor: MyTheme.airGreenColor,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      )
                    ],
                  ),
                )
              : SizedBox(),
          Flexible(
            child: Text(
              getCurSign() + price.toStringAsFixed(2),
              style: TextStyle(
                color: MyTheme.gray5Color,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  /*drawBankCard() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DropDownListDialog(
            context: context,
            title: optTitle.title,
            ddTitleList: ddTitle,
            callback: (optionItem) {
              optTitle = optionItem;
              setState(() {});
            },
          ),         
          SizedBox(height: 20),
          Theme(
            data: theme,
            child: Container(
              height: 150,
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              color: theme.scaffoldBackgroundColor,
              child: CardField(
                autofocus: true,
                enablePostalCode: true,
                style: TextStyle(fontFamily: 'OtomanopeeOne'),
                onCardChanged: (_) {},
                decoration: InputDecoration(
                  labelText: theme.inputDecorationTheme.floatingLabelBehavior ==
                          FloatingLabelBehavior.always
                      ? userData.userModel.name
                      : null,
                ),
              ),
            ),
          ),
          SizedBox(height: 20),
          Txt(
              txt:
                  "The funds for the task have now been secured. You're in control to release the funds once the task is completed. No further payment for the task is required.",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }*/

  /*drawCash() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Txt(
                txt: "Your payment will be paid cash directly to " +
                    widget.taskBidding.ownerName +
                    ". who will confirm the received payment",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }*/

  drawReleaseFundBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Txt(
          txt:
              "Please note that by releasing payment you're agreeing to our payment terms and conditions.",
          txtColor: MyTheme.gray4Color,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.center,
          isBold: false),
    );
  }
}

extension PrettyJson on Map<String, dynamic> {
  String toPrettyString() {
    var encoder = new JsonEncoder.withIndent("     ");
    return encoder.convert(this);
  }
}
