import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditDashBoardReport.dart';
import 'package:aitl/data/model/ads-on/credit_report/getSummaryResponse.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/CreditDashboardWidget/accountAndAlertsWidget.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/CreditDashboardWidget/myCreditOverViewWidget.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/CreditDashboardWidget/myCreditScoreWidget.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/CreditDashboardWidget/myCurrentScoreWidget.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/CreditDashboardWidget/myScoreHistoryGraphWidget.dart';
import 'package:flutter/material.dart';

import '../CreditDashboardTabController.dart';

class CreditDashboard extends StatefulWidget {
  const CreditDashboard({Key key}) : super(key: key);

  @override
  _CreditDashboardState createState() => _CreditDashboardState();
}

class _CreditDashboardState extends State<CreditDashboard> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  CreditDashBoardReport creditDashBoardReport;
  GetSummaryResponseData getSummaryResponse;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;
    getSummaryResponse = CreditDashBoardTabControllerState.getSummaryResponse;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Container(
            child: Column(
              children: [
                (creditDashBoardReport != null)
                    ? MyCreditScore(creditDashBoardReport.cRUserManage)
                    : SizedBox(),
                SizedBox(
                  height: 10,
                ),
                (creditDashBoardReport != null)
                    ? MyCreditOverView(
                        creditDashBoardReport, getSummaryResponse)
                    : SizedBox(),
                SizedBox(
                  height: 10,
                ),
                AccountAndAlerts(),
                SizedBox(
                  height: 10,
                ),
                (getSummaryResponse != null)
                    ? MyScoreHistoryGraph()
                    : SizedBox(),
                SizedBox(
                  height: 10,
                ),
                MyCurrentScore()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
