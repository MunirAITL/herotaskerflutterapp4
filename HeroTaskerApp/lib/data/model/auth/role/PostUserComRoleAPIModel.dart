import '../UserModel.dart';

class PostUserComRoleAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  PostUserComRoleAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostUserComRoleAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> postUser;
  Messages({this.postUser});

  Messages.fromJson(Map<String, dynamic> json) {
    postUser = json['post_user'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_user'] = this.postUser;
    return data;
  }
}

class ResponseData {
  UserModel user;
  ResponseData({this.user});

  ResponseData.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
/*
class User {
	String? firstName;
	String? lastName;
	String? name;
	String? userName;
	String? profileImageUrl;
	String? coverImageUrl;
	Null? profileUrl;
	String? dateCreatedUtc;
	String? dateCreatedLocal;
	bool? active;
	Null? lastLoginDateUtc;
	Null? lastLoginDateLocal;
	int? followerCount;
	int? followingCount;
	int? friendCount;
	bool? canFollow;
	int? followStatus;
	int? friendStatus;
	int? unreadNotificationCount;
	bool? isOnline;
	String? seName;
	Null? stanfordWorkplaceURL;
	String? headline;
	String? briefBio;
	Null? referenceId;
	Null? referenceType;
	Null? remarks;
	Null? community;
	Null? locations;
	String? cohort;
	String? communityId;
	bool? isFirstLogin;
	String? mobileNumber;
	String? dateofBirth;
	bool? isMobileNumberVerified;
	bool? isProfileImageVerified;
	bool? isDateOfBirthVerified;
	bool? isEmailVerified;
	String? lastLoginDate;
	String? userProfileUrl;
	int? userAccesType;
	String? address;
	String? email;
	String? latitude;
	String? longitude;
	int? bankVerifiedStatus;
	int? nationalIDVerifiedStatus;
	int? billingAccountVerifiedStatus;
	int? profileImageId;
	Null? workHistory;
	int? userTaskCategoryId;
	Null? deviceName;
	Null? skillName;
	Null? userTaskCategoryName;
	String? status;
	int? unreadMessageCount;
	int? groupId;
	Null? countryCode;
	Null? namePrefix;
	Null? middleName;
	Null? areYouASmoker;
	Null? addressLine1;
	Null? addressLine2;
	Null? addressLine3;
	Null? town;
	Null? county;
	Null? postcode;
	Null? telNumber;
	Null? nationalInsuranceNumber;
	Null? nationality;
	Null? countryofBirth;
	Null? countryofResidency;
	Null? passportNumber;
	Null? maritalStatus;
	int? userCompanyId;
	Null? companyName;
	Null? occupantType;
	String? livingDate;
	String? visaExpiryDate;
	String? passportExpiryDate;
	Null? visaName;
	Null? otherVisaName;
	Null? reportLogoUrl;
	Null? userCompanyInfo;
	Null? companyType;
	int? companyTypeId;
	Null? isActiveCustomerPrivacy;
	Null? isActiveCustomerAgreement;
	Null? companyWebsite;
	Null? isCallCenterActive;
	int? companyCount;
	bool? isSelected;
	int? id;

	User({this.firstName, this.lastName, this.name, this.userName, this.profileImageUrl, this.coverImageUrl, this.profileUrl, this.dateCreatedUtc, this.dateCreatedLocal, this.active, this.lastLoginDateUtc, this.lastLoginDateLocal, this.followerCount, this.followingCount, this.friendCount, this.canFollow, this.followStatus, this.friendStatus, this.unreadNotificationCount, this.isOnline, this.seName, this.stanfordWorkplaceURL, this.headline, this.briefBio, this.referenceId, this.referenceType, this.remarks, this.community, this.locations, this.cohort, this.communityId, this.isFirstLogin, this.mobileNumber, this.dateofBirth, this.isMobileNumberVerified, this.isProfileImageVerified, this.isDateOfBirthVerified, this.isEmailVerified, this.lastLoginDate, this.userProfileUrl, this.userAccesType, this.address, this.email, this.latitude, this.longitude, this.bankVerifiedStatus, this.nationalIDVerifiedStatus, this.billingAccountVerifiedStatus, this.profileImageId, this.workHistory, this.userTaskCategoryId, this.deviceName, this.skillName, this.userTaskCategoryName, this.status, this.unreadMessageCount, this.groupId, this.countryCode, this.namePrefix, this.middleName, this.areYouASmoker, this.addressLine1, this.addressLine2, this.addressLine3, this.town, this.county, this.postcode, this.telNumber, this.nationalInsuranceNumber, this.nationality, this.countryofBirth, this.countryofResidency, this.passportNumber, this.maritalStatus, this.userCompanyId, this.companyName, this.occupantType, this.livingDate, this.visaExpiryDate, this.passportExpiryDate, this.visaName, this.otherVisaName, this.reportLogoUrl, this.userCompanyInfo, this.companyType, this.companyTypeId, this.isActiveCustomerPrivacy, this.isActiveCustomerAgreement, this.companyWebsite, this.isCallCenterActive, this.companyCount, this.isSelected, this.id});

	User.fromJson(Map<String, dynamic> json) {
		firstName = json['FirstName'];
		lastName = json['LastName'];
		name = json['Name'];
		userName = json['UserName'];
		profileImageUrl = json['ProfileImageUrl'];
		coverImageUrl = json['CoverImageUrl'];
		profileUrl = json['ProfileUrl'];
		dateCreatedUtc = json['DateCreatedUtc'];
		dateCreatedLocal = json['DateCreatedLocal'];
		active = json['Active'];
		lastLoginDateUtc = json['LastLoginDateUtc'];
		lastLoginDateLocal = json['LastLoginDateLocal'];
		followerCount = json['FollowerCount'];
		followingCount = json['FollowingCount'];
		friendCount = json['FriendCount'];
		canFollow = json['CanFollow'];
		followStatus = json['FollowStatus'];
		friendStatus = json['FriendStatus'];
		unreadNotificationCount = json['UnreadNotificationCount'];
		isOnline = json['IsOnline'];
		seName = json['SeName'];
		stanfordWorkplaceURL = json['StanfordWorkplaceURL'];
		headline = json['Headline'];
		briefBio = json['BriefBio'];
		referenceId = json['ReferenceId'];
		referenceType = json['ReferenceType'];
		remarks = json['Remarks'];
		community = json['Community'];
		locations = json['Locations'];
		cohort = json['Cohort'];
		communityId = json['CommunityId'];
		isFirstLogin = json['IsFirstLogin'];
		mobileNumber = json['MobileNumber'];
		dateofBirth = json['DateofBirth'];
		isMobileNumberVerified = json['IsMobileNumberVerified'];
		isProfileImageVerified = json['IsProfileImageVerified'];
		isDateOfBirthVerified = json['IsDateOfBirthVerified'];
		isEmailVerified = json['IsEmailVerified'];
		lastLoginDate = json['LastLoginDate'];
		userProfileUrl = json['UserProfileUrl'];
		userAccesType = json['UserAccesType'];
		address = json['Address'];
		email = json['Email'];
		latitude = json['Latitude'];
		longitude = json['Longitude'];
		bankVerifiedStatus = json['BankVerifiedStatus'];
		nationalIDVerifiedStatus = json['NationalIDVerifiedStatus'];
		billingAccountVerifiedStatus = json['BillingAccountVerifiedStatus'];
		profileImageId = json['ProfileImageId'];
		workHistory = json['WorkHistory'];
		userTaskCategoryId = json['UserTaskCategoryId'];
		deviceName = json['DeviceName'];
		skillName = json['SkillName'];
		userTaskCategoryName = json['UserTaskCategoryName'];
		status = json['Status'];
		unreadMessageCount = json['UnreadMessageCount'];
		groupId = json['GroupId'];
		countryCode = json['CountryCode'];
		namePrefix = json['NamePrefix'];
		middleName = json['MiddleName'];
		areYouASmoker = json['AreYouASmoker'];
		addressLine1 = json['AddressLine1'];
		addressLine2 = json['AddressLine2'];
		addressLine3 = json['AddressLine3'];
		town = json['Town'];
		county = json['County'];
		postcode = json['Postcode'];
		telNumber = json['TelNumber'];
		nationalInsuranceNumber = json['NationalInsuranceNumber'];
		nationality = json['Nationality'];
		countryofBirth = json['CountryofBirth'];
		countryofResidency = json['CountryofResidency'];
		passportNumber = json['PassportNumber'];
		maritalStatus = json['MaritalStatus'];
		userCompanyId = json['UserCompanyId'];
		companyName = json['CompanyName'];
		occupantType = json['OccupantType'];
		livingDate = json['LivingDate'];
		visaExpiryDate = json['VisaExpiryDate'];
		passportExpiryDate = json['PassportExpiryDate'];
		visaName = json['VisaName'];
		otherVisaName = json['OtherVisaName'];
		reportLogoUrl = json['ReportLogoUrl'];
		userCompanyInfo = json['UserCompanyInfo'];
		companyType = json['CompanyType'];
		companyTypeId = json['CompanyTypeId'];
		isActiveCustomerPrivacy = json['IsActiveCustomerPrivacy'];
		isActiveCustomerAgreement = json['IsActiveCustomerAgreement'];
		companyWebsite = json['CompanyWebsite'];
		isCallCenterActive = json['IsCallCenterActive'];
		companyCount = json['CompanyCount'];
		isSelected = json['IsSelected'];
		id = json['Id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['FirstName'] = this.firstName;
		data['LastName'] = this.lastName;
		data['Name'] = this.name;
		data['UserName'] = this.userName;
		data['ProfileImageUrl'] = this.profileImageUrl;
		data['CoverImageUrl'] = this.coverImageUrl;
		data['ProfileUrl'] = this.profileUrl;
		data['DateCreatedUtc'] = this.dateCreatedUtc;
		data['DateCreatedLocal'] = this.dateCreatedLocal;
		data['Active'] = this.active;
		data['LastLoginDateUtc'] = this.lastLoginDateUtc;
		data['LastLoginDateLocal'] = this.lastLoginDateLocal;
		data['FollowerCount'] = this.followerCount;
		data['FollowingCount'] = this.followingCount;
		data['FriendCount'] = this.friendCount;
		data['CanFollow'] = this.canFollow;
		data['FollowStatus'] = this.followStatus;
		data['FriendStatus'] = this.friendStatus;
		data['UnreadNotificationCount'] = this.unreadNotificationCount;
		data['IsOnline'] = this.isOnline;
		data['SeName'] = this.seName;
		data['StanfordWorkplaceURL'] = this.stanfordWorkplaceURL;
		data['Headline'] = this.headline;
		data['BriefBio'] = this.briefBio;
		data['ReferenceId'] = this.referenceId;
		data['ReferenceType'] = this.referenceType;
		data['Remarks'] = this.remarks;
		data['Community'] = this.community;
		data['Locations'] = this.locations;
		data['Cohort'] = this.cohort;
		data['CommunityId'] = this.communityId;
		data['IsFirstLogin'] = this.isFirstLogin;
		data['MobileNumber'] = this.mobileNumber;
		data['DateofBirth'] = this.dateofBirth;
		data['IsMobileNumberVerified'] = this.isMobileNumberVerified;
		data['IsProfileImageVerified'] = this.isProfileImageVerified;
		data['IsDateOfBirthVerified'] = this.isDateOfBirthVerified;
		data['IsEmailVerified'] = this.isEmailVerified;
		data['LastLoginDate'] = this.lastLoginDate;
		data['UserProfileUrl'] = this.userProfileUrl;
		data['UserAccesType'] = this.userAccesType;
		data['Address'] = this.address;
		data['Email'] = this.email;
		data['Latitude'] = this.latitude;
		data['Longitude'] = this.longitude;
		data['BankVerifiedStatus'] = this.bankVerifiedStatus;
		data['NationalIDVerifiedStatus'] = this.nationalIDVerifiedStatus;
		data['BillingAccountVerifiedStatus'] = this.billingAccountVerifiedStatus;
		data['ProfileImageId'] = this.profileImageId;
		data['WorkHistory'] = this.workHistory;
		data['UserTaskCategoryId'] = this.userTaskCategoryId;
		data['DeviceName'] = this.deviceName;
		data['SkillName'] = this.skillName;
		data['UserTaskCategoryName'] = this.userTaskCategoryName;
		data['Status'] = this.status;
		data['UnreadMessageCount'] = this.unreadMessageCount;
		data['GroupId'] = this.groupId;
		data['CountryCode'] = this.countryCode;
		data['NamePrefix'] = this.namePrefix;
		data['MiddleName'] = this.middleName;
		data['AreYouASmoker'] = this.areYouASmoker;
		data['AddressLine1'] = this.addressLine1;
		data['AddressLine2'] = this.addressLine2;
		data['AddressLine3'] = this.addressLine3;
		data['Town'] = this.town;
		data['County'] = this.county;
		data['Postcode'] = this.postcode;
		data['TelNumber'] = this.telNumber;
		data['NationalInsuranceNumber'] = this.nationalInsuranceNumber;
		data['Nationality'] = this.nationality;
		data['CountryofBirth'] = this.countryofBirth;
		data['CountryofResidency'] = this.countryofResidency;
		data['PassportNumber'] = this.passportNumber;
		data['MaritalStatus'] = this.maritalStatus;
		data['UserCompanyId'] = this.userCompanyId;
		data['CompanyName'] = this.companyName;
		data['OccupantType'] = this.occupantType;
		data['LivingDate'] = this.livingDate;
		data['VisaExpiryDate'] = this.visaExpiryDate;
		data['PassportExpiryDate'] = this.passportExpiryDate;
		data['VisaName'] = this.visaName;
		data['OtherVisaName'] = this.otherVisaName;
		data['ReportLogoUrl'] = this.reportLogoUrl;
		data['UserCompanyInfo'] = this.userCompanyInfo;
		data['CompanyType'] = this.companyType;
		data['CompanyTypeId'] = this.companyTypeId;
		data['IsActiveCustomerPrivacy'] = this.isActiveCustomerPrivacy;
		data['IsActiveCustomerAgreement'] = this.isActiveCustomerAgreement;
		data['CompanyWebsite'] = this.companyWebsite;
		data['IsCallCenterActive'] = this.isCallCenterActive;
		data['CompanyCount'] = this.companyCount;
		data['IsSelected'] = this.isSelected;
		data['Id'] = this.id;
		return data;
	}
}*/