import 'package:aitl/config/server/APICommunityIdCfg.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/data/model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/OtpByEmailPage.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/material.dart';
import '../../config/server/APIAuthCfg.dart';
import '../../config/server/Server.dart';
import '../../config/theme/MyTheme.dart';
import '../../controller/form_validator/UserProfileVal.dart';
import '../../data/app_data/UserData.dart';
import '../../data/db/DBMgr.dart';
import '../../data/model/auth/LoginAPIModel.dart';
import '../../data/network/NetworkMgr.dart';
import '../../view_model/api/api_view_model.dart';
import '../../view_model/helper/auth/ForgotAPIMgr.dart';
import '../dashboard/dashboard_page.dart';
import '../widgets/dialog/ConfirmDialog.dart';
import '../widgets/input/InputBox.dart';
import '../widgets/txt/Txt.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with Mixin {
  final email = TextEditingController();
  final pwd = TextEditingController();

  final focusEmail = FocusNode();
  final focusPwd = FocusNode();

  var isPwdVisable = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    super.dispose();
  }

  appInit() async {
    //  mesuk1@yopmail.com/123
    //  mesuk2@yopmail.com/123
  }

  validate() {
    if (UserProfileVal().isEmpty(context, email, 'Missing Email/Phone')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, pwd)) {
      return false;
    }
    return true;
  }

  getParam({
    String email,
    String pwd,
    bool persist = false,
    bool checkSignUpMobileNumber = false,
    String countryCode = "880",
    String status = "101",
    String oTPCode = "",
    String birthDay = "",
    String birthMonth = "",
    String birthYear = "",
    String userCompanyId = "0",
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": "880",
      "Status": "101",
      "OTPCode": "",
      "BirthDay": "",
      "BirthMonth": "",
      "BirthYear": "",
      "UserCompanyId": "0"
    };
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          //titleSpacing: 0,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(children: [
          Container(
            color: Colors.white,
            width: getW(context),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset(
                  "assets/images/welcome/login_logo.png",
                  fit: BoxFit.cover,
                  width: getWP(context, 60),
                ),
              ],
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  InputBox(
                    context: context,
                    ctrl: email,
                    lableTxt: "Email",
                    kbType: TextInputType.emailAddress,
                    inputAction: TextInputAction.next,
                    focusNode: focusEmail,
                    focusNodeNext: focusPwd,
                    len: 50,
                    autofocus: true,
                  ),
                  SizedBox(height: 2),
                  InputBox(
                    context: context,
                    ctrl: pwd,
                    lableTxt: "Password",
                    kbType: TextInputType.emailAddress,
                    inputAction: TextInputAction.done,
                    focusNode: focusPwd,
                    len: 20,
                    isPwd: !isPwdVisable,
                    suffixIcon: IconButton(
                        onPressed: () {
                          isPwdVisable = !isPwdVisable;
                          setState(() {});
                        },
                        icon: Icon(
                          !isPwdVisable
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined,
                          color: Colors.grey,
                          size: 20,
                        )),
                  ),
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                        child: new Text('Forgot password?',
                            style: TextStyle(color: Colors.black87)),
                        onTap: () {
                          confirmDialog(
                            context: context,
                            title: "Password Reset",
                            msg:
                                "Send an email to with instruction to reset your password?",
                            callbackYes: () {
                              if (email.text.trim().length > 0) {
                                ForgotAPIMgr().wsForgotAPI(
                                    context: context,
                                    email: email.text.trim(),
                                    callback: (model) {
                                      if (model != null && mounted) {
                                        try {
                                          if (model.success) {
                                            try {
                                              final msg = model
                                                  .messages.forgotpassword[0]
                                                  .toString();
                                              showToast(
                                                  context: context,
                                                  msg: msg,
                                                  which: 1);
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          } else {
                                            try {
                                              if (mounted) {
                                                final err = model.errorMessages
                                                    .forgotpassword[0]
                                                    .toString();
                                                showToast(
                                                    context: context, msg: err);
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    });
                              } else {
                                focusEmail.requestFocus();
                                showToast(
                                  context: context,
                                  msg: "Missing email",
                                );
                              }
                            },
                          );
                        }),
                  ),
                  SizedBox(height: 20),
                  MMBtn(
                      txt: "Sign In",
                      txtColor: Colors.white,
                      width: getW(context),
                      height: getHP(context, 7),
                      radius: 10,
                      callback: () async {
                        if (validate()) {
                          APIViewModel().req<LoginAPIModel>(
                            context: context,
                            url: APIAuthCfg.LOGIN_URL,
                            param: getParam(
                                email: email.text.trim(), pwd: pwd.text.trim()),
                            reqType: ReqType.Post,
                            callback: (model) async {
                              if (mounted && model != null) {
                                if (model.success) {
                                  try {
                                    await PrefMgr.shared.setPrefStr(
                                        "accessToken",
                                        model.responseData.accessToken);
                                    await PrefMgr.shared.setPrefStr(
                                        "refreshToken",
                                        model.responseData.refreshToken);

                                    //  @Sardar -> because we will add user by ourself , they there are interested, they will downlaod app and login there
                                    /*
                                      12    1    ACTIVE    Manually Added Tasker
                                      13    1    ACTIVE    Manually Added Poster 
                                      14    1    ACTIVE    Not Interest - Tasker
                                      15    1    ACTIVE    Not Interested - Poster
                                      * if community id => 12 & 14 login to mobile application => you will change comunityId to         11
                                      * if community id => 13 & 15 login to mobile application => you will change comunityId to         1
                                      for there first login only
                                      */
                                    for (final e in APICommunityIdCfg
                                        .mapCommunityIdChangeOnLogin.entries) {
                                      try {
                                        if (int.parse(model.responseData.user
                                                .communityId) ==
                                            e.key) {
                                          model.responseData.user.communityId =
                                              e.value.toString();
                                        }
                                      } catch (e) {}
                                    }
                                    //
                                    await DBMgr.shared.setUserProfile(
                                        user: model.responseData.user);
                                    await userData.setUserModel();
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                  if (!Server.isOtp) {
                                    Get.off(() => DashboardPage());
                                  } else {
                                    if (model.responseData.user
                                        .isMobileNumberVerified) {
                                      Get.off(() => DashboardPage());
                                    } else {
                                      await APIViewModel().req<
                                              PostEmailOtpAPIModel>(
                                          context: context,
                                          url: APIAuthCfg.POSTEMAILOTP_POST_URL,
                                          reqType: ReqType.Post,
                                          param: {
                                            "Email": email.text.trim(),
                                            "Status": "101",
                                          },
                                          callback: (model2) async {
                                            if (mounted) {
                                              if (model2 != null) {
                                                if (model2.success) {
                                                  await APIViewModel().req<
                                                          SendUserEmailOtpAPIModel>(
                                                      context: context,
                                                      url: APIAuthCfg
                                                          .SENDUSEREMAILOTP_GET_URL
                                                          .replaceAll(
                                                              "#otpId#",
                                                              model2
                                                                  .responseData
                                                                  .userOTP
                                                                  .id
                                                                  .toString()),
                                                      reqType: ReqType.Get,
                                                      callback: (model3) async {
                                                        if (mounted) {
                                                          if (model3 != null) {
                                                            if (model3
                                                                .success) {
                                                              final user = model
                                                                  .responseData
                                                                  .user;
                                                              Get.to(() =>
                                                                  OtpByEmailPage(
                                                                      email: user
                                                                          .email));
                                                            }
                                                          }
                                                        }
                                                      });
                                                }
                                              }
                                            }
                                          });
                                    }
                                  }
                                } else {
                                  try {
                                    final err = model.errorMessages.login[0];
                                    showToast(context: context, msg: err);
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              }
                            },
                            isCookie: true,
                          );
                        }
                      }),
                  SizedBox(height: getHP(context, 10)),
                ],
              ),
            ),
          ),

          //_drawSocialLoginBtn(),
        ]),
      ),
    );
  }

  _drawSocialLoginBtn() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            children: [
              Expanded(child: Divider(color: Colors.grey)),
              Txt(
                  txt: "or you can use",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Expanded(child: Divider(color: Colors.grey)),
            ],
          ),
        ),
        SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/google_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
            SizedBox(width: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/fb_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
