import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';

abstract class BaseReviewsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  refreshData();

  drawUserRatingList(List<UserRatingsModel> listUserRating) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        child: RefreshIndicator(
          color: Colors.white,
          backgroundColor: MyTheme.brandColor,
          onRefresh: refreshData,
          child: ListView.builder(
            addAutomaticKeepAlives: true,
            cacheExtent: AppConfig.page_limit.toDouble(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: true,
            itemCount: listUserRating.length,
            itemBuilder: (BuildContext context, int index) {
              return drawItem(listUserRating[index]);
            },
          ),
        ),
      ),
    );
  }

  drawItem(UserRatingsModel userRatingsModel) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
      child: ListTile(
        leading: CircleAvatar(
          radius: 40,
          backgroundColor: Colors.transparent,
          backgroundImage:
              MyNetworkImage.loadProfileImage(userRatingsModel.profileImageUrl),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: userRatingsModel.taskTitle,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 5),
            UIHelper().getStarRatingView(
              w: getW(context),
              h: getH(context),
              rate: userRatingsModel.rating.toInt(),
              reviews: null,
              starColor: MyTheme.brandColor,
              align: MainAxisAlignment.start,
            ),
            SizedBox(height: 5),
            Txt(
                txt: userRatingsModel.comments,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 5),
            drawLine(h: .1, colr: MyTheme.gray1Color),
          ],
        ),
      ),
    );
  }

  drawNF() {
    return Container(
      width: getW(context),
      height: double.infinity,
      alignment: Alignment.center,
      child: SingleChildScrollView(
        child: Column(
          //shrinkWrap: true,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: getWP(context, 60),
              height: getWP(context, 40),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_review.png"),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt: "There are no reviews to show!",
                  txtColor: MyTheme.gray5Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
