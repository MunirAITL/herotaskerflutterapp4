import 'dart:io';

import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/camera/cam_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/selfie_base.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/uploading_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelfiePage extends StatefulWidget {
  const SelfiePage({Key key}) : super(key: key);
  @override
  State createState() => _SelfiePageState();
}

class _SelfiePageState extends SelfieBase<SelfiePage> {
  @override
  void initState() {
    super.initState();
    scanDocData.file_selfie = null;
    scanDocData.file_drvlic_front = null;
    scanDocData.file_pp_front = null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Prove your identity"),
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.redColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    //print(userData.userModel.userCompanyID);
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getW(context),
              height: getHP(context, 55),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/doc_scan/selfie_bg.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "Take a selfie",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt:
                      "To prove that it is really you on the identity document, you need to take a photo of yourself, a selfie. Make sure your face is clearly visible and you're the only person in the photo.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 20),
                child: MMBtn(
                    txt: "Continue",
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () async {
                      Get.to(() => CamPage(
                            isFront: Server.isOtp,
                            title: "Take a selfie",
                          )).then((value) {
                        if (value != null) {
                          scanDocData.file_selfie = value;
                          Get.off(() => UploadingPage());
                        }
                      });
                    })),
          ],
        ),
      ),
    );
  }
}
