import 'package:aitl/mixin.dart';
import 'package:aitl/view_model/helper/utils/TaskDetailsHelper.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';

abstract class BasePaymentStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TaskDetailsHelper, TimeLineHelper {
  final taskCtrl = Get.put(TaskCtrl());
}
