import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

import '../../../config/server/APIAuthCfg.dart';
import '../../../config/server/Server.dart';
import '../../../data/model/auth/ChangePwdAPIModel.dart';
import '../../../data/network/NetworkMgr.dart';

class ChangePwdAPIMgr with Mixin {
  static final ChangePwdAPIMgr _shared = ChangePwdAPIMgr._internal();

  factory ChangePwdAPIMgr() {
    return _shared;
  }

  ChangePwdAPIMgr._internal();

  wsChangePwdAPI({
    BuildContext context,
    String curPwd,
    String newPwd,
    String newPwd2,
    Function(ChangePwdAPIModel) callback,
  }) async {
    try {
      await NetworkMgr().req<ChangePwdAPIModel, Null>(
        context: context,
        url: APIAuthCfg.CHANGE_PWD_URL,
        reqType: ReqType.Put,
        param: {
          'CurrentPassword': curPwd,
          'password': newPwd,
          'confirmPassword': newPwd2,
        },
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
