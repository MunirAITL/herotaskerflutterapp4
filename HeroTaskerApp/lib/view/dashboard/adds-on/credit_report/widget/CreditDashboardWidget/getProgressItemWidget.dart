import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class GetProgressItem extends StatelessWidget {
  String status;
  String value;
  String statusColor;

  GetProgressItem({this.status, this.value, this.statusColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(),
      height: 40,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              height: 25,
              child: Row(
                children: [
                  (status != "Excellent")
                      ? Container(
                          color: HexColor.fromHex(statusColor),
                          height: 30,
                          width: 25,
                        )
                      : Container(
                          color: HexColor.fromHex(statusColor),
                          height: 30,
                          width: 25,
                          child: Icon(
                            Icons.check_circle_outline,
                            color: Colors.white,
                          ),
                        ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: Txt(
                      txt: "$status",
                      txtColor: Colors.black,
                      txtAlign: TextAlign.start,
                      isBold: false,
                      txtSize: MyTheme.txtSize - .5,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
                child: Txt(
              txt: "$value",
              txtColor: Colors.black,
              txtAlign: TextAlign.center,
              isBold: false,
              txtSize: MyTheme.txtSize - .5,
            )),
          )
        ],
      ),
    );
  }
}
