import 'dart:developer';

import 'package:aitl/config/server/APIActionReqCfg.dart';
import 'package:aitl/data/model/dashboard/action_req/SubmitCaseAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class SubmitCaseAPIMgr with Mixin {
  static final SubmitCaseAPIMgr _shared = SubmitCaseAPIMgr._internal();

  factory SubmitCaseAPIMgr() {
    return _shared;
  }

  SubmitCaseAPIMgr._internal();

  wsOnPostCase({
    BuildContext context,
    dynamic param,
    Function(SubmitCaseAPIModel) callback,
  }) async {
    try {
      log(param);
      await NetworkMgr()
          .req<SubmitCaseAPIModel, Null>(
        context: context,
        url: APIActionReqCfg.SUBMITCASE_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }
}
