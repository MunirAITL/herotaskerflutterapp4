class GetTaskListGroupChatAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetTaskListGroupChatAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetTaskListGroupChatAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<CaseGroupMessageData> caseGroupMessageData;
  ResponseData({this.caseGroupMessageData});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['CaseGroupMessageData'] != null) {
      caseGroupMessageData = <CaseGroupMessageData>[];
      json['CaseGroupMessageData'].forEach((v) {
        caseGroupMessageData.add(new CaseGroupMessageData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.caseGroupMessageData != null) {
      data['CaseGroupMessageData'] =
          this.caseGroupMessageData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CaseGroupMessageData {
  int id;
  String title;
  String description;
  String lastMessage;
  bool isRead;
  String lastMessageDateTime;
  String name;
  String mobileNumber;
  String isOnline;
  String logOutTime;
  String referenceType;
  String imageServerUrl;
  String ownerImageUrl;
  String status;
  String preferedLocation;
  double fixedBudgetAmount;
  bool isFixedPrice;
  int totalHours;

  CaseGroupMessageData(
      {this.id,
      this.title,
      this.description,
      this.lastMessage,
      this.isRead,
      this.lastMessageDateTime,
      this.name,
      this.mobileNumber,
      this.isOnline,
      this.logOutTime,
      this.referenceType,
      this.imageServerUrl,
      this.ownerImageUrl,
      this.status,
      this.preferedLocation,
      this.fixedBudgetAmount,
      this.isFixedPrice,
      this.totalHours});

  CaseGroupMessageData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    title = json['Title'];
    description = json['Description'];
    lastMessage = json['LastMessage'];
    isRead = json['IsRead'].toString().toLowerCase() == "true" ? true : false;
    lastMessageDateTime = json['LastMessageDateTime'];
    name = json['Name'];
    mobileNumber = json['MobileNumber'];
    isOnline = json['IsOnline'];
    logOutTime = json['LogOutTime'];
    referenceType = json['ReferenceType'];
    imageServerUrl = json['ImageServerUrl'];
    ownerImageUrl = json['OwnerImageUrl'];
    status = json['Status'];
    preferedLocation = json['PreferedLocation'];
    fixedBudgetAmount = json['FixedBudgetAmount'];
    isFixedPrice =
        json['IsFixedPrice'].toString().toLowerCase() == "true" ? true : false;
    totalHours = json['TotalHours'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['LastMessage'] = this.lastMessage;
    data['IsRead'] = this.isRead;
    data['LastMessageDateTime'] = this.lastMessageDateTime;
    data['Name'] = this.name;
    data['MobileNumber'] = this.mobileNumber;
    data['IsOnline'] = this.isOnline;
    data['LogOutTime'] = this.logOutTime;
    data['ReferenceType'] = this.referenceType;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['Status'] = this.status;
    data['PreferedLocation'] = this.preferedLocation;
    data['FixedBudgetAmount'] = this.fixedBudgetAmount;
    data['IsFixedPrice'] = this.isFixedPrice;
    data['TotalHours'] = this.totalHours;
    return data;
  }
}
