import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../../main.dart';

drawAppbarText({
  bool isNext = true,
  String txt,
  Function callback,
}) =>
    GestureDetector(
      onTap: () {
        callback();
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
                flex: 2,
                child: Txt(
                    txt: txt,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    fontWeight: FontWeight.w600,
                    isBold: false)),
            Flexible(
                child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Icon(
                  isNext ? Icons.arrow_forward_ios : Icons.arrow_back_ios,
                  color: Colors.black,
                  size: 18),
            ))
          ],
        ),
      ),
    );
