import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class BtnOutlineBadge extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Function callback;
  double radius;
  BtnOutlineBadge({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
    this.radius = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {
        callback();
      },
      child: Text(
        txt,
        style: TextStyle(color: txtColor, fontSize: 12),
      ),
      style: OutlinedButton.styleFrom(
        minimumSize: Size.zero, // <-- Add this
        padding: EdgeInsets.only(left: 20, right: 20, top: 8, bottom: 8),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        side: BorderSide(width: .5, color: borderColor),
      ),
    );
  }
}
