import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/db/db_base.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:bloc_rest_api/bloc_rest_api.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../widgets/txt/Txt.dart';

class NotiPage extends StatefulWidget {
  final NotiModel notiModel;
  final int notificationId;
  final String title, body, webUrl;
  const NotiPage(
      {Key key,
      this.notificationId,
      this.title,
      this.body,
      this.webUrl,
      this.notiModel})
      : super(key: key);
  @override
  State createState() => _NotiPageState();
}

class _NotiPageState extends BaseDashboardMoreStatefull<NotiPage> {
//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  Future<void> refreshData() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        context
            .read<RequestCubit<NotiAPIModel>>()
            .request(NetworkMgr.shared.req<NotiAPIModel, Null>(
              context: context,
              url: APINotiCfg.NOTI_GET_URL
                  .replaceAll("#UserId#", userData.userModel.id.toString()),
              param: {"UserId": userData.userModel.id},
              reqType: ReqType.Get,
            ));
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    if (widget.notiModel != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        final map = getNotiEvents(widget.notiModel);
        if (map['callback'] != null) Function.apply(map['callback'], []);
      });
    }
    appInit();
  }

  @override
  void dispose() {
    try {
      taskCtrl.dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      final unreadNotificationCount = appData
          .taskNotificationCountAndChatUnreadCountData
          .numberOfUnReadNotification;
      if (unreadNotificationCount > 0)
        await PrefMgr.shared
            .setPrefInt("unreadNotificationCount", unreadNotificationCount);
    } catch (e) {}
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Notifications'),
          centerTitle: false,
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: RefreshIndicator(
        color: Colors.white,
        backgroundColor: MyTheme.brandColor,
        onRefresh: refreshData,
        child: BlocConsumer<RequestCubit<NotiAPIModel>,
            RequestState<NotiAPIModel>>(
          listener: (context, state) {
            if (state.status == RequestStatus.failure) {
              Scaffold.of(context).showSnackBar(
                SnackBar(content: Text(state.errorMessage)),
              );
            }
          },
          builder: (context, state) {
            switch (state.status) {
              case RequestStatus.empty:
              case RequestStatus.failure:
                return drawNF();
              case RequestStatus.loading:
                return Center(child: CircularProgressIndicator());
              case RequestStatus.success:
                return Container(
                  child: state.model.responseData.notifications.length > 0
                      ? SingleChildScrollView(
                          primary: true,
                          child: drawNotiView(
                              state.model.responseData.notifications, true),
                        )
                      : drawNF(),
                );

              default:
                return Container();
            }
          },
        ),
      ),
    );
  }

  drawNF() {
    return Container(
      width: getW(context),
      height: double.infinity,
      alignment: Alignment.center,
      child: SingleChildScrollView(
        child: Column(
          //shrinkWrap: true,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: getWP(context, 60),
              height: getWP(context, 40),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_noti.png"),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt: "There are no notifications to show!",
                  txtColor: MyTheme.gray5Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
