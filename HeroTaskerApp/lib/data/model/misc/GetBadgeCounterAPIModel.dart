class GetBadgeCounterAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetBadgeCounterAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetBadgeCounterAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  TaskNotificationCountAndChatUnreadCountData
      taskNotificationCountAndChatUnreadCountData;

  ResponseData({this.taskNotificationCountAndChatUnreadCountData});

  ResponseData.fromJson(Map<String, dynamic> json) {
    taskNotificationCountAndChatUnreadCountData =
        json['TaskNotificationCountAndChatUnreadCountData'] != null
            ? new TaskNotificationCountAndChatUnreadCountData.fromJson(
                json['TaskNotificationCountAndChatUnreadCountData'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskNotificationCountAndChatUnreadCountData != null) {
      data['TaskNotificationCountAndChatUnreadCountData'] =
          this.taskNotificationCountAndChatUnreadCountData.toJson();
    }
    return data;
  }
}

class TaskNotificationCountAndChatUnreadCountData {
  int numberOfUnReadMessage;
  int numberOfUnReadNotification;

  TaskNotificationCountAndChatUnreadCountData(
      {this.numberOfUnReadMessage, this.numberOfUnReadNotification});

  TaskNotificationCountAndChatUnreadCountData.fromJson(
      Map<String, dynamic> json) {
    numberOfUnReadMessage = json['NumberOfUnReadMessage'];
    numberOfUnReadNotification = json['NumberOfUnReadNotification'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['NumberOfUnReadMessage'] = this.numberOfUnReadMessage;
    data['NumberOfUnReadNotification'] = this.numberOfUnReadNotification;
    return data;
  }
}
