import 'Server.dart';

class ServerUrls {
  static const String MISSING_IMG =
      Server.BASE_URL + "/api/content/media/default_avatar.png";

  ///static const String ABOUTUS_URL =
  //"https://app.mortgage-magic.co.uk/apps/about-me/";
  //static const String HELP_INFO_URL =
  //"https://www.mortgagemagic.com/privacy-policy/";
  static const COMMUNITY_GUIDLINES_URL =
      "https://herotasker.com/community-guidelines";
  static const String TC_URL =
      "https://support.herotasker.com/terms-and-conditions/";
  static const String PRIVACY_URL =
      "https://support.herotasker.com/privacy-policy";

  static const String BADGE_SUPPORT_URL =
      "https://support.herotasker.com/badges";

  static const PAYMENT_TC_URL =
      "https://support.herotasker.com/terms-and-conditions/";

  static const String TERMS_POLICY = Server.BASE_URL +
      "/api/termsprivacynoticesetup/getbycompanyid?UserCompanyId=#UserCompanyId#";
}
