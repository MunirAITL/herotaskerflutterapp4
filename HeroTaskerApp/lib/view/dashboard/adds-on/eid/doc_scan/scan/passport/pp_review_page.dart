import 'dart:convert';
import 'dart:io';
import 'package:aitl/config/server/APIEidVeriCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/eid/doc_scan/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/data/model/ads-on/eid/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/camera/cam_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/passport/pp_result_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/passport/pp_review_base.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/scan_id_doc_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/scan_helper.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';
import 'package:mime_type/mime_type.dart';

class PPReviewPage extends StatefulWidget {
  const PPReviewPage({Key key}) : super(key: key);
  @override
  State createState() => _PPReviewPageState();
}

class _PPReviewPageState extends PPReviewBase<PPReviewPage> {
  final list = [
    "Check the following:",
    "* Image is sharp",
    "* There are no reflections",
    "* All personal details are visible"
  ];

  @override
  void initState() {
    animationController = new AnimationController(
        duration: new Duration(seconds: 1), vsync: this);
    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animateScanAnimation(true);
      } else if (status == AnimationStatus.dismissed) {
        animateScanAnimation(false);
      }
    });
    animateScanAnimation(false);
    setState(() {
      isFrontAnim = false;
    });

    super.initState();
  }

  @override
  void dispose() {
    if (animationController != null) animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Prove your identity"),
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.off(() => ScanIDDocPage());
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.redColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Container(
              color: MyTheme.greyColor,
              child: Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawPicBox(),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          ScanHelper().drawLine(h: 1),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Review photos of passport",
                              txtColor: Colors.black87,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          drawIssues(list),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 20, left: 20, right: 20, bottom: 10),
              child: (!isFrontAnim)
                  ? MMBtn(
                      txt: "Confirm",
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 0,
                      callback: () async {
                        /*isFrontAnim = true;
                        setState(() {});
                        doFrontOCR(() {
                          isFrontAnim = false;
                          setState(() {});
                        });*/
                        wsUploadDoc();
                      })
                  : SizedBox(),
            ),
            Center(
              child: TextButton(
                onPressed: () {
                  scanDocData.file_pp_front = null;
                  Get.off(() => CamPage(
                        isFront: false,
                        title: "Scan your passport",
                      )).then((value) {
                    if (value != null) {
                      scanDocData.file_pp_front = value;
                    }
                  });
                },
                child: Text(
                  "Rescan passport",
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: 17,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  wsUploadDoc() async {
    String mimeType = mime(scanDocData.file_pp_front);
    var fileName = (scanDocData.file_pp_front.split('/').last);
    //String mimee = mimeType.split('/')[0];
    String type = mimeType.split('/')[1];
    List<int> fileInByte = File(scanDocData.file_pp_front).readAsBytesSync();
    final imageData = base64Encode(fileInByte);
    //print(imageData);
    final param = {
      "UserCompanyId": userData.userModel.userCompanyID,
      "ImageType": type,
      "FileName": fileName,
      "UserId": userData.userModel.id,
      "ContentData": imageData,
      "DocumentType": "2",
      "RequestId": scanDocData.requestId,
    };
    final jsonString = JsonString(json.encode(param));
    myLog(jsonString.source);
    await APIViewModel().req<PostDocByBase64BitAPIModel>(
        context: context,
        url: APIEidCfg.DOC_POST_URL,
        reqType: ReqType.Post,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        param: param,
        callback: (model2) async {
          if (mounted) {
            if (model2 != null) {
              if (model2.success) {
                if (model2.responseData.responsedata.organizeOcrData == null) {
                  tryAgainAlert(
                      isDocVerify: false,
                      msg:
                          'Sorry, image quality is too lower. Please re-upload document image.');
                } else {
                  wsUploadDocVerify();
                }
              } else {
                tryAgainAlert(isDocVerify: false);
              }
            } else {
              tryAgainAlert(isDocVerify: false);
            }
          }
        });
  }

  wsUploadDocVerify() async {
    await APIViewModel().req<PostDocVerifyAPIModel>(
        context: context,
        url: APIEidCfg.VERIFY_POST_URL,
        reqType: ReqType.Post,
        param: {
          "UserId": userData.userModel.id,
          "Requestid": scanDocData.requestId,
        },
        callback: (model3) {
          if (mounted) {
            if (model3 != null) {
              if (model3.success) {
                Get.off(() => PPResultPage(model: model3));
              } else {
                tryAgainAlert(isDocVerify: true);
              }
            } else {
              tryAgainAlert(isDocVerify: true);
            }
          }
        });
  }

  tryAgainAlert(
      {bool isDocVerify,
      String msg = 'Passport image is not valid\nTry again'}) {
    confirmDialog(
      context: context,
      title: "Uploading Alert",
      msg: msg,
      callbackYes: () {
        if (!isDocVerify)
          wsUploadDoc();
        else
          wsUploadDocVerify();
      },
    );
  }
}
