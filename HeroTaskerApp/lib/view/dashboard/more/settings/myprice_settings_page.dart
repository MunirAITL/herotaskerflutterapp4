import 'package:aitl/config/server/APIPayCfg.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';

import '../../../../config/theme/MyTheme.dart';
import '../../../../data/model/dashboard/more/payment/UserPriceAPIModel.dart';
import '../../../../data/network/NetworkMgr.dart';
import '../../../../view_model/api/api_view_model.dart';
import '../../../../view_model/helper/auth/ChangePwdAPIMgr.dart';
import '../../../../view_model/helper/utils/UIHelper.dart';
import '../../../widgets/input/InputTitleBoxHT.dart';
import '../../../widgets/txt/PriceBox.dart';

class MyPriceSettingsPage extends StatefulWidget {
  @override
  State createState() => _MyPriceSettingsPageState();
}

class _MyPriceSettingsPageState extends State<MyPriceSettingsPage> with Mixin {
  final poundPerHr = TextEditingController();
  final poundPerDay = TextEditingController();
  final poundPerMonth = TextEditingController();
  //
  final focusPerHr = FocusNode();
  final focusPerDay = FocusNode();
  final focusPerMonth = FocusNode();

  bool isLoading = false;
  int userPriceId;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    poundPerHr.dispose();
    poundPerDay.dispose();
    poundPerMonth.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      APIViewModel().req<UserPriceAPIModel>(
          context: context,
          url: APIPayCfg.USERPRICE_GET_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                final priceModel = model.responseData.userPrice;
                userPriceId = priceModel.id;
                poundPerHr.text = priceModel.perHour.toString();
                poundPerDay.text = priceModel.perDay.toString();
                poundPerMonth.text = priceModel.perMonth.toString();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  validate() {
    if (poundPerHr.text.trim().length == 0 &&
        poundPerDay.text.trim().length == 0 &&
        poundPerMonth.text.trim().length == 0) {
      showToast(context: context, msg: "Invalid amounts");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: MyTheme.appbarElevation,
          /*leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),*/
          title: UIHelper().drawAppbarTitle(title: "My price settings"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 30),
                /*Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: 'Change your',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          )),
                      TextSpan(
                        text: '\npassword',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 40),*/
                InputTitleBoxHT(
                  title: "Pound per hour",
                  ph: "0.0",
                  input: poundPerHr,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusPerHr,
                  focusNodeNext: focusPerDay,
                  len: 7,
                  minLen: 0,
                  fontSize: 24,
                  prefixIco: Text(getCurSign(),
                      style: TextStyle(color: Colors.black, fontSize: 25)),
                  //onChange: calculation(),
                ),
                SizedBox(height: 20),
                InputTitleBoxHT(
                  title: "Pound per day",
                  ph: "0.0",
                  input: poundPerDay,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusPerDay,
                  focusNodeNext: focusPerMonth,
                  len: 7,
                  minLen: 0,
                  fontSize: 24,
                  prefixIco: Text(getCurSign(),
                      style: TextStyle(color: Colors.black, fontSize: 25)),
                  //onChange: calculation(),
                ),
                SizedBox(height: 20),
                InputTitleBoxHT(
                  title: "Pound per month",
                  ph: "0.0",
                  input: poundPerMonth,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.done,
                  focusNode: focusPerMonth,
                  len: 7,
                  minLen: 0,
                  fontSize: 24,
                  prefixIco: Text(getCurSign(),
                      style: TextStyle(color: Colors.black, fontSize: 25)),
                  //onChange: calculation(),
                ),
                SizedBox(height: 30),
                MMBtn(
                    txt: "Submit",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {
                      if (validate()) {
                        double priceHr = 0;
                        double priceDay = 0;
                        double priceMonth = 0;
                        try {
                          priceHr = double.parse(poundPerHr.text);
                        } catch (e) {}
                        try {
                          priceDay = double.parse(poundPerDay.text);
                        } catch (e) {}
                        try {
                          priceMonth = double.parse(poundPerMonth.text);
                        } catch (e) {}

                        if (userPriceId == null) {
                          APIViewModel().req<UserPriceAPIModel>(
                              context: context,
                              url: APIPayCfg.USERPRICE_POST_URL,
                              param: {
                                "UserId": userData.userModel.id,
                                "User": null,
                                "Status": 101,
                                "CreationDate": DateTime.now().toString(),
                                "PerHour": priceHr,
                                "PerDay": priceDay,
                                "PerMonth": priceMonth,
                                "Id": 0
                              },
                              reqType: ReqType.Post,
                              callback: (model) async {
                                if (mounted && model != null) {
                                  if (model.success) {
                                    showToast(
                                        context: context,
                                        msg: "UserPrice created successfully",
                                        which: 1);
                                  }
                                }
                              });
                        } else {
                          APIViewModel().req<UserPriceAPIModel>(
                              context: context,
                              url: APIPayCfg.USERPRICE_PUT_URL,
                              param: {
                                "UserId": 0,
                                "User": null,
                                "Status": 101,
                                "CreationDate": DateTime.now().toString(),
                                "PerHour": priceHr,
                                "PerDay": priceDay,
                                "PerMonth": priceMonth,
                                "Id": userPriceId
                              },
                              reqType: ReqType.Put,
                              callback: (model) async {
                                if (mounted && model != null) {
                                  if (model.success) {
                                    showToast(
                                        context: context,
                                        msg: "UserPrice updated successfully",
                                        which: 1);
                                  }
                                }
                              });
                        }
                      }
                    }),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
