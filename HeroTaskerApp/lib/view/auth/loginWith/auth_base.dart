import 'dart:io';

import 'package:aitl/config/server/APILoginWithCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/loginWith/LoginWithModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:auto_size_text/auto_size_text.dart';
import '../../../config/theme/MyTheme.dart';
import '../../../main.dart';

abstract class BaseAuth<T extends StatefulWidget> extends State<T>
    with Mixin, UIHelper {
  /*loginWithGoogle(cls) async {
    try {
      final FirebaseAuth _auth = FirebaseAuth.instance;
      final GoogleSignIn googleSignIn = GoogleSignIn();
      try {
        if (await googleSignIn.isSignedIn()) await googleSignIn.signOut();
      } catch (e) {
        myLog("sign out error " + e.toString());
      }

      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      // Create a new credential
      final credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      final authResult = await _auth.signInWithCredential(credential);
      final user = authResult.user;

      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      try {
        final nameMap = Common.splitName(user.displayName);
        final json = {
          'Email': user.email.toString(),
          'FirstName': nameMap['fname'],
          'LastName': nameMap['lname'],
          'Persist': true,
          'ReferenceId': user.uid.toString(),
          'ReferenceType': "google"
        };
        var model = LoginWithModel.fromJson(json);
        APIViewModel().req<LoginAPIModel>(
          context: context,
          apiState: APIState(APIType.loginWithG, cls, null),
          url: APILoginWithCfg.GOOGLE_LOGIN_URL,
          param: model.toJson(),
          reqType: ReqType.Post,
        );
      } catch (e) {
        myLog("google login uid = " + e.toString());
      }
    } catch (e) {
      myLog("google login uid 2 = " + e.toString());
    }
  }

  loginWithFB(cls) async {
    try {
      final facebookLogin = FacebookLogin();
      // Let's force the users to login using the login dialog based on WebViews. Yay!
      facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
      final result = await facebookLogin.logIn(['email', 'public_profile']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          //_sendTokenToServer(result.accessToken.token);
          //_showLoggedInUI();
          final OAuthCredential credential =
              FacebookAuthProvider.credential(result.accessToken.token);

          final FirebaseAuth _auth = FirebaseAuth.instance;
          final userCredential = await _auth.signInWithCredential(credential);
          final user = userCredential.user;

          try {
            final nameMap = Common.splitName(user.displayName);
            final json = {
              'Email': user.email.toString(),
              'FirstName': nameMap['fname'],
              'LastName': nameMap['lname'],
              'Persist': true,
              'ReferenceId': user.uid.toString(),
              'ReferenceType': "Facebook"
            };
            var model = LoginWithModel.fromJson(json);
            APIViewModel().req<LoginAPIModel>(
              context: context,
              apiState: APIState(APIType.loginWithFB, cls, null),
              url: APILoginWithCfg.FB_LOGIN_URL,
              param: model.toJson(),
              reqType: ReqType.Post,
            );
          } catch (e) {
            myLog("google login uid = " + e.toString());
          }
          break;
        case FacebookLoginStatus.cancelledByUser:
          //_showCancelledMessage();
          break;
        case FacebookLoginStatus.error:
          //_showErrorOnUI(result.errorMessage);
          showToast(context: context, msg: result.errorMessage);
          break;
      }
    } catch (e) {
      myLog("google login uid 2 = " + e.toString());
    }
  }

  Future<User> signInWithApple(cls) async {
    var redirectURL =
        "https://herotasker-fdaad.firebaseapp.com/__/auth/handler";
    var clientID = "com.herotasker";
    var appleIdCredential;
    if (Platform.isAndroid) {
      appleIdCredential = await SignInWithApple.getAppleIDCredential(
          scopes: [
            AppleIDAuthorizationScopes.email,
            AppleIDAuthorizationScopes.fullName,
          ],
          webAuthenticationOptions: WebAuthenticationOptions(
              clientId: clientID, redirectUri: Uri.parse(redirectURL)));
    } else {
      appleIdCredential = await SignInWithApple.getAppleIDCredential(scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ]);
    }
    final oAuthProvider = OAuthProvider('apple.com');
    final credential = oAuthProvider.credential(
      idToken: appleIdCredential.identityToken,
      accessToken: appleIdCredential.authorizationCode,
    );

    try {
      final FirebaseAuth _auth = FirebaseAuth.instance;

      final authResult = await _auth.signInWithCredential(credential);
      final user = authResult.user;

      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      try {
        // debugPrint("apple login uid = " + user.uid);
        try {
          debugPrint("apple login email = " + user.email);
        } catch (e) {
          debugPrint("apple login name error = " + e.toString());
        }
        try {
          debugPrint("apple login name = " + user.displayName.toString());
        } catch (e) {
          debugPrint("apple login name error = " + e.toString());
        }
        // debugPrint("apple login photo url  = " + user.photoURL);

        var firstName = "";
        var lastName = "";
        int position = 0;

        if (user.displayName != null && user.displayName.isNotEmpty) {
          for (int i = 0; i < user.displayName.length; i++) {
            if (user.displayName[i] == " ") {
              position = i;
              break;
            }
            firstName += user.displayName[i];
          }
          lastName = user.displayName.substring(position);
        }
        debugPrint("first name   = " + firstName);
        debugPrint("last name  = " + lastName);
        debugPrint("user id  = " + user.uid.toString());

        final nameMap = Common.splitName(user.displayName);
        final json = {
          'Email': user.email.toString(),
          'FirstName': nameMap['fname'],
          'LastName': nameMap['lname'],
          'Persist': true,
          'ReferenceId': user.uid.toString(),
          'ReferenceType': "apple"
        };
        var model = LoginWithModel.fromJson(json);
        APIViewModel().req<LoginAPIModel>(
          context: context,
          apiState: APIState(APIType.loginWithG, cls, null),
          url: APILoginWithCfg.GOOGLE_LOGIN_URL,
          param: model.toJson(),
          reqType: ReqType.Post,
        );
      } catch (e) {}
    } catch (e) {}
  }*/

  /*checkLoginRes(LoginAPIModel model) async {
    if (model.success) {
      try {
        await DBMgr.shared.setUserProfile(user: model.responseData.user);
        await userData.setUserModel();
      } catch (e) {
        myLog(e.toString());
      }
      Get.off(() => DashboardPage());
    } else {
      try {
        final err = model.errorMessages.login[0];
        showToast(context: context, msg: err);
      } catch (e) {
        myLog(e.toString());
      }
    }
  }*/

  //

  drawAppbar({
    Function onBack,
    String backTitle,
    String title,
    List<IconButton> listBtn,
    double elevation = 0,
    PreferredSizeWidget bottom,
    Color bgColor = Colors.transparent,
  }) {
    return AppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      elevation: elevation,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          onBack != null
              ? Flexible(
                  child: GestureDetector(
                    onTap: () {
                      onBack();
                    },
                    child: Container(
                      color: Colors.transparent,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.arrow_back_ios,
                            color: Colors.black,
                            size: MyTheme.fontSize,
                          ),
                          //SizedBox(width: 5),
                          /*(backTitle != null)
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 0),
                                  child: Text(backTitle,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.white,
                                          fontWeight: FontWeight.normal)),
                                )
                              : SizedBox()*/
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          (title != null)
              ? Expanded(
                  flex: 3,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      //color: Colors.yellow,
                      child: Flexible(
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: AutoSizeText(title,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
      actions: listBtn,
      bottom: bottom,
    );
  }
}
