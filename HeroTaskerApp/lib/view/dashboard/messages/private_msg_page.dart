import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../../data/app_data/PrefMgr.dart';
import '../../../data/model/dashboard/timeline/GetTaskListGroupChatAPIModel.dart';
import 'private_msg_base.dart';

class PrivateMsgPage extends StatefulWidget {
  final String title2;
  const PrivateMsgPage({Key key, this.title2}) : super(key: key);
  @override
  State createState() => _PrivateMsgPageState();
}

class _PrivateMsgPageState extends BasePrivateMsgStatefull<PrivateMsgPage>
    with StateListener, SingleTickerProviderStateMixin {
  List<CaseGroupMessageData> listCaseGroupMessageData = [];

  //  search stuff start
  //  0
  //  Sometime rebuilding whole screen might not be desirable with setState((){})
  //  for this situation you can wrap searchables with ValuelistenableBuilder widget.
  ValueNotifier<List<CaseGroupMessageData>> filtered =
      ValueNotifier<List<CaseGroupMessageData>>([]);
  FocusNode searchFocus = FocusNode();
  final searchText = TextEditingController();
  bool isSearchIconClicked = false;
  bool searching = false;
  bool isRefreshing = false;
  //  search stuff end

  //  page stuff start here
  bool isPageDone = false;
  int page = 0;
  int count = AppConfig.page_limit;

  //  tab stuff start here
  int status = TaskStatusCfg.STATUS_ALL_PRIVATEMESSAGE;

  double distance;
  double fromPrice;
  double toPrice;
  double lat;
  double lng;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_BADGE_MESSAGE_COUNT && data == 3) {
        if (mounted) {
          final unreadMessageCount =
              appData.taskNotificationCountAndChatUnreadCountData != null
                  ? appData.taskNotificationCountAndChatUnreadCountData
                      .numberOfUnReadMessage
                  : 0;
          if (unreadMessageCount > 0) {
            appData.taskNotificationCountAndChatUnreadCountData
                .numberOfUnReadMessage = 0;
            await PrefMgr.shared
                .setPrefInt("unreadMessageCount", unreadMessageCount);
          }
          StateProvider().notify(ObserverState.STATE_BOTNAV, null);
          setState(() {});
        }
      } else if (state == ObserverState.STATE_RELOAD_TAB ||
          state == ObserverState.STATE_RELOAD_TAB_MESSAGES) {
        refreshData();
      }
    } catch (e) {}
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await APIViewModel().req<GetTaskListGroupChatAPIModel>(
          context: context,
          url: APITimelineCfg.GET_TASKLIST4GROUPCHATDATA_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          isLoading: false,
          reqType: ReqType.Get,
          param: {
            "Count": count,
            "Distance": distance ?? 0,
            "FromPrice": fromPrice ?? 20,
            "InPersonOrOnline": 0,
            "IsHideAssignTask": false,
            "Latitude": lat ?? 0.0,
            "Location": "",
            "Longitude": lng ?? 0.0,
            "Page": page,
            "SearchText": searchText.text ?? '',
            "Status": status,
            "ToPrice": toPrice ?? 2000000,
            "UserId": userData.userModel.id,
          },
          callback: (model) {
            if (model != null && mounted) {
              final caseGroupMessageData =
                  model.responseData.caseGroupMessageData;
              if (caseGroupMessageData != null) {
                //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                if (caseGroupMessageData.length != count) {
                  isPageDone = true;
                }
                try {
                  for (CaseGroupMessageData task in caseGroupMessageData) {
                    listCaseGroupMessageData.add(task);
                  }
                  setState(() {});
                } catch (e) {
                  myLog(e.toString());
                }
              } else {}
            }
          });
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> refreshData() async {
    if (mounted) {
      setState(() {
        isSearchIconClicked = false;
        searchText.clear();
        searching = false;
        filtered.value = [];
        if (searchFocus.hasFocus) searchFocus.unfocus();
        //
        page = 0;
        isPageDone = false;
        isLoading = true;
        listCaseGroupMessageData.clear();
      });
      onLazyLoadAPI();
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    listCaseGroupMessageData = null;
    searchText.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    String title = "Private messages";
    try {
      /*if (taskCtrl.getTaskModel() != null &&
          taskCtrl.getTaskModel().title.isNotEmpty) {
        title = title + " : " + taskCtrl.getTaskModel().title;
      } else {*/
      if (widget.title2 != null) {
        title = title + " : " + widget.title2;
      }
      //}
    } catch (e) {}

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: MyTheme.appbarElevation,
          //automaticallyImplyLeading: !isSearchIconClicked,
          leadingWidth: (!isSearchIconClicked) ? 0 : 56,
          leading: (!isSearchIconClicked)
              ? SizedBox()
              : IconButton(
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    isSearchIconClicked = !isSearchIconClicked;
                    searchText.clear();
                    searching = false;
                    filtered.value = [];
                    if (searchFocus.hasFocus) searchFocus.unfocus();
                    setState(() {});
                  },
                  icon: Icon(Icons.arrow_back_ios, size: MyTheme.fontSize)),
          title: (!isSearchIconClicked)
              ? UIHelper().drawAppbarTitle(title: title)
              : drawSearchbar(searchText, (text) {
                  if (text.length > 0) {
                    searching = true;
                    filtered.value = [];
                    listCaseGroupMessageData.forEach((locModel) {
                      if (locModel.title
                              .toString()
                              .toLowerCase()
                              .contains(text.toLowerCase()) ||
                          locModel.name
                              .toString()
                              .toLowerCase()
                              .contains(text.toLowerCase())) {
                        filtered.value.add(locModel);
                      }
                    });
                  } else {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    searchText.clear();
                    searching = false;
                    filtered.value = [];
                    if (searchFocus.hasFocus) searchFocus.unfocus();
                  }
                }),
          centerTitle: false,
          actions: <Widget>[
            (!isSearchIconClicked)
                ? Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: IconButton(
                        icon: Icon(
                          Icons.search,
                          size: 30,
                        ),
                        onPressed: () {
                          isSearchIconClicked = !isSearchIconClicked;
                          setState(() {});
                        }),
                  )
                : SizedBox()
          ],
          bottom: drawAppbarNavBar((index) {
            if (!isLoading) {}
          }),
        ),
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (listCaseGroupMessageData.length > 0)
          ? ValueListenableBuilder<List>(
              valueListenable: filtered,
              builder: (context, value, _) {
                return RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.brandColor,
                  onRefresh: () async {
                    isRefreshing = true;
                    refreshData();
                    return;
                  },
                  notificationPredicate: (scrollNotification) {
                    if (scrollNotification is ScrollStartNotification) {
                      //print('Widget has started scrolling');
                    } else if (scrollNotification is ScrollEndNotification) {
                      Future.delayed(Duration(seconds: 1), () {
                        if (!isRefreshing && !isPageDone) {
                          page++;
                          onLazyLoadAPI();
                        }
                      });
                    }
                    return true;
                  },
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: searching
                        ? filtered.value.length
                        : listCaseGroupMessageData.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawItem(searching
                          ? filtered.value[index]
                          : listCaseGroupMessageData[index]);
                    },
                  ),
                );
              },
            )
          : (!isLoading)
              ? drawNF()
              : Container(),
    );
  }
}
