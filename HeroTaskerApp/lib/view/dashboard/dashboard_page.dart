import 'dart:developer';
import 'dart:io';

import 'package:aitl/config/app/events/DeviceEventTypesCfg.dart';
import 'package:aitl/config/server/APIBadgeCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/model/misc/rating/PendingReviewRatingByUserIdAPIModel.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/dashboard/findworks/find_works_page.dart';
import 'package:aitl/view/dashboard/messages/private_msg_page.dart';
import 'package:aitl/view/dashboard/more/more_page.dart';
import 'package:aitl/view/dashboard/mytasks/mytasks_page.dart';
import 'package:aitl/view/dashboard/post_task/post_task_page.dart';
import 'package:aitl/view/widgets/botnav/bottomNavigation.dart';
import 'package:aitl/view/widgets/botnav/tabItem.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/dialog/HelpTutDialog.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/APIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../data/app_data/AppData.dart';
import '../../data/model/misc/GetBadgeCounterAPIModel.dart';
import 'mytasks/funds/review_page.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashboardPage extends StatefulWidget {
  static const int TAB_POSTTASK = 0;
  static const int TAB_MYTASK = 1;
  static const int TAB_FINDWORKS = 2;
  static const int TAB_MESSAGES = 3;
  static const int TAB_MORE = 4;

  @override
  State createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage>
    with Mixin, APIStateListener, StateListener {
  final List<Widget> listTabbar = [];
  bool isDialogHelpOpenned = false;

  final botNavController = Get.put(BotNavController());

  //var _androidAppRetain = MethodChannel("android_app_retain");

  //static int currentTab = 0;

  Future<bool> onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Confirmation!'),
            content:
                new Text('Are you sure, do you want to log out from the app?'),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(false),
                child: Text("No"),
              ),
              SizedBox(height: 16),
              new GestureDetector(
                onTap: () {
                  StateProvider().notify(ObserverState.STATE_LOGOUT, null);
                },
                child: Text("Yes"),
              ),
            ],
          ),
        ) ??
        false;
  }

  final List<TabItem> tabs = [
    TabItem(
      tabName: "Dashboard",
      icon: AssetImage("assets/images/tabbar/ic_new_case.png"),
      //page: NewCaseTab(),
      page: PostTaskListPage(),
    ),
    TabItem(
      tabName: "My Tasks",
      icon: AssetImage("assets/images/tabbar/ic_task.png"),
      page: MyTasksPage(),
    ),
    TabItem(
      tabName: "Find Work",
      icon: AssetImage("assets/images/tabbar/ic_search.png"),
      page: FindWorksPage(),
    ),
    TabItem(
      tabName: "Messages",
      icon: AssetImage("assets/images/tabbar/ic_message.png"),
      page: PrivateMsgPage(),
    ),
    TabItem(
      tabName: "More",
      icon: AssetImage("assets/images/tabbar/ic_more.png"),
      page: MorePage(),
    )
  ];

  DashboardPageState() {
    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  void _selectTab(int index) async {
    //if (index == botNavController.index.value) {
    // pop to first route
    // if the user taps on the active tab
    //tabs[index].key.currentState.popUntil((route) => route.isFirst);
    //setState(() {});
    //} else {
    // update the state
    // in order to repaint

    if (mounted) {
      switch (index) {
        case 0:
          StateProvider().notify(ObserverState.STATE_RELOAD_TAB_NEW_TASK, null);
          break;
        case 1:
          StateProvider().notify(ObserverState.STATE_RELOAD_TAB_MY_TASKS, null);
          break;
        case 2:
          StateProvider()
              .notify(ObserverState.STATE_RELOAD_TAB_FIND_WORK, null);
          break;
        case 3:
          StateProvider().notify(ObserverState.STATE_RELOAD_TAB_MESSAGES, null);
          wsBadgeCounterAPI(index);
          break;
        case 4:
          StateProvider().notify(ObserverState.STATE_RELOAD_TAB_MORE, null);
          wsBadgeCounterAPI(index);
          break;
        default:
      }
      setState(() => botNavController.index.value = index);
    }
    //}
  }

  wsBadgeCounterAPI(int tabIndex) async {
    try {
      await APIViewModel().req<GetBadgeCounterAPIModel>(
          context: context,
          url: APIBadgeCfg.GET_BADGE_COUNTER_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          isLoading: false,
          reqType: ReqType.Get,
          callback: (model) async {
            if (model != null && mounted) {
              //appData.taskNotificationCountAndChatUnreadCountData = model
              //.responseData.taskNotificationCountAndChatUnreadCountData;

              final taskNotificationCountAndChatUnreadCountData = model
                  .responseData.taskNotificationCountAndChatUnreadCountData;

              // tmp
              //taskNotificationCountAndChatUnreadCountData
              //.numberOfUnReadNotification = 1;
              //taskNotificationCountAndChatUnreadCountData
              //.numberOfUnReadMessage = 2;

              final unreadNotificationCount =
                  await PrefMgr.shared.getPrefInt("unreadNotificationCount");
              final unreadMessageCount =
                  await PrefMgr.shared.getPrefInt("unreadMessageCount");
              try {
                appData.taskNotificationCountAndChatUnreadCountData =
                    TaskNotificationCountAndChatUnreadCountData.fromJson({
                  "NumberOfUnReadMessage": unreadMessageCount ==
                          taskNotificationCountAndChatUnreadCountData
                              .numberOfUnReadMessage
                      ? 0
                      : taskNotificationCountAndChatUnreadCountData
                          .numberOfUnReadMessage,
                  "NumberOfUnReadNotification": unreadNotificationCount ==
                          taskNotificationCountAndChatUnreadCountData
                              .numberOfUnReadNotification
                      ? 0
                      : taskNotificationCountAndChatUnreadCountData
                          .numberOfUnReadNotification
                });
              } catch (e) {}
              switch (tabIndex) {
                case 3:
                  StateProvider().notify(
                      ObserverState.STATE_BADGE_MESSAGE_COUNT, tabIndex);
                  break;
                case 4:
                  StateProvider().notify(
                      ObserverState.STATE_BADGE_NOTIFICATION_COUNT, tabIndex);
                  break;
                default:
              }

              StateProvider().notify(ObserverState.STATE_BOTNAV, null);
            }
          });
    } catch (e) {}
  }

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        botNavController.isShowHelpDialogExtraHand.value = false;
        _selectTab(data ?? 0);
      } else if (state == ObserverState.STATE_LOGOUT) {
        await CookieMgr().delCookiee();
        await DBMgr.shared.delTable("User");
        await PrefMgr.shared.setPrefStr("accessToken", null);
        await PrefMgr.shared.setPrefStr("refreshToken", null);
        Get.offAll(() => LoginLandingScreen());
      } else if (state == ObserverState.STATE_OPEN_HELP_DIALOG) {
        if (!isDialogHelpOpenned) {
          isDialogHelpOpenned = true;
          botNavController.isShowHelpDialogExtraHand.value = false;
          _selectTab(DashboardPage.TAB_POSTTASK);
          Get.dialog(HelpTutDialog()).then((value) {
            setState(() {
              isDialogHelpOpenned = false;
              _selectTab(DashboardPage.TAB_FINDWORKS);
            });
          });
        }
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.pending_review_rating_userid &&
          apiState.cls == this.runtimeType) {
        if (mounted && model != null) {
          final mod = (model as PendingReviewRatingByUserIdAPIModel);
          if (mod.success) {
            if (mod.responseData.userRatings != null) {
              if (mod.responseData.userRatings.length > 0) {
                await APIViewModel().req<TaskBiddingsAPIModel>(
                    context: context,
                    url: APIMyTasksCfg.GET_TASKBIDDING_URL.replaceAll(
                        "#taskBiddingId#",
                        mod.responseData.userRatings[0].taskBiddingId
                            .toString()),
                    reqType: ReqType.Get,
                    isLoading: false,
                    callback: (model) {
                      try {
                        if (mounted && model != null) {
                          if (model.responseData.taskBiddings.length > 0) {
                            WidgetsBinding.instance.addPostFrameCallback((_) {
                              Get.to(() => ReviewPage(
                                  taskBiddingModel:
                                      model.responseData.taskBiddings[0]));
                            });
                          }
                        }
                      } catch (e) {}
                    });
              }
            }
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      botNavController.dispose();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (userData.userModel.isFirstLogin) {
        await APIHelper().wsUserDevice(
            context: context,
            eventType: DeviceEventTypesCfg.USERDEVICE_EVENTTYPE_INSTALL,
            callback: (model) {});
        Future.delayed(Duration.zero, () {
          StateProvider().notify(ObserverState.STATE_OPEN_HELP_DIALOG, null);
        });
      }
    } catch (e) {}
    try {
      await APIHelper().wsFCMDeviceInfo(context, (model) {});
    } catch (e) {}
    try {
      APIViewModel().req<PendingReviewRatingByUserIdAPIModel>(
        context: context,
        apiState: APIState(
            APIType.pending_review_rating_userid, this.runtimeType, null),
        url: APIPostTaskCfg.PENDING_REVIEWRATING_BYUSERID_GET_URL
            .replaceAll("#userId#", userData.userModel.id.toString()),
        isLoading: false,
        reqType: ReqType.Get,
      );
    } catch (e) {}
    try {
      await wsBadgeCounterAPI(0);
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      /*onWillPop: () {
        if (Platform.isAndroid) {
          if (Navigator.of(context).canPop()) {
            return Future.value(true);
          } else {
            _androidAppRetain.invokeMethod("sendToBackground");
            return Future.value(false);
          }
        } else {
          return Future.value(true);
        }
      },*/
      onWillPop: () {
        confirmDialog(
            context: context,
            title: "Logout",
            msg: "Are you sure, do you want to log out from the app?",
            callbackYes: () {
              StateProvider().notify(ObserverState.STATE_LOGOUT, null);
            });
        return;
      },
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          body: IndexedStack(
            index: botNavController.index.value,
            children: tabs.map((e) => e.page).toList(),
          ),
          // Bottom navigation
          bottomNavigationBar: BottomNavigation(
            context: context,
            onSelectTab: _selectTab,
            botNavController: botNavController,
            tabs: tabs,
            isHelpTut: isDialogHelpOpenned,
          ),
          //),
        ),
      ),
    );
  }
}
