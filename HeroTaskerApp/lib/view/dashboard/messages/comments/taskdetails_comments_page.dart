import 'dart:io';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/pubnub/PubnubMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:flutter/material.dart';
import 'taskdetails_comments_base.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class TaskDetailsCommentsPage extends StatefulWidget {
  int timelineId;
  final int channelId;
  TaskDetailsCommentsPage(
      {Key key, @required this.timelineId, @required this.channelId})
      : super(key: key);
  @override
  State createState() => _TaskDetailsCommentsState();
}

class _TaskDetailsCommentsState
    extends BaseTaskDetailsCommentsStatefull<TaskDetailsCommentsPage>
    with APIStateListener {
  final TextEditingController textController = TextEditingController();
  final ScrollController scrollController = new ScrollController();

  List<TimelinePostModel> listTimelinePostModel;
  String comments = '';

  PubnubMgr pubnubMgr;
  bool isScrolling = true;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_timeline_by_app &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listTimelinePostModel = [];
            if (widget.timelineId == null) {
              listTimelinePostModel = (model as GetTimeLineByAppAPIModel)
                  .responseData
                  .timelinePosts;
            } else {
              final listTimelinePostsModel2 =
                  (model as GetTimeLineByAppAPIModel)
                      .responseData
                      .timelinePosts;
              for (var item in listTimelinePostsModel2) {
                if (item.id == widget.timelineId) {
                  listTimelinePostModel.add(item);
                  break;
                }
              }
            }
            setState(() {
              Future.delayed(const Duration(seconds: 1), () {
                //scrollDown(scrollController,
                //getH(context) * AppConfig.chatScrollHeight);
                scrollController
                    .jumpTo(scrollController.position.maxScrollExtent);
              });
            });
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostComments(
                (model as MediaUploadFilesAPIModel).responseData.images[0].url);
          }
        }
      } else if (apiState.type == APIType.post_comment &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetTimelineByAPI();
            try {
              final tModal = (model as CommentAPIModel).comment;
              final user = new UserModel(
                  id: userData.userModel.id, name: userData.userModel.name);
              final chatModel = new ChatModel(commentModel: tModal, user: user);
              pubnubMgr.postMessage(
                  chatModel: chatModel, receiverId: tModal.user.id);
            } catch (e) {}
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.COMMENTS_EMAI_NOTI_GET_URL.replaceAll(
                  "#commentId#",
                  (model as CommentAPIModel).comment.id.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              StateProvider().notify(
                  ObserverState.STATE_RELOAD_TASKDETAILS_GET_TIMELINE, null);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  wsPostComments(String url) async {
    try {
      await APIViewModel().req<CommentAPIModel>(
        context: context,
        apiState: APIState(APIType.post_comment, this.runtimeType, null),
        url: APITimelineCfg.COMMENTS_POST_URL,
        param: {
          "AdditionalData": url,
          "CanDelete": true,
          "CommentText": comments,
          "EntityId": widget.timelineId,
          "EntityName": "TimelinePost",
          "UserId": userData.userModel.id,
        },
        isLoading: false,
        reqType: ReqType.Post,
      );
      comments = '';
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    textController.dispose();
    scrollController.dispose();
    listTimelinePostModel = null;
    comments = null;
    pubnubMgr.destroy();
    super.dispose();
  }

  Future<void> wsGetTimelineByAPI() async {
    try {
      await APIViewModel().req<GetTimeLineByAppAPIModel>(
        context: context,
        apiState: APIState(APIType.get_timeline_by_app, this.runtimeType, null),
        url: APITimelineCfg.GET_TIMELINE_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": 8,
          "CustomerId": 0,
          "IsPrivate": false,
          "Page": 1,
          "TaskId": taskCtrl.getTaskModel().id,
          "timeLineId": 0,
        },
      );
    } catch (e) {}
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    if (taskCtrl.getTaskModel().userId == userData.userModel.id) {
      isPoster = false;
    }

    if (widget.timelineId == null) {
      widget.timelineId = listTimelinePostModel.length - 1;
    }

    //var channelId = widget.modelUser.user.id; //taskCtrl.getTaskModel().id;
    pubnubMgr = PubnubMgr();
    await pubnubMgr.initPubNub(
        channelId: widget.channelId,
        callback: (envelope) async {
          myLog(envelope.payload);
          isScrolling = false;
          await wsGetTimelineByAPI();
        });

    wsGetTimelineByAPI();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Chat'),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    var ownerName = "";
    try {
      ownerName = listTimelinePostModel[0].ownerName;
    } catch (e) {}

    return Container(
        //width: double.infinity,
        height: double.maxFinite,
        //color: Colors.amber,
        child: NotificationListener(
          onNotification: (scrollNotification) {
            if (scrollNotification is ScrollStartNotification) {
              isScrolling = true;
            } else if (scrollNotification is ScrollEndNotification) {
              isScrolling = false;
            }
            return true;
          },
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.brandColor,
                  onRefresh: wsGetTimelineByAPI,
                  child: ListView(
                    controller: scrollController,
                    //shrinkWrap: true,
                    reverse: false,
                    //primary: true,
                    children: [
                      drawTimelineHeading(),
                      drawLine(),
                      Container(
                        child: drawMessageList(
                            listTimelinePostsModel: listTimelinePostModel),
                      ),
                      //Spacer(),
                    ],
                  ),
                ),
              ),
              Positioned(
                child: new Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: drawMessageBox(
                      context: context,
                      hintText: 'Reply to ' + ownerName,
                      textController: textController,
                      callbackCam: (File file, String txt) async {
                        textController.clear();
                        comments = txt.trim();
                        comments = Common.isNotAllowdPhoneNumber(comments);
                        comments = Common.isNotAllowdEmailAddress(comments);
                        if (file != null) {
                          await APIViewModel().upload(
                            context: context,
                            apiState: APIState(APIType.media_upload_file,
                                this.runtimeType, null),
                            file: file,
                          );
                        } else {
                          FocusScope.of(context).requestFocus(FocusNode());
                          if (comments.trim().length > 0) {
                            String formattedDate = DateFormat('dd-MMM-yyyy')
                                .format(DateTime.now());
                            final TimelinePostModel timeLinePostModel =
                                TimelinePostModel();
                            timeLinePostModel.id = listTimelinePostModel[0].id;
                            timeLinePostModel.ownerId =
                                listTimelinePostModel[0].ownerId;
                            timeLinePostModel.ownerName =
                                listTimelinePostModel[0].ownerName;
                            timeLinePostModel.ownerImageUrl =
                                listTimelinePostModel[0].ownerImageUrl;
                            timeLinePostModel.additionalAttributeValue = '';
                            timeLinePostModel.message = comments.trim();
                            timeLinePostModel.dateCreated = formattedDate;
                            listTimelinePostModel.add(timeLinePostModel);
                            scrollDown(scrollController,
                                getH(context) * AppConfig.chatScrollHeight);
                            setState(() {});
                            Future.delayed(Duration(seconds: 1), () async {
                              wsPostComments("");
                            });
                          }
                        }
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
