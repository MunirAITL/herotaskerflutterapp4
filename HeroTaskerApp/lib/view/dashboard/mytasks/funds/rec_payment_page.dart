import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIPayCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/funds/PaymentConfirmationAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/mytasks/funds/review_page.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'fund_payment_base.dart';

class RecPaymentPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const RecPaymentPage({Key key, this.taskBidding}) : super(key: key);
  @override
  State createState() => _RecPaymentPageState();
}

class _RecPaymentPageState extends BasePaymentStatefull<RecPaymentPage>
    with APIStateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.put_payment_confirmation &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            Get.off(
              () => ReviewPage(
                taskBiddingModel: widget.taskBidding,
              ),
            );
          }
        }
      }
    } catch (e) {}
  }

  wsReqPayment() async {
    try {
      widget.taskBidding.paymentStatus = TaskStatusCfg()
          .getSatusCode(TaskStatusCfg.STATUS_TASK_BIDDING_RECEIVED_PAYMENT);
      await APIViewModel().req<PaymentConfirmationAPIModel>(
        context: context,
        apiState:
            APIState(APIType.put_payment_confirmation, this.runtimeType, null),
        url: APIPayCfg.PAYMENTCONFIRMATION_PUT_URL,
        isLoading: true,
        reqType: ReqType.Put,
        param: {
          "CoverLetter": widget.taskBidding.coverLetter,
          "DeliveryDate": taskCtrl.getTaskModel().deliveryDate,
          "DeliveryTime": taskCtrl.getTaskModel().deliveryTime,
          "Description": taskCtrl.getTaskModel().description,
          "DiscountAmount": widget.taskBidding.discountAmount,
          "FixedbiddigAmount": widget.taskBidding.fixedbiddigAmount,
          "HourlyRate": taskCtrl.getTaskModel().hourlyRate,
          "NetTotalAmount": widget.taskBidding.netTotalAmount.round(),
          "PaymentStatus": widget.taskBidding.paymentStatus,
          "ReferenceId": widget.taskBidding.referenceId,
          "ReferenceType": widget.taskBidding.referenceType,
          "ServiceFeeAmount": 0.0,
          "ShohokariAmount": 0.0,
          "Status": taskCtrl.getStatusCode(),
          "Id": widget.taskBidding.id,
          "TaskId": taskCtrl.getTaskModel().id,
          "TotalHour": widget.taskBidding.totalHour,
          "TotalHourPerWeek": widget.taskBidding.totalHourPerWeek,
          "UserId": userData.userModel.id,
          "UserPromotionId": widget.taskBidding.userPromotionId,
        },
      );
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: "Receive payment"),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Received",
            callback: () async {
              wsReqPayment();
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                decoration: MyTheme.picEmboseCircleDeco,
                alignment: Alignment.center,
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                      MyNetworkImage.checkUrl(
                          widget.taskBidding.ownerImageUrl)),
                ),
              ),
              SizedBox(height: 10),
              Center(
                child: Txt(
                    txt: widget.taskBidding.ownerName,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ),
              SizedBox(height: 40),
              drawLine(),
              SizedBox(height: 10),
              drawPriceBox(),
            ],
          ),
        ),
      ),
    );
  }

  drawPriceBox() {
    int price = widget.taskBidding.netTotalAmount.round();
    if (price < 1) {
      price = widget.taskBidding.fixedbiddigAmount.round();
    }
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Txt(
              txt: "Task price",
              txtColor: MyTheme.gray5Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          Text(
            getCurSign() + price.toString(),
            style: TextStyle(
              color: MyTheme.gray5Color,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
