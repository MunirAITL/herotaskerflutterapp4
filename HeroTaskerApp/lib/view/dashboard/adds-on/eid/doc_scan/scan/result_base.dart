import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/model/ads-on/eid/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum eScanDocType {
  DL,
  Passport,
}

abstract class ResultBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout(eScanDocType docType, PostDocVerifyAPIModel model2) {
    try {
      final model = model2.responseData.response[0];

      var ppExpiry = "";
      try {
        ppExpiry = DateFun.getDateWhatsAppFormat(model.passportExpiryDate);
      } catch (e) {}

      return Container(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20),
                drawPicBox(model2),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Txt(
                          txt: model.name,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize + 1,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 20),
                      Txt(
                          txt: "Identification data",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      drawLabelBox("Birthday:", model.dateofBirth ?? ''),
                      (docType == eScanDocType.Passport)
                          ? (model.passportNumber.length > 0)
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 10),
                                    drawLabelBox("Passport number:",
                                        model.passportNumber ?? ''),
                                    SizedBox(height: 10),
                                    drawLabelBox("Passport expiry:", ppExpiry),
                                  ],
                                )
                              : SizedBox()
                          : SizedBox(),
                      SizedBox(height: 10),
                      drawLabelBox(
                          "Personal ID number:", model.id.toString() ?? ''),
                      SizedBox(height: 10),
                      drawLabelBox("Document type:", model.documentType ?? ''),
                      SizedBox(height: 10),
                      drawLabelBox("Identification type:",
                          "Identification without Liveness"),
                      SizedBox(height: 10),
                      drawLabelBox(
                          "Identification status:",
                          ((model.isIdentity)
                              ? 'automatically confirmed'
                              : 'waiting agent review')),
                      SizedBox(height: 20),
                      Txt(
                          txt: "Result of automatic identification",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      drawLabelBox("Automatic Identification Setting:", 'on'),
                      SizedBox(height: 10),
                      drawLabelBox(
                          "Identification result:",
                          ((model.isIdentity)
                              ? 'Identification is automatically verified'
                              : 'Identification is not automatically verified')),
                      SizedBox(height: 20),
                      Txt(
                          txt: "Analysis result",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 10),
                      Container(
                        color: Colors.grey.withOpacity(.2),
                        width: getW(context),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: (model.isIdentity)
                                        ? ("1- The probability that in image and in document are the same person as in the document " +
                                            model.confidence
                                                .toStringAsFixed(2) +
                                            "%")
                                        : ("1- The probability that in image and in document are not the same person as in the document " +
                                            model.confidence
                                                .toStringAsFixed(2) +
                                            "%"),
                                    txtColor: Colors.red.shade300,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                                SizedBox(height: 10),
                                Txt(
                                    txt: "2- Guessed age: " +
                                        model.guessedAge.toString() +
                                        ", Real age: " +
                                        model.realAge.toString(),
                                    txtColor: Colors.green.shade300,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ]),
                        ),
                      ),
                    ],
                  ),
                ),
                Center(
                  child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20, right: 20, bottom: 20),
                      child: MMBtn(
                          txt: "Ok",
                          width: getWP(context, 30),
                          height: getHP(context, 6),
                          radius: 0,
                          callback: () {
                            Get.offAll(
                              () => DashboardPage(),
                            ).then((value) {
                              //callback(route);
                            });
                          })),
                )
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      return SizedBox();
    }
  }

  drawPicBox(PostDocVerifyAPIModel model2) {
    final model = model2.responseData.response[0];
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: getWP(context, 43),
            height: getHP(context, 15),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(color: MyTheme.redColor, width: 1),
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl(model.imageUrl1)),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(width: 5),
          Container(
            width: getWP(context, 43),
            height: getHP(context, 15),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(color: MyTheme.redColor, width: 1),
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl(model.imageUrl2)),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawLabelBox(String label, String val) {
    return Container(
      color: Colors.grey.withOpacity(.2),
      width: getW(context),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: label,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            Txt(
                txt: val,
                txtColor: Colors.black54,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ],
        ),
      ),
    );
  }
}
