import 'package:aitl/data/app_data/UserData.dart';

class BadgeEmailHelper {
  getParam() {
    return {
      "UserId": userData.userModel.id,
      "Status": 101,
      "CreationDate": DateTime.now().toString(),
      "Title": userData.userModel.email,
      "Type": "Email",
      "Description": "",
      "ReferenceId": "",
      "ReferenceType": "",
      "RefrenceNumber": "",
      "RefrenceUrl": "",
      "Refrence": "",
      "AccountNumber": "",
      "AccountName": "",
      "SwiftCode": "",
      "IsVerified": false,
      "VerificationCode": "",
      "Id": 0
    };
  }
}
