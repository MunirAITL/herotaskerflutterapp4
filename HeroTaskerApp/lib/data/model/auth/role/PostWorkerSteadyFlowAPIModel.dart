import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/auth/role/SteadyFlowData.dart';

class PostWorkerSteadyFlowAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostWorkerSteadyFlowAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostWorkerSteadyFlowAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  SteadyFlowData steadyFlowData;
  ResponseData({this.steadyFlowData});

  ResponseData.fromJson(Map<String, dynamic> json) {
    steadyFlowData = json['SteadyFlowData'] != null
        ? new SteadyFlowData.fromJson(json['SteadyFlowData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.steadyFlowData != null) {
      data['SteadyFlowData'] = this.steadyFlowData.toJson();
    }
    return data;
  }
}
