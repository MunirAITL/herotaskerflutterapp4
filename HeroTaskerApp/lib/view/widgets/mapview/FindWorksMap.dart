import 'dart:async';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/FindWorksConfig.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APIYoutubeCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/MapFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/findworks/FiltersModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/findworks/filters/filters_page.dart';
import 'package:aitl/view/dashboard/findworks/find_works_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/rx/FiltersController.dart';
import 'package:custom_info_window/custom_info_window.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/model.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../main.dart';

class FindWorksMap extends StatefulWidget {
  List<TaskModel> listTaskModel;
  FindWorksMap({Key key, @required this.listTaskModel}) : super(key: key);
  @override
  State createState() => _FindWorksMapState();
}

class _FindWorksMapState extends BaseFindWorksStatefull<FindWorksMap>
    with APIStateListener, Mixin {
  CustomInfoWindowController _customInfoWindowController =
      CustomInfoWindowController();
  List<Marker> listMarker = [];
  Coordinates myLoc;
  String preferedLocation;
  bool isLoading = true;

  int tabNo = 2;
  int distance = FindWorksConfig.distance;
  int fromPrice = FindWorksConfig.minPrice;
  int toPrice = FindWorksConfig.maxPrice;
  double lat = 0.0;
  double lng = 0.0;
  String location = '';
  bool isHideAssignTask = false;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.taskinfo_search &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          widget.listTaskModel = model.responseData.locations;
          await drawAllTaskPins();
          setState(() {});
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      int inPersonOrOnline = 0; //  default=all
      if (tabNo == 2)
        inPersonOrOnline = 0; //  all
      else if (tabNo == 1)
        inPersonOrOnline = 2; //  remotely
      else if (tabNo == 0) inPersonOrOnline = 1; //  in person

      await APIViewModel().req<TaskInfoSearchAPIModel>(
        context: context,
        apiState: APIState(APIType.taskinfo_search, this.runtimeType, null),
        url: APIPostTaskCfg.TASKINFOBYSEARCH_POST_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": 50,
          "Distance": distance ?? FindWorksConfig.distance,
          "FromPrice": fromPrice ?? FindWorksConfig.minPrice,
          "InPersonOrOnline": 1, //inPersonOrOnline,
          "IsHideAssignTask": isHideAssignTask,
          "Latitude": lat ?? 0.0,
          "Location": location ?? '',
          "Longitude": lng ?? 0.0,
          "Page": 0,
          "SearchText": '',
          "Status": TaskStatusCfg.TASK_STATUS_ALL,
          "ToPrice": toPrice ?? FindWorksConfig.maxPrice,
          "UserId": 0,
        },
      );
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  @override
  Future<void> refreshData() async {
    try {
      final filtersModel = await FiltersSharedPref().get();
      tabNo = filtersModel.tabNo;
      if (tabNo != 1) {
        //  NOT remotely
        distance = FiltersController().getDistance(filtersModel.distance);
        lat = filtersModel.lat;
        lng = filtersModel.lng;
        location = filtersModel.location;
      } else {
        distance = FindWorksConfig.distance;
        location = '';
        lat = 0.0;
        lng = 0.0;
      }
      fromPrice = FiltersController().getMinPrice(filtersModel.minPrice);
      toPrice = FiltersController().getMaxPrice(filtersModel.maxPrice);
      isHideAssignTask = filtersModel.isAvailableTasksOnly;
    } catch (e) {}

    widget.listTaskModel.clear();

    onLazyLoadAPI();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      _customInfoWindowController.dispose();
      _customInfoWindowController = null;
      listMarker = null;
      myLoc = null;
    } catch (e) {}

    super.dispose();
  }

  appInit() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
    ].request();

    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      preferedLocation = userData.userModel.address ?? '';
      if (preferedLocation != '')
        myLoc = await MapFun().getCordByAddr(userData.userModel.address);
      if (myLoc == null) {
        myLoc = Coordinates(FindWorksConfig.lat, FindWorksConfig.lng);
        preferedLocation = await MapFun().getAddrByCord(
            myLoc.latitude.toString() + ',' + myLoc.longitude.toString());
      }

      refreshData();
    } catch (e) {
      myLog(e.toString());
    }
  }

  drawAllTaskPins() async {
    listMarker.clear();
    for (var taskModel in widget.listTaskModel) {
      try {
        //if (taskModel.latitude > 0 && taskModel.longitude >= 0) {
        listMarker.add(
          Marker(
            markerId: MarkerId(taskModel.id.toString()),
            position: LatLng(taskModel.latitude, taskModel.longitude),
            //infoWindow: InfoWindow(title: taskModel.title),
            onTap: () {
              drawMarkerInfoWindow(
                  cord: LatLng(taskModel.latitude, taskModel.longitude),
                  taskModel: taskModel);
            },
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          ),
        );
        //}
      } catch (e) {
        myLog(e.toString());
      }
    }
  }

  drawMarkerInfoWindow({cord, TaskModel taskModel}) {
    _customInfoWindowController.addInfoWindow(
      ListView(
          shrinkWrap: true,
          children: [drawItem(taskModel: taskModel, isFromMap: true)]),
      cord,
    );
  }

  @override
  Widget build(BuildContext context) {
    final xtraH = isIPAD || isTablet ? 20 : 0;
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(getHP(context,
              isIPAD || isTablet ? 20 : 15)), // here the desired height
          child: AppBar(
            backgroundColor: MyTheme.appbarColor,
            iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
            elevation: MyTheme.appbarElevation,
            automaticallyImplyLeading: false,
            title: UIHelper().drawAppbarTitle(title: 'Find Work'),
            centerTitle: false,
            actions: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.menu,
                  ),
                  onPressed: () {
                    Get.back(result: true);
                  }),
              SizedBox(width: 10),
              IconButton(
                  icon: Icon(Icons.filter_alt_outlined),
                  onPressed: () {
                    Get.to(() => FilterPage()).then((value) async {
                      if (value != null) {
                        refreshData();
                      }
                    });
                  }),
              IconButton(
                  icon: Icon(Icons.help),
                  onPressed: () {
                    openUrl(context, APIYoutubeCfg.HELP_YOUTUBE_URL);
                  }),
            ],
            bottom: PreferredSize(
              preferredSize:
                  Size.fromHeight(AppConfig.findworks_map_height + xtraH),
              child: Container(
                color: MyTheme.grayColor,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 15, bottom: 15),
                  child: Center(
                    child: Txt(
                        txt: "To earn money select suitable job from below",
                        txtColor: Colors.white70,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ),
              ),
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return (myLoc != null)
        ? Container(
            width: getW(context),
            height: getH(context),
            child: Stack(
              children: [
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: GoogleMap(
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                    zoomControlsEnabled: false,
                    zoomGesturesEnabled: true,
                    scrollGesturesEnabled: true,
                    compassEnabled: true,
                    rotateGesturesEnabled: true,
                    mapToolbarEnabled: true,
                    tiltGesturesEnabled: true,
                    mapType: MapType.normal,
                    markers: Set<Marker>.of(listMarker),
                    initialCameraPosition: CameraPosition(
                        target: LatLng(myLoc.latitude, myLoc.longitude),
                        zoom: 10),
                    gestureRecognizers: Set()
                      ..add(Factory<PanGestureRecognizer>(
                          () => PanGestureRecognizer())),
                    onMapCreated: _onMapCreated,
                    onTap: (position) {
                      _customInfoWindowController.hideInfoWindow();
                    },
                    onCameraMove: (position) {
                      _customInfoWindowController.onCameraMove();
                    },
                  ),
                ),
                CustomInfoWindow(
                  controller: _customInfoWindowController,
                  height: getHP(context, 15),
                  width: getWP(context, 70),
                  offset: 50,
                ),
                if (isLoading) drawLoading(),
              ],
            ),
          )
        : drawLoading();
  }

  drawLoading() {
    return Container(
        color: Colors.white,
        width: getW(context),
        height: getH(context),
        child: Center(
          child: CircularProgressIndicator(
            backgroundColor: MyTheme.brandColor,
          ),
        ));
  }

  void _onMapCreated(GoogleMapController controller) {
    _customInfoWindowController.googleMapController = controller;
    Timer(Duration(seconds: 1), () {
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    });
  }
}
