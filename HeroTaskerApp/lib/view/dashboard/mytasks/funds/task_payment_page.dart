import 'dart:developer';

import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/funds/PaymentConfirmationAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/funds/TaskPaymentByTaskAndBiddingIDAPIModel.dart';
import 'package:aitl/data/model/dashboard/stripe/CreateorUpdatePaymentAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/review_page.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class TaskPaymentPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const TaskPaymentPage({Key key, @required this.taskBidding})
      : super(key: key);
  @override
  State createState() => _TaskPaymentPageState();
}

class _TaskPaymentPageState extends State<TaskPaymentPage> with Mixin {
  final taskCtrl = Get.put(TaskCtrl());

  TaskPaymentInfo taskPaymentInfo;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    taskPaymentInfo = null;
    super.dispose();
  }

  appInit() async {
    /*await APIViewModel().req<CreateorUpdatePaymentAPIModel>(
        context: context,
        url: APIPaymentCfg.GetSecretKeyWithDetails_URL.replaceAll(
            "#id#", widget.taskBidding.id.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              taskPaymentInfo = model.responseData.taskPaymentInfo;
              if (taskPaymentInfo.id == 0) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  onPaymentFailed(
                      "Failed!\nPlease try again later or contact us");
                });
              } else {
                
                await APIViewModel().req<PaymentConfirmationAPIModel>(
                    context: context,
                    url: APIPaymentCfg.TASKPAYMENT_PUT,
                    isLoading: true,
                    reqType: ReqType.Put,
                    param: {
                      "TaskPaymentId": taskPaymentInfo.id,
                      "CreationDate": taskPaymentInfo.creationDate,
                      "UpdatedDate": taskPaymentInfo.updatedDate,
                      "VersionNumber": taskPaymentInfo.versionNumber,
                      "UserId": taskPaymentInfo.userId,
                      "TaskId": taskPaymentInfo.taskId,
                      "PaymentType": taskPaymentInfo.paymentType,
                      "PaymentMethod": taskPaymentInfo.paymentMethod,
                      "EmployeeID": taskPaymentInfo.employeeID,
                      "PayableAmount": taskPaymentInfo.payableAmount,
                      "DueAmount": taskPaymentInfo.dueAmount,
                      "PaymentAmount": taskPaymentInfo.payableAmount,
                      "TaskBiddingId": taskPaymentInfo.taskBiddingId,
                      "Status": 101,
                      "Description": taskPaymentInfo.description,
                    },
                    callback: (model) {
                      if (mounted && model != null) {
                        if (model.success) {
                          Get.off(() => FundPaymentPage(
                                taskBidding: widget.taskBidding,
                                taskPaymentInfo: taskPaymentInfo,
                                isReleasePayment: true,
                              ));
                        }
                      }
                    });
              }
            }
          }
        });*/
  }

  onPaymentFailed(String msg) {
    confirmDialog(
        context: context,
        yesBtnTxt: "Contact",
        noBtnTxt: "Cancel",
        title: msg,
        callbackYes: () {
          Get.to(() => ResolutionScreen());
        });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Release payment'),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Submit",
            callback: () async {
              final url =
                  APIPaymentCfg.GET_TASKPAYMENTBYTASKIDANDTASKBIDDINGID_URL +
                      ("?TaskId=" +
                          widget.taskBidding.taskId.toString() +
                          "&TaskBiddingId=" +
                          widget.taskBidding.id.toString());
              print(url);
              await APIViewModel().req<TaskPaymentByTaskAndBiddingIDAPIModel>(
                  context: context,
                  url: url,
                  reqType: ReqType.Get,
                  callback: (model) async {
                    if (model != null && mounted) {
                      if (model.success) {
                        model.responseData.taskPayment.status = 101;
                        final jsn = model.toJson();
                        log(jsn.toString());
                        final mod =
                            TaskPaymentByTaskAndBiddingIDAPIModel.fromJson(jsn);
                        log(mod.responseData.taskPayment.toJson().toString());
                        await APIViewModel().req<PaymentConfirmationAPIModel>(
                          context: context,
                          url: APIPaymentCfg.TASKPAYMENT_PUT,
                          isLoading: true,
                          reqType: ReqType.Put,
                          param: mod.responseData.taskPayment.toJson(),
                          callback: (model2) {
                            if (mounted && model2 != null) {
                              if (model2.success) {
                                Get.off(() => ReviewPage(
                                    taskBiddingModel: widget.taskBidding));
                              }
                            }
                          },
                        );
                      }
                    }
                  });
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          (widget.taskBidding == null) ? drawNF() : acceptOffer(),
        ],
      ),
    );
  }

  acceptOffer() {
    final taskerName = widget.taskBidding.ownerName ?? '';
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: MyTheme.picEmboseCircleDeco,
              child: CircleAvatar(
                radius: 30,
                backgroundColor: Colors.transparent,
                backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl(widget.taskBidding.ownerImageUrl)),
              ),
            ),
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Txt(
                  txt: "Are you sure " +
                      taskerName +
                      " completed your task. Do you want to release payment?",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            SizedBox(height: 50),
            drawPriceBox(),
          ],
        ),
      ),
    );
  }

  drawPriceBox() {
    int price = widget.taskBidding.netTotalAmount.round();
    if (price < 1) {
      price = widget.taskBidding.fixedbiddigAmount.round();
    }
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 40),
          child: Container(
            width: getWP(context, 70),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: MyTheme.gray1Color,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Padding(
              padding: const EdgeInsets.only(top: 50, bottom: 40),
              child: Text(
                getCurSign() + price.toString(),
                style: TextStyle(
                  color: MyTheme.gray5Color,
                  fontSize: 50,
                ),
              ),
            ),
          ),
        ),
        ClipRRect(
            borderRadius: new BorderRadius.circular(50.0),
            child: SvgPicture.asset(
              'assets/images/svg/ic_lock.svg',
            )),
      ],
    );
  }

  drawNF() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  "assets/images/icons/ic_waiting_offers.png",
                  fit: BoxFit.cover,
                  width: getW(context) / 3.5,
                  height: getW(context) / 5,
                ),
              ),
              SizedBox(height: 10),
              Txt(
                  txt: "You have not yet received any Offer for this Task",
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
