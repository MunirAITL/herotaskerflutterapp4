import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';

class AppConfig {
  //static const

  static const STORAGE_PATH = "/storage/emulated/0/Download";

  static const int page_limit_dashboard = 3;
  static const int page_limit = 50;
  static const int totalUploadLimit = 5;

  static const double picSize = 400;
  static const double chatScrollHeight = .2;
  static const int textExpandableSize = 70;
  static const AlertDismisSec = 3;

  //  appbar height per page fixed pixel
  static const double post_add_height = 22;
  static const double mytasks_height = 60;
  static const double findworks_height = 70;
  static const double findworks_map_height = 50;
  static const double private_msg_height = 75;
  static const double myprofile_icon_height = 10;

  Future<bool> isIpad() async {
    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo info = await deviceInfo.iosInfo;
      if (info.name.toLowerCase().contains("ipad")) {
        return true;
      }
    }
    return false;
  }

  isTablet(context) {
    // The equivalent of the "smallestWidth" qualifier on Android.
    var shortestSide = MediaQuery.of(context).size.shortestSide;

    // Determine if we should use mobile layout or not, 600 here is
    // a common breakpoint for a typical 7-inch tablet.
    return shortestSide > 600 ? true : false;
  }
}
