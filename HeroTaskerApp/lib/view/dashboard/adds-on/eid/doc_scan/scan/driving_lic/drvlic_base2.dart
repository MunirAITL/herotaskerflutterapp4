import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/camera/cam_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/driving_lic/drvlic_review_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/scan_helper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../../../mixin.dart';

abstract class DrvLic2Base<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();

  drawRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10),
            drawCell(
                0,
                Icon(
                  Icons.drive_eta,
                  color: Colors.green,
                  size: 20,
                ),
                "Yes, English with a transparent window",
                true),
            drawCell(
                1,
                Icon(
                  Icons.drive_eta,
                  color: Colors.green,
                  size: 20,
                ),
                "No, not English and/or no window",
                false),
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () {
          Get.to(() => CamPage(
                isFront: false,
                title: "Scan your driving licence",
              )).then((value) {
            if (value != null) {
              scanDocData.file_drvlic_front = value;
              Get.off(() => DrvLicReviewPage());
            }
          });
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: ScanHelper().drawLine())
                : SizedBox(height: 20)
          ],
        ),
      ),
    );
  }
}
