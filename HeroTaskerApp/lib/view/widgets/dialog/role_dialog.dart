import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/main.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:get/get.dart';
import '../txt/Txt.dart';

openRoleDialog({
  BuildContext context,
  Function(int) callback,
}) {
  var type = 1.obs; //  1= customer, 2= worker

  AwesomeDialog(
    dialogBackgroundColor: Colors.white,
    dismissOnBackKeyPress: false,
    dismissOnTouchOutside: false,
    context: context,
    animType: AnimType.SCALE,
    dialogType: DialogType.NO_HEADER,
    //showCloseIcon: true,
    //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
    //customHeader: Icon(Icons.info, size: 50),
    body: Center(
      child: Stack(
          overflow: Overflow.visible,
          alignment: Alignment.center,
          children: [
            Obx(() => Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Txt(
                        txt: "What is your role",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + 1,
                        isBold: true,
                        txtAlign: TextAlign.center,
                      ),
                      SizedBox(height: 20.0),
                      IntrinsicHeight(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      type.value = 1;
                                    },
                                    child: _drawBox(
                                        context,
                                        "Employer",
                                        "I want to give work",
                                        Color(0xFFf0f7ff),
                                        type.value == 1 ? true : false))),
                            Flexible(
                                child: GestureDetector(
                                    onTap: () {
                                      type.value = 11;
                                    },
                                    child: _drawBox(
                                        context,
                                        "Worker",
                                        "I want to work\nand earn money",
                                        Color(0xFFcce6ff),
                                        type.value == 11 ? true : false))),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
            Positioned(
              top: isIPAD || isTablet ? -70 : -50,
              child: Container(
                  //width: 50.0,
                  //height: 50.0,
                  padding: const EdgeInsets.all(
                      10), //I used some padding without fixed width and height
                  decoration: new BoxDecoration(
                    shape: BoxShape
                        .circle, // You can use like this way or like the below line
                    //borderRadius: new BorderRadius.circular(30.0),
                    color: Colors.white,
                  ),
                  child: Icon(Icons.group,
                      color: MyTheme.redColor,
                      size: isIPAD || isTablet ? 80 : 40)),
            )
          ]),
    ),
    //title: 'This is Ignored',
    //desc: 'This is also Ignored',
    buttonsTextStyle: TextStyle(fontSize: MyTheme.fontSize),
    btnOkText: "Submit",
    btnOkColor: MyTheme.redColor,
    btnOkOnPress: () {
      callback(type.value);
    },
  )..show();
}

_drawBox(context, title, txt, bgColor, isChecked) {
  return Container(
    decoration: new BoxDecoration(
        color: bgColor,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(8.0))),
    child: Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: true),
          SizedBox(height: 10),
          Txt(
              txt: txt,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.center,
              isBold: false),
          SizedBox(height: 10),
          Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 1,
                  color: Colors.grey,
                  style: BorderStyle.solid,
                ),
              ),
              child: isChecked
                  ? Padding(
                      padding: const EdgeInsets.all(3),
                      child: UIHelper().drawCircle(
                          context: context, size: 3, color: Colors.red))
                  : SizedBox(
                      width: 20,
                      height: 20,
                    ))
        ],
      ),
    ),
  );
}
