import 'Server.dart';

class APIAuthCfg {
  //  ***************************************************************************** LOGIN REGISTER START

  //  Login
  static const String LOGIN_URL = Server.BASE_URL + "/api/authentication/login";

  //  href login
  static const LOGINACCEXISTSBYEMAIL_POST_URL = Server.BASE_URL +
      "/api/authentication/post/loginaccountexistscheckbyEmail";
  static const POSTEMAILOTP_POST_URL =
      Server.BASE_URL + "/api/userotp/postemailotp";
  static const SENDUSEREMAILOTP_GET_URL =
      Server.BASE_URL + "/api/userotp/senduseremailotp?otpId=#otpId#";
  static const LOGINEMAILOTPBYMOBAPP_POST_URL =
      Server.BASE_URL + "/api/authentication/loginemailotpbymobileapp";

  //  Login by Mobile OTP
  static const String LOGIN_MOBILE_OTP_POST_URL =
      Server.BASE_URL + "/api/userotp/post";
  static const String SEND_OTP_NOTI_URL =
      Server.BASE_URL + "/api/userotp/sendotpnotification?otpId=#otpId#";
  static const String LOGIN_MOBILE_OTP_PUT_URL =
      Server.BASE_URL + "/api/userotp/put";
  static const String LOGIN_REG_OTP_FB_URL =
      Server.BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Forgot
  static const String FORGOT_URL =
      Server.BASE_URL + "/api/authentication/forgotpassword";

  //  ***************************************************************************** LOGIN REGISTER END...

  // Change password
  static const String CHANGE_PWD_URL =
      Server.BASE_URL + "/api/users/put/change-password";

  //  Register
  static const String REG_URL =
      Server.BASE_URL + "/api/authentication/register";

  //  reg
  static const String REG_PROFILE_PUT_URL = Server.BASE_URL + "/api/users/put";

  static const String LIGIN_REG_FB_MOBILE =
      Server.BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  EmailVerify
  static const String EMAILVERIFY_URL =
      Server.BASE_URL + "/api/authentication/verifyemail";

  static const POST_ROLETYPE_URL =
      Server.BASE_URL + "/api/authentication/post/updateusercommunityprofile";

  static const POST_ROLETYPE_WORKER_FLOWDATA_URL =
      Server.BASE_URL + "/api/steadyflowdata/post";
}
