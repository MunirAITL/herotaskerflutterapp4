import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/stripe/CreateorUpdatePaymentAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_page.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class AcceptOfferPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const AcceptOfferPage({Key key, @required this.taskBidding})
      : super(key: key);
  @override
  State createState() => _AcceptOfferPageState();
}

class _AcceptOfferPageState extends State<AcceptOfferPage> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: 'Accept offer'),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Accept",
            callback: () async {
              Get.to(
                () => FundPaymentPage(
                  taskBidding: widget.taskBidding,
                ),
              ).then((value) => Get.back(result: value));
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  onPaymentFailed(String msg) {
    confirmDialog(
        context: context,
        yesBtnTxt: "Contact",
        noBtnTxt: "Cancel",
        title: msg,
        callbackYes: () {
          Get.to(() => ResolutionScreen());
        });
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          (widget.taskBidding == null) ? drawNF() : acceptOffer(),
        ],
      ),
    );
  }

  acceptOffer() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: MyTheme.picEmboseCircleDeco,
              child: CircleAvatar(
                radius: 30,
                backgroundColor: Colors.transparent,
                backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl(widget.taskBidding.ownerImageUrl)),
              ),
            ),
            SizedBox(height: 20),
            Txt(
                txt: widget.taskBidding.coverLetter,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 20),
            Txt(
                txt:
                    "By accepting this offer, you agree to pay the 'Hero' for the agreed job. You may be charged a fee if you discontinue the Task after accepting it.",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 50),
            drawPriceBox(),
          ],
        ),
      ),
    );
  }

  drawPriceBox() {
    int price = widget.taskBidding.netTotalAmount.round();
    if (price < 1) {
      price = widget.taskBidding.fixedbiddigAmount.round();
    }
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 40),
          child: Container(
            width: getWP(context, 70),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: MyTheme.gray1Color,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Padding(
              padding: const EdgeInsets.only(top: 50, bottom: 40),
              child: Text(
                getCurSign() + price.toString(),
                style: TextStyle(
                  color: MyTheme.gray5Color,
                  fontSize: 50,
                ),
              ),
            ),
          ),
        ),
        ClipRRect(
            borderRadius: new BorderRadius.circular(50.0),
            child: SvgPicture.asset(
              'assets/images/svg/ic_lock.svg',
            )),
      ],
    );
  }

  drawNF() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  "assets/images/icons/ic_waiting_offers.png",
                  fit: BoxFit.cover,
                  width: getW(context) / 3.5,
                  height: getW(context) / 5,
                ),
              ),
              SizedBox(height: 10),
              Txt(
                  txt: "You have not yet received any Offer for this Task",
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
