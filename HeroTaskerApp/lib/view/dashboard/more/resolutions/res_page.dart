import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../main.dart';
import 'res_base.dart';

class ResPage extends StatefulWidget {
  final String from;
  final int userId;
  ResPage({@required this.from, this.userId});
  @override
  State createState() => _ResState();
}

class _ResState extends BaseResStatefull<ResPage> with APIStateListener {
  final cmt = TextEditingController();

  DropListModel ddTitle = DropListModel([
    OptionItem(id: 1, title: "Spam"),
    OptionItem(id: 2, title: "Rude or offensive"),
    OptionItem(id: 3, title: "Breach of marketplace rules"),
    OptionItem(id: 4, title: "Other"),
  ]);

  OptionItem optTitle = OptionItem(id: null, title: "Select Report Type");

  String title = "";
  String resolutionType = "";
  int status;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.res && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            cmt.clear();
            showToast(
                context: context, msg: "Submitted successfully", which: 1);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    //taskCtrl.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    cmt.dispose();
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (optTitle.id == null) {
      showToast(context: context, msg: "Choose report type");
      return false;
    } else if (cmt.text.trim().isEmpty) {
      showToast(context: context, msg: "Description cannot be blank");
      return false;
    }
    return true;
  }

  /*AppConstants.RESOULUTION_TYPE_TASK_SPAMREPORT -> 
    presenter.onReportTaskClicked(
      AppConstants.TASK_STATUS_ACTIVE, 
      AppConstants.RESOULUTION_TYPE_TASK_SPAMREPORT, 
      reportType + " : " +  report_task_edit_text_description.text.toString(), 
      TaskManager.task?.Id.toString(), 
      appPreferenceHelper.getCurrentUserId()!!, 
      DateUtil.getCurrentDate(), 
      AppConstants.RESOULUTION_TYPE_TASK)
                
AppConstants.RESOULUTION_TYPE_TASK_OFFERCOMMENT_SPAMREPORT -> 
    presenter.onReportTaskClicked(
      AppConstants.TASK_STATUS_ACTIVE, 
      AppConstants.RESOULUTION_TYPE_TASK_OFFERCOMMENT_SPAMREPORT, 
      reportType + " : " +  report_task_edit_text_description.text.toString(), 
      TaskManager.task?.Id.toString(), 
      appPreferenceHelper.getCurrentUserId()!!, 
      DateUtil.getCurrentDate(), 
      AppConstants.RESOULUTION_TYPE_TASK)
                
AppConstants.RESOULUTION_TYPE_TASK_COMMENT_SPAMREPORT -> 
    presenter.onReportTaskClicked(
      AppConstants.TASK_STATUS_ACTIVE, 
      AppConstants.RESOULUTION_TYPE_TASK_COMMENT_SPAMREPORT, 
      reportType + " : " +  report_task_edit_text_description.text.toString(), 
      TaskManager.task?.Id.toString(), 
      appPreferenceHelper.getCurrentUserId()!!, 
      DateUtil.getCurrentDate(), 
      AppConstants.RESOULUTION_TYPE_TASK)
                
    presenter.onReportTaskClicked(
      AppConstants.TASK_STATUS_ACTIVE, 
      reportType, 
      reportType + " : " +  report_task_edit_text_description.text.toString(), 
      userID, 
      appPreferenceHelper.getCurrentUserId()!!, 
      DateUtil.getCurrentDate(), 
      AppConstants.RESOULUTION_TYPE_USER)
*/

  @override
  Widget build(BuildContext context) {
    switch (widget.from) {
      case ResCfg.RESOULUTION_TYPE_TASK:
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
        title = "Report Task";
        resolutionType = ResCfg.RESOULUTION_TYPE_USER;
        break;
      case ResCfg.RESOULUTION_TYPE_COMMENT:
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
        title = "Report Comment";
        resolutionType = ResCfg.RESOULUTION_TYPE_USER;
        break;
      case ResCfg.RESOULUTION_TYPE_TASK_SPAMREPORT:
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
        title = "Report User";
        resolutionType = ResCfg.RESOULUTION_TYPE_TASK;
        break;
      case ResCfg.RESOULUTION_TYPE_TASK_OFFERCOMMENT_SPAMREPORT:
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
        title = "Report User";
        resolutionType = ResCfg.RESOULUTION_TYPE_TASK;
        break;
      case ResCfg.RESOULUTION_TYPE_TASK_COMMENT_SPAMREPORT:
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
        title = "Report User";
        resolutionType = ResCfg.RESOULUTION_TYPE_TASK;
        break;
      default:
        title = "Report User";
        status = TaskStatusCfg.TASK_STATUS_ACTIVE;
        resolutionType = ResCfg.RESOULUTION_TYPE_USER;
    }

    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: title),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, size: MyTheme.fontSize),
              onPressed: () async {
                Get.back();
              }),
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Send report",
            callback: () async {
              if (validate()) {
                APIViewModel().req<ResAPIModel>(
                  context: context,
                  apiState: APIState(APIType.res, this.runtimeType, null),
                  url: ResCfg.RES_POST_URL,
                  reqType: ReqType.Post,
                  param: {
                    "Description": optTitle.title + ' : ' + cmt.text.trim(),
                    "InitiatorId": widget.userId,
                    "Remarks": taskCtrl.getTaskModel().id.toString(),
                    "ResolutionType": resolutionType,
                    "ServiceDate": DateTime.now().toString(),
                    "Status": status,
                    "Title": title,
                  },
                );
              }
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: DropDownListDialog(
              context: context,
              title: optTitle.title,
              ddTitleList: ddTitle,
              callback: (optionItem) {
                optTitle = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextFormField(
                  controller: cmt,
                  minLines: 10,
                  maxLines: 20,
                  autocorrect: false,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: MyTheme.fontSize,
                  ),
                  decoration: new InputDecoration(
                    counter: Offstage(),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: "Please enter a comment",
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: MyTheme.fontSize,
                    ),
                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
