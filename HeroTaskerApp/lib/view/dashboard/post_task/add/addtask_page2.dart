import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/tooltips/TooltipDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_base.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page3.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/picker/DatePickerView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

class AddTask2Page extends StatefulWidget {
  final UserModel userModel;
  final bool isCopyTask;

  AddTask2Page({
    Key key,
    @required this.userModel,
    this.isCopyTask,
  }) : super(key: key);
  @override
  State createState() => _AddTask2PageState();
}

class _AddTask2PageState extends BaseAddTaskStatefull<AddTask2Page>
    with APIStateListener {
  var dueDate = "";
  int dateType = 0;
  //

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.del_task &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              taskController.setTaskModel(null);
              Get.back(result: 0);
            } catch (e) {}
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  uploadTaskImages(int pageNo) {}

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      taskController.dispose();
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (taskController.getTaskModel().id != null) {
        dueDate = taskController.getTaskModel().deliveryDate;
        final f1 = new DateFormat('dd-MMM-yyyy');
        final f2 = new DateFormat(DateFun.getDateUKFormat());
        final d = f1.parse(dueDate);
        dueDate = f2.format(d);
        myLog(dueDate);

        var d2 = dueDate.split("/");
        DateTime now = DateTime.now();
        var t1 = DateTime(int.parse(d2[2]), int.parse(d2[1]), int.parse(d2[0]));
        var t2 = DateTime(now.year, now.month, now.day);
        final difference = t1.difference(t2).inDays;
        if (difference == 0) {
          dateType = 1;
        } else if (difference > 6) {
          dateType = 2;
        } else if (difference < 7) {
          dateType = 3;
        }
      } else {
        dateType = 1;
        final date = new DateTime.now();
        dueDate = DateFun.getFormatedDate(
            format: DateFun.getDateUKFormat(),
            mDate: DateTime(date.year, date.month, date.day));
      }
    } catch (e) {}
    try {
      wsUserDevice(eventType: 'Task Post - Task Date');
      setState(() {});
    } catch (e) {}
  }

  onNextClicked() {
    if (dateType == 0) {
      showToast(context: context, msg: "Please choose task due date");
      return;
    }
    Get.to(
      () => AddTask3Page(
        dueDate: dueDate,
        userModel: widget.userModel,
        isCopyTask: widget.isCopyTask,
      ),
    ).then(
      (value) {
        if (value == 0 || value == 4) Get.back(result: value);
      },
    );
  }

  onDelTaskClicked() {
    try {
      APIViewModel().req<DelTaskAPIModel>(
        context: context,
        apiState: APIState(APIType.del_task, this.runtimeType, null),
        url: APIPostTaskCfg.DEL_TASK_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //myLog(taskController.getTaskModel().id.toString());

    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          pageNo: 2,
          userModel: widget.userModel,
          title: 'Create Task',
          pos: 1,
          isBold1: true,
          isBold2: true,
          isBold3: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Continue",
            icon: Icons.arrow_forward,
            callback: () async {
              onNextClicked();
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            Txt(
                txt: 'Due Date',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false),
            SizedBox(height: 15),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                      child: _drawDropDownButton(text: "Today", dtType: 1)),
                  SizedBox(width: 10),
                  Flexible(
                      child: _drawDropDownButton(
                          text: "By a certain day", dtType: 2)),
                  SizedBox(width: 10),
                  Flexible(
                      child: _drawDropDownButton(
                          text: "Within 1 week", dtType: 3)),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _drawDropDownButton({
    String text,
    int dtType = 1,
  }) {
    final DateTime dateNow = DateTime.now();

    var firstDate;
    var lastDate;

    if (dtType == 1) {
      firstDate = dateNow;
      lastDate = dateNow;
    } else if (dtType == 2) {
      firstDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
      lastDate = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    } else if (dtType == 3) {
      firstDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
      lastDate = DateTime(dateNow.year, dateNow.month, dateNow.day + 6);
    }

    return GestureDetector(
      onTap: () {
        showDatePicker(
          context: context,
          initialDate: dateNow,
          firstDate: firstDate,
          lastDate: lastDate,
          builder: (context, child) {
            return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: ColorScheme.light(primary: MyTheme.redColor),
                buttonTheme:
                    ButtonThemeData(textTheme: ButtonTextTheme.primary),
              ), // This will change to light theme.
              child: child,
            );
          },
        ).then((value) {
          if (value != null) {
            try {
              dateType = dtType;
              dueDate = DateFormat('dd-MMM-yyyy').format(value).toString();
              setState(() {});
            } catch (e) {
              myLog(e.toString());
            }
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: dtType == dateType ? MyTheme.brandColor : Colors.transparent,
            border: Border.all(
              color: dtType == dateType ? Colors.transparent : Colors.black38,
            ),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 4,
                child: Text(
                  dtType == dateType ? dueDate : text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: dtType == dateType ? Colors.white : Colors.black,
                      fontSize: 15),
                ),
              ),
              Flexible(
                child: Icon(
                  Icons.arrow_drop_down,
                  color: dtType == dateType ? Colors.white : Colors.black,
                  size: 30,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _drawTime(
      {bool isTime,
      String txt1,
      String txt2,
      String ico,
      Function(bool) callback}) {
    return GestureDetector(
      onTap: () {
        callback(!isTime);
      },
      child: Container(
        decoration: BoxDecoration(
            color: isTime ? MyTheme.brandColor : Colors.white,
            border: Border.all(
              color: isTime ? Colors.transparent : MyTheme.brandColor,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: Column(
            children: [
              _drawTimeBox(
                  isTime: isTime,
                  txt1: txt1,
                  txt2: txt2,
                  ico: ico,
                  callback: (v) {
                    callback(v);
                  }),
            ],
          ),
        ),
      ),
    );
  }

  _drawTimeBox(
      {bool isTime,
      String txt1,
      String txt2,
      String ico,
      Function(bool) callback}) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Align(
              alignment: Alignment.centerRight,
              child: SizedBox(
                width: 15,
                height: 15,
                child: Container(
                  margin: const EdgeInsets.all(0),
                  padding: const EdgeInsets.all(0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: isTime ? MyTheme.brandColor : Colors.black,
                        width: 1),
                    //borderRadius: BorderRadius.circular(5),
                  ),
                  child: Checkbox(
                      splashRadius: 10,
                      fillColor: MaterialStateProperty.all(Colors.transparent),
                      focusColor: MyTheme.brandColor,
                      checkColor: Colors.black,
                      value: isTime,
                      onChanged: (value) {
                        callback(value);
                      }),
                ),
              ),
            ),
          ),
          Container(
            width: getWP(context, 6),
            height: getWP(context, 6),
            child: Image.asset(
              "assets/images/icons/times/" + ico,
              color: isTime ? Colors.white : Colors.black,
            ),
          ),
          SizedBox(height: 5),
          Txt(
            txt: txt1,
            txtColor: isTime ? Colors.white : Colors.black87,
            txtSize: MyTheme.txtSize - .7,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          SizedBox(height: 5),
          Txt(
            txt: txt2,
            txtColor: isTime ? Colors.white : Colors.black87,
            txtSize: MyTheme.txtSize - .8,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
        ],
      ),
    );
  }
}
