import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AddrCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl_pkg/classes/MapFun.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/PostTask1APIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/SavePicAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_base.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page2.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view/widgets/input/InputBoxHT.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxHT.dart';
import 'package:aitl/view/widgets/switchview/SwitchTitle.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/PostTaskHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';

class AddTask1Screen extends StatefulWidget {
  final int index;
  final UserModel userModel;
  String categoryPH;
  final TaskModel taskModel;
  final bool isCopyTask;
  AddTask1Screen({
    Key key,
    this.index,
    @required this.userModel,
    this.categoryPH = '',
    this.taskModel,
    this.isCopyTask = false,
  }) : super(key: key);
  @override
  State createState() => _AddTask1ScreenState();
}

class _AddTask1ScreenState extends BaseAddTaskStatefull<AddTask1Screen>
    with APIStateListener {
  final taskHeadline = TextEditingController();
  final taskDesc = TextEditingController();
  //
  final focusHeadline = FocusNode();
  final focusDesc = FocusNode();

  bool isOnlineSwitch = false;
  String taskAddress = AddrCfg.AddrHint;
  Location taskCord;
  final int minLenTaskHeadline = 10;
  final int minLenTaskDesc = 25;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.del_task &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              taskController.setTaskModel(null);
              Get.back(result: 0);
            } catch (e) {}
          }
        }
      } else if ((apiState.type == APIType.post_task1 ||
              apiState.type == APIType.put_task1) &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              taskController.setTaskModel(model.responseData.task);
              Get.to(() => AddTask2Page(
                    userModel: widget.userModel,
                    isCopyTask: widget.isCopyTask,
                  )).then((value) {
                if (value == 0 || value == 4) {
                  Get.back(result: value);
                }
              });
            } catch (e) {}
          }
        }
      } else if (apiState.type == APIType.save_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  uploadTaskImages(int pageNo) async {
    //  0= deleted task
    //  4= task done
    if (pageNo != 0 && pageNo != 4) {
      try {
        for (var map in taskController.getTaskModel().taskImages) {
          if (taskController.getTaskModel().id != null) {
            await APIViewModel().req<SavePicAPIModel>(
              context: context,
              apiState: APIState(APIType.save_pic, this.runtimeType, null),
              url: APIPostTaskCfg.SAVE_PIC_URL,
              reqType: ReqType.Post,
              param: {
                "EntityId": taskController.getTaskModel().id,
                "EntityName": "Task",
                "PropertyName": "Task",
                "Value": map['id'],
              },
            );
          }
        }
        taskController.setTaskModel(null);
      } catch (e) {
        myLog(e.toString());
      }
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      taskController.dispose();
      taskHeadline.dispose();
      taskDesc.dispose();
    } catch (e) {
      myLog(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      //taskData.listTaskImages = [];
    } catch (e) {}
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (widget.taskModel != null) {
        taskController.setTaskModel(widget.taskModel);
        isOnlineSwitch = widget.taskModel.isInPersonOrOnline ?? false;
        taskHeadline.text = widget.taskModel.title ?? '';
        taskDesc.text = widget.taskModel.description ?? '';
        taskAddress = widget.taskModel.preferedLocation;
        taskCord = Location(
            lat: widget.taskModel.latitude, lng: widget.taskModel.longitude);
      } else {
        taskController.setTaskModel(null);
      }
    } catch (e) {}

    try {
      if (widget.index != null) {
        final map = PostTaskHelper.listTask[widget.index];
        widget.categoryPH = map['ph'];
        await wsUserDevice(eventType: map['name']);
      }
      wsUserDevice(eventType: 'Task Post - Task Details');
    } catch (e) {}
  }

  validate() {
    if (taskHeadline.text.length < 11) {
      showToast(
        context: context,
        msg: 'Please enter a valid title\n(at least ' +
            minLenTaskHeadline.toString() +
            ' characters)',
      );
      return false;
    } else if (taskDesc.text.length < 26) {
      showToast(
        context: context,
        msg: 'Please enter a descriptions\n(at least ' +
            minLenTaskDesc.toString() +
            ' characters)',
      );
      return false;
    } else if (!isOnlineSwitch &&
        (taskAddress == AddrCfg.AddrHint || taskAddress == "")) {
      showToast(context: context, msg: 'Please pick your address');
      return false;
    }

    return true;
  }

  onNextClicked() async {
    if (validate()) {
      if (widget.taskModel == null || widget.isCopyTask) {
        APIViewModel().req<PostTask1APIModel>(
            context: context,
            apiState: APIState(APIType.post_task1, this.runtimeType, null),
            url: APIPostTaskCfg.POSTTASK1_URL,
            reqType: ReqType.Post,
            param: {
              "DeliveryDate": DateFun.getFormatedDate(format: "dd-MMM-yyyy"),
              "DeliveryTime": "",
              "Description": taskDesc.text.trim(),
              "DueAmount": 0.0,
              "DutDateType": 0,
              "EmployeeId": 0,
              "FixedBudgetAmount": 0.0,
              "HourlyRate": 0.0,
              "IsFixedPrice": true,
              "IsInPersonOrOnline": isOnlineSwitch,
              "JobCategory": "",
              "Latitude": (taskCord != null) ? taskCord.lat : 0.0,
              "Longitude": (taskCord != null) ? taskCord.lng : 0.0,
              "NetTotalAmount": 0.0,
              "PaidAmount": 0.0,
              "PreferedLocation":
                  taskAddress.trim().replaceAll(AddrCfg.AddrHint, ""),
              "Requirements": "",
              "Skill": "",
              "Status": TaskStatusCfg.TASK_STATUS_DRAFT,
              "Id": 0,
              "Title": taskHeadline.text.trim(),
              "TotalBidsNumber": 0,
              "TotalHours": 0.0,
              "UserId": widget.userModel.id,
              "WorkerNumber": 0,
            });
      } else {
        if (!isOnlineSwitch) {
          try {
            final Coordinates cord =
                await MapFun().getCordByAddr(taskAddress.trim());
            taskCord = Location(lat: cord.latitude, lng: cord.longitude);
          } catch (e) {}
        }

        APIViewModel().req<PostTask1APIModel>(
          context: context,
          apiState: APIState(APIType.put_task1, this.runtimeType, null),
          url: APIPostTaskCfg.POSTTASK1_PUT_URL,
          reqType: ReqType.Put,
          param: {
            "DeliveryDate": taskController.getTaskModel().deliveryDate,
            "DeliveryTime": taskController.getTaskModel().deliveryTime,
            "Description": taskDesc.text.trim(),
            "DueAmount": taskController.getTaskModel().dueAmount,
            "DutDateType": taskController.getTaskModel().dutDateType,
            "EmployeeId": taskController.getTaskModel().employeeId,
            "FixedBudgetAmount":
                taskController.getTaskModel().fixedBudgetAmount,
            "HourlyRate": taskController.getTaskModel().hourlyRate,
            "IsFixedPrice": taskController.getTaskModel().isFixedPrice,
            "IsInPersonOrOnline": isOnlineSwitch,
            "JobCategory": taskController.getTaskModel().jobCategory,
            "Latitude": (taskCord != null)
                ? taskCord.lat
                : taskController.getTaskModel().latitude,
            "Longitude": (taskCord != null)
                ? taskCord.lng
                : taskController.getTaskModel().longitude,
            "NetTotalAmount": taskController.getTaskModel().netTotalAmount,
            "PaidAmount": taskController.getTaskModel().paidAmount,
            "PreferedLocation":
                taskAddress.trim().replaceAll(AddrCfg.AddrHint, ""),
            "Requirements": taskController.getTaskModel().requirements,
            "Skill": taskController.getTaskModel().skill,
            "Status": TaskStatusCfg()
                .getSatusCode(taskController.getTaskModel().status.toString()),
            "Id": taskController.getTaskModel().id,
            "Title": taskHeadline.text.trim(),
            "TotalBidsNumber": taskController.getTaskModel().totalBidsNumber,
            "TotalHours": taskController.getTaskModel().totalHours,
            "UserId": taskController.getTaskModel().userId,
            "WorkerNumber": taskController.getTaskModel().workerNumber,
          },
        );
      }
    }
  }

  onDelTaskClicked() {
    try {
      APIViewModel().req<DelTaskAPIModel>(
        context: context,
        apiState: APIState(APIType.del_task, this.runtimeType, null),
        url: APIPostTaskCfg.DEL_TASK_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        reqType: ReqType.Delete,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          pageNo: 1,
          userModel: widget.userModel,
          title: 'Create Task',
          pos: 0,
          isBold1: true,
          isBold2: false,
          isBold3: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Continue",
            icon: Icons.arrow_forward,
            callback: () async {
              onNextClicked();
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          children: [
            Column(
              children: [
                InputTitleBoxHT(
                  title: "Task Headline",
                  ph: widget.categoryPH,
                  input: taskHeadline,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusHeadline,
                  focusNodeNext: focusDesc,
                  len: 50,
                  minLen: minLenTaskHeadline,
                  ecap: eCap.Sentence,
                ),
                SizedBox(height: 20),
                InputTitleBoxHT(
                  title: "Description",
                  ph: '',
                  input: taskDesc,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.done,
                  focusNode: focusDesc,
                  len: 255,
                  minLen: minLenTaskDesc,
                  minLine: 1,
                  maxLine: 3,
                  ecap: eCap.Sentence,
                ),
                SizedBox(height: 20),
                SwitchTitle(
                  txt: "Will the job be based online?",
                  switchIndex: (isOnlineSwitch) ? 0 : 1,
                  callback: (_isSwitch) {
                    isOnlineSwitch = _isSwitch;
                    setState(() {});
                  },
                ),
                (!isOnlineSwitch)
                    ? Column(
                        children: [
                          SizedBox(height: 20),
                          GPlacesView(
                            title: "Task Location",
                            txtColor: MyTheme.gray5Color,
                            isTxtBold: false,
                            address: taskAddress,
                            bgColor: Colors.white,
                            callback: (String _address, Location _loc) {
                              //callback(address);
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              taskAddress = _address;
                              taskCord = _loc;
                              setState(() {});
                            },
                          ),
                        ],
                      )
                    : SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
