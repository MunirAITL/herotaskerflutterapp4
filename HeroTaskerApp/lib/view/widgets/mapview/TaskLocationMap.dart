import 'dart:async';

import 'package:aitl/config/cfg/FindWorksConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/ImageLib.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class TaskLocationMap extends StatefulWidget {
  const TaskLocationMap({Key key}) : super(key: key);
  @override
  State createState() => _TaskLocationMapState();
}

class _TaskLocationMapState extends State<TaskLocationMap> with Mixin {
  final taskCtrl = Get.put(TaskCtrl());

  GoogleMapController mapController;
  Marker marker;
  LatLng cord;
  String preferedLocation;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      mapController.dispose();
      mapController = null;
      marker = null;
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
    ].request();
    try {
      cord = LatLng(
          taskCtrl.getTaskModel().latitude, taskCtrl.getTaskModel().longitude);
      preferedLocation = taskCtrl.getTaskModel().preferedLocation;
      // creating a new MARKER
      if (taskCtrl.getTaskModel().latitude < 1) {
        taskCtrl.getTaskModel().latitude = FindWorksConfig.lat;
        taskCtrl.getTaskModel().longitude = FindWorksConfig.lng;
      }
      marker = Marker(
        markerId: MarkerId(taskCtrl.getTaskModel().id.toString()),
        position: cord,
        infoWindow: InfoWindow(title: taskCtrl.getTaskModel().title),
        onTap: () {},
        icon: BitmapDescriptor.fromBytes(await ImageLib()
            .getBytesFromAsset('assets/images/map/boot_marker.png', 100)),
      );

      setState(() {});
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: 'Task location'),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return (marker != null)
        ? Container(
            width: getW(context),
            height: getH(context),
            child: Stack(
              children: [
                Positioned(
                  top: 0,
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: GoogleMap(
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                    zoomControlsEnabled: false,
                    zoomGesturesEnabled: true,
                    scrollGesturesEnabled: true,
                    compassEnabled: true,
                    rotateGesturesEnabled: true,
                    mapToolbarEnabled: true,
                    tiltGesturesEnabled: true,
                    mapType: MapType.normal,
                    markers: Set<Marker>.of([marker]),
                    initialCameraPosition:
                        CameraPosition(target: cord, zoom: 15),
                    gestureRecognizers: Set()
                      ..add(Factory<PanGestureRecognizer>(
                          () => PanGestureRecognizer())),
                    onMapCreated: _onMapCreated,
                  ),
                ),
                cord.latitude != null && cord.longitude != null
                    ? Positioned(
                        bottom: 50,
                        left: 0,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 40, right: 40),
                          child: MMBtn(
                              width: getWP(context, 80),
                              height: getHP(context, MyTheme.btnHpa),
                              txt: "Get directions",
                              radius: 30,
                              txtColor: MyTheme.brandColor,
                              bgColor: MyTheme.gray1Color,
                              callback: () {
                                openMap(context, cord.latitude, cord.longitude,
                                    preferedLocation);
                              }),
                        ),
                      )
                    : SizedBox(),
                if (isLoading) drawLoading(),
              ],
            ),
          )
        : drawLoading();
  }

  drawLoading() {
    return Container(
        color: Colors.white,
        width: getW(context),
        height: getH(context),
        child: Center(
          child: CircularProgressIndicator(
            backgroundColor: MyTheme.brandColor,
          ),
        ));
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    Timer(Duration(seconds: 1), () {
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    });
  }
}
