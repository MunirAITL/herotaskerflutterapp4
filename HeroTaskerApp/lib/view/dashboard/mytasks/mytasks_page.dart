import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../../main.dart';
import 'mytasks_base.dart';

class MyTasksPage extends StatefulWidget {
  @override
  State createState() => _MyTasksPageState();
}

class _MyTasksPageState extends BaseMyTaskStatefull<MyTasksPage>
    with APIStateListener, StateListener, SingleTickerProviderStateMixin {
  List<TaskModel> listTaskModel = [];

  //  search stuff start
  //  0
  //  Sometime rebuilding whole screen might not be desirable with setState((){})
  //  for this situation you can wrap searchables with ValuelistenableBuilder widget.
  ValueNotifier<List<TaskModel>> filtered = ValueNotifier<List<TaskModel>>([]);
  FocusNode searchFocus = FocusNode();
  final searchText = TextEditingController();
  bool isSearchIconClicked = false;
  bool searching = false;
  bool isRefreshing = false;
  //  search stuff end

  TabController tabController;

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int page = 0;
  int count = AppConfig.page_limit;

  int totalTabs = 6;

  double distance;
  double fromPrice;
  double toPrice;
  double lat;
  double lng;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB ||
          state == ObserverState.STATE_RELOAD_TAB_MY_TASKS) {
        refreshData();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.taskinfo_search &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          final List<dynamic> locations = model.responseData.locations;
          if (locations != null) {
            //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
            if (locations.length != count) {
              isPageDone = true;
            }
            try {
              for (TaskModel task in locations) {
                listTaskModel.add(task);
              }
              setState(() {});
            } catch (e) {
              myLog(e.toString());
            }
          } else {}
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await APIViewModel().req<TaskInfoSearchAPIModel>(
        context: context,
        apiState: APIState(APIType.taskinfo_search, this.runtimeType, null),
        url: APIPostTaskCfg.TASKINFOBYSEARCH_POST_URL,
        //isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": count,
          "Distance": distance ?? 500,
          "FromPrice": fromPrice ?? 20,
          "InPersonOrOnline": 0,
          "IsHideAssignTask": false,
          "Latitude": lat ?? 0.0,
          "Location": "",
          "Longitude": lng ?? 0.0,
          "Page": page,
          "SearchText": searchText.text ?? '',
          "Status": taskStatus,
          "ToPrice": toPrice ?? 100000,
          "UserId": userData.userModel.id,
        },
      );
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> refreshData() async {
    if (mounted) {
      setState(() {
        isSearchIconClicked = false;
        searchText.clear();
        searching = false;
        filtered.value = [];
        if (searchFocus.hasFocus) searchFocus.unfocus();
        //
        page = 0;
        isPageDone = false;
        isLoading = true;
        listTaskModel.clear();
      });
      onLazyLoadAPI();
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    listTaskModel = null;
    searchText.dispose();

    try {
      tabController.dispose();
      tabController = null;
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      tabController = new TabController(vsync: this, length: totalTabs);
      tabController.addListener(() {
        if (!isLoading) {
          myLog('top tab index = ' + tabController.index.toString());
          onTopTabbarIndexChanged(tabController.index, (map) {
            taskStatus = map['status'];
            refreshData();
          });
        }
      });
    } catch (e) {}
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: totalTabs,
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            backgroundColor: MyTheme.appbarColor,
            iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
            elevation: 0,
            //automaticallyImplyLeading: !isSearchIconClicked,
            leadingWidth: (!isSearchIconClicked) ? 0 : 56,
            leading: (!isSearchIconClicked)
                ? SizedBox()
                : IconButton(
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      isSearchIconClicked = !isSearchIconClicked;
                      searchText.clear();
                      searching = false;
                      filtered.value = [];
                      if (searchFocus.hasFocus) searchFocus.unfocus();
                      setState(() {});
                    },
                    icon: Icon(Icons.arrow_back_ios, size: MyTheme.fontSize)),
            title: (!isSearchIconClicked)
                ? UIHelper().drawAppbarTitle(title: 'My Tasks')
                : drawSearchbar(searchText, (text) {
                    if (text.length > 0) {
                      searching = true;
                      filtered.value = [];
                      listTaskModel.forEach((locModel) {
                        if (locModel.title
                                .toString()
                                .toLowerCase()
                                .contains(text.toLowerCase()) ||
                            locModel.ownerName
                                .toString()
                                .toLowerCase()
                                .contains(text.toLowerCase())) {
                          filtered.value.add(locModel);
                        }
                      });
                    } else {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                      searchText.clear();
                      searching = false;
                      filtered.value = [];
                      if (searchFocus.hasFocus) searchFocus.unfocus();
                    }
                  }),
            centerTitle: false,
            actions: <Widget>[
              (!isSearchIconClicked)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () {
                            isSearchIconClicked = !isSearchIconClicked;
                            setState(() {});
                          }),
                    )
                  : SizedBox()
            ],
            bottom: drawAppbarNavBar(tabController, (topTabindex) {
              if (!isLoading) {
                tabController.index = topTabindex;
              }
            }),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: TabBarView(
              physics: (isLoading)
                  ? NeverScrollableScrollPhysics()
                  : AlwaysScrollableScrollPhysics(),
              controller: tabController,
              children: <Widget>[
                drawLayout(),
                drawLayout(),
                drawLayout(),
                drawLayout(),
                drawLayout(),
                drawLayout(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //color: MyTheme.gray1Color,
      child: (listTaskModel.length > 0)
          ? ValueListenableBuilder<List>(
              valueListenable: filtered,
              builder: (context, value, _) {
                return RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.brandColor,
                  onRefresh: () async {
                    isRefreshing = true;
                    refreshData();
                    return;
                  },
                  notificationPredicate: (scrollNotification) {
                    if (scrollNotification is ScrollStartNotification) {
                      //print('Widget has started scrolling');
                    } else if (scrollNotification is ScrollEndNotification) {
                      Future.delayed(Duration(seconds: 1), () {
                        if (!isRefreshing && !isPageDone) {
                          page++;
                          onLazyLoadAPI();
                        }
                      });
                    }
                    return true;
                  },
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: searching
                        ? filtered.value.length
                        : listTaskModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawItem(
                          searching
                              ? filtered.value[index]
                              : listTaskModel[index],
                          false);
                    },
                  ),
                );
              },
            )
          : (!isLoading)
              ? drawNF()
              : Container(),
    );
  }
}
