import 'package:aitl/data/model/auth/UserModel.dart';

class UserPriceAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  UserPriceAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  UserPriceAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  UserPrice userPrice;
  ResponseData({this.userPrice});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userPrice = json['UserPrice'] != null
        ? new UserPrice.fromJson(json['UserPrice'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userPrice != null) {
      data['UserPrice'] = this.userPrice.toJson();
    }
    return data;
  }
}

class UserPrice {
  int userId;
  dynamic user;
  int status;
  String creationDate;
  double perHour;
  double perDay;
  double perMonth;
  int id;

  UserPrice(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.perHour,
      this.perDay,
      this.perMonth,
      this.id});

  UserPrice.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    perHour = json['PerHour'];
    perDay = json['PerDay'];
    perMonth = json['PerMonth'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['PerHour'] = this.perHour;
    data['PerDay'] = this.perDay;
    data['PerMonth'] = this.perMonth;
    data['Id'] = this.id;
    return data;
  }
}
