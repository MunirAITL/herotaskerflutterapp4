import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/mytasks/taskdetails_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../main.dart';

abstract class BaseMyTaskStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  bool isLoading = false;
  //  tab stuff start here
  int taskStatus = TaskStatusCfg.TASK_STATUS_ALL;

  refreshData();

  drawSearchbar(TextEditingController searchText, Function(String) onChange) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: TextField(
        controller: searchText,
        autofocus: true,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.search,
        onChanged: (value) => onChange(value),
        autocorrect: false,
        style: TextStyle(
          color: MyTheme.appbarTxtColor,
          fontSize: MyTheme.fontSize,
        ),
        decoration: InputDecoration(
          counter: Offstage(),
          prefixIcon: Icon(
            Icons.search,
            color: MyTheme.appbarTxtColor,
          ),
          suffixIcon: IconButton(
              onPressed: () => onChange(""),
              icon: Icon(
                Icons.close,
                color: MyTheme.appbarTxtColor,
              )),
          hintText: "Search by title",
          hintStyle: new TextStyle(
            color: Colors.black45,
            fontSize: MyTheme.fontSize,
            //height: MyTheme.txtLineSpace,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }

  drawAppbarNavBar(TabController tabController, Function callback) {
    final xtraH = isIPAD || isTablet ? 20 : 0;
    return PreferredSize(
      //preferredSize: Size.fromHeight(getHP(context, 12)),
      preferredSize: Size.fromHeight(AppConfig.mytasks_height + xtraH),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*Container(
              color: MyTheme.appbarColor,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, top: 10, bottom: 10),
                child: Center(
                  child: Txt(
                      txt: "View the ongoing Task",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
            ),*/
            Container(
              color: Colors.transparent,
              child: TabBar(
                onTap: (index2) {
                  if (isLoading) {
                    callback(index2);
                  }
                },
                controller: tabController,
                isScrollable: true,
                indicatorColor: Colors.transparent, // MyTheme.appbarDarkColor,
                unselectedLabelColor: Colors.black54,
                labelColor: Colors.red,
                indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(width: 2, color: Colors.red),
                    insets: EdgeInsets.symmetric(horizontal: 5)),
                tabs: [
                  Tab(
                      child: Txt(
                          txt: "All",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Posted",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Draft",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Assigned",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Offered",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Completed",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                ],
              ),
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }

  onTopTabbarIndexChanged(int index, Function(Map<String, dynamic>) callback) {
    switch (index) {
      case 0:
        callback({'status': TaskStatusCfg.TASK_STATUS_ALL});
        break;
      case 1:
        callback({'status': TaskStatusCfg.TASK_STATUS_ACTIVE});
        break;
      case 2:
        callback({'status': TaskStatusCfg.TASK_STATUS_DRAFT});
        break;
      case 3:
        callback({'status': TaskStatusCfg.TASK_STATUS_ACCEPTED});
        break;
      case 4:
        callback({'status': TaskStatusCfg.TASK_STATUS_PENDINGTASK});
        break;
      case 5:
        callback({'status': TaskStatusCfg.TASK_STATUS_PAYMENTED});
        break;
      default:
    }
  }

  go2PostTaskScreen(TaskModel taskModel) async {
    Get.to(
      () => AddTask1Screen(
        index: null,
        userModel: userData.userModel,
        taskModel: taskModel,
      ),
    ).then(
      (pageNo) {
        obsUpdateTabs(pageNo);
      },
    );
  }

  go2TaskDetailScreen(TaskModel taskModel) async {
    Get.to(() => TaskDetailsPage(
          taskModel: taskModel,
        )).then((value) {
      if (value == "STATE_RELOAD_TAB_FIND_WORK") {
        StateProvider().notify(ObserverState.STATE_RELOAD_TAB, 2);
      } else {
        refreshData();
      }
    });
  }

  drawItem(TaskModel taskModel, bool isFromDashboard) {
    final priceTxt = (taskModel.isFixedPrice)
        ? taskModel.fixedBudgetAmount.round().toString()
        : taskModel.hourlyRate.round().toString() + '/hr';
    //
    var status = taskModel.status;
    try {
      if (int.parse(status) > 0) {
        status = TaskStatusCfg().getSatus(int.parse(status));
      }
    } catch (e) {
      status = taskModel.status;
    }

    if (status == "CANCELLED" && taskStatus != TaskStatusCfg.TASK_STATUS_ALL) {
      return SizedBox();
    }
    //
    String preferedLocation = taskModel.preferedLocation;
    if (taskModel.isInPersonOrOnline) preferedLocation = "Remote";
    //
    Color ribbonColor = MyTheme.airBlueColor;
    if (status.toString().toLowerCase() == "active") {
      ribbonColor = MyTheme.airGreenColor;
    }
    //
    Color statusColor = Colors.grey;
    switch (status.toString().toLowerCase()) {
      case 'active':
        statusColor = MyTheme.dGreenColor;
        break;
      case 'draft':
        statusColor = Color(0xFFFFC100);
        break;
      case 'paid':
        statusColor = Colors.grey;
        break;
      case 'assigned':
        statusColor = Colors.grey;
        break;
      default:
    }

    String statusAddtionalInfo = "";
    if (status.toString().toLowerCase() == "active") {
      if (taskModel.totalBidsNumber > 0) {
        statusAddtionalInfo =
            " - " + taskModel.totalBidsNumber.toString() + " offer";
      } else {
        statusAddtionalInfo = "";
      }
    }
    //

    return Padding(
      padding: const EdgeInsets.only(top: 1, bottom: 1),
      child: GestureDetector(
        onTap: () {
          if (taskModel.status.toString().toLowerCase() == "draft") {
            go2PostTaskScreen(taskModel);
          } else {
            go2TaskDetailScreen(taskModel);
          }
        },
        child: Card(
          elevation: 1,
          margin: EdgeInsets.only(top: 2),
          //color: Colors.transparent,
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(child: Container(color: ribbonColor)),
                Container(
                  width: getWP(context, !isFromDashboard ? 98.5 : 90),
                  //alignment: Alignment.centerRight,
                  //color: Colors.black,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, left: 10, right: 10, bottom: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Txt(
                                  txt: taskModel.title,
                                  txtColor: MyTheme.gray5Color,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ),
                            Container(
                              //color: Colors.black,
                              child: drawPrice(
                                price: priceTxt,
                                txtSize: 22,
                                txtColor: MyTheme.gray5Color,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.location_on_outlined,
                                        size: 20,
                                        color: MyTheme.gray4Color,
                                      ),
                                      SizedBox(width: 5),
                                      Expanded(
                                        child: Txt(
                                          txt: preferedLocation,
                                          txtColor: MyTheme.gray4Color,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                          isOverflow: true,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.calendar_today_outlined,
                                        size: 20,
                                        color: MyTheme.gray4Color,
                                      ),
                                      SizedBox(width: 5),
                                      Expanded(
                                        child: Txt(
                                          txt: DateFun.getFormatedDate(
                                              mDate: DateFormat("dd-MMM-yyyy")
                                                  .parse(
                                                      taskModel.deliveryDate),
                                              format: "EEE MMM dd, yyyy"),
                                          txtColor: MyTheme.gray4Color,
                                          txtSize: MyTheme.txtSize - .4,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    children: [
                                      Flexible(
                                          child: Txt(
                                              txt: status,
                                              txtColor: statusColor,
                                              txtSize: MyTheme.txtSize - .6,
                                              txtAlign: TextAlign.start,
                                              isBold: false)),
                                      Flexible(
                                          child: Txt(
                                              txt: statusAddtionalInfo,
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .6,
                                              txtAlign: TextAlign.start,
                                              isBold: false)),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Container(
                              width: getWP(context, 10),
                              height: getWP(context, 10),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: MyNetworkImage.loadProfileImage(
                                      taskModel.ownerImageUrl),
                                  fit: BoxFit.cover,
                                ),
                                shape: BoxShape.circle,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawNF() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          //shrinkWrap: true,
          children: [
            Container(
              width: getWP(context, 50),
              height: getWP(context, 30),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_my_tasks.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt:
                      "Looks like you haven't posted a task or made any offers just yet. How about doing that now?",
                  txtColor: MyTheme.mycasesNFBtnColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
            SizedBox(height: 10),
            Btn(
                txt: "Refresh",
                txtColor: Colors.white,
                bgColor: MyTheme.brandColor,
                txtSize: 1.8,
                radius: 0,
                callback: () {
                  refreshData();
                }),
          ],
        ),
      ),
    );
  }
}
