import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/auth/role/PostWorkerSteadyFlowAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/otp/OtpByEmailPage.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'worker_base.dart';
import 'package:get/get.dart';

class WorkerRolePage extends StatefulWidget {
  final UserModel userModel;
  const WorkerRolePage({Key key, @required this.userModel}) : super(key: key);
  @override
  State<WorkerRolePage> createState() => _WorkerRolePageState();
}

class _WorkerRolePageState extends WorkerBase<WorkerRolePage> {
  var prog = 0.4.obs;

  onPostWorker() async {
    var whatsMostImportantInTheOportunitiesYouAreLookingFor = [];
    var whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari = [];
    var whereDoYouWantToWork = [];
    var whatDoesYourWorkSituationLookLikeRightNow = [];
    var whichOfTheFollowinDoYouUse = [];
    var howDoYouKeepTrackOfYourBudgetOrFinancialPlan = [];
    for (int i = 0; i < list1Index.length; i++) {
      if (list1Index[i]) {
        whatsMostImportantInTheOportunitiesYouAreLookingFor.add(list1[i]);
      }
    }
    for (int i = 0; i < list2Index.length; i++) {
      if (list2Index[i]) {
        whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari.add(list2[i]);
      }
    }
    for (int i = 0; i < list3Index.length; i++) {
      if (list2Index[i]) {
        whereDoYouWantToWork.add(list3[i]);
      }
    }
    for (int i = 0; i < list5Index.length; i++) {
      if (list5Index[i]) {
        whatDoesYourWorkSituationLookLikeRightNow.add(list5[i]);
      }
    }
    for (int i = 0; i < list6Index.length; i++) {
      if (list6Index[i]) {
        whichOfTheFollowinDoYouUse.add(list6[i]);
      }
    }
    for (int i = 0; i < list7Index.length; i++) {
      if (list7Index[i]) {
        howDoYouKeepTrackOfYourBudgetOrFinancialPlan.add(list7[i]);
      }
    }

    await APIViewModel().req<PostWorkerSteadyFlowAPIModel>(
      context: context,
      url: APIAuthCfg.POST_ROLETYPE_WORKER_FLOWDATA_URL,
      param: {
        "UserId": userData.userModel.id,
        "WhatsMostImportantInTheOportunitiesYouAreLookingFor":
            whatsMostImportantInTheOportunitiesYouAreLookingFor,
        "WhatDoYouWantToDoWithExtraCashYouEarnThroughShohokari":
            whatDoYouWantToDoWithExtraCashYouEarnThroughShohokari,
        "WhereDoYouWantToWork": whereDoYouWantToWork,
        "WhatsYourHighestLevelOfEducation": list4[list4Index.value],
        "WhatDoesYourWorkSituationLookLikeRightNow":
            whatDoesYourWorkSituationLookLikeRightNow,
        "WhichOfTheFollowinDoYouUse": whichOfTheFollowinDoYouUse,
        "HowDoYouKeepTrackOfYourBudgetOrFinancialPlan":
            howDoYouKeepTrackOfYourBudgetOrFinancialPlan,
        "InTheLastThreeMonthsHowOftenDidYouSearchForNewWorkOportunities": [""]
      },
      reqType: ReqType.Post,
      callback: (model) async {
        if (mounted && model != null) {
          if (model.success) {
            if (!Server.isOtp) {
              Get.off(() => DashboardPage());
            } else {
              if (widget.userModel.isMobileNumberVerified) {
                Get.off(() => DashboardPage());
              } else {
                Get.to(() => OtpByEmailPage(email: widget.userModel.email));
              }
            }
          } else {}
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Obx(
      () => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          titleSpacing: 20,
          title: Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                height: 10,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: LinearProgressIndicator(
                    backgroundColor: Colors.grey,
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.red,
                    ),
                    value: prog.value,
                  ),
                ),
              )),
          //titleSpacing: 0,
        ),
        body: drawLayout(),
      ),
    ));
  }

  drawLayout() => Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            prog.value == 0.4
                ? drawStep1()
                : prog.value == 0.5
                    ? drawStep2()
                    : prog.value == 0.6
                        ? drawStep3()
                        : prog.value == 0.7
                            ? drawStep4()
                            : prog.value == 0.8
                                ? drawStep5()
                                : prog.value == 0.9
                                    ? drawStep6()
                                    : prog.value == 1
                                        ? drawStep7()
                                        : drawStep7(),
            SizedBox(height: 40),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                prog.value < 1
                    ? Flexible(
                        child: Btn(
                            txt: "Save & finish later",
                            txtColor: Colors.white,
                            bgColor: Colors.grey,
                            isTxtBold: false,
                            txtSize: 1.6,
                            radius: 20,
                            callback: () {
                              if (widget.userModel.isMobileNumberVerified) {
                                Get.off(() => DashboardPage());
                              } else {
                                Get.to(() => OtpByEmailPage(
                                    email: widget.userModel.email));
                              }
                            }))
                    : SizedBox(),
                //Spacer(),
                Expanded(
                    flex: 2,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        prog.value < 1
                            ? Flexible(
                                child: InkWell(
                                    onTap: () {
                                      if (widget
                                          .userModel.isMobileNumberVerified) {
                                        Get.off(() => DashboardPage());
                                      } else {
                                        Get.to(() => OtpByEmailPage(
                                            email: widget.userModel.email));
                                      }
                                    },
                                    child: Txt(
                                      txt: "Skip",
                                      txtColor: Colors.red,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                    )))
                            : SizedBox(),
                        SizedBox(width: 20),
                        Expanded(
                            child: Btn(
                                txt: prog.value < 1 ? "Continue" : "Completed",
                                txtColor: Colors.white,
                                bgColor: Colors.green,
                                txtSize: 1.6,
                                isTxtBold: false,
                                radius: 20,
                                callback: () {
                                  if (prog.value == 1) {
                                    onPostWorker();
                                  } else {
                                    prog.value = prog.value == 0.4
                                        ? 0.5
                                        : prog.value == 0.5
                                            ? 0.6
                                            : prog.value == 0.6
                                                ? 0.7
                                                : prog.value == 0.7
                                                    ? 0.8
                                                    : prog.value == 0.8
                                                        ? 0.9
                                                        : prog.value == 0.9
                                                            ? 1
                                                            : 1;
                                  }
                                }))
                      ],
                    ))
              ],
            )
          ],
        ),
      );
}
