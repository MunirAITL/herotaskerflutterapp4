import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APIOfferCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/offers/GetServiceFeeAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/offers/UpdateOfferTaskBiddingAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/input/InputBoxHT.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../main.dart';
import '../taskdetails_base.dart';

class UpdateOfferPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const UpdateOfferPage({Key key, @required this.taskBidding})
      : super(key: key);
  @override
  State createState() => _UpdateOfferPageState();
}

class _UpdateOfferPageState extends BaseTaskDetailsStatefull<UpdateOfferPage>
    with APIStateListener {
  final offer = TextEditingController();
  final cmt = TextEditingController();
  final focusOffer = FocusNode();
  final focusCmt = FocusNode();

  String serviceFeeTxt = "";
  String youReceiveTxt = "";

  double bidderAmount = 0;

  double serviceFeeAmount = 0;
  double shohokariAmount = 0;

  var _offer = 0.0;

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.task_bidding_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await APIViewModel().req<GetTaskAPIModel>(
                context: context,
                url: APIMyTasksCfg.GET_TASK_URL.replaceAll(
                    "#taskId#", taskCtrl.getTaskModel().id.toString()),
                reqType: ReqType.Get,
                callback: (model) async {
                  if (model != null && mounted) {
                    if (model.success) {
                      final taskModel = model.responseData.task;
                      if (taskModel != null) taskCtrl.setTaskModel(taskModel);
                    }
                  }
                },
              );
            } catch (e) {}

            final taskBiddingId = (model as UpdateOfferTaskBiddingAPIModel)
                .responseData
                .taskBidding
                .id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TASKBIDDING_EMAI_NOTI_GET_URL
                  .replaceAll("#taskBiddingId#", taskBiddingId.toString()),
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.task_bidding_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await APIViewModel().req<GetTaskAPIModel>(
                context: context,
                url: APIMyTasksCfg.GET_TASK_URL.replaceAll(
                    "#taskId#", taskCtrl.getTaskModel().id.toString()),
                reqType: ReqType.Get,
                callback: (model) async {
                  if (model != null && mounted) {
                    if (model.success) {
                      final taskModel = model.responseData.task;
                      if (taskModel != null) taskCtrl.setTaskModel(taskModel);
                    }
                  }
                },
              );
            } catch (e) {}

            final taskBiddingId = (model as UpdateOfferTaskBiddingAPIModel)
                .responseData
                .taskBidding
                .id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TASKBIDDING_EMAI_NOTI_GET_URL
                  .replaceAll("#taskBiddingId#", taskBiddingId.toString()),
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: true);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    offer.dispose();
    cmt.dispose();
    serviceFeeTxt = null;
    youReceiveTxt = null;
    super.dispose();
  }

  onReloadAPI() {}
  wsGetTimelineByApp() {}
  wsWithdrawTaskbidding(TaskBiddingModel model) {}
  onDelTaskClicked() {}

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      offer.text = widget.taskBidding.fixedbiddigAmount.round().toString();
      cmt.text = widget.taskBidding.coverLetter;
    } catch (e) {}

    calculateServiceCharge();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      // executes after build
      focusOffer.requestFocus();
    });
  }

  calculateServiceCharge() async {
    if (offer.text.length > 0) {
      try {
        _offer = double.parse(offer.text);
      } catch (e) {
        _offer = 0;
      }
      await APIViewModel().req<GetServiceFeeAPIModel>(
        context: context,
        url: APIOfferCfg.GET_CAL_SERVICE_AMOUNTS +
            "taskId=" +
            taskCtrl.getTaskModel().id.toString() +
            "&fixedbiddigAmount=" +
            _offer.round().toString(),
        reqType: ReqType.Get,
        isLoading: false,
        callback: (model) {
          if (mounted && model != null) {
            if (model.success) {
              serviceFeeAmount =
                  model.responseData.taskBidding.serviceFeeAmount;
              shohokariAmount = model.responseData.taskBidding.shohokariAmount;
              //offerAmount = double.parse(offer.text);
              //serviceFeeAmount = (offerAmount * serviceFeeAmount) / 100;
              bidderAmount = _offer - serviceFeeAmount;
              youReceiveTxt = "You'll receive: " +
                  getCurSign() +
                  " " +
                  bidderAmount.toStringAsFixed(2).toString();
              serviceFeeTxt = "(Service fee: " +
                  getCurSign() +
                  " " +
                  serviceFeeAmount.toStringAsFixed(2).toString() +
                  ")";
              setState(() {});
            } else {
              double serviceCharge = 10;
              if (offer.text.length > 0) {
                //offerAmount = double.parse(offer.text);
                serviceFeeAmount = (_offer * serviceCharge) / 100;
                bidderAmount = _offer - serviceFeeAmount;
                youReceiveTxt = "You'll receive: " +
                    getCurSign() +
                    " " +
                    bidderAmount.toStringAsFixed(2).toString();
                serviceFeeTxt = "(Service fee: " +
                    getCurSign() +
                    " " +
                    serviceFeeAmount.toStringAsFixed(2).toString() +
                    ")";
                setState(() {});
              }
            }
          }
        },
      );
    } else {
      youReceiveTxt = "You'll receive: " + getCurSign() + " 0.0";
      serviceFeeTxt = "(Service fee: " + getCurSign() + " 0.0)";
      setState(() {});
    }

    youReceiveTxt = youReceiveTxt.replaceAll(".00", "");
    serviceFeeTxt = serviceFeeTxt.replaceAll(".00", "");
  }

  wsMakeOffer() async {
    try {
      try {
        _offer = double.parse(offer.text);
      } catch (e) {}

      if (widget.taskBidding == null) {
        APIViewModel().req<UpdateOfferTaskBiddingAPIModel>(
          context: context,
          apiState: APIState(APIType.task_bidding_post, this.runtimeType, null),
          url: APIMyTasksCfg.POST_TASKBIDDING_URL,
          reqType: ReqType.Post,
          param: {
            "TaskId": taskCtrl.getTaskModel().id,
            "CoverLetter": cmt.text.trim(),
            "Description": taskCtrl.getTaskModel().description,
            "DeliveryDate": DateTime.now().toString(),
            "DeliveryTime": "",
            "FixedbiddigAmount": _offer,
            "HourlyBiddingAmount": 0,
            "TotalHourPerWeek": 0,
            "TotalHour": 0,
            "ServiceFeeAmount": serviceFeeAmount,
            "ShohokariAmount": shohokariAmount,
            "PaymentStatus": 0
          },
        );
      } else {
        final paymentStatus = (widget.taskBidding.paymentStatus != '')
            ? widget.taskBidding.paymentStatus
            : 0;
        APIViewModel().req<UpdateOfferTaskBiddingAPIModel>(
            context: context,
            apiState:
                APIState(APIType.task_bidding_put, this.runtimeType, null),
            url: APIMyTasksCfg.PUT_TASKBIDDING_URL,
            reqType: ReqType.Put,
            param: {
              "CoverLetter": cmt.text.trim(),
              "DeliveryDate":
                  widget.taskBidding.deliveryDate ?? DateTime.now().toString(),
              "DeliveryTime":
                  widget.taskBidding.deliveryTime ?? DateTime.now().toString(),
              "Description": widget.taskBidding.description ?? '',
              "DiscountAmount": widget.taskBidding.discountAmount ?? 0,
              "FixedbiddigAmount": double.parse(offer.text).round(),
              "HourlyRate": taskCtrl.getTaskModel().hourlyRate ?? 0.0,
              "NetTotalAmount": taskCtrl.getTaskModel().netTotalAmount ?? 0.0,
              "PaymentStatus": paymentStatus,
              "ReferenceId": widget.taskBidding.referenceId ?? 0,
              "ReferenceType": widget.taskBidding.referenceType ?? 0,
              "ServiceFeeAmount": serviceFeeAmount,
              "ShohokariAmount": shohokariAmount,
              "Status": taskCtrl.getStatusCode(),
              "Id": widget.taskBidding.id ?? 0,
              "TaskId": taskCtrl.getTaskModel().id,
              "TotalHour": taskCtrl.getTaskModel().totalHours,
              "TotalHourPerWeek": widget.taskBidding.totalHourPerWeek ?? 0.0,
              "UserId": userData.userModel.id,
              "UserPromotionId": widget.taskBidding.userPromotionId ?? 0,
            });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    var title = 'Make an offer';
    if (widget.taskBidding != null) {
      title = 'Update an offer';
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: title),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Continue",
            callback: () async {
              try {
                _offer = double.parse(offer.text);
              } catch (e) {}
              if (_offer < 1) {
                showToast(
                    context: context, msg: "Please enter an offer amount");
              } else if (cmt.text.trim().length < 25) {
                showToast(
                    context: context,
                    msg:
                        "Please enter a valid description\n(at least 25 characters)");
              } else {
                wsMakeOffer();
              }
            }),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        child: ListView(shrinkWrap: true, primary: true, children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Card(
            elevation: 0,
            color: MyTheme.gray1Color,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                children: [
                  Txt(
                      txt: "Your offer:",
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: true),
                  !taskCtrl.getTaskModel().isFixedPrice
                      ? Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Txt(
                              txt: 'Per Hour',
                              txtColor: MyTheme.gray5Color,
                              txtSize: MyTheme.txtSize - .4,
                              txtAlign: TextAlign.center,
                              isBold: true),
                        )
                      : SizedBox(),
                  SizedBox(height: 10),
                  InputBoxHT(
                    context: context,
                    ctrl: offer,
                    lableTxt: null,
                    //widget.taskBidding.fixedbiddigAmount.round().toString(),
                    kbType: TextInputType.phone,
                    inputAction: TextInputAction.next,
                    focusNode: focusOffer,
                    focusNodeNext: focusCmt,
                    len: 8,
                    txtAlign: TextAlign.center,
                    prefixIco: Text(
                      getCurSign(),
                      style: TextStyle(color: Colors.black, fontSize: 25),
                    ),
                    onChange: (txt) {
                      calculateServiceCharge();
                    },
                  ),
                  SizedBox(height: 10),
                  Txt(
                      txt: youReceiveTxt,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 5),
                  Txt(
                      txt: serviceFeeTxt,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ],
              ),
            ),
          ),
          drawCoverLetter()
        ],
      )
    ]));
  }

  drawCoverLetter() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          children: [
            Txt(
                txt: "Why are you the best person for this task?",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 10),
            Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextField(
                  controller: cmt,
                  focusNode: focusCmt,
                  minLines: 7,
                  maxLines: 10,
                  autocorrect: false,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.done,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: MyTheme.fontSize,
                  ),
                  decoration: new InputDecoration(
                    counter: Offstage(),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: "",
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: MyTheme.fontSize,
                    ),
                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
