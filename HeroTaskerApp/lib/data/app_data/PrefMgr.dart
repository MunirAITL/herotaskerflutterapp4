import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/mixin.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefMgr with Mixin {
  static final PrefMgr shared = PrefMgr._internal();
  factory PrefMgr() {
    return shared;
  }

  PrefMgr._internal();

  //  String
  setPrefStr(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(key, val);
    } catch (e) {}
  }

  Future<String> getPrefStr(key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    } catch (e) {}
    return null;
  }

  //  int
  setPrefInt(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setInt(key, val);
    } catch (e) {}
  }

  Future<int> getPrefInt(key) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getInt(key) ?? 0;
    } catch (e) {}
    return 0;
  }

  //  Bool
  setPrefBool(key, val) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool(key, val);
    } catch (e) {}
  }

  Future<bool> getPrefBool(key) async {
    bool isOk = false;
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      isOk = prefs.getBool(key);
    } catch (e) {
      myLog(e.toString());
    }
    return isOk;
  }

  //  Payment Pref Start Here...
  setBikashAccount(bool isYes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(
        TaskStatusCfg.PREF_KEY_USER_SEND_PAYMENT_METHOD_BIKASH, isYes);
  }

  getBikashAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs
        .getString(TaskStatusCfg.PREF_KEY_USER_SEND_PAYMENT_METHOD_BIKASH);
  }

  setRocketAccount(bool isYes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(
        TaskStatusCfg.PREF_KEY_USER_SEND_PAYMENT_METHOD_ROCKET, isYes);
  }

  getRocketAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs
        .getString(TaskStatusCfg.PREF_KEY_USER_SEND_PAYMENT_METHOD_ROCKET);
  }

  setBackAccount(bool isYes) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(TaskStatusCfg.PREF_KEY_USER_SEND_PAYMENT_METHOD_BANK, isYes);
  }

  getBackAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(TaskStatusCfg.PREF_KEY_USER_SEND_PAYMENT_METHOD_BANK);
  }

  //  Payment Pref End Here...

  //  Confirm your offer -> National ID Card
  /*setNationalIDCardStatus(int status) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(TaskStatusCfg.PREF_KEY_NID_NUMBER, status);
  }

  Future<int> getNationalIDCardStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(TaskStatusCfg.PREF_KEY_NID_NUMBER);
  }*/
}
