import 'dart:developer';

import 'package:aitl/Mixin.dart';
import 'package:aitl/data/model/ads-on/credit_report/GetUserValidationOldUserApiModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';

import 'CreditUrlHelper.dart';

class GetUserValidationOldUserApiMgr with Mixin {
  static final GetUserValidationOldUserApiMgr _shared =
      GetUserValidationOldUserApiMgr._internal();

  factory GetUserValidationOldUserApiMgr() {
    return _shared;
  }

  GetUserValidationOldUserApiMgr._internal();

  creditCaseOldUserInformation({
    BuildContext context,
    int userID,
    int userCompanyId,
    Function(GetUserValidationOldUserApiModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<GetUserValidationOldUserApiModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: CreditURLbHelper()
            .getUrl(userCompanyId: userCompanyId, userID: userID),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log("GET_USER_VALIDATION_FOR_OLD_USER_URL  ERROR = " + e.toString());
    }
  }
}
