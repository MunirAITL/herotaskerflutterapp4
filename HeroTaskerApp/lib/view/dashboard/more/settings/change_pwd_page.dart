import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';

import '../../../../config/theme/MyTheme.dart';
import '../../../../data/network/NetworkMgr.dart';
import '../../../../view_model/helper/auth/ChangePwdAPIMgr.dart';
import '../../../../view_model/helper/utils/UIHelper.dart';

class ChangePwdScreen extends StatefulWidget {
  @override
  State createState() => _ChangePwdScreenState();
}

class _ChangePwdScreenState extends State<ChangePwdScreen> with Mixin {
  final TextEditingController _curPwd = TextEditingController();
  final TextEditingController _newPwd = TextEditingController();
  final TextEditingController _newPwd2 = TextEditingController();
  //
  final focusCurPwd = FocusNode();
  final focusPwd1 = FocusNode();
  final focusPwd2 = FocusNode();

  bool isLoading = false;
  bool isobscureText = true;
  bool isobscureText1 = true;
  bool isobscureText2 = true;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _newPwd.dispose();
    _newPwd2.dispose();
    _curPwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {}

  validate() {
    if (UserProfileVal()
        .isEmpty(context, _curPwd, 'Please enter current password')) {
      return false;
    }
    if (UserProfileVal().isEmpty(context, _newPwd, 'Please enter password')) {
      return false;
    }
    if (UserProfileVal()
        .isEmpty(context, _newPwd2, 'Please enter cofirm password')) {
      return false;
    }
    if (_newPwd.text.trim() != _newPwd2.text.trim()) {
      showToast(context: context, msg: "Confirm password does not match");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          elevation: MyTheme.appbarElevation,
          /*leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),*/
          title: UIHelper().drawAppbarTitle(title: "Change password"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 30),
                /*Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: 'Change your',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          )),
                      TextSpan(
                        text: '\npassword',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 40),*/
                drawInputBox(
                  context: context,
                  title: "Current Password",
                  ph: "",
                  input: _curPwd,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusCurPwd,
                  focusNodeNext: focusPwd1,
                  len: 20,
                  isPwd: true,
                ),
                SizedBox(height: 20),
                drawInputBox(
                  context: context,
                  title: "New Password",
                  ph: "",
                  input: _newPwd,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusPwd1,
                  focusNodeNext: focusPwd2,
                  len: 20,
                  isPwd: true,
                ),
                SizedBox(height: 20),
                drawInputBox(
                  context: context,
                  title: "Repeat Password",
                  ph: "",
                  input: _newPwd2,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.done,
                  focusNode: focusPwd2,
                  len: 20,
                  isPwd: true,
                ),
                SizedBox(height: 30),
                MMBtn(
                    txt: "Reset Password",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {
                      if (validate()) {
                        ChangePwdAPIMgr().wsChangePwdAPI(
                          context: context,
                          curPwd: _curPwd.text.trim(),
                          newPwd: _newPwd.text.trim(),
                          newPwd2: _newPwd2.text.trim(),
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  try {
                                    final msg = model.messages.changePassword[0]
                                        .toString();
                                    showToast(
                                        context: context, msg: msg, which: 1);
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                } else {
                                  try {
                                    if (mounted) {
                                      final err = model
                                          .errorMessages.changePassword[0]
                                          .toString();
                                      showToast(context: context, msg: err);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          },
                        );
                      }
                    }),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
