import UIKit
import Flutter
import GoogleMaps
import Firebase

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    FirebaseApp.configure()
    GMSServices.provideAPIKey("AIzaSyCxVk1c2Sd8t9p94YBSleBENGAbcLGDteA")

    //  https://flutter.dev/docs/development/platform-integration/platform-channels
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let CHANNEL = FlutterMethodChannel(name: "flutter.native/herotasker",
                                                  binaryMessenger: controller.binaryMessenger)
        CHANNEL.setMethodCallHandler({
          (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          // Note: this method is invoked on the UI thread.
          switch call.method {
            case "openMM3App":
              if let args = call.arguments as? Dictionary<String, Any>,
                let appstoreAppName = args["appstoreAppName"] as? String {
                let appstoreAppId = args["appstoreAppId"] as? String
                  let appName = args["appName"] as? String
                  self.checkAndOpenApp(appstoreAppName:appstoreAppName , appstoreAppId:appstoreAppId ?? "", appName:appName ?? "")
              }
              default:
                break
          }
        })


    GeneratedPluginRegistrant.register(with: self)
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }

    func checkAndOpenApp(appstoreAppName:String, appstoreAppId:String, appName:String){
    let app = UIApplication.shared
    let appScheme = appstoreAppName+"://app"
    if app.canOpenURL(URL(string: appScheme)!) {
        print("App is install and can be opened")
        let url = URL(string:appScheme)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        print("App in not installed. Go to AppStore")
        let uri = "itms-apps://itunes.apple.com/app/Mortgage Magic/id"+appstoreAppId+"?mt=8"
        print(uri)
        if let url = URL(string: uri),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
  }
}