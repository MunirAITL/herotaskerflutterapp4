import 'dart:async';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/AppData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/widgets/dialog/role_dialog.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/more/settings/fcm_noti_page.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../config/cfg/AppConfig.dart';
import '../../main.dart';
import '../auth/role/worker/worker_page.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  State createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  bool isDoneCookieCheck = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        isTablet = AppConfig().isTablet(context);
      });

      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: MyTheme.brandColor));

      try {
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();

          //if (!Server.isOtp) {
          //Get.to(() => DashboardPage());
          //} else {
          //if (userData.userModel.isMobileNumberVerified || userData.userModel.isEmailVerified) {
          Get.to(() => DashboardPage()).then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
          //} else {
          //Get.to(() => LoginLandingScreen());
          //Get.to(() => WorkerRolePage());
          //}
          //}
        } else {
          Future.delayed(const Duration(seconds: 2), () {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        }
      } catch (e) {
        myLog(e.toString());
        Future.delayed(const Duration(seconds: 2), () {
          setState(() {
            isDoneCookieCheck = true;
          });
        });
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        Get.to(() => CaseAlertScreen(
              message: message,
            )).then((value) async {
          await userData.setUserModel();
          Get.offAll(
            () => DashboardPage(),
          ).then((value) {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        });
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? Container(
                    color: MyTheme.bgColor,
                  )
                : callLoginPage(),
          )),
    );
  }

  callLoginPage() {
    Timer(Duration(seconds: 1), () {
      try {
        Get.off(() => LoginLandingScreen());
        //Get.to(() => WorkerRolePage());
      } catch (e) {
        debugPrint("Welcome screen Error catch ");
      }
      // ignore: unnecessary_statements
    });

    return Container(
      child: Center(
        //color: MyTheme.themeData.accentColor,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Container(
              //width: getWP(context, 70),
              child: Image.asset(
            'assets/images/logo/logo.png',
            fit: BoxFit.cover,
            width: getWP(context, 30),
          )),
        ),
      ),
    );
  }
}
