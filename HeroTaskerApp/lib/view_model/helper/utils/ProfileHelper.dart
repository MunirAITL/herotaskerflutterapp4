import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileHelper {
  drawProfileUserRatingView(
      double w,
      double h,
      List<UserRatingsModel> listUserRating,
      bool isPoster,
      Function(UserRatingsModel) callback) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (UserRatingsModel ratingModel in listUserRating)
            ratingModel.isPoster == isPoster
                ? Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Card(
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: CircleAvatar(
                                    radius: 30,
                                    backgroundColor: Colors.transparent,
                                    backgroundImage:
                                        MyNetworkImage.loadProfileImage(
                                            ratingModel.profileImageUrl),
                                  ),
                                ),
                              ),
                              SizedBox(width: 20),
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    UIHelper().getStarRatingView(
                                      w: w,
                                      h: h,
                                      rate: ratingModel.rating.toInt(),
                                      reviews: null,
                                      starColor: Colors.blueAccent,
                                      align: MainAxisAlignment.start,
                                    ),
                                    SizedBox(height: 5),
                                    Txt(
                                        txt: DateFun.getTimeAgoTxt(
                                            ratingModel.creationDate),
                                        txtColor: MyTheme.gray4Color,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          GestureDetector(
                            onTap: () {
                              callback(ratingModel);
                            },
                            child: Text(ratingModel.taskTitle,
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    color: MyTheme.blueColor,
                                    fontSize: 17)),
                          ),
                          SizedBox(height: 10),
                          Txt(
                              txt: ratingModel.comments,
                              txtColor: MyTheme.gray5Color,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false)
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
        ],
      ),
    );
  }

  drawTaskDetailsUserRatingView(
      {double w,
      double h,
      TaskBiddingModel model,
      List<UserRatingsModel> listUserRating,
      Function callback}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (UserRatingsModel ratingModel in listUserRating)
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: GestureDetector(
                          onTap: () {
                            Get.to(() =>
                                ProfilePage(profileUserID: ratingModel.userId));
                          },
                          child: CircleAvatar(
                            radius: 30,
                            backgroundColor: Colors.transparent,
                            backgroundImage: MyNetworkImage.loadProfileImage(
                                ratingModel.profileImageUrl),
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.to(() => ProfilePage(
                                    profileUserID: ratingModel.userId));
                              },
                              child: Txt(
                                  txt: ratingModel.initiatorName,
                                  txtColor: MyTheme.brandColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                            ),
                            SizedBox(height: 5),
                            UIHelper().getStarRatingView(
                                w: w,
                                h: h,
                                rate: ratingModel.rating.toInt(),
                                reviews: null,
                                align: MainAxisAlignment.start,
                                starColor: Colors.blueAccent),
                            ratingModel.comments != ''
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 5),
                                    child: Txt(
                                        txt: ("Left a review for " +
                                                ratingModel.employeeName) +
                                            " : " +
                                            ratingModel.comments,
                                        txtColor: MyTheme.gray5Color,
                                        txtSize: MyTheme.txtSize - .2,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  )
                                : SizedBox(),
                            SizedBox(height: 5),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Txt(
                                      txt: DateFun.getTimeAgoTxt(
                                          ratingModel.creationDate),
                                      txtColor: MyTheme.gray4Color,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                                //  condition 1: (taskbidding.status == 'PAID'
                                //  condition 1A: taskbidding.userId==user.id && taskbidding.isReviewByShohoKari -> Poster/Customer
                                //  condition 1B: taskbidding.userId != user.id && taskbidding.isReview -> Tasker
                                model.status == 'PAID'
                                    ? ((model.userId == userData.userModel.id &&
                                                model.isReviewByPoster) ||
                                            (model.userId ==
                                                    userData.userModel.id &&
                                                model.isReviewByShohoKari))
                                        ? GestureDetector(
                                            onTap: () {
                                              callback();
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: Colors.black54,
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10,
                                                    right: 10,
                                                    top: 6,
                                                    bottom: 6),
                                                child: Txt(
                                                    txt: "Review",
                                                    txtColor:
                                                        MyTheme.brandColor,
                                                    txtSize: 1.6,
                                                    txtAlign: TextAlign.right,
                                                    isBold: true),
                                              ),
                                            ))
                                        : SizedBox()
                                    : SizedBox(),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  /*Txt(
                      txt: (ratingModel.initiatorName +
                          " left a review for " +
                          ratingModel.employeeName),
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),*/
                ],
              ),
            ),
        ],
      ),
    );
  }
}
