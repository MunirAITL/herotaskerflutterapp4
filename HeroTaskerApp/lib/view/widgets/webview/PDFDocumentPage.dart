import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:get/get.dart';

import '../../../main.dart';

class PDFDocumentPage extends StatefulWidget {
  final String url;
  final String title;
  final bool isRightArrow;

  const PDFDocumentPage({
    Key key,
    @required this.url,
    @required this.title,
    this.isRightArrow = false,
  }) : super(key: key);

  @override
  State createState() => _PDFDocumentPageState();
}

class _PDFDocumentPageState extends State<PDFDocumentPage> with Mixin {
  final GlobalKey webViewKey = GlobalKey();

  double progress = 0;
  bool isLoading = true;
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    try {} catch (e) {}

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        elevation: MyTheme.appbarElevation,
        backgroundColor: MyTheme.appbarColor,
        iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
        title: UIHelper().drawAppbarTitle(title: widget.title),
        leading: IconButton(
            onPressed: () {
              if (!widget.isRightArrow) {
                Get.back();
              } else {
                _pdfViewerKey.currentState?.openBookmarkView();
              }
            },
            icon: Icon(
                !widget.isRightArrow
                    ? Icons.arrow_back_ios
                    : Icons.bookmark_outline,
                size: MyTheme.fontSize)),
        actions: <Widget>[
          IconButton(
            icon: Icon(!widget.isRightArrow
                ? Icons.bookmark_outline
                : Icons.arrow_forward),
            onPressed: () {
              if (widget.isRightArrow) {
                Get.back();
              } else {
                _pdfViewerKey.currentState?.openBookmarkView();
              }
            },
          ),
        ],
      ),
      body: Container(
        width: getW(context),
        height: getH(context),
        color: Colors.white,
        child: SfPdfViewer.network(
          '${widget.url}',
          key: _pdfViewerKey,
        ),
      ),
    );
  }
  /*@override
  Widget build(BuildContext context) {
    print("url Privacy ====== " + widget.url);
    return isLoading
            ? Container(
                child: Center(
                    child: CircularProgressIndicator(
                  backgroundColor: MyTheme.brandColor,
                  strokeWidth: 4,
                )),
              )
            : Container(
      width: getW(context),
              height: getH(context),
              child: PDFViewer(
                  document: document,*/ /*
                  indicatorBackground: Colors.white,
                  indicatorText: Colors.black,
                  tooltip: const PDFViewerTooltip(),
                  enableSwipeNavigation: true,
                  scrollDirection: Axis.vertical,
                  pickerButtonColor: MyTheme.brandColor,
                  pickerIconColor: Colors.white,
                  showNavigation: true,
                  zoomSteps: 1,
                  minScale: getH(context),*/ /*
                ),
            );
  }*/
}
