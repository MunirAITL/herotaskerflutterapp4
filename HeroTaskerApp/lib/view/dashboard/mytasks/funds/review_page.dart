import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/ratings/TaskUserRatingAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

import '../../../../main.dart';

class ReviewPage extends StatefulWidget {
  final TaskBiddingModel taskBiddingModel;
  const ReviewPage({Key key, @required this.taskBiddingModel})
      : super(key: key);
  @override
  State createState() => _ReviewPageState();
}

class _ReviewPageState extends BasePaymentStatefull<ReviewPage>
    with APIStateListener {
  final taskCtrl = Get.put(TaskCtrl());

  final cmt = TextEditingController();
  int rating = 0;
  var ownerName = "";
  var imageUrl = "";
//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.post_review &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final userRatingId =
                (model as TaskUserRatingAPIModel).responseData.userRating.id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TASK_USER_RATING_URL
                  .replaceAll("#userRatingId#", userRatingId.toString()),
              isLoading: true,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            StateProvider()
                .notify(ObserverState.STATE_RELOAD_TASKDETAILS, null);
            Get.back(result: true);
          }
        }
      }
    } catch (e) {}
  }

  wsPostReview() async {
    try {
      //int employeeId = 0;
      bool isPoster = false;
      if (widget.taskBiddingModel.isTaskOwner) {
        // employeeId = widget.taskBiddingModel.userId;
        isPoster = true;
      } else {
        // employeeId = userData.userModel.id;
        isPoster = false;
      }

      final param = {
        "UserRatingId": 0,
        "IsPoster": isPoster,
        "Comments": cmt.text.trim(),
        "CreationDate": DateTime.now().toString(),
        "EmployeeId": 0,
        "Rating": rating,
        "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
        "TaskBiddingId": widget.taskBiddingModel.id,
        "TaskTitle": widget.taskBiddingModel.description,
        "UserId": userData.userModel.id,
        "Id": 0,
      };
      myLog(param.toString());
      await APIViewModel().req<TaskUserRatingAPIModel>(
        context: context,
        apiState: APIState(APIType.post_review, this.runtimeType, null),
        url: APIProfileCFg.USER_RATING_POST_URL,
        isLoading: true,
        reqType: ReqType.Post,
        param: param,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    cmt.dispose();
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                StateProvider()
                    .notify(ObserverState.STATE_RELOAD_TASKDETAILS, null);
                Get.back();
              },
              icon: Icon(Icons.arrow_back_ios)),
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.appbarColor,
          iconTheme: IconThemeData(color: MyTheme.appbarTxtColor),
          title: UIHelper().drawAppbarTitle(title: "Leave review"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawAvatorView(),
            SizedBox(height: 20),
            drawStarRatingView(),
            drawReviewBox(),
            SizedBox(height: 5),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MMBtn(
                txt: "Submit review",
                height: getHP(context, MyTheme.btnHpa),
                width: getW(context),
                callback: () async {
                  wsPostReview();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawAvatorView() {
    /*if (!widget.taskBiddingModel.isTaskOwner) {
      ownerName = widget.taskBiddingModel.ownerName;
      imageUrl = widget.taskBiddingModel.ownerImageUrl;
    } else {
      ownerName = userData.userModel.name;
      imageUrl = userData.userModel.profileImageUrl;
    }*/
    if (widget.taskBiddingModel.userId == userData.userModel.id &&
        taskCtrl.getTaskModel().userId == widget.taskBiddingModel.taskOwnerId) {
      ownerName = taskCtrl.getTaskModel().ownerName;
      imageUrl = taskCtrl.getTaskModel().ownerImageUrl;
    } else if (taskCtrl.getTaskModel().userId == userData.userModel.id &&
        taskCtrl.getTaskModel().userId == widget.taskBiddingModel.taskOwnerId) {
      ownerName = widget.taskBiddingModel.ownerName;
      imageUrl = widget.taskBiddingModel.ownerImageUrl;
    }

    return Container(
      color: MyTheme.gray1Color,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircleAvatar(
              radius: 30,
              backgroundColor: Colors.transparent,
              backgroundImage: MyNetworkImage.loadProfileImage(imageUrl),
            ),
            SizedBox(width: 5),
            Expanded(
              flex: 4,
              child: Container(
                //color: Colors.black,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Txt(
                        txt: ownerName,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize + .3,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    SizedBox(height: 5),
                    Txt(
                        txt: taskCtrl.getTaskModel().title,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Txt(
                      txt: "Task price",
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 5),
                  Txt(
                      txt: getCurSign() +
                          widget.taskBiddingModel.netTotalAmount
                              .round()
                              .toString(),
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize + .5,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawStarRatingView() {
    return Container(
      width: getW(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: 'Leave a review for \n',
                  style: TextStyle(color: MyTheme.gray4Color, fontSize: 20),
                  children: <TextSpan>[
                    TextSpan(
                        text: ownerName,
                        style:
                            TextStyle(color: MyTheme.blueColor, fontSize: 20)),
                  ],
                ),
              )),
              SizedBox(width: 10),
              Flexible(
                  child: CircleAvatar(
                radius: 30,
                backgroundColor: Colors.transparent,
                backgroundImage: MyNetworkImage.loadProfileImage(imageUrl),
              )),
            ],
          ),
          SizedBox(height: 10),
          RatingBar.builder(
            initialRating: 0,
            minRating: 1,
            direction: Axis.horizontal,
            allowHalfRating: false,
            itemCount: 5,
            itemPadding: EdgeInsets.symmetric(horizontal: 2),
            itemSize: 50,
            unratedColor: Colors.grey,
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: MyTheme.redColor,
            ),
            onRatingUpdate: (rating2) {
              rating = rating2.round();
            },
          ),
          SizedBox(height: 10),
          Txt(
              txt: "Tap the number of stars for this review",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.center,
              isBold: false),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  drawReviewBox() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Container(
          //padding: const EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: TextFormField(
              controller: cmt,
              minLines: 5,
              maxLines: 10,
              autocorrect: false,
              onChanged: (v) {},
              keyboardType: TextInputType.multiline,
              style: TextStyle(
                color: Colors.black,
                fontSize: MyTheme.fontSize,
              ),
              decoration: new InputDecoration(
                counter: Offstage(),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                hintText: "Please leave a review here",
                hintStyle: new TextStyle(
                  color: Colors.grey,
                  fontSize: MyTheme.fontSize,
                ),
                contentPadding: const EdgeInsets.symmetric(vertical: 0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
