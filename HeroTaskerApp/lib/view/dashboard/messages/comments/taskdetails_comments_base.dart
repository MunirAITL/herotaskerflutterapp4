import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskCtrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';

abstract class BaseTaskDetailsCommentsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TimeLineHelper {
  final taskCtrl = Get.put(TaskCtrl());
  bool isPoster = true;

  drawMessageList({List<TimelinePostModel> listTimelinePostsModel}) {
    return (listTimelinePostsModel != null)
        ? Padding(
            padding: EdgeInsets.only(bottom: getHP(context, 10)),
            child: drawQ(
              context: context,
              taskCtrl: taskCtrl,
              listTimelinePostsModel: listTimelinePostsModel,
              isPoster: isPoster,
              isReply: false,
              callback: null,
            ),
          )
        : SizedBox();
  }
}
