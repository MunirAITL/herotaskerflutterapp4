import 'dart:convert';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class RegAPIMgr with Mixin {
  static final RegAPIMgr _shared = RegAPIMgr._internal();

  factory RegAPIMgr() {
    return _shared;
  }

  RegAPIMgr._internal();

  wsRegAPI({
    BuildContext context,
    String email,
    String pwd,
    String fname,
    String lname,
    String fullName,
    String mobile,
    String countryCode,
    String dob,
    String dobDD,
    String dobMM,
    String dobYY,
    Function(RegAPIModel) callback,
  }) async {
    try {
      final param = getParam(
        email: email,
        pwd: pwd,
        fname: fname,
        lname: lname,
        fullName: fullName,
        phone: mobile,
        countryCode: countryCode,
        dob: dob,
        dobDD: dobDD,
        dobMM: dobMM,
        dobYY: dobYY,
      );
      final jsonString = JsonString(json.encode(param));
      myLog(jsonString.source);
      await NetworkMgr()
          .req<RegAPIModel, Null>(
        context: context,
        url: APIAuthCfg.REG_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  getParam({
    String email,
    String pwd,
    String fname = '',
    String lname = '',
    String fullName = '',
    String phone,
    String countryCode,
    String dob = '',
    String dobDD = '',
    String dobMM = '',
    String dobYY = '',
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "FirstName": fname,
      "LastName": lname,
      "MobileNumber": phone,
      "CommunityId": "1",
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": countryCode,
      "Status": "101",
      "OTPCode": "",
      "BirthDay": dobDD,
      "BirthMonth": dobMM,
      "BirthYear": dobYY,
      // "UserCompanyId": 1003,
      // "UserCompanyId": 2,
      "UserCompanyId": 1367,
      "dialCode": countryCode,
      "ConfirmPassword": pwd,
      "confirmPassword": pwd,
      "DateofBirth": dob,
    };
  }
}
