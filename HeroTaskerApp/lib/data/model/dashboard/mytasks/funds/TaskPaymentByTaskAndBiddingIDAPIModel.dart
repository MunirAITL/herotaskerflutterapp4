import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsModel.dart';

class TaskPaymentByTaskAndBiddingIDAPIModel {
	bool success;
	ErrorMessages errorMessages;
	ErrorMessages messages;
	ResponseData responseData;

	TaskPaymentByTaskAndBiddingIDAPIModel({this.success, this.errorMessages, this.messages, this.responseData});

	TaskPaymentByTaskAndBiddingIDAPIModel.fromJson(Map<String, dynamic> json) {
		success = json['Success'];
		errorMessages = json['ErrorMessages'] != null ? new ErrorMessages.fromJson(json['ErrorMessages']) : null;
		messages = json['Messages'] != null ? new ErrorMessages.fromJson(json['Messages']) : null;
		responseData = json['ResponseData'] != null ? new ResponseData.fromJson(json['ResponseData']) : null;
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['Success'] = this.success;
		if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
		if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
		if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
		return data;
	}
}

class ErrorMessages {
	ErrorMessages();
	ErrorMessages.fromJson(Map<String, dynamic> json);
	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		return data;
	}
}

class ResponseData {
	TaskPaymentsModel taskPayment;
	ResponseData({this.taskPayment});
	ResponseData.fromJson(Map<String, dynamic> json) {
		taskPayment = json['TaskPayment'] != null ? new TaskPaymentsModel.fromJson(json['TaskPayment']) : null;
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		if (this.taskPayment != null) {
      data['TaskPayment'] = this.taskPayment.toJson();
    }
		return data;
	}
}

