import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/data/model/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class Sms2APIMgr with Mixin {
  static final Sms2APIMgr _shared = Sms2APIMgr._internal();

  factory Sms2APIMgr() {
    return _shared;
  }

  Sms2APIMgr._internal();

  wsLoginMobileOtpPostAPI({
    BuildContext context,
    String countryCode,
    String mobile,
    Function(MobileUserOtpPostAPIModel) callback,
  }) async {
    try {
      debugPrint("country phone 1 = " + mobile);

      if (countryCode.isEmpty) {
        if (mobile.startsWith("01") ||
            mobile.startsWith("880") ||
            mobile.startsWith("+880")) {
          countryCode = "+88";
        } else {
          countryCode = "+44";
        }
      }

      debugPrint("country Code 2= " + countryCode);
      debugPrint("country phone 2 = " + mobile);

      await NetworkMgr()
          .req<MobileUserOtpPostAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_MOBILE_OTP_POST_URL,
        param: getParam(mobileNumber: mobile, countryCode: countryCode),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      print("Imran wsLoginMobileOtpPostAPI Error " + e.toString());
    }
  }

  getParam({
    String mobileNumber,
    String countryCode,
    String status = '101',
    String otpCode = '',
  }) {
    return {
      "MobileNo": mobileNumber,
      "MobileNumber": countryCode + mobileNumber.replaceAll(countryCode, ''),
      "CountryCode": countryCode,
      "Status": "101",
      "OTPCode": "",
    };
  }

  wsSendOtpNotiAPI({
    BuildContext context,
    int otpId,
    Function(SendOtpNotiAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<SendOtpNotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: getUrl(otpId: otpId),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {}
  }

  getUrl({int otpId}) {
    var url = APIAuthCfg.SEND_OTP_NOTI_URL;
    url = url.replaceAll("#otpId#", otpId.toString());
    return url;
  }
}
