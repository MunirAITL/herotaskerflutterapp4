class GetPicModel {
  int id;
  int mediaType;
  String mimeType;
  String thumbnailPath;
  String attachmentType;

  GetPicModel(
      {this.id,
      this.mediaType,
      this.mimeType,
      this.thumbnailPath,
      this.attachmentType});

  GetPicModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    mediaType = json['MediaType'] ?? 0;
    mimeType = json['MimeType'] ?? '';
    thumbnailPath = json['ThumbnailPath'] ?? '';
    attachmentType = json['attachmentType'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['MediaType'] = this.mediaType;
    data['MimeType'] = this.mimeType;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['attachmentType'] = this.attachmentType;
    return data;
  }
}
